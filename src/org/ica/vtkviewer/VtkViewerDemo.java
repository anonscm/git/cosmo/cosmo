package org.ica.vtkviewer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkObject;
import org.ica.vtkviewer.model.VtkPolyline;
import org.ica.vtkviewer.model.VtkSurface;
import org.lgmt.dgl.curves.PointsCurve;
import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

import vtk.vtkNativeLibrary;

public class VtkViewerDemo implements PropertyChangeListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	public static void main(String[] args) {
		new VtkViewerDemo();
	}

	private class DataModel {
		private final PropertyChangeSupport dataSupport = new PropertyChangeSupport(this);
		Surface surface;
		ArrayList<PointsCurve<Point3d>> curvesList;

		public DataModel() {
			super();
			createModel();
		}

		// Model utilities
		private SurfacePoint calcPoint(double u, double v) {
			SurfacePoint p;
			Vector3d n;
			n = surface.normal(u, v);
			n.normalize();
			p = new SurfacePoint(surface, u, v);
			p.add(n);

			return p;
		}

		private void createModel() {
			Point3d[][] points = new Point3d[3][3];
			points[0][0] = new Point3d(0.0, 0.0, 0.0);
			points[0][1] = new Point3d(0.0, 20.0, 10.0);
			points[0][2] = new Point3d(0.0, 40.0, 0.0);
			points[1][0] = new Point3d(40.0, 0.0, 5.0);
			points[1][1] = new Point3d(40.0, 20.0, 15.0);
			points[1][2] = new Point3d(40.0, 40.0, 5.0);
			points[2][0] = new Point3d(80.0, 0.0, 20.0);
			points[2][1] = new Point3d(80.0, 20.0, 35.0);
			points[2][2] = new Point3d(80.0, 40.0, 20.0);

			surface = new BezierSurface(points);
			curvesList = new ArrayList<PointsCurve<Point3d>>();

			for (int i = 0; i < 9; i++) {
				double u = (double) i / 10.0 + 0.1;
				ArrayList<Point3d> curvePoints = new ArrayList<Point3d>();
				for (int j = 0; j < 9; j++) {
					double v = (double) j / 10.0 + 0.1;
					Vector3d n = surface.normal(u, v);
					n.normalize();
					SurfacePoint p = new SurfacePoint(surface, u, v);
					p.add(n);
					curvePoints.add(p);
				}
				curvesList.add(new PointsCurve<Point3d>(curvePoints));
			}
		}

		public Surface getSurface() {
			return surface;
		}

		public ArrayList<PointsCurve<Point3d>> getCurvesList() {
			return curvesList;
		}

		// Data model update
		public void update() {
			double u, v;
			SurfacePoint p;

			for (int i = 0; i < 9; i++) {
				PointsCurve<Point3d> c = new PointsCurve<Point3d>();
				curvesList.add(c);
				this.dataSupport.firePropertyChange("AddObject", null, c);
				v = (double) i / 10.0 + 0.1;
				u = 0.5;
				p = calcPoint(u, v);
				c.addPoint(p);
				for (int j = 5; j < 9; j++) {
					u = (double) j / 10.0 + 0.1;
					p = calcPoint(u, v);
					c.addPoint(p);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				for (int j = 3; j >= 0; j--) {
					u = (double) j / 10.0 + 0.1;
					p = calcPoint(u, v);
					c.insertPoint(0, p);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		public void removeCurve(PointsCurve<Point3d> curve) {
			this.dataSupport.firePropertyChange("RemoveObject", null, curve);
			curvesList.remove(curve);
		}

		/**
		 * Observer design pattern implementation.
		 * 
		 * Add a listener
		 * 
		 * @param listener
		 */
		public void addPropertyChangeListener(PropertyChangeListener listener) {
			this.dataSupport.addPropertyChangeListener(listener);
		}

		/**
		 * Observer design pattern implementation.
		 * 
		 * Remove a listener
		 * 
		 * @param listener
		 */
		@SuppressWarnings("unused")
		public void removePropertyChangeListener(PropertyChangeListener listener) {
			this.dataSupport.removePropertyChangeListener(listener);
		}

	}

	private class MyVtkTimerCallback extends VtkTimerCallback {

		public MyVtkTimerCallback(VtkViewer viewer) {
			super(viewer);
		}

		/*
		 * This example is just displaying a counter on console. But this framework may
		 * be used to alter or update model. For example, an animation may be done using
		 * setPosition() for some vtkActors.
		 */
		@Override
		public void run() {
			if (status == STATUS.RUN) {
				timerCount++;
				System.out.println(timerCount);
			}
			interactor.GetRenderWindow().Render();
		}
	}

	private VtkModel model;

	public VtkViewerDemo() {
		// The viewer cannot be run with an empty model.
		// Thus an initial model is created first.
		DataModel dataModel = new DataModel();
		dataModel.addPropertyChangeListener(this);

		model = new VtkModel();

		// VtkObjects corresponding to already known data model objects are directly
		// added to VtlModel
		VtkSurface vtkSurf = new VtkSurface(dataModel.getSurface());
		vtkSurf.setNormalsLength(2.0);
		model.add(vtkSurf);
		for (PointsCurve<Point3d> pc : dataModel.getCurvesList())
			model.add(new VtkPolyline(pc));

		// Then the viewer is set up and run.
		VtkViewer viewer = new VtkViewer(model, "VtkViewer Démo");
		viewer.run();
		System.out.println("Wainting for Timer callback to be attached...");

		// Model can be updated after running viewer.
		// Viewer will be updated automatically.
		dataModel.update();

		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// WARNING: curvesList is re-indexed after each removal
		dataModel.removeCurve(dataModel.curvesList.get(1));
		dataModel.removeCurve(dataModel.curvesList.get(3));
		dataModel.removeCurve(dataModel.curvesList.get(5));
		dataModel.removeCurve(dataModel.curvesList.get(7));
		dataModel.removeCurve(dataModel.curvesList.get(9));
		dataModel.removeCurve(dataModel.curvesList.get(11));

		// Custom callback can be attached to viewer.
		// See MyVtKTimerCallback for an example.
		viewer.attachCallback(new MyVtkTimerCallback(viewer));
		System.out.println("Timer callback attached. Hit \"Run\" button to start");
	}

	// Controller side
	@Override
	@SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "AddObject") {
			Object obj = event.getNewValue();
			if (obj instanceof PointsCurve<?>) {
				PointsCurve<Point3d> c = (PointsCurve<Point3d>) event.getNewValue();
				VtkPolyline traj = new VtkPolyline(c);
				model.add(traj);
			}
		}
		if (event.getPropertyName() == "RemoveObject") {
			Object obj = event.getNewValue();
			for (VtkObject o : model.getObjects()) {
				if (obj == o.getSource()) {
					model.remove(o);
					break;
				}
			}
		}
	}
}
