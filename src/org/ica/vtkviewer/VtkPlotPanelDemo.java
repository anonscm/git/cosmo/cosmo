package org.ica.vtkviewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import vtk.vtkFloatArray;
import vtk.vtkNativeLibrary;
import vtk.vtkTable;

public class VtkPlotPanelDemo implements ActionListener {
	private JButton exitButton;
	private VtkPlotPanel panel;

	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	public VtkPlotPanelDemo() {
		vtkTable table = new vtkTable();
		vtkFloatArray arrX = new vtkFloatArray();
		arrX.SetName("X Axis");
		table.AddColumn(arrX);

		vtkFloatArray arrC = new vtkFloatArray();
		arrC.SetName("Cosine");
		table.AddColumn(arrC);

		vtkFloatArray arrS = new vtkFloatArray();
		arrS.SetName("Sine");
		table.AddColumn(arrS);

		int numPoints = 50;
		float inc = 7.0f / (numPoints - 1);
		for (int i = 0; i < numPoints; ++i) {
			arrX.InsertNextValue(i * inc);
			arrC.InsertNextValue((float) Math.cos(i * inc));
			arrS.InsertNextValue((float) Math.sin(i * inc));
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Plot");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setLayout(new BorderLayout());
				panel = new VtkPlotPanel(table);
				exitButton = new JButton("Exit");
				exitButton.addActionListener(VtkPlotPanelDemo.this);
				panel.setColor(1, 1.0, 0.0, 0.0);
				panel.setColor(2, 0.0, 0.0, 1.0);
				panel.setWidth(1, 1.0f);
				panel.setWidth(2, 2.0f);
				frame.getContentPane().add(panel, BorderLayout.CENTER);
				frame.getContentPane().add(exitButton, BorderLayout.SOUTH);
				frame.setMinimumSize(new Dimension(640, 480));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.validate();
				frame.pack();
			}
		});

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(exitButton)) {
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		new VtkPlotPanelDemo();
	}

}
