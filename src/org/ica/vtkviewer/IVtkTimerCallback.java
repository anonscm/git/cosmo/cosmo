package org.ica.vtkviewer;

import org.ica.vtkviewer.VtkTimerCallback.STATUS;

import vtk.vtkRenderWindowInteractor;

public interface IVtkTimerCallback {
	public void run();

	public boolean isRunning();

	public vtkRenderWindowInteractor getInteractor();

	public void setStatus(STATUS status);

}