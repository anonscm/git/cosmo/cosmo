package org.ica.vtkviewer;

import org.ica.support.EventManager;
import org.ica.support.IListener;

import vtk.vtkRenderWindowInteractor;

public abstract class VtkTimerCallback implements IVtkTimerCallback  {
	public enum STATUS {
		STOP, RUN
	};
	public STATUS status = STATUS.STOP;
	protected final EventManager emitter = new EventManager(this);
	protected vtkRenderWindowInteractor interactor;
	protected int timerCount = 0;
	protected int timerId;
	
	public VtkTimerCallback(vtkRenderWindowInteractor interactor) {
		super();
		this.interactor = interactor;
		timerId = interactor.CreateRepeatingTimer(1);
		interactor.AddObserver("TimerEvent", this, "run");
		interactor.AddObserver("DestroyTimerEvent", this, "done");
		interactor.Start();
	}

	public VtkTimerCallback(VtkViewer viewer) {
		this(viewer.getPanel3D().getRenderWindowInteractor());
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	@Override
	public boolean isRunning() {
		if (status == STATUS.RUN)
			return true;
		return false;
	}

	@Override
	public vtkRenderWindowInteractor getInteractor() {
		return interactor;
	}

	public int getTimerId() {
		return timerId;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	public void resetTimer() {
		this.timerCount = 0;
	}
}
