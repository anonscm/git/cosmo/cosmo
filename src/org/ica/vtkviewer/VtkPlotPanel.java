package org.ica.vtkviewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import vtk.vtkChartXY;
import vtk.vtkColorSeries;
import vtk.vtkContextActor;
import vtk.vtkContextScene;
import vtk.vtkLookupTable;
import vtk.vtkPanel;
import vtk.vtkPlotLine;
import vtk.vtkRenderer;
import vtk.vtkTable;

public class VtkPlotPanel extends JPanel implements ComponentListener{
	private static final long serialVersionUID = 1L;
	private vtkTable table;
	private vtkChartXY chart;
	private int nbCols;
	private Map<Integer, vtkPlotLine> plots;
	private Map<Integer, ColumnAttributes> attributes;
	
	private class ColumnAttributes {
		private int id;
		private float size;
		private double[] color;

		public ColumnAttributes(int id) {
			this.id = id;
		}
	}

	public VtkPlotPanel(vtkTable table) {
		super(new BorderLayout());
		this.table = table;
		this.nbCols = (int) table.GetNumberOfColumns();
		
	    vtkColorSeries colors = new vtkColorSeries();
		colors.SetColorScheme(0); // Spectrum
		colors.SetNumberOfColors(nbCols);
	    
	    vtkLookupTable lut = new vtkLookupTable();
	    colors.BuildLookupTable(lut, 0);
	    
		this.attributes = new HashMap<Integer, ColumnAttributes>();
		
		for (int i = 0; i < nbCols; i++) {
			ColumnAttributes cd = new ColumnAttributes(i);
			cd.size = 2.0f;
			cd.color = new double[] {1.0, 0.5, 0.0};
			attributes.put(i, cd);
		}
		
		vtkPanel panel = new vtkPanel();
		vtkRenderer renderer = panel.GetRenderer();
		renderer.SetBackground(1.0, 1.0, 1.0);

		panel.resetCamera();

		chart = new vtkChartXY();
		vtkContextScene chartScene = new vtkContextScene();
		chartScene.SetGeometry(640, 480);
		vtkContextActor chartActor = new vtkContextActor();
		
		//chart.SetAutoSize(true);
		//System.out.println("size = "+this.getParent().getSize());
//		chart.SetGeometry(640, 480);
		chart.SetLayoutStrategy(0);

		chartScene.AddItem(chart);
		chartActor.SetScene(chartScene);
		chart.SetShowLegend(true);
		chart.SetTitle("Title");

		renderer.AddActor(chartActor);
		chartScene.SetRenderer(renderer);

		plots = new HashMap<Integer, vtkPlotLine>();
		for (int i = 1; i < nbCols; i++) {
			vtkPlotLine plot = new vtkPlotLine();
			plots.put(i, plot);
			plot.SetInputData(table, 0, i);
			double[] color = attributes.get(i).color; 
			plot.SetColor(color[0], color[1], color[2]);
			plot.SetWidth(attributes.get(i).size);
//			plot.SetLookupTable(lut);
			chart.AddPlot(plot);
		}
		this.add(panel);
		this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
	}

	public void setColor(int column, double r, double g, double b) {
		if(column == 0)
			throw new IndexOutOfBoundsException("column 0 (X axis values) has no attributes");
		if(column >nbCols)
			throw new IndexOutOfBoundsException("column "+column+" does not exists");
		attributes.get(column).color = new double[] {r,g,b};
		plots.get(column).SetColor(r, g, b);
	}
	
	public void setWidth(int column, float size) {
		if(column == 0)
			throw new IndexOutOfBoundsException("column 0 (X axis values) has no attributes");
		if(column >nbCols)
			throw new IndexOutOfBoundsException("column "+column+" does not exists");
		attributes.get(column).size = size;
		plots.get(column).SetWidth(size);
	}
	
	public vtkTable getTable() {
		return table;
	}

	public void setTable(vtkTable table) {
		this.table = table;
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentResized(ComponentEvent e) {
		System.out.println("resized");
		Dimension d = this.getParent().getSize();
		chart.SetGeometry(d.width, d.height);
	}

	@Override
	public void componentShown(ComponentEvent e) {}

	
}
