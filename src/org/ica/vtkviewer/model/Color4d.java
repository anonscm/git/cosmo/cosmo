/*
** Color4d.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the jCAMGraphics software package (jCAM stands for
** java Computer Aided Manufacturing). This software provides graphics and
** visualization tools for jCAM software package.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.ica.vtkviewer.model;

import java.util.Locale;

/**
 * Une couleur RGBA.
 * 
 * <p>
 * Réimplémentation de la classe Color4f du package vecmath en renommant les
 * paramètres x,y,z et w en r,g,b et a (respectivement) pour plus de lisibilité.
 * 
 * @author redonnet
 * 
 */
public class Color4d {
	public double r;
	public double g;
	public double b;
	public double a;

	public static final Color4d white = new Color4d(1.0, 1.0, 1.0);
	public static final Color4d black = new Color4d(0.0, 0.0, 0.0);
	public static final Color4d red = new Color4d(1.0, 0.0, 0.0);
	public static final Color4d green = new Color4d(0.0, 1.0, 0.0);
	public static final Color4d blue = new Color4d(0.0, 0.0, 1.0);
	public static final Color4d bluegray = new Color4d(0.42, 0.54, 0.65);
	public static final Color4d lightbluegray = new Color4d(0.76, 0.81, 0.86);
	public static final Color4d yellow = new Color4d(1.0, 1.0, 0.0);
	public static final Color4d cyan = new Color4d(0.0, 1.0, 1.0);
	public static final Color4d magenta = new Color4d(1.0, 0.0, 1.0);
	public static final Color4d teal = new Color4d(0.0, 0.5, 0.5);
	public static final Color4d orange = new Color4d(1.0, 0.65, 0.0);
	

	public Color4d(double r, double g, double b, double a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color4d(double r, double g, double b) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1.0;
	}

	public Color4d(double c[]) {
		r = c[0];
		g = c[1];
		b = c[2];
		a = c[3];
	}

	public Color4d(Color4d c) {
		r = c.r;
		g = c.g;
		b = c.b;
		a = c.a;
	}

	public Color4d() {
		r = 1.0;
		g = 1.0;
		b = 1.0;
		a = 1.0;
	}

	public String toString() {
		return new String(String.format(Locale.US, "(%.1f, %.1f, %.1f, %.1f)", this.r, this.g, this.b, this.a));
	}

	public double[] toVTK() {
		return new double[] { this.r, this.g, this.b, this.a };
	}

}
