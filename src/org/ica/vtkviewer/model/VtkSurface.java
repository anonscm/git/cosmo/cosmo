package org.ica.vtkviewer.model;

import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkFollower;
import vtk.vtkLine;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkQuad;
import vtk.vtkVectorText;
import vtk.vtkVertexGlyphFilter;

/**
 * A VTK object to render parametric surfaces.
 * 
 * Prepare and store VTK actors ready to be displayed. Default tessellation is
 * 100 over each parametric direction.
 * 
 * @author redonnet
 *
 */
public class VtkSurface extends VtkObject {
	private static int defaultTess = 100;
	private static Color4d defaultColor = new Color4d(0.0, 0.5, 0.6, 1.0);
	private static double defaultOpacity = 1.0;

	private Surface surface;
	private vtkActor surfActor;
	private vtkActor hullActor = null;
	private vtkActor pointsActor = null;
	private vtkActor normalsActor = null;

	private double umin;
	private double umax;
	private double vmin;
	private double vmax;

	private int tessU = defaultTess; // nombre de facettes en u
	private int tessV = defaultTess; // nombre de facettes en v

	private boolean visibleHull = false;
	private boolean visiblePoints = false;
	private boolean visibleNormals = false;
	private double normalsLength = 1.0;
	private int normalsStep = 5; // tessStep for normals
	private Color4d normalsColor = Color4d.black;

	public VtkSurface(Surface s, int tessU, int tessV, Color4d color, double opacity) {
		super(s);
		this.surface = s;
		this.color = color;

		umin = surface.getUmin();
		umax = surface.getUmax();
		vmin = surface.getVmin();
		vmax = surface.getVmax();

		vtkPoints surfPoints = new vtkPoints();
		double u, v;
		Point3d p = new Point3d();

		for (int i = 0; i <= tessU; i++) {
			for (int j = 0; j <= tessV; j++) {
				u = umin + ((double) i) * (umax - umin) / tessU;
				v = vmin + ((double) j) * (vmax - vmin) / tessV;
				surface.eval(p, u, v);
				surfPoints.InsertNextPoint(p.x, p.y, p.z);
			}
		}

		vtkCellArray quads = new vtkCellArray();
		vtkQuad quad = new vtkQuad();
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				quad.GetPointIds().SetId(0, i * (tessU + 1) + j);
				quad.GetPointIds().SetId(1, i * (tessV + 1) + j + 1);
				quad.GetPointIds().SetId(2, (i + 1) * (tessU + 1) + j + 1);
				quad.GetPointIds().SetId(3, (i + 1) * (tessV + 1) + j);
				quads.InsertNextCell(quad);
			}
		}

		// Build a simple VTK pipeline
		vtkPolyData surfPolyData = new vtkPolyData();
		surfPolyData.SetPoints(surfPoints);
		surfPolyData.SetPolys(quads);
		vtkDataSetMapper surfMapper = new vtkDataSetMapper();
		surfMapper.SetInputData(surfPolyData);

		// Main surface actor
		surfActor = new vtkActor();
		surfActor.SetMapper(surfMapper);
		surfActor.GetProperty().SetRepresentationToSurface();
		surfActor.GetProperty().BackfaceCullingOff();
		this.setOpacity(opacity);
		this.setColor(color);

		actors.add(surfActor);
	}

	public VtkSurface(Surface surface, Color4d color) {
		this(surface, defaultTess, defaultTess, color, defaultOpacity);
	}

	public VtkSurface(Surface surface, double opacity) {
		this(surface, defaultTess, defaultTess, defaultColor, opacity);
	}

	public VtkSurface(Surface surface) {
		this(surface, defaultTess, defaultTess, defaultColor, defaultOpacity);
	}

	public Surface getSurface() {
		return surface;
	}

	public void setColor(Color4d c) {
		surfActor.GetProperty().SetColor(c.toVTK());
	}

	public void setOpacity(double opacity) {
		surfActor.GetProperty().SetOpacity(opacity);
	}

	private void addHullActor() {
		if (!(surface instanceof BezierSurface))
			return;

		BezierSurface sbez;
		sbez = (BezierSurface) surface;

		Point3d p = new Point3d();

		vtkPoints hullPoints = new vtkPoints();
		for (int i = 0; i < sbez.getNpu(); i++) {
			for (int j = 0; j < sbez.getNpv(); j++) {
				p = sbez.getPoint(i, j);
				hullPoints.InsertNextPoint(p.x, p.y, p.z);
			}
		}

		vtkCellArray lines = new vtkCellArray();
		vtkLine line = new vtkLine();
		for (int i = 0; i < sbez.getNpu(); i++) {
			for (int j = 0; j < sbez.getNpv() - 1; j++) {
				line.GetPointIds().SetId(0, i * sbez.getNpv() + j);
				line.GetPointIds().SetId(1, i * sbez.getNpv() + j + 1);
				lines.InsertNextCell(line);
			}
		}
		for (int i = 0; i < sbez.getNpu(); i++) {
			for (int j = 0; j < sbez.getNpv() - 1; j++) {
				line.GetPointIds().SetId(0, j * sbez.getNpu() + i);
				line.GetPointIds().SetId(1, (j + 1) * sbez.getNpu() + i);
				lines.InsertNextCell(line);
			}
		}
		vtkPolyData hullPolyData = new vtkPolyData();
		hullPolyData.SetPoints(hullPoints);
		hullPolyData.SetLines(lines);

		vtkDataSetMapper hullMapper = new vtkDataSetMapper();
		hullMapper.SetInputData(hullPolyData);
		this.hullActor = new vtkActor();
		hullActor.SetMapper(hullMapper);
		hullActor.GetProperty().SetColor(1.0, 1.0, 0.0);
		hullActor.GetProperty().SetLineWidth(2);
		hullActor.SetVisibility(VTKBOOL_TRUE);
		actors.add(hullActor);
	}

	private void addPointsActor() {
		if (!(surface instanceof BezierSurface))
			return;

		BezierSurface sbez;
		sbez = (BezierSurface) surface;

		Point3d p = new Point3d();
		vtkPoints points = new vtkPoints();
		for (int i = 0; i < sbez.getNpu(); i++) {
			for (int j = 0; j < sbez.getNpv(); j++) {
				p = sbez.getPoint(i, j);
				points.InsertNextPoint(p.x, p.y, p.z);
			}
		}

		vtkPolyData pointsPolyData = new vtkPolyData();
		pointsPolyData.SetPoints(points);

		vtkVertexGlyphFilter VGF = new vtkVertexGlyphFilter();
		VGF.AddInputData(pointsPolyData);
		VGF.Update();

		vtkPolyDataMapper pointsMapper = new vtkPolyDataMapper();
		pointsMapper.SetInputConnection(VGF.GetOutputPort());

		this.pointsActor = new vtkActor();
		pointsActor.SetMapper(pointsMapper);
		pointsActor.GetProperty().SetColor(1.0, 1.0, 0.0);
		pointsActor.GetProperty().SetPointSize(5.0f);
		actors.add(pointsActor);
	}

	private void addNormalsActor() {
		vtkPoints normalsPoints = new vtkPoints();
		double u, v;
		Point3d p = new Point3d();
		Vector3d n = new Vector3d();

		for (int i = 0; i <= tessU; i = i + normalsStep) {
			for (int j = 0; j <= tessV; j = j + normalsStep) {
				u = umin + ((double) i) * (umax - umin) / tessU;
				v = vmin + ((double) j) * (vmax - vmin) / tessV;
				surface.eval(p, u, v);
				normalsPoints.InsertNextPoint(p.x, p.y, p.z);
				n = surface.normal(u, v);
				n.scale(normalsLength);
				p.add(n);
				normalsPoints.InsertNextPoint(p.x, p.y, p.z);
			}
		}

		vtkCellArray normals = new vtkCellArray();
		vtkLine line = new vtkLine();
		for (int i = 0; i < normalsPoints.GetNumberOfPoints(); i = i + 2) {
			line.GetPointIds().SetId(0, i);
			line.GetPointIds().SetId(1, i + 1);
			normals.InsertNextCell(line);
		}

		vtkPolyData normalsPolyData = new vtkPolyData();
		normalsPolyData.SetPoints(normalsPoints);
		normalsPolyData.SetLines(normals);

		vtkDataSetMapper normalsMapper = new vtkDataSetMapper();
		normalsMapper.SetInputData(normalsPolyData);

		// Main surface actor
		normalsActor = new vtkActor();
		normalsActor.SetMapper(normalsMapper);
		normalsActor.GetProperty().SetColor(normalsColor.toVTK());

		actors.add(normalsActor);
	}

	@SuppressWarnings("unused")
	private void addCoordsActor() {
		// FIXME Text intersects surface
		vtkVectorText textSource = new vtkVectorText();
		textSource.SetText("");

		vtkPolyDataMapper textMapper = new vtkPolyDataMapper();
		textMapper.SetInputConnection(textSource.GetOutputPort());

		vtkFollower follower = new vtkFollower();
		follower.SetMapper(textMapper);
		follower.GetProperty().SetColor(1, 1, 1);

		actors.add(follower);
	}

	public int getTessU() {
		return tessU;
	}

	public void setTessU(int tessU) {
		this.tessU = tessU;
	}

	public int getTessV() {
		return tessV;
	}

	public void setTessV(int tessV) {
		this.tessV = tessV;
	}

	public vtkActor getSurfActor() {
		return surfActor;
	}

	public vtkActor getHullActor() {
		return actors.get(1);
	}

	public boolean isVisibleHull() {
		return visibleHull;
	}

	public void setHullVisibility(boolean hullVisiblity) {
		if (hullVisiblity)
			if (this.hullActor == null)
				this.addHullActor();
			else
				hullActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.hullActor != null)
			hullActor.SetVisibility(VTKBOOL_FALSE);
		setPointsVisibility(hullVisiblity);
		this.visibleHull = hullVisiblity;
	}

	public boolean isVisiblePoints() {
		return visiblePoints;
	}

	public void setPointsVisibility(boolean pointsVisibility) {
		if (pointsVisibility)
			if (this.pointsActor == null)
				this.addPointsActor();
			else
				pointsActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.pointsActor != null)
			pointsActor.SetVisibility(VTKBOOL_FALSE);
		this.visiblePoints = pointsVisibility;
	}

	public void showHull() {
		setHullVisibility(true);
	}

	public void hideHull() {
		setHullVisibility(false);
	}

	public boolean isVisibleNormals() {
		return visibleNormals;
	}

	public void setVisibleNormals(boolean visibleNormals) {
		this.visibleNormals = visibleNormals;
		if (visibleNormals)
			if (this.normalsActor == null)
				this.addNormalsActor();
			else
				normalsActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.normalsActor != null)
			normalsActor.SetVisibility(VTKBOOL_FALSE);
	}

	public double getNormalsLength() {
		return normalsLength;
	}

	public void setNormalsLength(double normalsLength) {
		this.normalsLength = normalsLength;
	}

	public Color4d getNormalsColor() {
		return normalsColor;
	}

	public void setNormalsColor(Color4d normalsColor) {
		this.normalsColor = normalsColor;
	}

	public int getNormalsStep() {
		return normalsStep;
	}

	public void setNormalsStep(int normalsStep) {
		this.normalsStep = normalsStep;
	}

}
