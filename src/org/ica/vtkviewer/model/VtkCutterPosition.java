package org.ica.vtkviewer.model;

import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.PRG_TYPE;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyLine;

public class VtkCutterPosition extends VtkObject {
	private vtkPoints points;
	private vtkCellArray lines;
	private vtkPolyData data;
	private vtkDataSetMapper mapper;
	private vtkActor actor;
	private int nSeg = 2;;

	public VtkCutterPosition(List<? extends Point3d> pts, float linewidth, Color4d color) {
		super(pts);
		if (MillingContext.getInstance().getPrgType() == PRG_TYPE.CL)
			nSeg = 2;
		if (MillingContext.getInstance().getPrgType() == PRG_TYPE.CLT)
			nSeg = 3;

		points = new vtkPoints();
		lines = new vtkCellArray();

		for (Point3d p : pts) {
			points.InsertNextPoint(p.x, p.y, p.z);
		}
		for (int i = 0; i < pts.size(); i = i + nSeg + 1) {
			vtkPolyLine polyLine = new vtkPolyLine();
			polyLine.GetPointIds().SetNumberOfIds(nSeg + 1);
			for (int j = 0; j < nSeg + 1; j++)
				polyLine.GetPointIds().SetId(j, i + j);
			lines.InsertNextCell(polyLine);
		}

		data = new vtkPolyData();

		data.SetPoints(points);
		data.SetLines(lines);
		mapper = new vtkDataSetMapper();
		mapper.SetInputData(data);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetLineWidth(linewidth);
		actor.GetProperty().SetOpacity(color.a);
		actors.add(actor);
	}
}
