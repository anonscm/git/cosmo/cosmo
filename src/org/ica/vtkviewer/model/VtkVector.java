package org.ica.vtkviewer.model;

import java.util.Random;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

import vtk.vtkActor;
import vtk.vtkArrowSource;
import vtk.vtkMath;
import vtk.vtkMatrix4x4;
import vtk.vtkPolyDataMapper;
import vtk.vtkTransform;
import vtk.vtkTransformPolyDataFilter;

/**
 * A VTK object to render a single vector
 * 
 * Actually a source vector (1,0,0) is created and then transformed to match
 * start and end vector.
 *
 * The rotation matrix between vector (1,0,0) and given vector (x,y,z) is
 * calculated as follows:
 * 
 * vecZ = vecX x randomVec
 * vecY = vecZ x vecX
 * 
 * rotation = [ vecX | vecY | vecZ]
 * 
 * @author redonnet
 *
 */
public class VtkVector extends VtkObject {

	public VtkVector(Point3d start, Point3d end) {
		this(start, end, new Color4d(1.0, 0.0, 0.0));
	}

	public VtkVector(Point3d start, Point3d end, Color4d color) {
		super(null);

		this.color = color;
		
		double sp[] = new double[3];
		start.get(sp);

		Vector3d v = new Vector3d(start, end);
		double length = v.length();

		vtkMath vecMath = new vtkMath();

		// Compute a basis
		double normalizedX[] = new double[3];
		double normalizedY[] = new double[3];
		double normalizedZ[] = new double[3];

		v.get(normalizedX);
		vecMath.Normalize(normalizedX);

		Random r = new Random();
		double randomVec[] = new double[] { r.nextDouble() * 10, r.nextDouble() * 10, r.nextDouble() * 10 };
		vecMath.Cross(normalizedX, randomVec, normalizedZ);
		vecMath.Normalize(normalizedZ);
		vecMath.Cross(normalizedZ, normalizedX, normalizedY);
		
	    vtkMatrix4x4 matrix = new vtkMatrix4x4();
	    matrix.Identity();
	    for(int i=0;i<3;i++) {
	      matrix.SetElement(i, 0, normalizedX[i]);
	      matrix.SetElement(i, 1, normalizedY[i]);
	      matrix.SetElement(i, 2, normalizedZ[i]);
	    } 

	    vtkArrowSource arrowSource = new vtkArrowSource();

	    vtkTransform transform = new vtkTransform();
	    transform.Translate(sp);
	    transform.Concatenate(matrix);
	    transform.Scale(length, length, length);

	    // Transform the polydata
	    vtkTransformPolyDataFilter transformPD = new vtkTransformPolyDataFilter();
	    transformPD.SetTransform(transform);
	    transformPD.SetInputConnection(arrowSource.GetOutputPort());

	    vtkPolyDataMapper mapper = new vtkPolyDataMapper();
	    vtkActor arrowActor = new vtkActor();
	    mapper.SetInputConnection(arrowSource.GetOutputPort());

	    arrowActor.SetMapper(mapper);
	    arrowActor.SetUserMatrix(transform.GetMatrix());
	    arrowActor.GetProperty().SetColor(this.color.toVTK());
	    
	    actors.add(arrowActor);
	    
	}
}
