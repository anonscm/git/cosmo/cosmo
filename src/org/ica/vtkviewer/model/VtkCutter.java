package org.ica.vtkviewer.model;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.cutter.Cutter;

import vtk.vtkActor;
import vtk.vtkAppendPolyData;
import vtk.vtkCellArray;
import vtk.vtkCleanPolyData;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataNormals;
import vtk.vtkQuad;
import vtk.vtkTriangle;

public class VtkCutter extends VtkObject {
	private Cutter cutter;
	private double opacity;
	private int tessU = 36;
	private int tessV = 9;
	private vtkPolyData polyData;

	public VtkCutter(Cutter cutter, Color4d color, double opacity) {
		super(cutter);
		this.cutter = cutter;
		this.color = color;
		this.opacity = opacity;

		long nbPts;
		double u, v;
		double x, y, z;
		double R = cutter.getRadius();
		double r = cutter.getTipRadius();
		double l = cutter.getLenght();

		// Create a triangle fan
		vtkPoints basePoints = new vtkPoints();
		z = -cutter.getTipRadius();
		basePoints.InsertNextPoint(0.0, 0.0, z);
		for (int i = 0; i < tessU; i++) {
			u = toRadians((double) i * 360.0 / tessU);
			x = (R - r) * cos(u);
			y = (R - r) * sin(u);
			basePoints.InsertNextPoint(x, y, z);
		}
		nbPts = basePoints.GetNumberOfPoints();
		
		vtkCellArray baseFan = new vtkCellArray();
		vtkTriangle triangle;
		for (int i = 1; i < tessU; i++) {
			triangle = new vtkTriangle();
			triangle.GetPointIds().SetId(0, 0);
			triangle.GetPointIds().SetId(1, i);
			triangle.GetPointIds().SetId(2, i + 1);
			baseFan.InsertNextCell(triangle);
		}
		triangle = new vtkTriangle();
		triangle.GetPointIds().SetId(0, 0);
		triangle.GetPointIds().SetId(1, nbPts - 1);
		triangle.GetPointIds().SetId(2, 1);
		baseFan.InsertNextCell(triangle);

		vtkPolyData basePolyData = new vtkPolyData();
		basePolyData.SetPoints(basePoints);
		basePolyData.SetPolys(baseFan);
		vtkDataSetMapper baseMapper = new vtkDataSetMapper();
		baseMapper.SetInputData(basePolyData);

		vtkActor baseActor = new vtkActor();
		baseActor.SetMapper(baseMapper);
		baseActor.GetProperty().SetColor(color.toVTK());
		baseActor.GetProperty().SetRepresentationToSurface();
		baseActor.GetProperty().BackfaceCullingOff();
		actors.add(baseActor);

		// Create torus part
		vtkPoints torusPoints = new vtkPoints();
		for (int i = 0; i < tessU; i++) {
			u = toRadians((double) i * 360.0 / (double) tessU);
			for (int j = 0; j < tessV + 1; j++) {
				v = toRadians((double) j * 90.0 / (double) tessV);
				x = (R - r + r * cos(v)) * cos(u);
				y = (R - r + r * cos(v)) * sin(u);
				z = -r * sin(v);
				torusPoints.InsertNextPoint(x, y, z);
			}
		}
		nbPts = torusPoints.GetNumberOfPoints();

		vtkCellArray torus = new vtkCellArray();
		vtkQuad quad;
		for (int i = 0; i < tessU - 1; i++) {
			quad = new vtkQuad();
			for (int j = 0; j < tessV; j++) {
				quad.GetPointIds().SetId(0, (tessV + 1) * i + j);
				quad.GetPointIds().SetId(1, (tessV + 1) * i + j + 1);
				quad.GetPointIds().SetId(2, (tessV + 1) * (i + 1) + j + 1);
				quad.GetPointIds().SetId(3, (tessV + 1) * (i + 1) + j);
				torus.InsertNextCell(quad);
			}
		}
		for (int j = 0; j < tessV; j++) {
			quad = new vtkQuad();
			quad.GetPointIds().SetId(0, nbPts - tessV - 1 + j);
			quad.GetPointIds().SetId(1, nbPts - tessV - 1 + j + 1);
			quad.GetPointIds().SetId(2, j+1);
			quad.GetPointIds().SetId(3, j);
			torus.InsertNextCell(quad);
		}
		
		vtkPolyData torusPolyData = new vtkPolyData();
		torusPolyData.SetPoints(torusPoints);
		torusPolyData.SetPolys(torus);
		vtkPolyDataNormals torusNormals = new vtkPolyDataNormals();
		torusNormals.SetInputData(torusPolyData);
		torusNormals.SetFeatureAngle(30.0);
		torusNormals.ComputePointNormalsOn();
		torusNormals.SplittingOn();
		torusNormals.Update();
		vtkPolyData torusNormalsPolyData = new vtkPolyData();
		torusNormalsPolyData.DeepCopy(torusNormals.GetOutput());
		vtkDataSetMapper torusMapper = new vtkDataSetMapper();
		torusMapper.SetInputData(torusPolyData);
		torusMapper.SetInputData(torusNormalsPolyData);
		torusMapper.ScalarVisibilityOff();

		vtkActor torusActor = new vtkActor();
		torusActor.SetMapper(torusMapper);
		torusActor.GetProperty().SetColor(color.toVTK());
		torusActor.GetProperty().SetRepresentationToSurface();
		torusActor.GetProperty().BackfaceCullingOff();
		actors.add(torusActor);

		// Create cylinder part
		vtkPoints cylinderPoints = new vtkPoints();
		for (int i = 0; i < tessU; i++) {
			u = toRadians((double) i * 360.0 / (double) tessU);
			for (int j = 0; j < 2; j++) {
				x = R * cos(u);
				y = R * sin(u);
				z = l * j;
				cylinderPoints.InsertNextPoint(x, y, z);
			}
		}
		nbPts = cylinderPoints.GetNumberOfPoints();
		
		vtkCellArray cylinder = new vtkCellArray();
		for (int i = 0; i < tessU - 1; i++) {
			quad = new vtkQuad();
			quad.GetPointIds().SetId(0, 2 * i);
			quad.GetPointIds().SetId(1, 2 * i + 1);
			quad.GetPointIds().SetId(2, 2 * i + 3);
			quad.GetPointIds().SetId(3, 2 * i + 2);
			cylinder.InsertNextCell(quad);
		}
		quad = new vtkQuad();
		quad.GetPointIds().SetId(0, nbPts-2);
		quad.GetPointIds().SetId(1, nbPts-1);
		quad.GetPointIds().SetId(2, 1);
		quad.GetPointIds().SetId(3, 0);
		cylinder.InsertNextCell(quad);

		vtkPolyData cylinderPolyData = new vtkPolyData();
		cylinderPolyData.SetPoints(cylinderPoints);
		cylinderPolyData.SetPolys(cylinder);
		vtkPolyDataNormals cylinderNormals = new vtkPolyDataNormals();
		cylinderNormals.SetInputData(cylinderPolyData);
		cylinderNormals.SetFeatureAngle(30.0);
		cylinderNormals.ComputePointNormalsOn();
		cylinderNormals.SplittingOn();
		cylinderNormals.Update();
		vtkPolyData cylinderNormalsPolyData = new vtkPolyData();
		cylinderNormalsPolyData.DeepCopy(cylinderNormals.GetOutput());
		vtkDataSetMapper cylinderMapper = new vtkDataSetMapper();
		cylinderMapper.SetInputData(cylinderPolyData);
		cylinderMapper.SetInputData(cylinderNormalsPolyData);
		cylinderMapper.ScalarVisibilityOff();

		vtkActor cylinderActor = new vtkActor();
		cylinderActor.SetMapper(cylinderMapper);
		cylinderActor.GetProperty().SetColor(color.toVTK());
		cylinderActor.GetProperty().SetRepresentationToSurface();
		cylinderActor.GetProperty().BackfaceCullingOff();
		actors.add(cylinderActor);

		// Create a top fan
		vtkPoints topPoints = new vtkPoints();
		z = l;
		topPoints.InsertNextPoint(0.0, 0.0, z);
		for (int i = 0; i < tessU; i++) {
			double a = toRadians((double) i * 360 / tessU);
			x = R * cos(a);
			y = R * sin(a);
			topPoints.InsertNextPoint(x, y, z);
		}
		nbPts = topPoints.GetNumberOfPoints();
		vtkCellArray topFan = new vtkCellArray();
		for (int i = 1; i < tessU; i++) {
			triangle = new vtkTriangle();
			triangle.GetPointIds().SetId(0, 0);
			triangle.GetPointIds().SetId(1, i);
			triangle.GetPointIds().SetId(2, i + 1);
			topFan.InsertNextCell(triangle);
		}
		triangle = new vtkTriangle();
		triangle.GetPointIds().SetId(0, 0);
		triangle.GetPointIds().SetId(1, topPoints.GetNumberOfPoints() - 1);
		triangle.GetPointIds().SetId(2, 1);
		topFan.InsertNextCell(triangle);

		vtkPolyData topPolyData = new vtkPolyData();
		topPolyData.SetPoints(topPoints);
		topPolyData.SetPolys(topFan);
		vtkDataSetMapper topMapper = new vtkDataSetMapper();
		topMapper.SetInputData(topPolyData);

		vtkActor topActor = new vtkActor();
		topActor.SetMapper(topMapper);
		topActor.GetProperty().SetColor(color.toVTK());
		topActor.GetProperty().SetRepresentationToSurface();
		topActor.GetProperty().BackfaceCullingOff();
		actors.add(topActor);
		
		// Combine all polydatas
		vtkAppendPolyData appendFilter = new vtkAppendPolyData();
		appendFilter.AddInputData(basePolyData);
		appendFilter.AddInputData(torusPolyData);
		appendFilter.AddInputData(cylinderPolyData);
		appendFilter.AddInputData(topPolyData);
		
		vtkCleanPolyData cleanFilter = new vtkCleanPolyData();
		cleanFilter.SetInputConnection(appendFilter.GetOutputPort());
		cleanFilter.Update();
		this.polyData = cleanFilter.GetOutput();
		
	}

	public Cutter getCutter() {
		return cutter;
	}

	public void setPosition(Point3d p) {
		for(vtkActor a : actors) {
			a.SetPosition(p.x, p.y,  p.z);
		}
	}
	
	public double getOpacity() {
		return opacity;
	}

	public vtkPolyData getPolyData() {
		return polyData;
	}

}
