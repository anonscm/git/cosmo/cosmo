package org.ica.vtkviewer.model;

import vtk.vtkActor;

/**
 * Generic VtkObject built directly from an actor.
 * 
 * For debug purpose only.
 * 
 * @author redonnet
 *
 */
public class VtkActorObject extends VtkObject {

	public VtkActorObject(vtkActor actor) {
		super(null);
		this.actors.add(actor);
	}

}
