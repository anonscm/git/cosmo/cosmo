package org.ica.vtkviewer.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import vtk.vtkActor;

/**
 * Model class for graphical objects.
 * 
 * Intended to be observed be actual rendering objects (i.e. VtkPanel)
 *
 * @author Jean-Max Redonnet
 */
public class VtkObject {
	public static final int VTKBOOL_FALSE = 0;
	public static final int VTKBOOL_TRUE = 1;

	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	protected List<vtkActor> actors = new ArrayList<vtkActor>();
	protected boolean visible;
	protected Color4d color;
	protected Object source;

	protected VtkObject(Object source) {
		this.source = source;
		visible = true;
	}

	public Object getSource() {
		return source;
	}

	protected boolean isVisible() {
		return visible;
	}

	protected void show() {
		for (vtkActor a : actors)
			a.VisibilityOn();
		this.visible = true;
		this.pcs.firePropertyChange("visibility", !visible, visible);
	}

	protected void hide() {
		for (vtkActor a : actors)
			a.VisibilityOff();
		this.visible = false;
		this.pcs.firePropertyChange("visibility", !visible, visible);
	}

	protected void setVisibility(boolean visible) {
		if (visible)
			for (vtkActor a : actors)
				a.VisibilityOn();
		else
			for (vtkActor a : actors)
				a.VisibilityOff();
		this.visible = visible;
		this.pcs.firePropertyChange("visibility", !visible, visible);
	}

	public void setOpacity(double opacity) {
		for (vtkActor a : actors)
			a.GetProperty().SetOpacity(opacity);
	}

	public List<vtkActor> getActors() {
		return actors;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
}