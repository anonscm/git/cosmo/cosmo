package org.ica.vtkviewer.model;

import java.util.List;

import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Tuple3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkDelaunay2D;
import vtk.vtkFloatArray;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkProbeFilter;
import vtk.vtkQuad;

/**
 * An object to display color map surfaces.
 * 
 * The data parameter contains values to map to color scale associated with
 * (u,v) parameters. The structure of each tuple is (u,v,value).
 * 
 * Data do not have to be organized. A 2D Delaunay algorithm is performed to
 * reconstruct a basic structure.
 * 
 * If probe is set to <b>true</b> a regular grid is used to reconstruct final
 * surface structure. Data values associated to grid points are then
 * interpolated from Delaunay's algorithm output.
 * 
 * If probe is set to <b> false </b> the Delaunay algorithm output points are
 * directly displayed.
 * 
 * If sufficient data, well distributed across the parameter domain, are
 * provided isProbe may be set to <b>false</b>
 * 
 * @author redonnet
 *
 */
public class VtkColorMapSurface extends VtkObject {
	private static int defaultTess = 100;

	/** the surface */
	private Surface surface;
	private vtkDataSetMapper surfMapper;
	private vtkActor surfActor;

	private int tessU = defaultTess; // nombre de facettes en u
	private int tessV = defaultTess; // nombre de facettes en v

	/** the data. Format is (u,v,value) */
	private List<Tuple3d> data;

	/** indicates if probing data through a regular grid should be used or not */
	private boolean isProbe;

	private double umin;
	private double umax;
	private double vmin;
	private double vmax;

	public VtkColorMapSurface(Surface s, int tessU, int tessV, List<Tuple3d> data, boolean probe) {
		super(s);
		this.surface = s;
		this.tessU = tessU;
		this.tessV = tessV;
		this.data = data;
		this.isProbe = probe;

		umin = surface.getUmin();
		umax = surface.getUmax();
		vmin = surface.getVmin();
		vmax = surface.getVmax();

		vtkPoints uvPoints = new vtkPoints();
		vtkFloatArray values = new vtkFloatArray();

		for (Tuple3d t : data) {
			uvPoints.InsertNextPoint(t.x, t.y, 0);
			values.InsertNextValue((float) t.z);
		}

		vtkPolyData sourcePolyData = new vtkPolyData();
		sourcePolyData.SetPoints(uvPoints);
		sourcePolyData.GetPointData().SetScalars(values);

		vtkDelaunay2D delaunay = new vtkDelaunay2D();
		delaunay.SetInputData(sourcePolyData);
		delaunay.Update();

		if (isProbe)
			buildProbeSurface(delaunay);
		else
			buildDirectDataSurface(delaunay);

		// Main surface actor
		surfActor = new vtkActor();
		surfActor.SetMapper(surfMapper);
		surfActor.GetProperty().SetRepresentationToSurface();
		surfActor.GetProperty().BackfaceCullingOff();

		actors.add(surfActor);

	}

	public VtkColorMapSurface(Surface s, int tessU, int tessV, List<Tuple3d> data) {
		this(s, tessU, tessV, data, false);
	}

	public VtkColorMapSurface(Surface s, List<Tuple3d> data, boolean probe) {
		this(s, defaultTess, defaultTess, data, probe);
	}

	public VtkColorMapSurface(Surface s, List<Tuple3d> data) {
		this(s, defaultTess, defaultTess, data, false);
	}

	private void buildDirectDataSurface(vtkDelaunay2D delaunay) {
		vtkPoints surfPoints = new vtkPoints();

		vtkPolyData output = delaunay.GetOutput();
		surfPoints = output.GetPoints();
		double[] uv = new double[3];
		Point3d p;
		for (int i = 0; i < surfPoints.GetNumberOfPoints(); i++) {
			uv = surfPoints.GetPoint(i); // FIXME: Grab uv from uvPoints instead ?
			p = surface.eval(uv[0], uv[1]);
			surfPoints.SetPoint(i, p.x, p.y, p.z);
		}

		surfMapper = new vtkDataSetMapper();
		surfMapper.SetInputData(output);
	}

	private void buildProbeSurface(vtkDelaunay2D delaunay) {
		vtkPoints surfPoints = new vtkPoints();
		double u, v;

		for (int i = 0; i <= tessU; i++) {
			for (int j = 0; j <= tessV; j++) {
				u = umin + ((double) i) * (umax - umin) / tessU;
				v = vmin + ((double) j) * (vmax - vmin) / tessV;
				surfPoints.InsertNextPoint(u, v, 0);
			}
		}

		vtkPolyData surfPolyData = new vtkPolyData();
		surfPolyData.SetPoints(surfPoints);

		vtkProbeFilter probe = new vtkProbeFilter();
		probe.SetSourceConnection(delaunay.GetOutputPort());
		probe.SetInputData(surfPolyData); // Interpolate 'Source' at these points
		// probe.GetImageDataOutput(); // TODO: explore this
		probe.Update();
		
		if(probe.GetOutput().GetNumberOfPoints() != probe.GetValidPoints().GetNumberOfTuples())
			System.err.println("Missing points in probe... May conduct to unpredictable result");
		
		// Assign interpolated values to surfPolyData (grid) points
		surfPolyData.GetPointData().SetScalars(probe.GetOutput().GetPointData().GetScalars());

		double[] uv = new double[3];
		Point3d p;
		for (int i = 0; i < surfPoints.GetNumberOfPoints(); i++) {
			uv = surfPoints.GetPoint(i);
			p = surface.eval(uv[0], uv[1]);
			surfPoints.SetPoint(i, p.x, p.y, p.z);
		}
		
		vtkCellArray quads = new vtkCellArray();
		vtkQuad quad = new vtkQuad();
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				quad.GetPointIds().SetId(0, i * (tessU + 1) + j);
				quad.GetPointIds().SetId(1, i * (tessV + 1) + j + 1);
				quad.GetPointIds().SetId(2, (i + 1) * (tessU + 1) + j + 1);
				quad.GetPointIds().SetId(3, (i + 1) * (tessV + 1) + j);
				quads.InsertNextCell(quad);
			}
		}

		surfPolyData.SetPolys(quads);
		surfMapper = new vtkDataSetMapper();
		surfMapper.SetInputData(surfPolyData);

	}

	public List<Tuple3d> getData() {
		return data;
	}

	public vtkDataSetMapper getSurfMapper() {
		return surfMapper;
	}

	public boolean isProbe() {
		return isProbe;
	}

	public static int getDefaultTess() {
		return defaultTess;
	}
}
