package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;

import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.ica.support.IObservable;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ObservableToolpath extends ObservablePoints implements PropertyChangeListener, IObservable {

	private static final long serialVersionUID = -14480258115397988L;

	private final EventManager emitter = new EventManager(this);

	private boolean autoSync = true;
	
	public ObservableToolpath(Toolpath<? extends Point3d> tp) {
		super();
		this.addAll(tp);
		tp.addPropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		switch (evt.getPropertyName()) {
		case "InsertPoint":
		case "AppendPoint":
		case "PrependPoint":
			if(autoSync == true) {
				super.add((Point3d) evt.getNewValue());
				fireAddPointEvent((Point3d) evt.getNewValue());
			}
			break;
		case "RemovePoint":
			if(autoSync == true) {
				super.remove((Point3d) evt.getNewValue());
				fireRemovePointEvent((Point3d) evt.getNewValue());
			}
			break;
		case "Sync":
			//TODO
			break;
		default:
		}
	}

	@Override
	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	@Override
	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	@Override
	public EventManager getEventManager() {
		return emitter;
	}

}
