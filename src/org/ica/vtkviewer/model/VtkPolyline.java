package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.lgmt.dgl.curves.PointsCurve;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkVertexGlyphFilter;

/**
 * A class to display PointsCurve
 * 
 * The PointsCurve to display is observed by <b>this</b> to reflect its changes.
 * 
 * @author redonnet
 *
 */
public class VtkPolyline extends VtkObject implements PropertyChangeListener {
	protected vtkPoints points;
	protected vtkCellArray lines;
	protected vtkPolyData track;
	protected vtkPolyDataMapper mapper;
	protected vtkActor actor;
	protected vtkActor pointsActor = null;
	protected PointsCurve<? extends Point3d> curve;
	protected float width;
	protected boolean visiblePoints = true;
	protected long startId; // first vtkPoint Id
	protected long endId; // last VtkPoint Id

	public VtkPolyline(PointsCurve<? extends Point3d> curve, float width, Color4d color) {
		super(curve);
		this.curve = curve;
		this.width = width;
		this.color = color;
		this.curve.addPropertyChangeListener(this);
		points = new vtkPoints();

		lines = new vtkCellArray();

		track = new vtkPolyData();
		track.SetPoints(points);
		track.SetLines(lines);

		if (curve.getNbPoints() >= 2) {
			initData(curve);
			for (int i = 2; i < curve.getNbPoints(); i++) {
				this.appendPoint(curve.get(i));
			}
		}

		mapper = new vtkPolyDataMapper();
		mapper.SetInputData(track);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetLineWidth(width);
		actor.GetProperty().SetColor(color.r, color.g, color.b);
		actors.add(actor);
	}

	public VtkPolyline(PointsCurve<? extends Point3d> curve) {
		this(curve, 2.0f, new Color4d(1.0, 0.0, 1.0));
	}

	public VtkPolyline(ArrayList<Point3d> pList, float width, Color4d color) {
		this(new PointsCurve<Point3d>(pList), width, color);
	}

	protected void initData(PointsCurve<? extends Point3d> curve) {
		points.InsertPoint(0, curve.get(0).x, curve.get(0).y, curve.get(0).z);
		points.InsertPoint(1, curve.get(1).x, curve.get(1).y, curve.get(1).z);
		startId = 0;
		endId = 1;
		points.Modified();
		lines.InsertNextCell(2);
		lines.InsertCellPoint(0);
		lines.InsertCellPoint(1);
		return;
	}

	public void appendPoint(Point3d point) {
		if (curve.getNbPoints() < 2)
			return;
		if (curve.getNbPoints() == 2) {
			initData(curve);
			return;
		}

		long n = points.GetNumberOfPoints();
		points.InsertPoint(n, point.x, point.y, point.z);
		points.Modified();
		lines.InsertNextCell(2);
		lines.InsertCellPoint(endId);
		lines.InsertCellPoint(n);
		endId = n;
		lines.Modified();
	}

	public void prependPoint(Point3d point) {
		if (curve.getNbPoints() < 2)
			return;
		if (curve.getNbPoints() == 2) {
			initData(curve);
			return;
		}

		long n = points.GetNumberOfPoints();
		points.InsertPoint(n, point.x, point.y, point.z);
		points.Modified();
		lines.InsertNextCell(2);
		lines.InsertCellPoint(n);
		lines.InsertCellPoint(startId);
		startId = n;
		lines.Modified();
	}

	public void setVisiblePoints(boolean visiblePoints) {
		this.visiblePoints = visiblePoints;
		if (visiblePoints)
			if (this.pointsActor == null)
				this.addPointsActor();
			else
				pointsActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.pointsActor != null)
			pointsActor.SetVisibility(VTKBOOL_FALSE);
	}

	public boolean isVisiblePoints() {
		return visiblePoints;
	}

	protected void addPointsActor() {
		vtkPolyData pointsPolyData = new vtkPolyData();
		pointsPolyData.SetPoints(points);

		vtkVertexGlyphFilter VGF = new vtkVertexGlyphFilter();
		VGF.AddInputData(pointsPolyData);
		VGF.Update();

		vtkPolyDataMapper pointsMapper = new vtkPolyDataMapper();
		pointsMapper.SetInputConnection(VGF.GetOutputPort());

		vtkActor pointsActor = new vtkActor();
		pointsActor.SetMapper(pointsMapper);
		pointsActor.GetProperty().SetColor(1.0, 1.0, 0.0);
		pointsActor.GetProperty().SetPointSize(5);
		actors.add(pointsActor);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "addPoint") {
			appendPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "insertPoint") {
			int i = (int) event.getOldValue();
			if (i == 0)
				prependPoint((Point3d) event.getNewValue());
			// TODO : else
		}
		if (event.getPropertyName() == "removePoint") {
			// TODO
		}
		if (event.getPropertyName() == "replacePoint") {
			// TODO
		}
	}
}
