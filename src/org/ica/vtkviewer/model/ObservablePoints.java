package org.ica.vtkviewer.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import org.ica.cosmo.util.PointSignal;
import org.ica.support.EventManager;
import org.lgmt.dgl.vecmath.Point3d;

public class ObservablePoints extends ArrayList<Point3d>{
	private static final long serialVersionUID = -6303675815201345590L;
	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	protected final EventManager emitter = new EventManager(this);

	public ObservablePoints(){
		super();
	}

	@Override
	public boolean add(Point3d point) {
		boolean result = super.add(point);
		this.fireAddPointEvent(point);
		return result;
	}

	@Override
	public void add(int index, Point3d point) {
		super.add(index, point);
		this.fireAddPointEvent(point);
	}

	@Override
	public Point3d remove(int index) {
		Point3d result = super.remove(index);
		this.fireRemovePointEvent(result);
		return result;
	}

	public boolean remove(Point3d p) {
		boolean result = super.remove(p);
		if(result == true)
			this.fireRemovePointEvent(p);
		return result;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void fireAddPointEvent(Point3d point) {
		emitter.sendSignal(new PointSignal("AddPoint", point));
	}

	public void fireRemovePointEvent(Point3d point) {
		emitter.sendSignal(new PointSignal("RemovePoint", point));
	}

}
