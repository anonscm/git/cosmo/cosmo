package org.ica.vtkviewer.model;

import java.util.ArrayList;
import java.util.List;

import org.ica.cosmo.machining.Hull;
import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkDataSetMapper;
import vtk.vtkHull;
import vtk.vtkPoints;
import vtk.vtkPolyData;

public class VtkHull extends VtkObject {
	private vtkHull hull;
	private vtkActor hullActor = null;
	private double zMax = Double.NEGATIVE_INFINITY;
	
	public VtkHull(List<? extends Point3d> pointsList) {
		super(pointsList);
		createHull(pointsList);
	}

	public VtkHull(Surface surface) {
		super(surface);

		if (!(surface instanceof BezierSurface)) {
			throw new IllegalArgumentException("VtkHull can handle only Bezier surfaces for moment");
		}
		BezierSurface sbez = (BezierSurface) surface;
		int npu = sbez.getNpu();
		int npv = sbez.getNpv();

		List<Point3d> pList = new ArrayList<Point3d>();
		for (int i = 0; i < npu; i++) {
			for (int j = 0; j < npv; j++) {
				pList.add(sbez.getPoint(i, j));
			}

		}
		createHull(pList);
	}

	public VtkHull(Hull hull) {
		super(hull);
		this.hull = hull;
		commonBuild();
	}

	private void createHull(List<? extends Point3d> pointsList) {
		vtkPoints points = new vtkPoints();
		for (Point3d p : pointsList) {
			points.InsertNextPoint(p.x, p.y, p.z);
			if (p.z > zMax)
				zMax = p.z;
		}
		vtkPolyData hullPolyData = new vtkPolyData();
		hullPolyData.SetPoints(points);
		hull = new vtkHull();
		hull.SetInputData(hullPolyData);
		hull.AddCubeFacePlanes();
		hull.AddCubeVertexPlanes();
		hull.AddCubeEdgePlanes();
		hull.AddRecursiveSpherePlanes(1);
		hull.Update();
		commonBuild();
	}

	private void commonBuild() {
		vtkDataSetMapper hullMapper = new vtkDataSetMapper();
		hullMapper.SetInputConnection(hull.GetOutputPort());
		hullMapper.ScalarVisibilityOff();

		hullActor = new vtkActor();
		Color4d color = new Color4d(1.0, 1.0, 1.0, 0.5);
		hullActor.SetMapper(hullMapper);
		hullActor.GetProperty().SetDiffuseColor(color.toVTK());
		hullActor.GetProperty().SetEdgeColor(color.toVTK());
		hullActor.GetProperty().SetOpacity(color.a);
		hullActor.GetProperty().EdgeVisibilityOn();

		actors.add(hullActor);
	}

	public void setColor(Color4d c) {
		hullActor.GetProperty().SetColor(c.toVTK());
	}

	public vtkHull getHull() {
		return hull;
	}

}
