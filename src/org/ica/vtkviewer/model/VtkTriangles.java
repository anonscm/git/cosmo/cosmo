package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkTriangle;

/**
 * A class to represent a set of Triangles.
 *
 * Triangle are defined by triplets of points. Points belonging to several
 * triangles are repeated.
 * 
 * @author redonnet
 *
 */
public class VtkTriangles extends VtkObject implements PropertyChangeListener {
	private vtkPoints points;
	private float linewidth;
	private vtkPolyData data;
	private vtkDataSetMapper mapper;
	private vtkActor actor;
	private boolean showLines = true;

	public VtkTriangles(List<? extends Point3d> pointsList, float width, Color4d color) {
		super(pointsList);
		this.color = color;
		this.linewidth = width;
		points = new vtkPoints();
		for (Point3d p : pointsList)
			points.InsertNextPoint(p.x, p.y, p.z);

		vtkCellArray tList = new vtkCellArray();
		vtkTriangle triangle = new vtkTriangle();
		for (int i = 0; i < pointsList.size(); i = i + 3) {
			triangle.GetPointIds().SetId(0, i);
			triangle.GetPointIds().SetId(1, i+1);
			triangle.GetPointIds().SetId(2, i+2);
			tList.InsertNextCell(triangle);
		}

		data = new vtkPolyData();
		data.SetPoints(points);
		if(showLines)
			data.SetLines(tList);
		data.SetPolys(tList);
		mapper = new vtkDataSetMapper();
		mapper.SetInputData(data);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetLineWidth(linewidth);
		actor.GetProperty().SetOpacity(color.a);
		actors.add(actor);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}

}
