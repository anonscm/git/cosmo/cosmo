package org.ica.vtkviewer.model;

import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.vecmath.Matrix4d;

import vtk.vtkActor;
import vtk.vtkArrowSource;
import vtk.vtkMatrix4x4;
import vtk.vtkPolyDataMapper;
import vtk.vtkTransform;
import vtk.vtkTransformPolyDataFilter;

public class VtkFrame extends VtkObject {
	private String name;
	private double size;

	public VtkFrame(Frame f, String name, double size) {
		super(f);
		this.name = name;
		this.size = size;

		Matrix4d m = f.getTransform();
		double[] pos = new double[3];
		m.getTranslation().get(pos);

		vtkMatrix4x4 matrix = new vtkMatrix4x4();
		matrix.Identity();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix.SetElement(i, j, m.getElement(i, j));
			}
		}

		vtkArrowSource arrowSource = new vtkArrowSource();

		vtkTransform transformX = new vtkTransform();
		transformX.SetMatrix(matrix);
		transformX.Scale(size, size, size);

		// Transform the polydata
		vtkTransformPolyDataFilter transformPDX = new vtkTransformPolyDataFilter();
		transformPDX.SetTransform(transformX);
		transformPDX.SetInputConnection(arrowSource.GetOutputPort());

		vtkPolyDataMapper mapperX = new vtkPolyDataMapper();
		vtkActor arrowXActor = new vtkActor();
		mapperX.SetInputConnection(arrowSource.GetOutputPort());

		arrowXActor.SetMapper(mapperX);
		arrowXActor.SetUserMatrix(transformX.GetMatrix());
		arrowXActor.GetProperty().SetColor(Color4d.red.toVTK());

		this.actors.add(arrowXActor);

		vtkTransform transformY = new vtkTransform();
		transformY.SetMatrix(matrix);
		transformY.Scale(size, size, size);

		vtkMatrix4x4 mX2Y = new vtkMatrix4x4();
		mX2Y.Identity();
		mX2Y.SetElement(0, 0, 0);
		mX2Y.SetElement(0, 1, -1);
		mX2Y.SetElement(1, 0, 1);
		mX2Y.SetElement(1, 1, 0);
		transformY.Concatenate(mX2Y);

		// Transform the polydata
		vtkTransformPolyDataFilter transformPDY = new vtkTransformPolyDataFilter();
		transformPDY.SetTransform(transformY);
		transformPDY.SetInputConnection(arrowSource.GetOutputPort());

		vtkPolyDataMapper mapperY = new vtkPolyDataMapper();
		vtkActor arrowYActor = new vtkActor();
		mapperY.SetInputConnection(arrowSource.GetOutputPort());

		arrowYActor.SetMapper(mapperY);
		arrowYActor.SetUserMatrix(transformY.GetMatrix());
		arrowYActor.GetProperty().SetColor(Color4d.green.toVTK());

		this.actors.add(arrowYActor);

		vtkTransform transformZ = new vtkTransform();
		transformZ.SetMatrix(matrix);
		transformZ.Scale(size, size, size);

		vtkMatrix4x4 mX2Z = new vtkMatrix4x4();
		mX2Z.Identity();
		mX2Z.SetElement(0, 0, 0);
		mX2Z.SetElement(0, 2, -1);
		mX2Z.SetElement(2, 0, 1);
		mX2Z.SetElement(2, 2, 0);
		transformZ.Concatenate(mX2Z);

		// Transform the polydata
		vtkTransformPolyDataFilter transformPDZ = new vtkTransformPolyDataFilter();
		transformPDZ.SetTransform(transformZ);
		transformPDZ.SetInputConnection(arrowSource.GetOutputPort());

		vtkPolyDataMapper mapperZ = new vtkPolyDataMapper();
		vtkActor arrowZActor = new vtkActor();
		mapperZ.SetInputConnection(arrowSource.GetOutputPort());

		arrowZActor.SetMapper(mapperZ);
		arrowZActor.SetUserMatrix(transformZ.GetMatrix());
		arrowZActor.GetProperty().SetColor(Color4d.blue.toVTK());

		this.actors.add(arrowZActor);

		if (name != null) {
			VtkText vtkName = new VtkText(f.getPosition(), name, size / 10, Color4d.black);
			for (vtkActor a : vtkName.getActors())
				this.actors.add(a);
		}

	}

	public VtkFrame(Frame f) {
		this(f, null, 10);
	}

	public String getName() {
		return name;
	}

	public double getSize() {
		return size;
	}
}
