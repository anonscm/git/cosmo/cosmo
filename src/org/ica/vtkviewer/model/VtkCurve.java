package org.ica.vtkviewer.model;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.curves.Curve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkFollower;
import vtk.vtkLine;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkVectorText;

/**
 * A VTK object to render parametric surfaces.
 * 
 * Prepare and store VTK actors ready to be displayed. Default tessellation is
 * 100 over each parametric direction.
 * 
 * @author redonnet
 *
 */
public class VtkCurve extends VtkObject {
	private static int defaultTess = 100;
	private Curve curve;
	private vtkActor curvActor;
	private vtkActor polygonActor = null;
	private vtkActor pointsActor = null;
	private vtkActor normalsActor = null;

	private double umin;
	private double umax;

	private int tessU = defaultTess; // nombre de segments en u

	private boolean visiblePolygon = false;
	private boolean visiblePoints = false;
	private boolean visibleNormals = false;
	private double normalsLength = 1.0;
	private int normalsStep = 5; // tessStep for normals

	public VtkCurve(Curve c, int tessU) {
		super(c);
		this.curve = c;
		this.color = new Color4d(0.0, 0.5, 0.6, 1.0);

		umin = curve.getUmin();
		umax = curve.getUmax();

		vtkPoints curvPoints = new vtkPoints();
		double u;
		Point3d p = new Point3d();

		for (int i = 0; i <= tessU; i++) {
				u = umin + ((double) i) * (umax - umin) / tessU;
				curve.eval(p, u);
				curvPoints.InsertNextPoint(p.x, p.y, p.z);
		}

		vtkCellArray lines = new vtkCellArray();
		for (int i = 0; i < tessU-1; i++) {
			lines.InsertNextCell(2);
			lines.InsertCellPoint(i);
			lines.InsertCellPoint(i+1);
		}

		// Build a simple VTK pipeline
		vtkPolyData curvPolyData = new vtkPolyData();
		curvPolyData.SetPoints(curvPoints);
		curvPolyData.SetLines(lines);
		vtkDataSetMapper curvMapper = new vtkDataSetMapper();
		curvMapper.SetInputData(curvPolyData);

		// Main curve actor
		curvActor = new vtkActor();
		curvActor.SetMapper(curvMapper);
		this.setColor(color);

		actors.add(curvActor);
	}

	public VtkCurve(Curve curve) {
		this(curve, defaultTess);
	}

	public Curve getCurve() {
		return curve;
	}

	public void setColor(Color4d c) {
		curvActor.GetProperty().SetColor(c.toVTK());
	}


	private void addPolygonActor() {
		if (!(curve instanceof BezierCurve))
			return;

		BezierCurve cbez;
		cbez = (BezierCurve) curve;

		Point3d p = new Point3d();

		vtkPoints polygonPoints = new vtkPoints();
		for (int i = 0; i < cbez.getNpu(); i++) {
			p = cbez.getControlPoint(i);
				polygonPoints.InsertNextPoint(p.x, p.y, p.z);
		}

		vtkCellArray lines = new vtkCellArray();
		vtkLine line = new vtkLine();
		for (int i = 0; i < cbez.getNpu()-1; i++) {
				line.GetPointIds().SetId(0, i);
				line.GetPointIds().SetId(1, i + 1);
				lines.InsertNextCell(line);
		}
		vtkPolyData polygonPolyData = new vtkPolyData();
		polygonPolyData.SetPoints(polygonPoints);
		polygonPolyData.SetLines(lines);

		vtkDataSetMapper polygonMapper = new vtkDataSetMapper();
		polygonMapper.SetInputData(polygonPolyData);
		this.polygonActor = new vtkActor();
		polygonActor.SetMapper(polygonMapper);
		polygonActor.GetProperty().SetColor(1.0, 1.0, 0.0);
		polygonActor.GetProperty().SetLineWidth(2);
		polygonActor.SetVisibility(VTKBOOL_TRUE);
		actors.add(polygonActor);
	}


	private void addNormalsActor() {
		vtkPoints normalsPoints = new vtkPoints();
		double u, v;
		Point3d p = new Point3d();
		Vector3d n = new Vector3d();

		for (int i = 0; i <= tessU; i = i + normalsStep) {
				u = umin + ((double) i) * (umax - umin) / tessU;
				curve.eval(p, u);
				normalsPoints.InsertNextPoint(p.x, p.y, p.z);
				n = curve.normal(u);
				n.scale(normalsLength);
				p.add(n);
				normalsPoints.InsertNextPoint(p.x, p.y, p.z);
		}

		vtkCellArray normals = new vtkCellArray();
		vtkLine line = new vtkLine();
		for (int i = 0; i < normalsPoints.GetNumberOfPoints(); i = i + 2) {
			line.GetPointIds().SetId(0, i);
			line.GetPointIds().SetId(1, i + 1);
			normals.InsertNextCell(line);
		}

		vtkPolyData normalsPolyData = new vtkPolyData();
		normalsPolyData.SetPoints(normalsPoints);
		normalsPolyData.SetLines(normals);

		vtkDataSetMapper normalsMapper = new vtkDataSetMapper();
		normalsMapper.SetInputData(normalsPolyData);

		// Main surface actor
		normalsActor = new vtkActor();
		normalsActor.SetMapper(normalsMapper);
		normalsActor.GetProperty().SetRepresentationToSurface();

		actors.add(normalsActor);
	}

	@SuppressWarnings("unused")
	private void addCoordsActor() {
		// FIXME Text intersects surface
		vtkVectorText textSource = new vtkVectorText();
		textSource.SetText("");

		vtkPolyDataMapper textMapper = new vtkPolyDataMapper();
		textMapper.SetInputConnection(textSource.GetOutputPort());

		vtkFollower follower = new vtkFollower();
		follower.SetMapper(textMapper);
		follower.GetProperty().SetColor(1, 1, 1);

		actors.add(follower);
	}

	public int getTessU() {
		return tessU;
	}

	public void setTessU(int tessU) {
		this.tessU = tessU;
	}

	public vtkActor getCurvActor() {
		return curvActor;
	}

	public vtkActor getPolygonActor() {
		return actors.get(1);
	}

	public boolean isVisiblePolygon() {
		return visiblePolygon;
	}

	public void setPolygonVisibility(boolean polygonVisiblity) {
		if (polygonVisiblity)
			if (this.polygonActor == null)
				this.addPolygonActor();
			else
				polygonActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.polygonActor != null)
			polygonActor.SetVisibility(VTKBOOL_FALSE);
		this.visiblePolygon = polygonVisiblity;
	}

	public void showPolygon() {
		setPolygonVisibility(true);
	}

	public void hidePolygon() {
		setPolygonVisibility(false);
	}


	public boolean isVisibleNormals() {
		return visibleNormals;
	}

	public void setVisibleNormals(boolean visibleNormals) {
		this.visibleNormals = visibleNormals;
		if (visibleNormals)
			if (this.normalsActor == null)
				this.addNormalsActor();
			else
				normalsActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.normalsActor != null)
			normalsActor.SetVisibility(VTKBOOL_FALSE);
	}

	public double getNormalsLength() {
		return normalsLength;
	}

	public void setNormalsLength(double normalsLength) {
		this.normalsLength = normalsLength;
	}

	public int getNormalsStep() {
		return normalsStep;
	}

	public void setNormalsStep(int normalsStep) {
		this.normalsStep = normalsStep;
	}

}
