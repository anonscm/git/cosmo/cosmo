package org.ica.vtkviewer.model;

import java.util.Collections;

import org.ica.cosmo.util.PointSignal;
import org.ica.support.IListener;
import org.ica.support.Signal;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkVertexGlyphFilter;

/**
 * A class to represent a set of Points.
 *
 * Intended to be used with ObservablePointsList
 * 
 * @author redonnet
 *
 */
public class VtkObservablePoints extends VtkObject implements IListener {
	private vtkPoints points;
	private float size;
	private vtkPolyData data;
	private vtkPolyDataMapper mapper;
	private vtkActor actor;
	private ObservablePoints pointsList;

	public VtkObservablePoints(ObservablePoints pointsList, float size, Color4d color) {
		super(Collections.synchronizedList(pointsList));
		this.pointsList = pointsList;
		this.pointsList.emitter.addListener(this);
		this.size = size;
		this.color = color;

		points = new vtkPoints();
		for (Point3d p : pointsList)
			points.InsertNextPoint(p.x, p.y, p.z);

		data = new vtkPolyData();
		data.SetPoints(points);

		vtkVertexGlyphFilter VGF = new vtkVertexGlyphFilter();
		VGF.AddInputData(data);
		VGF.Update();

		mapper = new vtkPolyDataMapper();
		mapper.SetInputConnection(VGF.GetOutputPort());

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetPointSize(this.size);
		actors.add(actor);
	}

	public void addPoint(Point3d p) {
		points.InsertNextPoint(p.x, p.y, p.z);
		points.Modified();
	}

	public void rebuild() {
		vtkPoints newPoints = new vtkPoints();
		for (Point3d p : pointsList)
			newPoints.InsertNextPoint(p.x, p.y, p.z);
		this.points.ShallowCopy(newPoints);
		this.points.Modified();
	}

	@Override
	public void onReceivedSignal(Signal signal) {
		if (signal instanceof PointSignal) {
			PointSignal s = (PointSignal) signal;
			if (s.getName() == "AddPoint") {
				this.addPoint(s.getPoint());
			}
			if (s.getName() == "RemovePoint") {
				this.rebuild();
			}
		}
	}

}
