package org.ica.vtkviewer.model;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkFollower;
import vtk.vtkPolyDataMapper;
import vtk.vtkVectorText;

/**
 * A VTK object to render a simple text
 */
public class VtkText extends VtkObject {
	public VtkText(Point3d location, String text) {
		this(location, text, 1.0, new Color4d(1.0, 0.0, 0.0));
	}

	public VtkText(Point3d location, String text, double scale) {
		this(location, text, scale, new Color4d(1.0, 0.0, 0.0));
	}

	public VtkText(Point3d location, String text, Color4d color) {
		this(location, text, 1.0, color);
	}

	public VtkText(Point3d location, String text, double scale, Color4d color) {
		super(text);
		vtkVectorText aText = new vtkVectorText();
		aText.SetText(text);
		vtkPolyDataMapper mapper = new vtkPolyDataMapper();
		mapper.SetInputConnection(aText.GetOutputPort());
		vtkFollower follower = new vtkFollower();
		follower.SetMapper(mapper);
		follower.SetScale(scale, scale, scale);
		follower.GetProperty().SetColor(color.r, color.g, color.b);
		follower.AddPosition(location.x, location.y, location.z);

		actors.add(follower);
	}
}
