package org.ica.vtkviewer.model;

import java.util.List;

import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.curves.PointsCurve;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkFloatArray;
import vtk.vtkLookupTable;

/**
 * A class to display a colored PointsCurve
 * 
 * The PointsCurve to display is observed by <b>this</b> to reflect its changes.
 * 
 * By default, the color scale is defined from min/max values of data. But a
 * specific range may be defined using setRange()
 * 
 * @author redonnet
 *
 */
public class VtkColorMapPolyline extends VtkPolyline {
	private vtkFloatArray values;
	private Interval range;
	private vtkLookupTable lut;

	public VtkColorMapPolyline(PointsCurve<? extends Point3d> curve, float width, List<Double> data) {
		super(curve, width, Color4d.white);
		lut = new vtkLookupTable();
		setData(data);
	}

	public VtkColorMapPolyline(PointsCurve<? extends Point3d> curve, List<Double> data) {
		super(curve);
		lut = new vtkLookupTable();
		setData(data);
	}

	public VtkColorMapPolyline(VtkPolyline polyline, List<Double> data) {
		super(polyline.curve, polyline.width, polyline.color);
		lut = new vtkLookupTable();
		setData(data);
	}

	public void setData(List<Double> data) {
		values = new vtkFloatArray();
		if (data.size() != this.points.GetNumberOfPoints())
			System.err.println("Warning : data size do not match polyline size");

		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		Double val;
		for (int i = 0; i < data.size(); i++) {
			val = data.get(i);
			if (val > max)
				max = val;
			if (val < min)
				min = val;
			values.InsertNextValue(val.floatValue());
		}
		this.track.GetPointData().SetScalars(values);
		range = new Interval(min, max);
		this.updateLUT();
	}

	public void setRange(double min, double max) {
		this.range = new Interval(min, max);
		this.updateLUT();
	}

	public Interval getRange() {
		return range;
	}

	public vtkLookupTable getLookupTable() {
		return lut;
	}

	private void updateLUT() {
		lut.SetTableRange(range.getLeftValue(), range.getRightValue());
		lut.SetHueRange(0.0, 0.4);
		lut.SetSaturationRange(1, 1);
		lut.SetValueRange(1,1);
		lut.Build();
		mapper.SetLookupTable(lut);
	}
}
