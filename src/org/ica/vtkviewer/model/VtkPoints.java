package org.ica.vtkviewer.model;

import java.util.ArrayList;
import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkVertexGlyphFilter;

/**
 * A class to represent a set of Points.
 *
 * Intended to be used with ObservablePointsList
 * 
 * @author redonnet
 *
 */
public class VtkPoints extends VtkObject {
	private vtkPoints points;
	private float size;
	private vtkPolyData data;
	private vtkPolyDataMapper mapper;
	private vtkActor actor;

	public VtkPoints() {
		this(new ArrayList<Point3d>(), 3.0f, new Color4d(1.0, 1.0, 0.0));
	}
	
	public VtkPoints(List<? extends Point3d> pointsList, float size, Color4d color) {
		super(pointsList);
		this.size = size;
		this.color = color;

		points = new vtkPoints();
		for (Point3d p : pointsList)
			points.InsertNextPoint(p.x, p.y, p.z);

		data = new vtkPolyData();
		data.SetPoints(points);

		vtkVertexGlyphFilter VGF = new vtkVertexGlyphFilter();
		VGF.AddInputData(data);
		VGF.Update();

		mapper = new vtkPolyDataMapper();
		mapper.SetInputConnection(VGF.GetOutputPort());

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetPointSize(this.size);
		actors.add(actor);
	}
	
	public void addPoint(Point3d p) {
		points.InsertNextPoint(p.x, p.y, p.z);
		points.Modified();
	}

	public void setSize(float size) {
		this.size = size;
	}

}
