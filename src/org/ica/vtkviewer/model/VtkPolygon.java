package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolygon;

/**
 * A class to represent a polygon.
 *
 * @author redonnet
 *
 */
public class VtkPolygon extends VtkObject implements PropertyChangeListener {
	private vtkPoints points;
	private vtkPolyData data;
	private vtkDataSetMapper mapper;
	private vtkActor actor;

	public VtkPolygon(List<? extends Point3d> pointsList, Color4d color) {
		super(pointsList);
		this.color = color;

		int n = pointsList.size();
		if (n < 3)
			System.err.println("Not enough points to make a polygon");

		points = new vtkPoints();
		for (Point3d p : pointsList)
			points.InsertNextPoint(p.x, p.y, p.z);

		vtkPolygon polygon = new vtkPolygon();
		polygon.GetPointIds().SetNumberOfIds(n);
		for (int i = 0; i < n; i++)
			polygon.GetPointIds().SetId(i, i);

		vtkCellArray polygons = new vtkCellArray();
		polygons.InsertNextCell(polygon);

		data = new vtkPolyData();
		data.SetPoints(points);
		data.SetPolys(polygons);
		mapper = new vtkDataSetMapper();
		mapper.SetInputData(data);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetOpacity(color.a);
		actors.add(actor);
	}

	public void addPoint(Point3d p) {
		points.InsertNextPoint(p.x, p.y, p.z);
		points.Modified();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "AddPoint") {
			addPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "RemovePoint") {
			// TODO
		}
	}

}
