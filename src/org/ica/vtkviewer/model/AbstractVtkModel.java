package org.ica.vtkviewer.model;

import java.util.List;

public abstract class AbstractVtkModel {
	protected List<VtkObject> objects;

	public abstract List<VtkObject> getObjects();

}
