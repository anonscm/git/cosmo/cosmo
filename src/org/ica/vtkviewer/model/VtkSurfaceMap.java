package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkFloatArray;
import vtk.vtkLookupTable;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkQuad;

public class VtkSurfaceMap extends VtkObject implements PropertyChangeListener {
	private vtkActor surfActor;

	private int tessU; // nombre de facettes en u
	private int tessV; // nombre de facettes en v

	private vtkPolyData surfPolyData;
	private vtkPolyDataMapper surfMapper;
	private vtkFloatArray cellData;
	private vtkLookupTable lut;

	public VtkSurfaceMap(SurfaceMap map, double opacity) {
		super(map);
		this.tessU = map.getTessU();
		this.tessV = map.getTessV();

		int nbIsoU = tessU + 1;
		int nbIsoV = tessV + 1;

		Surface surface = map.getSurface();
		double umin = surface.getUmin();
		double umax = surface.getUmax();
		double vmin = surface.getVmin();
		double vmax = surface.getVmax();

		map.addPropertyChangeListener(this);

		for (UVMeshUnit[] meshLine : map.getMeshGrid())
			for (UVMeshUnit m : meshLine)
				m.addPropertyChangeListener(this);

		vtkPoints surfPoints = new vtkPoints();
		double u, v;
		Point3d p = new Point3d();

		for (int i = 0; i < nbIsoU; i++) {
			for (int j = 0; j < nbIsoV; j++) {
				u = umin + ((double) i) * (umax - umin) / (double) (tessU);
				v = vmin + ((double) j) * (vmax - vmin) / (double) (tessV);
				surface.eval(p, u, v);
				surfPoints.InsertNextPoint(p.x, p.y, p.z);
				// indice du point inséré k = i * nbIsoV + j
			}
		}

		lut = new vtkLookupTable();
		int tableSize = map.getZonesCount();
		lut.SetNumberOfColors(tableSize);
		lut.Build();

		// Create cell data
		vtkCellArray quads = new vtkCellArray();
		cellData = new vtkFloatArray();

		vtkQuad quad = new vtkQuad();
		int zoneId;
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				quad.GetPointIds().SetId(0, i * nbIsoV + j);
				quad.GetPointIds().SetId(1, i * nbIsoV + j + 1);
				quad.GetPointIds().SetId(2, (i + 1) * nbIsoV + j + 1);
				quad.GetPointIds().SetId(3, (i + 1) * nbIsoV + j);
				quads.InsertNextCell(quad);
				zoneId = map.getZoneId(i, j);
				cellData.InsertNextValue(zoneId);
			}
		}

		// Build a simple VTK pipeline
		surfPolyData = new vtkPolyData();
		surfPolyData.SetPoints(surfPoints);
		surfPolyData.SetPolys(quads);
		surfPolyData.GetCellData().SetScalars(cellData);

		surfMapper = new vtkPolyDataMapper();
		surfMapper.SetInputData(surfPolyData);
		surfMapper.SetScalarRange(0, tableSize - 1);

		// Main surfaceMap actor
		surfActor = new vtkActor();
		surfActor.SetMapper(surfMapper);
		surfActor.GetProperty().SetOpacity(opacity);

		actors.add(surfActor);
	}

	public VtkSurfaceMap(SurfaceMap map) {
		this(map, 1.0);
	}

	public vtkLookupTable getLut() {
		return lut;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "changeZone") {
			Zone newZone = (Zone) event.getNewValue();
			UVMeshUnit m = (UVMeshUnit) event.getSource();
			int i = m.getIndex().i;
			int j = m.getIndex().j;
			cellData.InsertValue(i * tessV + j, newZone.getId());
			surfPolyData.Modified();
		}
		if (event.getPropertyName() == "nbZones") {
			int tableSize = (int) event.getNewValue();
			vtkLookupTable lut = new vtkLookupTable();
			lut.SetNumberOfColors(tableSize);
			lut.Build();
			surfMapper.SetScalarRange(0, tableSize - 1);
			surfMapper.Modified();
		}
	}
}
