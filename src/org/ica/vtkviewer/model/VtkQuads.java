package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;

/**
 * A class to represent a set of Quads.
 *
 * @author redonnet
 *
 */
public class VtkQuads extends VtkObject implements PropertyChangeListener {
	private vtkPoints points;
	private float size;
	private vtkPolyData data;
	private vtkDataSetMapper mapper;
	private vtkActor actor;

	public VtkQuads(List<? extends Point3d> pointsList, float width, Color4d color) {
		super(pointsList);
		this.size = size;
		this.color = color;

		points = new vtkPoints();
		for (Point3d p : pointsList)
			points.InsertNextPoint(p.x, p.y, p.z);

		vtkCellArray quads = new vtkCellArray();
		for (int i = 0; i < pointsList.size(); i = i + 4) {
			quads.InsertNextCell(5);
			for (int j = 0; j < 4; j++)
				quads.InsertCellPoint(i + j);
			quads.InsertCellPoint(i);
		}

		data = new vtkPolyData();
		data.SetPoints(points);
		data.SetLines(quads);
		mapper = new vtkDataSetMapper();
		mapper.SetInputData(data);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetLineWidth(width);
		actors.add(actor);
	}

	public void addPoint(Point3d p) {
		points.InsertNextPoint(p.x, p.y, p.z);
		points.Modified();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "AddPoint") {
			addPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "RemovePoint") {
			// TODO
		}
	}

}
