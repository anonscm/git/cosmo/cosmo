package org.ica.vtkviewer.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class VtkModel extends AbstractVtkModel {
	protected PropertyChangeSupport support;

	public VtkModel() {
		support = new PropertyChangeSupport(this);
		objects = new CopyOnWriteArrayList<VtkObject>();
	}

	public boolean add(VtkObject object) {
		boolean result = objects.add(object);
		fireAddObjectEvent(object);
		return result;
	}

	public void clear() {
		objects.clear();
	}

	public VtkObject get(int object) {
		return objects.get(object);
	}

	public int getNbOf(Class<?> cls) {
		int n = 0;
		for (VtkObject o : objects)
			if (cls == o.getClass())
				n++;
		return n;
	}

	public List<? extends VtkObject> getByType(Class<?> cls) {
		List<? extends VtkObject> result = objects.stream().filter(o -> o.getClass() == cls)
				.collect(Collectors.toList());
		return result;
	}

	public boolean isEmpty() {
		return objects.isEmpty();
	}

	public Iterator<VtkObject> iterator() {
		return objects.iterator();
	}

	public VtkObject remove(int index) {
		fireRemoveObjectEvent(objects.get(index));
		return objects.remove(index);
	}

	public boolean remove(VtkObject object) {
		fireRemoveObjectEvent(object);
		return objects.remove(object);
	}

	public int size() {
		return objects.size();
	}

	@Override
	public List<VtkObject> getObjects() {
		return objects;
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	/**
	 * Signals to VtkPanel
	 */
	public void fireNewModelEvent() {
		support.firePropertyChange("NewModel", null, null);
	}

	public void fireAddObjectEvent(VtkObject o) {
		support.firePropertyChange("AddObject", null, o);
	}

	public void fireUpdateObjectEvent(VtkObject o) {
		support.firePropertyChange("UpdateObject", null, o);
	}

	public void fireRemoveObjectEvent(VtkObject o) {
		support.firePropertyChange("RemoveObject", null, o);
	}

}
