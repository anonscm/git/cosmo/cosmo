package org.ica.vtkviewer.model;

import org.ica.cosmo.machining.ConvexHull;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkPoints;
import vtk.vtkPolyData;

public class VtkConvexHull extends VtkObject {
	private ConvexHull hull;
	private vtkActor actor;
	
	public VtkConvexHull(ConvexHull hull) {
		this(hull, new Color4d(1.0, 0.0, 0.0, 0.5));
	}

	public VtkConvexHull(ConvexHull hull, Color4d color) {
		super(hull);
		this.hull = hull;
		vtkPoints points = hull.getOutputPoints();
		vtkCellArray polys = hull.getOutputPolys();
		
		vtkPolyData polydata = new vtkPolyData();
		polydata.SetPoints(points);
		polydata.SetPolys(polys);
		vtkDataSetMapper mapper = new vtkDataSetMapper();
		mapper.SetInputData(polydata);

		// Main surface actor
		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetDiffuseColor(color.toVTK());
		actor.GetProperty().SetEdgeColor(color.toVTK());
		actor.GetProperty().SetOpacity(color.a);
		actor.GetProperty().EdgeVisibilityOn();

		actors.add(actor);
	}

	public ConvexHull getHull() {
		return hull;
	}
}
