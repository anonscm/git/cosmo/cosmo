package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkVertexGlyphFilter;

/**
 * A class to represent a set of Points.
 *
 * Intended to be used with ObservablePointsList
 * 
 * @author redonnet
 *
 */
public class VtkObservablePolyline extends VtkObject implements PropertyChangeListener {
	private vtkPoints points;
	private vtkCellArray lines;
	private float width;
	private vtkPolyData linePolyData;
	private vtkPolyData pointsPolyData;
	private vtkPolyDataMapper lineMapper;
	private vtkPolyDataMapper pointsMapper;
	private vtkActor lineActor;
	private vtkActor pointsActor;
	private boolean visiblePoints = true;
	private ObservablePoints pointsList;

	public VtkObservablePolyline(ObservablePoints pointsList, float width, Color4d color) {
		super(pointsList);
		this.pointsList = pointsList;
		this.pointsList.addPropertyChangeListener(this);
		this.width = width;
		this.color = color;

		points = new vtkPoints();

		linePolyData = new vtkPolyData();
		linePolyData.SetPoints(points);
		lines = new vtkCellArray();
		linePolyData.SetLines(lines);

		for (Point3d p : pointsList)
			appendPoint(p);

		lineMapper = new vtkPolyDataMapper();
		lineMapper.SetInputData(linePolyData);

		lineActor = new vtkActor();
		lineActor.SetMapper(lineMapper);
		lineActor.GetProperty().SetLineWidth(width);
		lineActor.GetProperty().SetColor(color.toVTK());
		actors.add(lineActor);

		pointsPolyData = new vtkPolyData();
		pointsPolyData.SetPoints(points);

		vtkVertexGlyphFilter VGF = new vtkVertexGlyphFilter();
		VGF.AddInputData(pointsPolyData);
		VGF.Update();

		pointsMapper = new vtkPolyDataMapper();
		pointsMapper.SetInputConnection(VGF.GetOutputPort());

		pointsActor = new vtkActor();
		pointsActor.SetMapper(pointsMapper);
		pointsActor.GetProperty().SetColor(color.toVTK());
		pointsActor.GetProperty().SetPointSize(this.width * 2.0f);
		actors.add(pointsActor);
	}

	public void appendPoint(Point3d p) {
		int n = pointsList.size();
		if (n == 1) {
			points.InsertPoint(0, p.x, p.y, p.z);
		} else {
			points.InsertPoint(n-1, p.x, p.y, p.z);
			lines.InsertNextCell(2);
			lines.InsertCellPoint(n - 2);
			lines.InsertCellPoint(n - 1);
		}
		points.Modified();
		lines.Modified();
	}
	
	public boolean isVisiblePoints() {
		return visiblePoints;
	}

	public void setVisiblePoints(boolean visiblePoints) {
		this.visiblePoints = visiblePoints;
		if(visiblePoints)
			pointsActor.SetVisibility(VTKBOOL_TRUE);
		if(!visiblePoints)
			pointsActor.SetVisibility(VTKBOOL_FALSE);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "AddPoint") {
			appendPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "RemovePoint") {
			// TODO
		}
	}

}
