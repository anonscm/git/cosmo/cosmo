package org.ica.vtkviewer.model;

import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkDataSetMapper;
import vtk.vtkLine;
import vtk.vtkPoints;
import vtk.vtkPolyData;

public class VtkLines extends VtkObject {
	private vtkPoints points;
	private vtkCellArray lines;
	private vtkPolyData data;
	private vtkDataSetMapper mapper;
	private vtkActor actor;

	public VtkLines(List<? extends Point3d> pts, float linewidth, Color4d color) {
		super(pts);
		points = new vtkPoints();
		lines = new vtkCellArray();
		
		for (Point3d p : pts) {
			points.InsertNextPoint(p.x, p.y, p.z);
		}
		
		vtkLine line;
		for (int i = 0; i < pts.size(); i = i + 2) {
			line = new vtkLine();
			line.GetPointIds().SetId(0, i);
			line.GetPointIds().SetId(1, i+1);
			lines.InsertNextCell(line);
		}
		
		data = new vtkPolyData();

		data.SetPoints(points);
		data.SetLines(lines);
		mapper = new vtkDataSetMapper();
		mapper.SetInputData(data);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetColor(color.toVTK());
		actor.GetProperty().SetLineWidth(linewidth);
		actor.GetProperty().SetOpacity(color.a);
		actors.add(actor);
	}
}
