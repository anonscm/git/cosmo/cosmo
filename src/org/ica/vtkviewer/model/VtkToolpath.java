package org.ica.vtkviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkActor;
import vtk.vtkCellArray;
import vtk.vtkGlyph3D;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;

/**
 * A class to display PointsCurve
 * 
 * The PointsCurve to display is observed by <b>this</b> to reflect its changes.
 * 
 * @author redonnet
 *
 */
public class VtkToolpath extends VtkObject implements PropertyChangeListener {
	private vtkPoints points;
	private vtkCellArray lines;
	private vtkPolyData track;
	private vtkPolyDataMapper mapper;
	private vtkActor actor;
	private vtkActor pointsActor = null;
	private Toolpath<? extends Point3d> toolpath;
	private long startId; // first vtkPoint Id
	private long endId; // last VtkPoint Id
	private boolean visiblePoints = false;
	private static float pointSize = 1.0f;
	
	
	public static float getPointSize() {
		return pointSize;
	}

	public static void setPointSize(float pointSize) {
		VtkToolpath.pointSize = pointSize;
	}

	private static vtkPolyData getCross() {
		vtkPoints crossPoints = new vtkPoints();
		crossPoints.InsertNextPoint(-pointSize/2.0f, 0.0, 0.0);
		crossPoints.InsertNextPoint(pointSize/2.0f, 0.0, 0.0);
		crossPoints.InsertNextPoint(0.0, -pointSize/2.0f, 0.0);
		crossPoints.InsertNextPoint(0.0, pointSize/2.0f, 0.0);
		crossPoints.InsertNextPoint(0.0, 0.0, -pointSize/2.0f);
		crossPoints.InsertNextPoint(0.0, 0.0, pointSize/2.0f);
		vtkCellArray crossLines = new vtkCellArray();
		crossLines.InsertNextCell(2);
		crossLines.InsertCellPoint(0);
		crossLines.InsertCellPoint(1);
		crossLines.InsertNextCell(2);
		crossLines.InsertCellPoint(2);
		crossLines.InsertCellPoint(3);
		crossLines.InsertNextCell(2);
		crossLines.InsertCellPoint(4);
		crossLines.InsertCellPoint(5);
		vtkPolyData cross = new vtkPolyData();
		cross.SetPoints(crossPoints);
		cross.SetLines(crossLines);

		return cross;
	}

	public VtkToolpath(Toolpath<? extends Point3d> toolpath, float width, Color4d color) {
		super(toolpath);
		this.toolpath = toolpath;
		this.toolpath.addPropertyChangeListener(this);
		points = new vtkPoints();
		points.InsertPoint(0, toolpath.get(0).x, toolpath.get(0).y, toolpath.get(0).z);
		lines = new vtkCellArray();

		Point3d point;
		for (int i = 1; i < toolpath.getNbPoints(); i++) {
			point = toolpath.get(i);
			points.InsertPoint(i, point.x, point.y, point.z);
			points.Modified();
			lines.InsertNextCell(2);
			lines.InsertCellPoint(i - 1);
			lines.InsertCellPoint(i);
			lines.Modified();
		}
		endId = toolpath.getNbPoints()-1;

		track = new vtkPolyData();
		track.SetPoints(points);
		track.SetLines(lines);

		mapper = new vtkPolyDataMapper();
		mapper.SetInputData(track);

		actor = new vtkActor();
		actor.SetMapper(mapper);
		actor.GetProperty().SetLineWidth(width);
		actor.GetProperty().SetColor(color.r, color.g, color.b);
		actors.add(actor);		
	}

	public VtkToolpath(Toolpath<? extends Point3d> toolpath) {
		this(toolpath, 2.0f, new Color4d(1.0, 0.0, 1.0));
	}

	public VtkToolpath(Toolpath<? extends Point3d> toolpath, Color4d color) {
		this(toolpath, 2.0f, color);
	}

	public VtkToolpath(Toolpath<? extends Point3d> toolpath, float width) {
		this(toolpath, width, new Color4d(1.0, 0.0, 1.0));
	}

	public void appendPoint(Point3d point) {
		long n = points.GetNumberOfPoints();
		points.InsertPoint(n, point.x, point.y, point.z);
		points.Modified();
		lines.InsertNextCell(2);
		lines.InsertCellPoint(endId);
		lines.InsertCellPoint(n);
		endId = n;
		lines.Modified();
	}

	public void prependPoint(Point3d point) {
		long n = points.GetNumberOfPoints();
		points.InsertPoint(n, point.x, point.y, point.z);
		points.Modified();
		lines.InsertNextCell(2);
		lines.InsertCellPoint(n);
		lines.InsertCellPoint(startId);
		startId = n;
		lines.Modified();
	}

	private void addPointsActor() {
		vtkPoints points = new vtkPoints();
		for (Point3d p : toolpath)
			points.InsertNextPoint(p.x, p.y, p.z);

		vtkPolyData pointsPolyData = new vtkPolyData();
		pointsPolyData.SetPoints(points);

		vtkGlyph3D Glyph3D = new vtkGlyph3D();
		Glyph3D.SetSourceData(VtkToolpath.getCross());
		Glyph3D.SetInputData(pointsPolyData);
		Glyph3D.Update();

		vtkPolyDataMapper pointsMapper = new vtkPolyDataMapper();
		pointsMapper.SetInputConnection(Glyph3D.GetOutputPort());
		pointsMapper.Update();

		this.pointsActor = new vtkActor();
		pointsActor.SetMapper(pointsMapper);
		pointsActor.GetProperty().SetColor(1.0, 1.0, 0.0);
		actors.add(pointsActor);
	}

	public boolean isVisiblePoints() {
		return visiblePoints;
	}

	public void setPointsVisibility(boolean pointsVisibility) {
		if (pointsVisibility)
			if (this.pointsActor == null)
				this.addPointsActor();
			else
				pointsActor.SetVisibility(VTKBOOL_TRUE);
		else if (this.pointsActor != null)
			pointsActor.SetVisibility(VTKBOOL_FALSE);
		this.visiblePoints = pointsVisibility;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "RenewToolpath") {
			// TODO
		}
		if (event.getPropertyName() == "AppendPoint") {
			appendPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "PrependPoint") {
			prependPoint((Point3d) event.getNewValue());
		}
		if (event.getPropertyName() == "InsertPoint") {
			// TODO
		}
		if (event.getPropertyName() == "ReplacePoint") {
			// TODO
		}
		if (event.getPropertyName() == "RemovePoint") {
			// TODO
		}
	}
}
