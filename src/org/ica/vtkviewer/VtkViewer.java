package org.ica.vtkviewer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkObject;
import org.ica.vtkviewer.model.VtkSurface;
import org.ica.vtkviewer.model.VtkText;
import org.ica.vtkviewer.model.VtkVector;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.common.BoundingBox;

import vtk.vtkActor;
import vtk.vtkNativeLibrary;
import vtk.vtkRenderWindowPanel;

public class VtkViewer implements Runnable, ActionListener, PropertyChangeListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	private static boolean multiFrame = false;
	private static Dimension defaultDimension = new Dimension(800, 600);

	private enum ViewActions {
		VIEW_XY, VIEW_XZ, VIEW_YZ, VIEW_XYZ, VIEW_RESET, Hull, Normals, Run, Stop
	}

	private VtkModel model; // empty model
	private VtkModel innerModel;
	private VtkTimerCallback callback = null;
	private BoundingBox bbox;
	private String title;
	private JFrame f;
	private Dimension dimension;
	private JLabel infoLbl;
	private JToggleButton runBtn = new JToggleButton("Run");

	// create panel at startup to make config possible even if no model is present
	private VtkPanel panel = new VtkPanel();

	public VtkViewer(VtkModel model, String title) {
		super();

		setModel(model, title);
		panel.addPropertyChangeListener(this);
	}

	public VtkViewer(VtkModel model) {
		this(model, null);
	}

	public VtkViewer() {
		this(new VtkModel(), null);
	}

	public void setModel(VtkModel model, String title) {
		this.model = model;
		this.title = title;
		this.innerModel = new VtkModel();
		init();
	}

	public void setModel(VtkModel model) {
		setModel(model, "VtkViewer");
	}

	@Override
	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				model.addPropertyChangeListener(panel);
				innerModel.addPropertyChangeListener(panel);

				f = new JFrame("Vtk Viewer" + (title == null ? "" : " : " + title));
				f.setMinimumSize(new Dimension(1080, 768));
				dimension = defaultDimension;

				f.getContentPane().setLayout(new BorderLayout());

				f.getContentPane().add(panel, BorderLayout.CENTER);

				JPanel btnPanel = getBtnPanel(f);
				f.getContentPane().add(btnPanel, BorderLayout.SOUTH);

				JPanel toolBar = getToolBar(f);
				f.getContentPane().add(toolBar, BorderLayout.EAST);

				if (multiFrame)
					f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				else
					f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				f.setSize(dimension);
				f.setVisible(true);
				f.validate();
				f.pack();

				panel.startWorking(model);
				panel.startWorking(innerModel);
				panel.setFocusable(true);
			}
		});
	}

	private JPanel getToolBar(JFrame f) {
		JPanel toolBar = new JPanel(new BorderLayout());
		toolBar.setBorder(new EmptyBorder(new Insets(5, 5, 27, 5)));

		JPanel viewPanel = new JPanel(new GridLayout(4, 1, 10, 5));
		JButton xyBtn = new JButton("XY");
		xyBtn.setActionCommand(ViewActions.VIEW_XY.name());
		JButton xzBtn = new JButton("XZ");
		xzBtn.setActionCommand(ViewActions.VIEW_XZ.name());
		JButton yzBtn = new JButton("YZ");
		yzBtn.setActionCommand(ViewActions.VIEW_YZ.name());
		JButton xyzBtn = new JButton("XYZ");
		xyzBtn.setActionCommand(ViewActions.VIEW_XYZ.name());
		JButton resetBtn = new JButton("RAZ");
		resetBtn.setActionCommand(ViewActions.VIEW_RESET.name());
		xyBtn.addActionListener(this);
		xzBtn.addActionListener(this);
		yzBtn.addActionListener(this);
		xyzBtn.addActionListener(this);
		resetBtn.addActionListener(this);

		viewPanel.add(xyBtn);
		viewPanel.add(xzBtn);
		viewPanel.add(yzBtn);
		viewPanel.add(xyzBtn);
		viewPanel.add(resetBtn);
		toolBar.add(viewPanel, BorderLayout.NORTH);

		JPanel actorsPanel = new JPanel(new FlowLayout());
		JToggleButton hullBtn = new JToggleButton("Hull");
		hullBtn.addActionListener(this);
		JToggleButton normalsBtn = new JToggleButton("Normals");
		normalsBtn.addActionListener(this);
		actorsPanel.add(hullBtn);
		actorsPanel.add(normalsBtn);
		toolBar.add(actorsPanel, BorderLayout.CENTER);

		JPanel callbackPanel = new JPanel(new GridLayout(1, 1, 10, 5));
		runBtn.addActionListener(this);
		runBtn.setEnabled(false);
		callbackPanel.add(runBtn);
		toolBar.add(callbackPanel, BorderLayout.SOUTH);

		return toolBar;
	}

	private JPanel getBtnPanel(JFrame f) {
		JPanel btnPanel = new JPanel();
		JButton exitBtn;
		infoLbl = new JLabel("Infos...");
		infoLbl.setEnabled(false);

		if (multiFrame) {
			exitBtn = new JButton("Fermer");
			exitBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					f.dispose();
				}
			});
		} else {
			exitBtn = new JButton("Quitter");
			exitBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}

		btnPanel.setLayout(new BorderLayout());
		btnPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		btnPanel.add(infoLbl, BorderLayout.WEST);
		btnPanel.add(exitBtn, BorderLayout.EAST);

		return btnPanel;
	}

	private void init() {
		resetBoundingBox();
		// Default eSize to use for empty models. May not fit all models once
		// VtkObjects added. TODO: update eSize when model is updated
		double eSize;
		if (!model.getObjects().isEmpty()) {
			for (VtkObject o : model.getObjects())
				updateBBox(o);
			eSize = bbox.getMaxEdgeLength() / 6.0; // vectors size
		} else {
			eSize = 20.0;
			bbox = new BoundingBox(new Point3d(0.0, 0.0, 0.0));
			bbox.include(new Point3d(100.0, 100.0, 100.0));
		}

		VtkVector e1 = new VtkVector(new Point3d(), new Point3d(eSize, 0.0, 0.0), new Color4d(1.0, 0.0, 0.0));
		VtkVector e2 = new VtkVector(new Point3d(), new Point3d(0.0, eSize, 0.0), new Color4d(0.0, 1.0, 0.0));
		VtkVector e3 = new VtkVector(new Point3d(), new Point3d(0.0, 0.0, eSize), new Color4d(0.0, 0.0, 1.0));
		innerModel.add(e1);
		innerModel.add(e2);
		innerModel.add(e3);
		double lSize = eSize / 3.0; // labels size
		VtkText xLabel = new VtkText(new Point3d(eSize, 0.0, 0.0), new String("X"), lSize, new Color4d(1.0, 0.0, 0.0));
		VtkText yLabel = new VtkText(new Point3d(0.0, eSize, 0.0), new String("Y"), lSize, new Color4d(0.0, 1.0, 0.0));
		VtkText zLabel = new VtkText(new Point3d(0.0, 0.0, eSize), new String("Z"), lSize, new Color4d(0.0, 0.0, 1.0));
		innerModel.add(xLabel);
		innerModel.add(yLabel);
		innerModel.add(zLabel);
		panel.resetCamera(bbox);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String name = e.getActionCommand();
		ViewActions a = ViewActions.valueOf(name);
		switch (a) {
		case VIEW_XY:
			panel.setViewXY();
			break;
		case VIEW_XZ:
			panel.setViewXZ();
			break;
		case VIEW_YZ:
			panel.setViewYZ();
			break;
		case VIEW_XYZ:
			panel.setViewXYZ();
			break;
		case VIEW_RESET:
			panel.resetCamera();
			break;
		case Hull:
			AbstractButton hullBtn = (AbstractButton) e.getSource();
			boolean hullSelected = hullBtn.getModel().isSelected();
			for (VtkObject o : model.getObjects())
				if (o instanceof VtkSurface) {
					((VtkSurface) o).setHullVisibility(hullSelected);
					model.fireUpdateObjectEvent(o);
				}
			break;
		case Normals:
			AbstractButton normalsBtn = (AbstractButton) e.getSource();
			boolean normalsSelected = normalsBtn.getModel().isSelected();
			for (VtkObject o : model.getObjects())
				if (o instanceof VtkSurface) {
					((VtkSurface) o).setVisibleNormals(normalsSelected);
					model.fireUpdateObjectEvent(o);
				}
			break;
		case Run:
			runBtn.setText("Stop");
			callback.setStatus(VtkTimerCallback.STATUS.RUN);
			break;
		case Stop:
			runBtn.setText("Run");
			callback.setStatus(VtkTimerCallback.STATUS.STOP);
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + e.getActionCommand());
		}
	}

	private void resetBoundingBox() {
		this.bbox = new BoundingBox();
	}

	private void updateBBox(VtkObject o) {
		for (vtkActor a : o.getActors())
			bbox.include(a.GetBounds());
	}

	public void repaint() {
		this.panel.resetCamera();
	}

	public void setSize(int width, int height) {
		this.dimension.setSize(width, height);
		this.repaint();
	}

	public vtkRenderWindowPanel getPanel3D() {
		return this.panel.getPanel3d();
	}

	public void setBackground(Color4d color) {
		panel.setBackground(color);
	}

	public void setBackground(Color4d colorBottom, Color4d colorTop) {
		panel.setBackground(colorBottom, colorTop);
	}

	public VtkTimerCallback getCallback() {
		return callback;
	}

	public void attachCallback(VtkTimerCallback callback) {
		this.callback = callback;
		runBtn.setEnabled(true);
	}

	public void detachCallback() {
		this.callback = null;
		runBtn.setText("Run");
		runBtn.setEnabled(false);
	}

	public static boolean isMultiFrame() {
		return multiFrame;
	}

	public static void setMultiFrame(boolean multiFrame) {
		VtkViewer.multiFrame = multiFrame;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "NewInfo") {
			Object obj = event.getNewValue();
			if (obj instanceof String) {
				String str = (String) obj;
				infoLbl.setText(str);
				if (!infoLbl.isEnabled())
					infoLbl.setEnabled(true);
			}
		}
		if (event.getPropertyName() == "ClearInfo") {
			infoLbl.setText("Infos...");
			infoLbl.setEnabled(false);
		}
	}
}
