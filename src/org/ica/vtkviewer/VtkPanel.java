package org.ica.vtkviewer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkObject;
import org.lgmt.jcam.common.BoundingBox;

import vtk.vtkActor;
import vtk.vtkCamera;
import vtk.vtkCellPicker;
import vtk.vtkFollower;
import vtk.vtkObject;
import vtk.vtkPanel;
import vtk.vtkReferenceInformation;
import vtk.vtkRenderWindowPanel;
import vtk.vtkRenderer;

/**
 * Un afficheur de modèles VTK permettant de rafraîchir automatiquement
 * l'affichage quand le modèle se met à jour.
 * 
 * <p>
 * Très largement inspiré de l'exemple fourni par Sébastien Jourdain
 * (sebastien.jourdain@kitware.com)
 *
 * @author redonnet
 */
public class VtkPanel extends JPanel implements PropertyChangeListener {
	private static final long serialVersionUID = 1L;

	private enum InteractionMode {
		NORMAL, COORDS, ROTATION_KEYS
	};

	private final vtkCellPicker cellPicker = new vtkCellPicker();
	private vtkRenderWindowPanel panel3d;
	private JCheckBox runGC;
	private JCheckBox debugMode;
	private JLabel gcStatus;
	private boolean parallelProjection = true;
	private InteractionMode mode = InteractionMode.NORMAL;
	private final CompletionService<vtkActor> exec;

	public static class AddActorRunnable implements Runnable {
		private vtkActor actorToAdd;
		private vtkRenderer renderer;
		private vtkPanel panel;
		private int resetCount = 0;
		
		void setRenderer(vtkPanel panel) {
			this.renderer = panel.GetRenderer();
			this.panel = panel;
		}

		void setActor(vtkActor a) {
			this.actorToAdd = a;
			if (a instanceof vtkFollower)
				((vtkFollower) a).SetCamera(renderer.GetActiveCamera());
		}

		public void run() {
			this.renderer.AddActor(this.actorToAdd);
			if(resetCount<7) { // resetCamera while only inner model is plotted
				this.panel.resetCamera();
				resetCount++;
			}
			this.panel.Render();
		}
	}

	public static class RemoveActorRunnable implements Runnable {
		private vtkActor actorToRemove;
		private vtkRenderer renderer;
		private vtkPanel panel;

		void setRenderer(vtkPanel panel) {
			this.renderer = panel.GetRenderer();
			this.panel = panel;
		}

		void setActor(vtkActor a) {
			this.actorToRemove = a;
		}

		public void run() {
			this.renderer.RemoveActor(this.actorToRemove);
			this.panel.Render();
		}
	}

	public class PipelineBuilder implements Callable<vtkActor> {
		private vtkActor actor;

		public PipelineBuilder(vtkActor actor) {
			this.actor = actor;
		}

		@Override
		public vtkActor call() throws Exception {
			return actor;
		}
	}

	public class CustomMouseAdapter extends MouseAdapter {
		JPanel panel;

		public CustomMouseAdapter(JPanel panel) {
			super();
			this.panel = panel;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (mode == InteractionMode.COORDS) {
				panel3d.lock();
				int pickSucceeded = cellPicker.Pick(e.getX(), panel3d.getHeight() - e.getY() - 1, 0.0,
						panel3d.GetRenderer());
				panel3d.unlock();

				if (pickSucceeded == 1) {
					double[] p = cellPicker.GetPickPosition();
					String str = String.format("(%.3f, %.3f, %.3f)\n", p[0], p[1], p[2]);
					firePropertyChange("NewInfo", null, str);
				}
			}
		}
	}

	public class CustomKeyAdapter extends KeyAdapter {
		JPanel panel;

		public CustomKeyAdapter(JPanel panel) {
			super();
			this.panel = panel;
		}

		private boolean isArrowKey(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN
					|| e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT)
				return true;
			return false;
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyLocation() == KeyEvent.KEY_LOCATION_NUMPAD) {
				if (e.getKeyChar() == '7')
					setViewXY();
				if (e.getKeyChar() == '1')
					setViewXZ();
				if (e.getKeyChar() == '3')
					setViewYZ();
				if (e.getKeyChar() == '5')
					setViewXYZ();
			}
			if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
				mode = InteractionMode.COORDS;
			}
			if (isArrowKey(e)) {
				mode = InteractionMode.ROTATION_KEYS;
				if (e.getKeyCode() == KeyEvent.VK_UP)
					elevation(-1.0);
				if (e.getKeyCode() == KeyEvent.VK_DOWN)
					elevation(1.0);
				if (e.getKeyCode() == KeyEvent.VK_LEFT)
					azimuth(1.0);
				if (e.getKeyCode() == KeyEvent.VK_RIGHT)
					azimuth(-1.0);
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
				mode = InteractionMode.NORMAL;
				firePropertyChange("ClearInfo", null, null);
			}
			if (isArrowKey(e)) {
				mode = InteractionMode.NORMAL;
			}
		}

	}

	public VtkPanel() {
		super(new BorderLayout());
		panel3d = new vtkRenderWindowPanel();
		gcStatus = new JLabel("");
		runGC = new JCheckBox("Enable GC", false);
		debugMode = new JCheckBox("Debug mode", false);
		exec = new ExecutorCompletionService<vtkActor>(
				Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));

		// Set the default background.
		// Cannot be set by this.setBackground() because swing is not running yet ?
		vtkRenderer renderer = panel3d.GetRenderer();
		renderer.GradientBackgroundOff();
		renderer.SetBackground(1.0, 1.0, 1.0);

		// Setup UI
		JPanel statusBar = new JPanel();
		statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.X_AXIS));
		statusBar.add(runGC);
		statusBar.add(debugMode);
		statusBar.add(Box.createHorizontalGlue());
		statusBar.add(gcStatus);
		add(panel3d, BorderLayout.CENTER);
		add(statusBar, BorderLayout.SOUTH);

		CustomMouseAdapter mouseAdapter = new CustomMouseAdapter(this);
		panel3d.addMouseListener(mouseAdapter);
		CustomKeyAdapter keyAdapter = new CustomKeyAdapter(this);
		panel3d.addKeyListener(keyAdapter);

		this.setupWorkers();

		// Update GC info every seconds
		new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Run GC in local thread (EDT)
				if (runGC.isSelected()) {
					vtkReferenceInformation info = vtkObject.JAVA_OBJECT_MANAGER.gc(debugMode.isSelected());
					if (debugMode.isSelected()) {
						System.out.println(info.listKeptReferenceToString());
						System.out.println(info.listRemovedReferenceToString());
					}
					gcStatus.setText(info.toString());
				} else {
					gcStatus.setText("");
				}
				panel3d.Render();
			}
		}).start();
	}

	private void setupWorkers() {
		// Add actor thread: Consume the working queue and add the actor into
		// the render inside the EDT thread
		final AddActorRunnable adderRunnable = new AddActorRunnable();
		adderRunnable.setRenderer(panel3d);
		new Thread() {
			public void run() {
				while (true) {
					try {
						adderRunnable.setActor(exec.take().get());
						SwingUtilities.invokeAndWait(adderRunnable);
						//panel3d.resetCamera();
					} catch (InterruptedException e) {
						return;
					} catch (ExecutionException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
	}

	private void sendVtkObjectToPipelineBuilder(VtkObject o) {
		for (vtkActor a : o.getActors())
			exec.submit(new PipelineBuilder(a));
	}

	public void startWorking(VtkModel m) {
		for (VtkObject o : m.getObjects())
			sendVtkObjectToPipelineBuilder(o);
	}

	public void resetCamera() {
		Runnable code = new Runnable() {
			public void run() {
				panel3d.resetCamera();
				panel3d.Render();
			}
		};
		swingRun(code);
	}

	public void resetCamera(BoundingBox bbox) {
		Runnable code = new Runnable() {
			public void run() {
				vtkRenderer renderer = panel3d.GetRenderer();
				vtkCamera camera = renderer.GetActiveCamera();

				if (parallelProjection) {
					double cx = bbox.getCenter().x;
					double cy = bbox.getCenter().y;
					double cz = bbox.getCenter().z;
					camera.SetParallelProjection(1);
					camera.SetPosition(cx, cy - 1, cz);
					camera.SetFocalPoint(cx, cy, cz);
					camera.SetViewUp(0.0, 0.0, 1.0);
					camera.SetParallelScale(bbox.getMaxEdgeLength() / 2.0);
					renderer.ResetCamera();
					renderer.ResetCameraClippingRange();
				} else {
					double vn[] = camera.GetViewPlaneNormal();
					double cx = bbox.getCenter().x;
					double cy = bbox.getCenter().y;
					double cz = bbox.getCenter().z;

					double angle = Math.toRadians(camera.GetViewAngle());
					double distance = bbox.getSphereRadius() / Math.sin(angle * 0.5);

					camera.SetFocalPoint(cx, cy, cz);
					camera.SetPosition(cx + distance * vn[0], cy + distance * vn[1], cz + distance * vn[2]);
				}
			}
		};
		swingRun(code);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName() == "NewModel") {
			System.out.println("new model");
		}
		if (evt.getPropertyName() == "AddObject") {
			VtkObject o = (VtkObject) evt.getNewValue();
			if (o.getActors().size() > 0)
				sendVtkObjectToPipelineBuilder(o);
		}
		if (evt.getPropertyName() == "UpdateObject") {
			VtkObject o = (VtkObject) evt.getNewValue();
			removeObjectActors(o);
			if (o.getActors().size() > 0)
				sendVtkObjectToPipelineBuilder(o);
		}
		if (evt.getPropertyName() == "RemoveObject") {
			VtkObject o = (VtkObject) evt.getNewValue();
			removeObjectActors(o);
		}
	}

	private void removeObjectActors(VtkObject o) {
		Runnable code = new Runnable() {
			public void run() {
				for (vtkActor a : o.getActors())
					panel3d.GetRenderer().RemoveActor(a);
				panel3d.Render();
			}
		};
		swingRun(code);
	}

	private void swingRun(Runnable code) {
		if (SwingUtilities.isEventDispatchThread()) {
			code.run();
		} else {
			SwingUtilities.invokeLater(code);
		}
	}

	/**
	 * Define a single color background.
	 * 
	 * @param color
	 */
	public void setBackground(Color4d color) {
		Runnable code = new Runnable() {
			public void run() {
				panel3d.GetRenderer().GradientBackgroundOff();
				panel3d.GetRenderer().SetBackground(color.r, color.g, color.b);
				panel3d.GetRenderer().SetBackground(color.r, color.g, color.b);
				//panel3d.repaint();
				panel3d.Render();
			}
		};
		swingRun(code);
	}

	/**
	 * Define a gradient background.
	 * 
	 * @param colorBottom
	 * @param colorTop
	 */
	public void setBackground(Color4d colorBottom, Color4d colorTop) {
		Runnable code = new Runnable() {
			public void run() {
				panel3d.GetRenderer().GradientBackgroundOn();
				panel3d.GetRenderer().SetBackground(colorBottom.r, colorBottom.g, colorBottom.b);
				panel3d.GetRenderer().SetBackground2(colorTop.r, colorTop.g, colorTop.b);
				//panel3d.repaint();
				panel3d.Render();
			}
		};
		swingRun(code);
	}

	public vtkRenderWindowPanel getPanel3d() {
		return panel3d;
	}

	public boolean isParallelProjection() {
		return parallelProjection;
	}

	public void setParallelProjection(boolean parallelProjection) {
		this.parallelProjection = parallelProjection;
	}

	public void setViewXY() {
		setViewDirection(new double[] { 0.0, 0.0, 1.0 }, new double[] { 0.0, 1.0, 0.0 });
	}

	public void setViewXZ() {
		setViewDirection(new double[] { 0.0, -1.0, 0.0 }, new double[] { 0.0, 0.0, 1.0 });
	}

	public void setViewYZ() {
		setViewDirection(new double[] { 1.0, 0.0, 0.0 }, new double[] { 0.0, 0.0, 1.0 });
	}

	public void setViewXYZ() {
		setViewDirection(new double[] { 1.0, 1.0, 1.0 }, new double[] { 0.0, 0.0, 1.0 });
	}

	public void elevation(double value) {
		Runnable code = new Runnable() {
			public void run() {
				vtkCamera camera = panel3d.GetRenderer().GetActiveCamera();
				camera.Elevation(value);
				camera.ComputeViewPlaneNormal();
				camera.OrthogonalizeViewUp();
				camera.Modified();
				panel3d.GetRenderWindow().Render();
			}
		};
		swingRun(code);
	}

	public void azimuth(double value) {
		Runnable code = new Runnable() {
			public void run() {
				vtkCamera camera = panel3d.GetRenderer().GetActiveCamera();
				camera.Azimuth(value);
				camera.ComputeViewPlaneNormal();
				camera.OrthogonalizeViewUp();
				camera.Modified();
				panel3d.GetRenderWindow().Render();
			}
		};
		swingRun(code);
	}

	public void setViewDirection(double[] pos, double[] up) {
		Runnable code = new Runnable() {
			public void run() {
				vtkCamera camera = panel3d.GetRenderer().GetActiveCamera();
				double[] fp = camera.GetFocalPoint();
				for (int i = 0; i < 3; i++)
					pos[i] = pos[i] + fp[i];
				camera.SetPosition(pos);
				camera.SetViewUp(up);
				camera.ComputeViewPlaneNormal();
				camera.OrthogonalizeViewUp();
				panel3d.GetRenderer().ResetCamera();
				panel3d.GetRenderWindow().Render();
			}
		};
		swingRun(code);
	}

}
