package org.ica.support;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.ica.cosmo.tools.FileManager;

/**
 * <pre>
 * %1$ts epoch time: 1657069724
 * %1$tc Standard date/time format: Tue Jul 05 20:59:21 EDT 2022
 * %1$tY year - 2022
 * %1$tb 3 char month - Jul 
 * %1$th 3 char month - Jul
 * %1$tm 2 digit month - 07
 * %1$td 2 digit day of month - 05
 * %1$tH: hours (0-23) 21: 
 * %1$tM: minutes: 03: 
 * %1$tS. Seconds: 55. 
 * %1$tL Milleseconds 294 
 * %2$s logger name and method
 * %3$s logger name
 * %4$-7s log level of this message - WARNING
 * </pre>
 */
public class LoggingSetup {
	private static LogManager lm = LogManager.getLogManager();

	public static Logger setupLogger(String name, Level FileHandlerLevel, Level ConsoleHandlerLevel) {
		Logger logger = Logger.getLogger(name);
		logger.setUseParentHandlers(false);
		logger.setLevel(FileHandlerLevel);

		FileHandler fh;
		try {
			fh = new FileHandler(FileManager.getResourcesPath() + "cosmo.log");
			SimpleFormatter fh_formatter = new SimpleFormatter();
			fh.setFormatter(fh_formatter);
			logger.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		ConsoleHandler ch;
		ch = new ConsoleHandler();
		ch.setLevel(ConsoleHandlerLevel);
		SimpleFormatter ch_formatter = new SimpleFormatter() {
			private static final String format = "%1$-7s: %2$s %n";

			@Override
			public synchronized String format(LogRecord lr) {
				return String.format(format, lr.getLevel().getLocalizedName(), lr.getMessage());
			}
		};
		ch.setFormatter(ch_formatter);
		logger.addHandler(ch);

		lm.addLogger(logger);

		return logger;
	}

	public static Logger setupLogger(String name) {
		return setupLogger(name, Level.INFO, Level.INFO);
	}

}
