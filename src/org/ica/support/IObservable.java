package org.ica.support;

public interface IObservable {
	public void addListener(IListener l);
	public void removeListener(IListener l);
	public EventManager getEventManager();
}
