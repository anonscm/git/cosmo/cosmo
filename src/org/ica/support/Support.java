package org.ica.support;

import static java.lang.System.out;

import org.lgmt.dgl.vecmath.Matrix2d;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Tuple3d;

public class Support {
	public static void printMatrix2d(String s, Matrix2d m) {
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f )\n", m.m00, m.m01);
		out.printf("%s = (% .3f % .3f )\n", s, m.m10, m.m11);
	}

	public static void printMatrix3d(String s, Matrix3d m) {
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f % .3f )\n", m.m00, m.m01, m.m02);
		out.printf("%s = (% .3f % .3f % .3f )\n", s, m.m10, m.m11, m.m12);
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f % .3f )\n", m.m20, m.m21, m.m22);
	}

	public static void printMatrix4d(String s, Matrix4d m) {
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f % .3f % .3f )\n", m.m00, m.m01, m.m02, m.m03);
		out.printf("%s = (% .3f % .3f % .3f % .3f )\n", s, m.m10, m.m11, m.m12, m.m13);
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f % .3f % .3f )\n", m.m20, m.m21, m.m22, m.m23);
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f % .3f % .3f % .3f )\n", m.m30, m.m31, m.m32, m.m33);
	}

	public static void printTuple3d(String s, Tuple3d t) {
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f )\n", t.x);
		out.printf("%s = (% .3f )\n", s, t.y);
		for (int i = 0; i < s.length() + 3; i++)
			out.printf(" ");
		out.printf("(% .3f )\n", t.z);
	}

	public static void hitEnter() {
		System.out.println("Press Enter key to continue...");
		try {
			System.in.read();
		} catch (Exception e) {
		}
		return;
	}

}
