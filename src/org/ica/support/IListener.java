package org.ica.support;

public interface IListener {
	public void onReceivedSignal(Signal signal);
}
