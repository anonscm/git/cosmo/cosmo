package org.ica.support;

public class Signal {
	protected Object source;
	protected String name;
	protected Object data;
	protected Object context;
	protected boolean propagation; // if true, this should be relayed when received

	public Signal(String name, Object data, Object context, boolean propagation) {
		super();
		this.name = name;
		this.data = data;
		this.context = context;
		this.propagation = propagation;
	}
	
	public Signal(String name, Object data, Object context) {
		this(name, data, context, true);
	}

	public Signal(String name) {
		this(name, null, null, false);
	}

	public Object getData() {
		return data;
	}

	public Object getContext() {
		return context;
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public boolean getPropagation() {
		return propagation;
	}
}
