package org.ica.support;

import java.util.ArrayList;
import java.util.List;

public class EventManager {
	private Object source;

	private List<IListener> listeners = new ArrayList<IListener>();

	public EventManager(Object source) {
		super();
		this.source = source;
	}

	public void addListener(IListener l) {
		this.listeners.add(l);
	}

	public void removeListener(IListener l) {
		this.listeners.remove(l);
	}

	public void sendSignal(Signal signal) {
		signal.setSource(this.source);
		for (IListener l : listeners)
			l.onReceivedSignal(signal);
	}

	public Object getSource() {
		return source;
	}

	public List<IListener> getListeners() {
		return listeners;
	}

}
