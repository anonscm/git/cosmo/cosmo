package org.ica.cosmo.tools;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.demos.SurfaceBuilder;
import org.ica.cosmo.zoning.SurfaceMap;
import org.lgmt.dgl.surfaces.Surface;

public class SurfaceMapToFile {
	public static void main(String[] args) {
		new SurfaceMapToFile();
	}

	public SurfaceMapToFile() {
		super();

		/*
		 * Lines to modify to create a new .map file
		 */
		String name = new String("choi80x80KmeansSlope4");
		Surface surface = SurfaceBuilder.getChoi();
		int tessU = 80;
		int tessV = 80;
		ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;
		METRIC metric = METRIC.SLOPE;
		int k = 3;
		/*
		 * End of lines to modify to create a new .map file
		 */

		SurfaceMap map = new SurfaceMap(surface, tessU, tessV);
		map.initNZone(k); // map must be initialized before display

		Clustering c = new Clustering(k, map, algo_type, metric, true);
		c.run();
		try {
			map = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}

		k = c.getCentroids().length;

		FileManager.getInstance().saveMap(map, name);
	}
}
