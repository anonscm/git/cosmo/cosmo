package org.ica.cosmo.tools;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkSurfaceMap;

import vtk.vtkNativeLibrary;

public class SurfaceMapViewer extends JPanel implements ActionListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	private static final long serialVersionUID = 1L;
	private final String resourcesPath = FileManager.getResourcesPath();
	private JButton openBtn;
	private JButton quitBtn;
	private JFileChooser fcDlg;

	public class SurfaceMapDisplay {
		public SurfaceMapDisplay(File file) {
			SurfaceMap map;
			try {
				map = FileManager.getInstance().loadMap(file);
				String filename = file.getName();

				if (map != null) {
					VtkModel model = new VtkModel();
					model.add(new VtkSurfaceMap(map));
					VtkViewer viewer = new VtkViewer(model, filename);
					viewer.run();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		VtkViewer.setMultiFrame(true);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		JFrame frame = new JFrame("SurfaceMapViewer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new SurfaceMapViewer());
		frame.pack();
		frame.setVisible(true);
	}

	public SurfaceMapViewer() {
		super(new BorderLayout());

		fcDlg = new JFileChooser(resourcesPath);
		fcDlg.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		Font font = new Font("Arial", Font.PLAIN, 32);
		openBtn = new JButton("Open...");
		openBtn.setFont(font);
		openBtn.addActionListener(this);
		quitBtn = new JButton("Quit!");
		quitBtn.setFont(font);
		quitBtn.addActionListener(this);

		GridLayout layout = new GridLayout(0, 1);
		layout.setVgap(10);
		layout.setHgap(10);
		JPanel buttonPanel = new JPanel(layout);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		buttonPanel.add(openBtn);
		buttonPanel.add(quitBtn);

		add(buttonPanel, BorderLayout.PAGE_START);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == openBtn) {
			int returnVal = fcDlg.showOpenDialog(SurfaceMapViewer.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fcDlg.getSelectedFile();
				new SurfaceMapDisplay(file);
			}
		}
		if (e.getSource() == quitBtn) {
			System.exit(0);
		}
	}

}
