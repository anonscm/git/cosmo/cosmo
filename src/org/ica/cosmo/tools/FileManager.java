package org.ica.cosmo.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.ica.cosmo.zoning.SurfaceMap;
import org.lgmt.jcam.toolpath.ToolpathSet;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;

public class FileManager {
	private static volatile FileManager instance = null;
	private static String resourcesPath = new String("src/org/ica/cosmo/application/ressources/");
	private final String tmpPath = System.getProperty("java.io.tmpdir");

	private FileManager() {
		super();
	}

	public final static FileManager getInstance() {
		if (FileManager.instance == null) {
			synchronized (FileManager.class) {
				if (FileManager.instance == null) {
					FileManager.instance = new FileManager();
				}
			}
		}
		return FileManager.instance;
	}

	public static String getResourcesPath() {
		return resourcesPath;
	}

	public static void setResourcesPath(String path) {
		resourcesPath = path;
	}

	private static boolean isZipped(File f) {
		int fileSignature = 0;
		try (RandomAccessFile raf = new RandomAccessFile(f, "r")) {
			fileSignature = raf.readInt();
		} catch (IOException e) {
			Logger.getLogger("cosmo").info("Cant read file"+f.getName());
		}
		
		return fileSignature == 0x504B0304 || fileSignature == 0x504B0506 || fileSignature == 0x504B0708;
	}

	public String getTmpPath() {
		return tmpPath;
	}

	/*
	 * Fine tunning of permissions model
	 */
//	public String[] getAllowedTypes() {
//		ArrayList<String> list = new ArrayList<String>();
//		list.add(new String("java.**"));
//		list.add(new String("org.lgmt.dgl.**"));
//		list.add(new String("org.lgmt.jcam.**"));
//		list.add(new String("org.ica.cosmo.**"));
//		String[] a = new String[list.size()];
//		return list.toArray(a);
//	}
//
//	
	public String[] getDeniedTypes() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(new String("java.beans.PropertyChangeSupport.**"));
		String[] a = new String[list.size()];
		return list.toArray(a);
	}

//
//	private void setupXStreamSecurity(XStream xstream) {
//		xstream.addPermission(NoTypePermission.NONE);
//		xstream.addPermission(NullPermission.NULL);
//		xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
//		xstream.allowTypeHierarchy(Collection.class);
//		xstream.allowTypesByWildcard(FileManager.getInstance().getAllowedTypes());
////		xstream.denyTypesByWildcard(FileManager.getInstance().getDeniedTypes());
//	}
	public ToolpathSet loadToolpathSet(File f) {
		ToolpathSet tpSet = null;

		return tpSet;
	}

	public void saveToolpathSet(ToolpathSet toolpathSet, String name) {
		// creating a tmp xml uncompressed file
		XStream xstream = new XStream();
		xstream.addPermission(AnyTypePermission.ANY);
		xstream.denyTypes(this.getDeniedTypes());

		File tmpFile = new File(tmpPath + "/" + name + ".xml");
		String xml = xstream.toXML(toolpathSet);
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(tmpFile));
			writer.write(xml);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// compressing the tmp file
		FileInputStream fis;
		try {
			fis = new FileInputStream(tmpFile);
			FileOutputStream fos = new FileOutputStream(resourcesPath + "/" + name + ".path");
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			ZipEntry zipEntry = new ZipEntry(new String(name + ".xml"));
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
			zipOut.close();
			fis.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		tmpFile.delete();

		return;
	}

	public SurfaceMap loadMap(File f) throws FileNotFoundException {
		SurfaceMap map = null;

		String filename = f.getName();
		String zipFile = new String(resourcesPath + filename);

		File destDir = new File(tmpPath);
		byte[] buffer = new byte[1024];
		ZipInputStream zis;
		File tmpFile = null;
		try {
			zis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry zipEntry;
			zipEntry = zis.getNextEntry();
			tmpFile = new File(destDir, zipEntry.getName());

			FileOutputStream fos = new FileOutputStream(tmpFile);
			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			zipEntry = zis.getNextEntry();
			zis.closeEntry();
			zis.close();
		} catch (FileNotFoundException e1) {
			throw e1;
		} catch (IOException e) {
			e.printStackTrace();
		}

		XStream xstream = new XStream();
//		this.setupXStreamSecurity(xstream);
		xstream.addPermission(AnyTypePermission.ANY);
		xstream.denyTypes(this.getDeniedTypes());

		try {
			FileInputStream fis = new FileInputStream(tmpFile);
			map = (SurfaceMap) xstream.fromXML(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	public void saveMap(SurfaceMap map, String name) {
		// creating a tmp xml uncompressed file
		XStream xstream = new XStream();
//		xstream.addPermission(AnyTypePermission.ANY);

		File tmpFile = new File(tmpPath + "/" + name + ".xml");
		String xml = xstream.toXML(map);
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(tmpFile));
			writer.write(xml);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// compressing the tmp file
		FileInputStream fis;
		try {
			fis = new FileInputStream(tmpFile);
			FileOutputStream fos = new FileOutputStream(resourcesPath + "/" + name + ".map");
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			ZipEntry zipEntry = new ZipEntry(new String(name + ".xml"));
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
			zipOut.close();
			fis.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		tmpFile.delete();
	}

	public void saveAnyObject(Serializable object, String name, boolean zipped) {
		XStream xstream = new XStream();

		File tmpFile;
		if (zipped)
			tmpFile = new File(tmpPath + "/" + name + ".xml");
		else
			tmpFile = new File(resourcesPath + "/" + name + ".xml");

		String xml = xstream.toXML(object);
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(tmpFile));
			writer.write(xml);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (zipped) { // compressing the tmp file
			FileInputStream fis;
			try {
				fis = new FileInputStream(tmpFile);
				FileOutputStream fos = new FileOutputStream(resourcesPath + "/" + name + ".xml");
				ZipOutputStream zipOut = new ZipOutputStream(fos);
				ZipEntry zipEntry = new ZipEntry(new String(name + ".xml"));
				zipOut.putNextEntry(zipEntry);
				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
				zipOut.close();
				fis.close();
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			tmpFile.delete();
		}
	}

	public Object loadAnyObject(String name, Class<?> cls) {
		File f = new File(resourcesPath + "/" + name + ".xml");
		Object result = null;
		File tmpFile = null;
		if(!isZipped(f))
			tmpFile = f;
		
		if (isZipped(f)) {
			String filename = f.getName();
			String zipFile = new String(resourcesPath + filename);

			File destDir = new File(tmpPath);
			byte[] buffer = new byte[1024];
			ZipInputStream zis;
			try {
				zis = new ZipInputStream(new FileInputStream(zipFile));
				ZipEntry zipEntry;
				zipEntry = zis.getNextEntry();
				tmpFile = new File(destDir, zipEntry.getName());

				FileOutputStream fos = new FileOutputStream(tmpFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				zipEntry = zis.getNextEntry();
				zis.closeEntry();
				zis.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		XStream xstream = new XStream();
		xstream.addPermission(AnyTypePermission.ANY);

		try {
			FileInputStream fis = new FileInputStream(tmpFile);

			Object object = xstream.fromXML(fis);
			result = cls.cast(object);
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
		return result;
	}
}
