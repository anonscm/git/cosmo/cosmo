package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ConnectStrategyLined_save extends ConnectStrategy {
	private static double defaultSafetyFactor = 20;
	private final EventManager emitter = new EventManager(this);

	public ConnectStrategyLined_save(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath, double overrun) {
		super(prgToolpath, overrun);
	}

	public ConnectStrategyLined_save(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath) {
		super(prgToolpath);
	}

	@Override
	protected void run() {
		ArrayList<ToolpathSequence> tpsList = new ArrayList<ToolpathSequence>();

		Iterator<Double> itK = keys.iterator();
		Double prevK = null;

		Map<Double, Toolpath<Point3d>> tpSingle = new TreeMap<Double, Toolpath<Point3d>>();
		while (itK.hasNext()) {
			Double k = itK.next();
			ArrayList<Toolpath<Point3d>> tpList = prgToolpath.get(k);
			Toolpath<Point3d> tp = tpList.get(0);
			for (int i = 1; i < tpList.size(); i++) {
				Toolpath<Point3d> tpi = tpList.get(i);
				Toolpath<Point3d> connexion = safeConnect(tp, tpi);
				connexion.append(tpi);
				tp.append(connexion);
			}
			tpSingle.put(k, tp);
//			this.emitter.emitSignal(new ToolpathSignal("AddToolpath", tp, 2));
			prevK = k;
		}
		
		itK = tpSingle.keySet().iterator();
		if(itK.hasNext()) {
			Double k = itK.next();
			Toolpath<Point3d> tp = tpSingle.get(k);
			tpList.add(tp);
		}
	}

	private Toolpath<Point3d> safeConnect(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		Point3d p0 = tp0.get(tp0.size() - 1); // point de départ
		Point3d p5 = tp1.get(0); // point d'arrivée
		double distance = p0.distance(p5);

		Point3d prevP = tp0.get(tp0.size() - 2);
		Vector3d dir0 = new Vector3d(prevP, p0);
		dir0.normalize();
		dir0.scale(distance / 4.0);

		Point3d p1 = Points.translateClone(p0, dir0);
		Point3d p2 = new Point3d(p1);
		p2.z = p2.z + distance / 5.0;

		Point3d nextP = tp1.get(1);
		Vector3d dir1 = new Vector3d(nextP, p5);
		dir1.normalize();
		dir1.scale(distance / 4.0);

		Point3d p4 = Points.translateClone(p5, dir1);
		Point3d p3 = new Point3d(p4);
		p3.z = p3.z + distance / 5.0;

		BezierCurve c = new BezierCurve(new Point3d[] { p0, p1, p2, p3, p4, p5 });

		/*
		 * Estimation of number of interpolation points needed. Based on a rough
		 * estimation of the length of the Bézier cuurve.
		 */
		double l1 = c.polygonLength();
		double l2 = p0.distance(p5);
		int tess = (int) Math.floor((l1 + l2) / 1.5);
		
		return new Toolpath<Point3d>(c, tess);
	}

	private Toolpath<Point3d> endConnect(ToolpathSequence tps, Toolpath<Point3d> tp) {
		Toolpath<Point3d> tpLast = tps.getLast();
		Point3d p0 = tpLast.get(tpLast.size() - 1); // point de départ

		if (p0.distance(tp.getLast()) < p0.distance(tp.getFirst()))
			tp.reverse();
		Point3d p5 = tp.get(0); // point d'arrivée

		double distance = p0.distance(p5) > defaultSafetyFactor ? p0.distance(p5) : defaultSafetyFactor;

		Point3d prevP = tpLast.get(tpLast.size() - 2);
		Vector3d dir0 = new Vector3d(prevP, p0);
		dir0.normalize();
		dir0.scale(distance / 4.0);

		Point3d p1 = Points.translateClone(p0, dir0);
		Point3d p2 = new Point3d(p1);
		p2.z = p2.z + distance / 5.0;

		Point3d nextP = tp.get(1);
		Vector3d dir1 = new Vector3d(nextP, p5);
		dir1.normalize();
		dir1.scale(distance / 4.0);

		Point3d p4 = Points.translateClone(p5, dir1);
		Point3d p3 = new Point3d(p4);
		p3.z = p3.z + distance / 5.0;

		BezierCurve c = new BezierCurve(new Point3d[] { p0, p1, p2, p3, p4, p5 });
//		this.emitter.emitSignal(new CurveSignal("NewCurve", c));

		/*
		 * Estimation of number of interpolation points needed. Based on a rough
		 * estimation of the length of the Bézier cuurve.
		 */
		double l1 = c.polygonLength();
		double l2 = p0.distance(p5);
		int tess = (int) Math.floor((l1 + l2) / 1.5);
		return new Toolpath<Point3d>(c, tess);

	}

	public static double getDefaultSafetyFactor() {
		return defaultSafetyFactor;
	}

	public static void setDefaultSafetyFactor(double defaultSafetyFactor) {
		ConnectStrategyLined_save.defaultSafetyFactor = defaultSafetyFactor;
	}

	public EventManager getEmitter() {
		return emitter;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

}
