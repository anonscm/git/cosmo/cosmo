package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.ica.cosmo.util.ToolpathSignal;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ConnectStrategyLined extends ConnectStrategy {
	private Map<Point3d, SurfacePoint> pairsMap;

	public ConnectStrategyLined(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath,
			Map<Point3d, SurfacePoint> pairsMap) {
		super(prgToolpath);
		this.pairsMap = pairsMap;
	}

	@Override
	protected void run() {
		Iterator<Double> itK = keys.iterator();

		// creating a single lined tp for each key
		Map<Double, Toolpath<Point3d>> tpLined = new TreeMap<Double, Toolpath<Point3d>>();
		while (itK.hasNext()) {
			Double k = itK.next();
			ArrayList<Toolpath<Point3d>> tpList = prgToolpath.get(k);
			Toolpath<Point3d> tp = tpList.get(0);
			for (int i = 1; i < tpList.size(); i++) {
				Toolpath<Point3d> tpi = tpList.get(i);
				GenericConnector gc = factory.makeInlineConnector(tp, tpi);
				tp.append(gc.getToolpath());
				tp.append(tpi);
			}
			tpLined.put(k, tp);
			this.emitter.sendSignal(new ToolpathSignal("AddPrgToolpath", tp, 2));
		}

		ToolpathSequence tps = new ToolpathSequence();
		itK = tpLined.keySet().iterator();
		Toolpath<Point3d> firstTp = tpLined.get(itK.next());

		// Trying to start from the zone border
		SurfacePoint sp0 = pairsMap.get(firstTp.getFirst());
		SurfacePoint sp1 = pairsMap.get(firstTp.getLast());
		// if last point is closest to border than first point
		if (sp1.getDistanceToLimit(sp1.getClosestLimit()) < sp0.getDistanceToLimit(sp0.getClosestLimit()))
			firstTp.reverse();

		tps.add(firstTp);
		this.emitter.sendSignal(new ToolpathSignal("AddToolpath", tps.getLast(), 2));
		while (itK.hasNext()) {
			Toolpath<Point3d> nextTp = tpLined.get(itK.next());
			this.emitter.sendSignal(new ToolpathSignal("AddToolpath", nextTp, 2));
//			GenericConnector gc = factory.makeShortestConnector(tps.getLast(), nextTp, false, true);
			GenericConnector gc = factory.makeShortestConnector(tps.getLast(), nextTp);
			if (gc == null) { // pas de connecteur valable => on arrête la séquence
				this.tpsList.add(tps);
				tps = new ToolpathSequence();
				tps.add(nextTp);
			} else {
				if (gc.getEndConn() == TERM_TYPE.END)
					nextTp.reverse();
				tps.add(gc.getToolpath());
				this.emitter.sendSignal(new ToolpathSignal("AddToolpath", gc.getToolpath(), 2));
				tps.add(nextTp);
			}
		}
		this.tpsList.add(tps);
	}

	public EventManager getEmitter() {
		return emitter;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

}
