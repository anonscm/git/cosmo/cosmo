package org.ica.cosmo.machining;

import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Quat4d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.machine.Axis;
import org.lgmt.jcam.machine.Machine;
import org.lgmt.jcam.machine.RotationAxis;
import org.lgmt.jcam.machine.TranslationAxis;
import org.lgmt.jcam.machine.Axis.AxisName;

public class MachineBuilder {

	public static Machine getDMG() {
		Machine m = new Machine("DMG");
		
		Axis xAxis = new TranslationAxis(m, new Point3d(1, 1, 1), new Quat4d(0, 0, 0, 1));
		m.addAxis(AxisName.X, xAxis);
		Axis yAxis = new TranslationAxis(xAxis, new Point3d(0, 0, 0), new Quat4d(0, 0, 0, 1));
		m.addAxis(AxisName.Y, yAxis);
		Axis zAxis = new TranslationAxis(yAxis, new Point3d(0, 0, 0), new Quat4d(0, 0, 0, 1));
		m.addAxis(AxisName.Z, zAxis);
		
		Matrix4d mtOC = new Matrix4d();
		mtOC.setTranslation(new Vector3d(0.0, 2.0, 0.0)); //FIXME
		Axis cAxis = new RotationAxis(m, mtOC);
		m.addAxis(AxisName.C, cAxis);
		
		Matrix4d mtBA = new Matrix4d();
		mtBA.setTranslation(new Vector3d(70.0, 0.0, 0.0)); //FIXME
		Axis aAxis = new RotationAxis(cAxis, mtBA);
		m.addAxis(AxisName.A, aAxis);
		
		m.setCutterAxisName(AxisName.Z);
		m.setPartAxisName(AxisName.A);

		return m;
	}

}
