package org.ica.cosmo.machining;

import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.tan;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.Math.cbrt;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;
import org.lgmt.jcam.toolpath.Toolpath;

public class TransitionG642Circle extends TransitionG642 {
	private double radius;
	private Point3d center;
	private double angle;
	
	public TransitionG642Circle(Toolpath<Point3d> tp, int posIdx) {
		super(tp, posIdx);
		this.calc();
	}

	private void calc() {
		double dt;
		double t = vTol.length();

		double l1 = u0.length();
		double l2 = u1.length();

		double l = Math.min(l1, l2);
		angle = Math.PI - Vectors3.angleV(u0, u1);
		this.radius = t * (cos(angle / 2) / (1 - cos(angle / 2)));

		double d = radius*tan(angle/2);
		
		if (d>l) {
			radius = l / (sin(angle / 2)) - vTol.length();
			dt = radius/cos(angle/2)-radius;
		} else {
			dt = t;
		}
		
		center = new Point3d(s);
		Vector3d vTol1 = new Vector3d(vTol);
		vTol1.scale(dt/t);
		center.add(vTol1);
		Vector3d vTol2 = new Vector3d(vTol);
		vTol2.scale(radius/t);
		center.add(vTol2);
	}

	public double getRadius() {
		return radius;
	}
	
	public Point3d getCenter() {
		return center;
	}
	
	public double getAngle() {
		return angle;
	}

	/**
	 * 
	 * @return vf min in transition (m/s)
	 */
	public double getMinSpeed() {
		double vMin = vf;
		double va = sqrt(mc.getAmax()*radius);
		vMin = va < vMin ? va : vMin;
		double vj = cbrt(mc.getJmax()*pow(radius,2));
		vMin = vj < vMin ? vj : vMin;
		
		return vMin;
	}

}
