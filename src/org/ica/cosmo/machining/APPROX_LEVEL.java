package org.ica.cosmo.machining;

/**
 * Définit le niveau d'approximation de la simulation d'usinage.
 * 
 * <ul>
 * <li>COARSE : approximation grossière. Uilisé par exemple pour une estimation grossière du temps d'usinage.</li>
 * <li>MEDIUM : approximation médiane.</li>
 * <li>FINE : approximation fine.</li>
 * <li>ULTRA : pas d'approximation.</li>
 * </ul>
 * 
 */
public enum APPROX_LEVEL {
	COARSE(5.0), MEDIUM(2.0), FINE(1.0), ULTRA(0.5);

	private final double valeur;

	private APPROX_LEVEL(double valeur) {
		this.valeur = valeur;
	}

	public double getValeur() {
		return this.valeur;
	}
}
