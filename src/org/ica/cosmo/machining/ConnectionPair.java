package org.ica.cosmo.machining;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.toolpath.Toolpath;

/**
	 * For moment only connection to end of existing Tps is possible
	 * 
	 * @author redonnet
	 *
	 */
	class ConnectionPair {
		/**
		 * 
		 */
		private final ZoneMilling_PPV3A zoneMilling_PPV3A;
		private ToolpathSequence tps;
		private Toolpath<Point3d> tp;
		private TERM_TYPE tpTermType;
		private Toolpath<Point3d> connection = null;
		private double distance = Double.MAX_VALUE;

		public ConnectionPair(ZoneMilling_PPV3A zoneMilling_PPV3A, ToolpathSequence tps, Toolpath<Point3d> tp) {
			this.zoneMilling_PPV3A = zoneMilling_PPV3A;
			this.tps = tps;
			this.tp = tp;
			calculate();
		}

		public ConnectionPair(ZoneMilling_PPV3A zoneMilling_PPV3A, ConnectionPair cp) {
			this.zoneMilling_PPV3A = zoneMilling_PPV3A;
			this.tps = cp.tps;
			this.tp = cp.tp;
			this.tpTermType = cp.tpTermType;
			this.distance = cp.distance;
		}

		private void calculate() {
			Point3d tpsPoint = tps.getLastPoint(); // point considéré pour tps
			Point3d tpPoint = tp.getFirst(); // point considéré pour tp
			double d = tpsPoint.distance(tpPoint);
			if (d < distance) {
				distance = d;
				tpTermType = TERM_TYPE.START;
			}
			tpPoint = tp.getLast();
			d = tpsPoint.distance(tpPoint);
			if (d < distance) {
				distance = d;
				tpTermType = TERM_TYPE.END;
			}
			if (tps.size() == 1) { // when containing only one tp, tps may be reverted
				tpsPoint = tps.getFirstPoint();
				tpPoint = tp.getFirst();
				d = tpsPoint.distance(tpPoint);
				if (d < distance) {
					distance = d;
					tps.reverse();
					tpTermType = TERM_TYPE.START;
				}
				tpPoint = tp.getLast();
				d = tpsPoint.distance(tpPoint);
				if (d < distance) {
					distance = d;
					tps.reverse();
					tpTermType = TERM_TYPE.END;
				}
			}
			if (tpTermType == TERM_TYPE.END) {
				tp.reverse();
				tpTermType = TERM_TYPE.START;
			}
		}

		private void calcConnection() {
			Toolpath<Point3d> conn;
			Point3d previousPoint = null, startPoint = null, endPoint = null, nextPoint = null;
			Toolpath<Point3d> tpsLast = tps.getLast();
			startPoint = tpsLast.getLast();
			previousPoint = tpsLast.get(tpsLast.getNbPoints() - 2);
			if (tpTermType == TERM_TYPE.START) { // conn ends at first point of the tp
				endPoint = tp.get(0);
				nextPoint = tp.get(1);
			}
			if (tpTermType == TERM_TYPE.END) { // conn ends at last point of the tp
				endPoint = tp.getLast();
				nextPoint = tp.get(tp.getNbPoints() - 2);
			}
			SurfacePoint startSP = this.zoneMilling_PPV3A.pairsMap.get(startPoint);
			SurfacePoint endSP = this.zoneMilling_PPV3A.pairsMap.get(endPoint);
//			if (isRoundedConnection(startSP, endSP)) {
			if (true) {
				Vector3d startV = new Vector3d(previousPoint, startPoint);
				Vector3d endV = new Vector3d(endPoint, nextPoint);
				conn = getRoundedConnection(startPoint, endPoint, startV, endV);
			} else {
				conn = new Toolpath<Point3d>(startPoint, endPoint);
			}

			this.connection = conn;
		}

		public Toolpath<Point3d> getConnection() {
			if (this.connection == null)
				calcConnection();
			return connection;
		}

		public boolean checkConnection() {
			// TOOD: Insert here some special checks to validate connection
//			if (distance < cutter.getRadius() * 10.0)
//				return true;
//			else
//				System.out.println("long jump");
			return true;
		}

		private boolean isRoundedConnection(SurfacePoint startP, SurfacePoint endP) {
			if (!this.zoneMilling_PPV3A.isOverrunAllowed() || this.zoneMilling_PPV3A.maxOverrun <= 0)
				return false;
			if (this.zoneMilling_PPV3A.surface.isOnBounds(startP.u, startP.v) && this.zoneMilling_PPV3A.surface.isOnBounds(endP.u, endP.v))
				return true;
			return false;
		}

		private Toolpath<Point3d> getRoundedConnection(Point3d startP, Point3d endP, Vector3d startV, Vector3d endV) {
			Point3d lastP = startP;
//			ZoneMilling_PPV3A.this.pcs.firePropertyChange("AddPoint", new Color4d(0.0, 0.0, 1.0, 1.0), startP);
			Point3d firstP = endP;
//			ZoneMilling_PPV3A.this.pcs.firePropertyChange("AddPoint", new Color4d(0.0, 0.0, 0.2, 1.0), firstP);
			double gap = startP.distance(endP);
			double eI = this.zoneMilling_PPV3A.zone.getEigenValueI();
			
			if(gap > 0.01*eI) { // points are too far from each other. clearance is required
				System.out.println("scale = "+gap+", eI = "+eI);
//				return null;
			}
			

			double scaleH = gap < this.zoneMilling_PPV3A.minOverrun ? this.zoneMilling_PPV3A.minOverrun : gap;
			double scaleV = gap > this.zoneMilling_PPV3A.maxOverrun ? this.zoneMilling_PPV3A.maxOverrun : gap;

			Point3d nextP = new Point3d(lastP);
			Vector3d lastDir = startV;
			lastDir.normalize();
			lastDir.scale(scaleH);
			nextP.add(lastDir);
			Point3d nextPup = new Point3d(nextP);
			nextPup.z = nextPup.z + scaleV;
//			ZoneMilling_PPV3A.this.pcs.firePropertyChange("AddPoint", new Color4d(0.0, 0.0, 0.8, 1.0), nextP);
			Point3d prevP = new Point3d(firstP);
			Vector3d nextDir = endV;
			nextDir.normalize();
			nextDir.scale(scaleH);
			prevP.sub(nextDir);
			Point3d prevPup = new Point3d(prevP);
			prevPup.z = prevPup.z + scaleV;
//			ZoneMilling_PPV3A.this.pcs.firePropertyChange("AddPoint", new Color4d(0.0, 0.0, 0.5, 1.0), prevP);
			BezierCurve c = new BezierCurve(new Point3d[] { lastP, nextP, nextPup, prevPup, prevP, firstP });
			int tess = 20;
			Toolpath<Point3d> tp = new Toolpath<Point3d>(c, tess);

			return tp;
		}

		@SuppressWarnings("unused")
		public ToolpathSequence getTps() {
			return tps;
		}

		public Toolpath<Point3d> getTp() {
			return tp;
		}

		@SuppressWarnings("unused")
		public TERM_TYPE getTpTermType() {
			return tpTermType;
		}

		public double getDistance() {
			return distance;
		}
	}