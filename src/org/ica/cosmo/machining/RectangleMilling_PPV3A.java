package org.ica.cosmo.machining;

import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.tan;

import java.util.ArrayList;

import org.ica.cosmo.zoning.Zone;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.shapes.Rectangle;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;
import org.lgmt.jcam.toolpath.Toolpath;

public class RectangleMilling_PPV3A extends AbstractZoneMilling {
	private Rectangle rectangle;
	private double angle;
	private double millingTime;
	private double toolpathLength;

	public RectangleMilling_PPV3A(Zone zone, double angle) {
		super(zone);
		this.rectangle = new Rectangle(zone.getMedianPoint(), zone.getEigenVectorIII(), zone.getEigenVectorI(),
				zone.getEigenValueI(), zone.getEigenValueII());
		this.angle = angle;
	}

	public RectangleMilling_PPV3A(Zone zone, Rectangle rectangle, double angle) {
		super(zone);
		this.rectangle = rectangle;
		this.angle = angle;
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

	@Override
	public void run() throws SodException {
		Vector3d n = rectangle.getNormal();

		Vector3d eI = rectangle.getOrientation();

		double w = rectangle.getLength();
		double h = rectangle.getWidth();
		double w2 = w / 2.0;
		double h2 = h / 2.0;

		// fp : proj de F dans le plan d'usinage
		// F = (cos(angle), sin(angle), 0)
		// fp = (cos(angle), sin(angle), z)
		// avec fp . n = 0 d'où
		double z = -(n.x * cos(angle) + n.y * sin(angle)) / n.z;
		Vector3d fp = new Vector3d(cos(angle), sin(angle), z);

		double alpha = abs(Util.angle0(eI, fp));
		double p = getSOD(angle, n);

		// distance entre traj dans les directions principales
		double dx, dy;
		dx = p / sin(alpha);
		dy = p / cos(alpha);

		// liste des points de la traj
		ArrayList<Point3d> pList = new ArrayList<Point3d>();
		pList.add(new Point3d(-w2, h2, 0.0));

		// liste des points de départ d'un aller-retour
		ArrayList<Point3d> spl = new ArrayList<Point3d>();
		double x = -w2;
		double y = h2 - dy / 2;
		while (y > -h2) {
			spl.add(new Point3d(x, y, 0));
			y = y - dy;
		}
		x = -w2 - (p + (h2 + y) * cos(alpha)) / sin(alpha) + dx;
		while (x < w2) {
			spl.add(new Point3d(x, -h2, 0));
			x = x + dx;
		}
		boolean retour = false;
		for (Point3d pt : spl) {
			y = pt.y;
			// x = last x, y = new y
			if (!retour && abs(x + w2) < Util.PREC12 && abs(y + h2) < Util.PREC12) { // corner
				pList.add(new Point3d(-w2, -h2, 0));
			}
			x = pt.x;
			pList.add(pt);
			if (retour) {
				retour = !retour;
				continue;
			}

			double dw = (h2 - y) / tan(alpha);
			double dh = (w2 - x) * tan(alpha);
			if (x + dw < w2) {
				// lim en y
				pList.add(new Point3d(x + dw, h2, 0));
				if (x + dw + dx < w2)
					pList.add(new Point3d(x + dw + dx, h2, 0));
				else { // corner
					pList.add(new Point3d(w2, h2, 0));
					pList.add(new Point3d(w2, h2 - (x + dw + dx - w2) * tan(alpha), 0));
				}
			} else {
				// lim en x
				pList.add(new Point3d(w2, y + dh, 0));
				if (y + dh - dy > -h2)
					pList.add(new Point3d(w2, y + dh - dy, 0));
				else
					pList.add(new Point3d(w2, -h2, 0));
			}
			retour = !retour;
		}
		if (!retour)
			pList.add(new Point3d(w2, -h2, 0));

		if (pList.size() >= 2) {
			Matrix4d m = rectangle.getTransformMatrix();
			Toolpath<Point3d> tpZero = new Toolpath<Point3d>(pList);
			Toolpath<Point3d> tp = tpZero.transformClone(m);
			Point3d p00 = new Point3d(tp.getFirst());
			p00.z = p00.z + g0Arrival;
			tp.prepend(p00);
			Point3d pNN = new Point3d(tp.getLast());
			pNN.z = pNN.z + g0Arrival;
			tp.append(pNN);
			this.toolpathSet.add(tp);
//			this.emitter.sendSignal(new ToolpathSignal("AddToolpath", tp, zone.getId()));
		} else
			return;

		/*
		 * Calculate milling time
		 */

		double v = mc.getVf(); // m/s
		// double aMax = MillingContext.getInstance().getAmax(); // m/s^2
		double jMax = mc.getJmax(); // m/s^3

		if (tan(alpha) >= h / w) {
			/*
			 * Cas 1 : traj de même longueur sur la largeur du plan
			 */
			// zone 1 (traj de même longueur)
			double l1 = h / sin(alpha);
			double delta1 = p / sin(alpha);
			double n1 = (w - h / tan(alpha)) / delta1;
			double t1 = n1 * (l1 / v + 2 * sqrt(v / jMax) + 4 * Math.cbrt(delta1 / (2 * jMax)));
			// hors zone 1
			double delta2 = p / cos(alpha);
			y = delta2 / 2.0; // on commence à une demi-largeur
			double t2 = 0.0;
			double l2 = 0.0;
			double l, t;
			while (y < h) {
				l = y / sin(alpha);
				t = l / v + 2 * sqrt(v / jMax) + 4 * Math.cbrt(delta2 / (2 * jMax));
				t2 = t2 + t;
				l2 = l2 + l + delta2;
				y = y + delta2;
			}
			millingTime = t1 + 2 * t2;
			toolpathLength = n1 * (l1 + delta1) + 2 * l2;
		} else {
			/*
			 * Cas 2 : traj de même longueur sur la longueur du plan
			 */
			// zone 1 (traj de même longueur)
			double l1 = w / cos(alpha);
			double delta1 = p / cos(alpha);
			double n1 = (h - w * tan(alpha)) / delta1;
			double t1 = n1 * (l1 / v + 2 * sqrt(v / jMax) + 4 * Math.cbrt(delta1 / (2 * jMax)));
			// hors zone 1
			double delta2 = p / sin(alpha);
			x = delta2 / 2.0; // on commence à une demi-largeur
			double t2 = 0.0;
			double l2 = 0.0;
			double l, t;
			while (x < w) {
				l = x / cos(alpha);
				t = l / v + 2 * sqrt(v / jMax) + 4 * Math.cbrt(delta2 / (2 * jMax));
				t2 = t2 + t;
				l2 = l2 + l + delta2;
				x = x + delta2;
			}
			millingTime = t1 + 2 * t2;
			toolpathLength = n1 * (l1 + delta1) + 2 * l2;
		}
	}

	private double getSOD(double theta, Vector3d n) {
		// calcul du Reff (dans le plan normal à l'avance Vf)
		double R = mc.getCutter().getRadius();
		double r = mc.getCutter().getTipRadius();

		double S = Util.angle0(n, new Vector3d(0.0, 0.0, 1.0)); // slope
		if (S < 0)
			System.err.println("Contre-dépouille");
		Vector3d vf = new Vector3d(cos(theta), sin(theta), 0.0);
		Vector3d projSlope = new Vector3d(n.x, n.y, 0.0);

		double alpha = abs(Util.angle2(vf, projSlope));

		double reff = ((R - r) * pow(cos(alpha), 2)) / (sin(S) * (1 - pow(sin(S), 2) * pow(sin(alpha), 2))) + r;

		// calcul de la distance entre traj (dans le plan normal à l'avance Vf)
		double sh = mc.getScallopHeight();
		double d = 2 * sqrt(2 * reff * sh - pow(sh, 2));

		// calcul du pas sod (dans un plan horizontal, normal à la projection de Vf dans
		// (X,Y))
		Vector3d nxvf = Vectors3.crossV(vf, n);
		Vector3d nxvfProj = new Vector3d(nxvf.x, nxvf.y, 0.0);
		double gamma = Util.angle0(nxvf, nxvfProj);

		double sod = d * cos(gamma);
		return sod;
	}

	public double getMillingTime() {
		return millingTime;
	}

	public double getToolpathLength() {
		return toolpathLength;
	}

}
