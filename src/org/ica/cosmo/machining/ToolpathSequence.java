package org.ica.cosmo.machining;

import java.util.Collections;
import java.util.LinkedList;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

//package only class
class ToolpathSequence extends LinkedList<Toolpath<Point3d>> {
	private static final long serialVersionUID = -8541757511176074675L;

	public ToolpathSequence() {
		super();
	}

	public ToolpathSequence(Toolpath<Point3d> tp) {
		super();
		this.add(tp);
	}

	public void reverse() {
		Collections.reverse(this);
		for (Toolpath<Point3d> tp : this) {
			Collections.reverse(tp);
		}
	}

	public Toolpath<Point3d> convert2Toolpath() {
		Toolpath<Point3d> tp = this.getFirst();
		
		for (int i = 1; i < this.size(); i++) {
			Toolpath<Point3d> nextTp = this.get(i);
			if(nextTp.getFirst().distance(tp.getLast())<Util.PREC12)
				nextTp.remove(0);
			tp.append(nextTp);
		}

		return tp;
	}

	public Point3d getFirstPoint() {
		return this.getFirst().getFirst();
	}

	public Point3d getLastPoint() {
		return this.getLast().getLast();
	}
}
