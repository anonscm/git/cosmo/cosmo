package org.ica.cosmo.machining;

import java.util.List;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class TpTerm {
	static int count = 0; 
	private Toolpath<? extends Point3d> tp;
	private Point3d point;
	private TpTerm peer = null;
	int id;

	public TpTerm(Toolpath<? extends Point3d> tp, TERM_TYPE start) {
		this.tp = tp;
		this.id = count;
		count++;
		if (start == TERM_TYPE.START)
			this.point = tp.getFirst();
		if (start == TERM_TYPE.END)
			this.point = tp.getLast();
	}

	public Toolpath<? extends Point3d> getTp() {
		return tp;
	}

	public Point3d getPoint() {
		return point;
	}

	public TpTerm getPeer() {
		return peer;
	}

	public double getDistance(TpTerm tpEnd) {
		return this.point.distance(tpEnd.point);
	}

	
	@Override
	public String toString() {
		return String.format("%d(%d) ", this.id, this.getPeer().id);
	}

	public void setPeer(TpTerm peer) {
		this.peer = peer;
	}
	
	public static int setPeers(List<TpTerm> termList) {
		int alone = 0;
		
		for (TpTerm baseTerm: termList) {
			if (baseTerm.peer != null)
				continue;
			for(TpTerm term: termList) {
				if (term == baseTerm)
					continue;
				if (term.tp == baseTerm.tp) {
					term.peer = baseTerm;
					baseTerm.peer = term;
					break;
				}
			}
		}

		return alone;
	}
}