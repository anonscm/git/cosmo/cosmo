package org.ica.cosmo.machining;

import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.BivarFunction;
import org.lgmt.dgl.functions.MultivarEvaluable;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.optimization.NewtonSolver;
import org.lgmt.dgl.optimization.Status;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;
import org.lgmt.jcam.part.CLSurface;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

public class ZoneMilling_IsoScallop3A extends AbstractZoneMilling {
	private enum DIR {
		FW(1.0), BW(-1.0);

		private final double value;

		private DIR(double value) {
			this.value = value;
		}
	};

	private enum SIDE {
		LEFT(1.0), RIGHT(-1.0);

		private final double value;

		private SIDE(double value) {
			this.value = value;
		}
	};

	/** The first point of the first toolpath */
	private SurfacePoint startP;

	/** The first point of the last computed toolpath */
	private SurfacePoint initP;
	/** The first vector of the last computed toolpath */
	private Vector3d initV;

	/** The current direction vector */
	private Vector3d currentV;

	/** le gestionnaire de listeners pour cette opération d'usinage */
	protected final PropertyChangeSupport support = new PropertyChangeSupport(this);

	/** liste des mailles de la zone */
	private ArrayList<UVMeshUnit> meshList = new ArrayList<UVMeshUnit>();

	/** surface centre-outil dans le repère global */
	private Surface CLSurf;

	public ZoneMilling_IsoScallop3A(Zone zone, SurfacePoint start, boolean clSurf) {
		super(zone);
		this.startP = start;

		if (clSurf) {
			this.CLSurf = this.surface;
		} else {
			this.CLSurf = new CLSurface(surface, cutter);
		}

		for (UVMeshUnit m : zone.getMeshmap().values()) {
			meshList.add(new UVMeshUnit(CLSurf, m.getIndex(), m.getUmin(), m.getUmax(), m.getVmin(), m.getVmax()));
		}
	}

	@Override
	public void run() {
		Toolpath<SurfacePoint> lastToolpath = null;
		Toolpath<SurfacePoint> nextToolpath = null;

		Toolpath<SurfacePoint> firstTraj = calc1stPath();
		if (firstTraj != null) {
//			System.out.println(firstTraj.toString());
			finalizeToolpath(firstTraj);
			lastToolpath = firstTraj;
			nextToolpath = firstTraj; // to be sure entering the while
		} else {
			System.err.println("Cannot compute first traj. Aborting...");
			return;
		}

		for (SIDE side : SIDE.values()) {
			while (nextToolpath != null) {
				double sod = calcSOD(initP, initV);
				Vector3d sideV = Vectors3.crossV(new Vector3d(0.0, 0.0, 1.0), initV);
				sideV.normalize();
				sideV.scale(side.value * sod);
				Point3d ptmp = new Point3d(initP);
				ptmp.add(sideV);
				SurfacePoint p = CLSurf.getNormalPointProjection(ptmp, initP.u, initP.v);
				if (p != null)
					nextToolpath = calcToolpath(p, lastToolpath, side);
				else
					nextToolpath = null;
				if (nextToolpath != null) {
					finalizeToolpath(nextToolpath);
					lastToolpath = nextToolpath;
				}
			}
			// preparing the side change
			lastToolpath = firstTraj;
			nextToolpath = firstTraj; // to be sure entering the while
			initP = firstTraj.get(firstTraj.getNbPoints() / 2);
			initV = new Vector3d(initP, firstTraj.get(firstTraj.getNbPoints() / 2 + 1));
		}
	}

	private Toolpath<SurfacePoint> initToolpath(SurfacePoint start, SurfacePoint end) {
		Toolpath<SurfacePoint> tp = new Toolpath<SurfacePoint>(start, end);
		support.firePropertyChange("AddPoint", null, start);
		support.firePropertyChange("AddPoint", null, end);
		support.firePropertyChange("NewToolpath", null, tp);
		for (PropertyChangeListener pcl : support.getPropertyChangeListeners()) {
			tp.addPropertyChangeListener(pcl);
		}
		return tp;
	}

	private void finalizeToolpath(Toolpath<SurfacePoint> tp) {
//		tp = smoothToolpath(tp);
//		support.firePropertyChange("NewToolpath", null, tp);
		toolpathSet.add(tp);
		initP = tp.get(tp.getNbPoints() / 2);
		initV = new Vector3d(initP, tp.get(tp.getNbPoints() / 2 + 1));
		support.firePropertyChange("AddStartPoint", null, initP);
		support.firePropertyChange("AddDirPoint", null, tp.get(tp.getNbPoints() / 2 + 1));
	}

	private Toolpath<SurfacePoint> smoothToolpath(Toolpath<SurfacePoint> tp) {
		Toolpath<SurfacePoint> newTp = null;

		for (int i = 1; i < tp.getNbPoints() - 1; i++) {
			Point3d p = new Point3d();
			SurfacePoint pinf = tp.get(i - 1);
			SurfacePoint psup = tp.get(i + 1);
			p.interpolate(pinf, psup, 0.5);
			SurfacePoint pp = CLSurf.getNormalPointProjection(p, (pinf.u + psup.u) / 2, (pinf.v + psup.v) / 2);
			if (i == 1)
				newTp = initToolpath(tp.getFirst(), pp);
			else
				newTp.append(pp);
		}
		newTp.append(tp.getLast());

		return newTp;
	}

	private Toolpath<SurfacePoint> calc1stPath() {
		Toolpath<SurfacePoint> tp = null;
		SurfacePoint nextP = null;

		DIR initDir = DIR.FW;
		nextP = this.calcNextPointSSD(initDir, startP);
		if (nextP == null) {
			initDir = DIR.BW;
			nextP = this.calcNextPointSSD(initDir, startP);
		}
		if (nextP == null) {
			System.err.println("Toolpath calculation cannot start: no point found");
			return null;
		}

		if (initDir == DIR.FW)
			tp = initToolpath(startP, nextP);
		if (initDir == DIR.BW)
			tp = initToolpath(nextP, startP);

		propagatePath(DIR.FW, tp, null);
		propagatePath(DIR.BW, tp, null);

		return tp;
	}

	private Toolpath<SurfacePoint> calcToolpath(SurfacePoint p, Toolpath<SurfacePoint> lastToolPath, SIDE side) {
		Toolpath<SurfacePoint> tp = null;
		SurfacePoint nextP = null;
		SurfacePoint p0 = p; // this toolpath starting point
		SurfacePoint lastP = new SurfacePoint(p0);

		DIR initDir = DIR.FW;
		nextP = this.calcNextPointSH(initDir, lastP, lastToolPath);
		if (nextP == null) {
			initDir = DIR.BW;
			nextP = this.calcNextPointSH(initDir, lastP, lastToolPath);
		}
		if (nextP == null) {
			System.err.println("Toolpath calculation cannot start: no point found");
			return null;
		}

		if (initDir == DIR.FW)
			tp = initToolpath(p0, nextP);
		if (initDir == DIR.BW)
			tp = initToolpath(nextP, p0);

		propagatePath(DIR.FW, tp, lastToolPath);
		propagatePath(DIR.BW, tp, lastToolPath);

		return tp;
	}

	private void propagatePath(DIR dir, Toolpath<SurfacePoint> tp, Toolpath<SurfacePoint> sidePath) {
		SurfacePoint lastP = null;
		if (dir == DIR.FW)
			lastP = new SurfacePoint(tp.getLast());
		if (dir == DIR.BW)
			lastP = new SurfacePoint(tp.getFirst());
		SurfacePoint nextP = new SurfacePoint();
		while (nextP != null) {
			nextP = calcNextPoint(dir, lastP, sidePath);
			if (nextP != null) {
				if (dir == DIR.FW)
					tp.append(new SurfacePoint(nextP));
				if (dir == DIR.BW)
					tp.prepend(new SurfacePoint(nextP));
				currentV = new Vector3d(lastP, nextP);
				lastP = nextP;
				support.firePropertyChange("AddPoint", null, nextP);
			}
		}
	}

	private SurfacePoint calcNextPoint(DIR dir, SurfacePoint lastP, Toolpath<SurfacePoint> lastToolPath) {
		SurfacePoint nextP = null;

		if (lastToolPath == null) {
			nextP = calcNextPointSSD(dir, lastP);
			return nextP;
		}

		nextP = calcNextPointSH(dir, lastP, lastToolPath);
		if (nextP == null)
			nextP = calcNextPointSSD(dir, lastP, 0.5);

		return nextP;
	}

	/**
	 * Calcule le point suivant dans la direction de plus grande pente
	 * 
	 * @param dir
	 * @param point
	 * @return
	 */
	private SurfacePoint calcNextPointSSD(DIR dir, SurfacePoint point) {
		double u = point.u;
		double v = point.v;
		double sfd = MillingContext.getInstance().getPasLongitudinal();

		Vector3d n = CLSurf.normal(u, v);
		Vector3d slopeDir = new Vector3d(n.x, n.y, 0.0); // direction de plus grande pente
		slopeDir.scale(dir.value);

		Vector3d vdir = Vectors3.crossV(n, Vectors3.crossV(n, slopeDir));
		vdir.normalize();
		vdir.scale(sfd);

		Point3d p = new Point3d(point);
		p.add(vdir);

		SurfacePoint nextP = CLSurf.getNormalPointProjection(p, u, v);

		if (nextP != null)
			if (zone.includes(nextP.u, nextP.v))
				return nextP;
		return null;
	}

	/**
	 * Calcule le point suivant dans la direction de plus grande pente
	 * 
	 * @param dir
	 * @param point
	 * @param tf    currentV influence
	 * @return
	 */
	private SurfacePoint calcNextPointSSD(DIR dir, SurfacePoint point, double tf) {
		double u = point.u;
		double v = point.v;
		double sfd = MillingContext.getInstance().getPasLongitudinal();

		Vector3d n = CLSurf.normal(u, v);
		Vector3d slopeDir = new Vector3d(n.x, n.y, 0.0); // direction de plus grande pente
		slopeDir.scale(dir.value);

		Vector3d vdir = Vectors3.crossV(n, Vectors3.crossV(n, slopeDir));
		vdir.interpolate(currentV, tf);
		vdir.normalize();
		vdir.scale(sfd);

		Point3d p = new Point3d(point);
		p.add(vdir);

		SurfacePoint nextP = CLSurf.getNormalPointProjection(p, u, v);

		if (nextP != null)
			if (zone.includes(nextP.u, nextP.v))
				return nextP;
		return null;
	}

	private SurfacePoint calcNextPointSH(DIR dir, SurfacePoint lastP, Toolpath<SurfacePoint> lastToolPath) {
		Toolpath<SurfacePoint> ltp = lastToolPath;
		int i = 0;
		double dotprod = -1.0;
		while (dotprod < 0.0 && i < ltp.getNbPoints() - 1) {
			i++;
			dotprod = Vectors3.dotV(new Vector3d(ltp.get(i - 1), ltp.get(i)), new Vector3d(lastP, ltp.get(i)));
		}

		Integer refIndex = null;
		SolverInitValues siv = null;
		if (dir == DIR.FW) {
			refIndex = i;
			if (refIndex < ltp.getNbPoints() - 1)
				siv = new SolverInitValues(dir, lastToolPath, lastP, refIndex);
		}
		if (dir == DIR.BW) {
			refIndex = i - 2;
			if (refIndex > 0)
				siv = new SolverInitValues(dir, lastToolPath, lastP, refIndex);
		}

		if (refIndex == null || siv == null) {
			System.err.println("Could not find reference point. Aborting...");
			return null;
		}

		SurfacePoint nextP = null;
		GVector var = solveIsoScallop(siv);
//		if(var == null)
//			return null;
		while (var.get(2) > 1.0) {
			if (refIndex < ltp.getNbPoints() - 2) {
				siv.setupRefs(++refIndex);
				var = solveIsoScallop(siv);
			} else
				return null;
		}
		while (var.get(2) < 0.0) {
			if (refIndex > 1) {
				siv.setupRefs(--refIndex);
				var = solveIsoScallop(siv);
			} else
				return null;
		}
		if (var != null)
			if (zone.includes(var.get(0), var.get(1)))
				nextP = new SurfacePoint(CLSurf, var.get(0), var.get(1));

		return nextP;

	}

	private class SolverInitValues {
		SurfacePoint lastP;
		/** last point on current toolpath */
		DIR dir;
		Toolpath<SurfacePoint> lastToolPath;
		private int refIndex;
		/** index of the reference point on last toolpath */
		SurfacePoint pRef;
		/** ref point on last toolpath */
		SurfacePoint pDir = null;
		/** direction point on last toolpath */
		Vector3d vRef = null;
		/** reference vector on last toolpath */
		double u0;
		/** starting point u */
		double v0;

		/** starting point v */

		public SolverInitValues(DIR dir, Toolpath<SurfacePoint> lastToolPath, SurfacePoint lastP, int iRef) {
			super();
			this.dir = dir;
			this.lastToolPath = lastToolPath;
			this.lastP = lastP;
			setupRefs(iRef);
		}

		public void setupRefs(int iRef) {
			this.refIndex = iRef;
			this.pRef = lastToolPath.get(iRef); // p(i)

			if (dir == DIR.FW) {
				// if (startIndex + 1 >= lastToolPath.getNbPoints())
				// return null;
				pDir = lastToolPath.get(iRef + 1); // p(i+1)
				vRef = new Vector3d(pRef, pDir);
			}
			if (dir == DIR.BW) {
				// if (startIndex - 1 <= 0)
				// return null;
				pDir = lastToolPath.get(iRef - 1); // p(i-1)
				vRef = new Vector3d(pDir, pRef);
			}
			u0 = lastP.u + (pDir.u - pRef.u);
			v0 = lastP.v + (pDir.v - pRef.v);
		}
	}

	/**
	 * 
	 * @param p    le point de référence sur la traj adjacente
	 * @param q    le point extrême (premier ou dernier) de la traj en cours
	 * @param vRef le vecteur de référence sur la traj adjacente
	 * @param u0   point de départ
	 * @param v0   point de départ
	 * @return un point appratenant à la surface ou null
	 */
	private GVector solveIsoScallop(SolverInitValues siv) {
		SurfacePoint p = siv.pRef;
		SurfacePoint q = siv.lastP;
		Vector3d vref = siv.vRef;

		double sfd = MillingContext.getInstance().getPasLongitudinal();
		double sod = calcSOD(p, vref);

		// Var vector : [u, v, k]
		final BivarFunction fx = CLSurf.getV3f().getFx();
		final BivarFunction fy = CLSurf.getV3f().getFy();
		final BivarFunction fz = CLSurf.getV3f().getFz();
		MultivarFunction f1 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector v) {
				return pow(fx.eval(v.get(0), v.get(1)) - q.x, 2) + pow(fy.eval(v.get(0), v.get(1)) - q.y, 2)
						+ pow(fz.eval(v.get(0), v.get(1)) - q.z, 2) - pow(sfd, 2);
			}
		});
		MultivarFunction f2 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector v) {
				return vref.x * (fx.eval(v.get(0), v.get(1)) - v.get(2) * vref.x - p.x)
						+ vref.y * (fy.eval(v.get(0), v.get(1)) - v.get(2) * vref.y - p.y)
						+ vref.z * (fz.eval(v.get(0), v.get(1)) - v.get(2) * vref.z - p.z);
			}
		});
		MultivarFunction f3 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector v) {
				return pow(fx.eval(v.get(0), v.get(1)) - v.get(2) * vref.x - p.x, 2)
						+ pow(fy.eval(v.get(0), v.get(1)) - v.get(2) * vref.y - p.y, 2)
						+ pow(fz.eval(v.get(0), v.get(1)) - v.get(2) * vref.z - p.z, 2) - pow(sod, 2);
			}
		});
		List<MultivarFunction> functions = new ArrayList<MultivarFunction>();
		functions.add(f1);
		functions.add(f2);
		functions.add(f3);
		NewtonSolver solver = new NewtonSolver(functions, 10, Util.PREC9);
//		solver.setLogging(false);
		solver.setLogLevel(Level.FINE);
		solver.init(new GVector(new double[] { siv.u0, siv.v0, 0.5 }));

		if (solver.run() == Status.TOL_REACHED && CLSurf.isDefinedFor(solver.getVar(0), solver.getVar(1))) {
			return solver.getVars();
		}
		return null;
	}

	/**
	 * Calcule le pas transversal en un point donné.
	 * 
	 * Le pas transversale est calculé en p dans la direction perpendiculaire au
	 * vecteur v et du côté défini par side.
	 * 
	 * @param p
	 * @param v
	 * @param side
	 * 
	 * @return la valeur du pas transversal
	 */
	private double calcSOD(SurfacePoint p, Vector3d vf) {
		double sod = cutter.getRadius(); // le pas transversal à calculer, initialisé au max
		double reff; // le rayon effectif
		double d; // la distance entre les centreReff

		double gamma;
		reff = calcReffAt(p, vf);
		// dans un premier temps, on néglige la courbure de la surface
		// pour calculer la distance entre les deux centres Reff
		d = 2 * sqrt(2 * reff * sh - sh * sh);
		double u = p.u;
		double v = p.v;
		Vector3d n = CLSurf.normal(u, v);
		gamma = atan(sqrt(n.x * n.x + n.y * n.y) / n.z);

		// atan impaire => gamma peut être > 0 ou < 0, ce n'est pas grave puisqu'on
		// n'utilise que le cosinus
		sod = d * cos(gamma);

		return sod;
	}

	private double calcReffAt(SurfacePoint p, Vector3d vf) {
		double u = p.u;
		double v = p.v;

		Vector3d n = CLSurf.normal(u, v);

		// Pente maxi
		double S = abs(Util.angle0(n, new Vector3d(0.0, 0.0, 1.0)));

		double R = cutter.getRadius();
		double r = cutter.getTipRadius();

		// Direction de plus grande pente
		Vector2d maxSlopeDirection = new Vector2d(n.x, n.y);
		if (n.y < 0) // TODO: check me!
			maxSlopeDirection.scale(-1.0);

		// Direction d'usinage
		Vector2d vfDirection = new Vector2d(vf.x, vf.y); // TODO: check my sign!

		// alpha est l'angle entre la direction de plus grande pente et la
		// direction d'usinage.
		double alpha = maxSlopeDirection.angle(vfDirection);

		double reff = ((R - r) * Math.pow(Math.cos(alpha), 2))
				/ (Math.sin(S) * (1 - Math.pow(Math.sin(S), 2) * Math.pow(Math.sin(alpha), 2))) + r;

		return reff;
	}
}
