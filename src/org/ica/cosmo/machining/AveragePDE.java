package org.ica.cosmo.machining;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;

/**
 * Un évaluateur de la direction préférentielle d'usinage basé sur l'ensemble
 * des mailles de la zone.
 * 
 * <p>
 * Calcule la direction préférentielle d'usinage de la zone en faisant la
 * moyenne des directions de plus grande pente des mailles de la zone.
 * </p>
 * 
 * 
 * @author redonnet
 */
public class AveragePDE implements PrefDirEvaluator {
	private Zone zone;

	public AveragePDE(Zone zone) {
		this.zone = zone;
	}

	@Override
	public double eval() {
		int n = 0;
		double sum = 0.0;
		for (UVMeshUnit m : zone.getMeshmap().values()) {
			sum = sum + m.getOptimalDir();
			n++;
		}
		return sum / (double) n;
	}

	@Override
	public double eval(Double initialGuess) {
		return eval();
	}
}
