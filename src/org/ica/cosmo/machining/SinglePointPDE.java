package org.ica.cosmo.machining;

import org.ica.cosmo.clustering.DataPoint;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;

/**
 * Un évaluateur de la direction préférentielle d'usinage basé sur un point en
 * particulier.
 * 
 * <p>
 * Définit la direction de plus grande pente d'un point en particulier comme la
 * direction préférentielle d'usinage de la zone. On choisira par exemple un
 * point central.
 * </p>
 * 
 * 
 * @author redonnet
 */
public class SinglePointPDE implements PrefDirEvaluator {
	private UVMeshUnit m;

	public SinglePointPDE(UVMeshUnit m) {
		this.m = m;
	}

	/**
	 * On suppose que u et v sont les deux premières valeurs du DataPoint p
	 */
	public SinglePointPDE(Zone zone, DataPoint p) {
		this.m = zone.getSurfaceMap().getMeshUnit(p.getValue(0), p.getValue(1));
	}

	@Override
	public double eval() {
		return m.getOptimalDir();
	}

	@Override
	public double eval(Double initialGuess) {
		return eval();
	}
}
