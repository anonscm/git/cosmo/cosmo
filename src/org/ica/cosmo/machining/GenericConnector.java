package org.ica.cosmo.machining;

import org.ica.cosmo.machining.ConnectorFactory.LOCDATA;
import org.ica.cosmo.machining.ConnectorFactory.MODE;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

public class GenericConnector implements Comparable<GenericConnector> {
	protected static int defaultTessellation = 10;
	protected INTERPOLATION_TYPE iType;
	protected TERM_TYPE startConn = TERM_TYPE.END; // By default connector start is the end of first toolpath
	protected TERM_TYPE endConn = TERM_TYPE.START; // By default connector end is the start of second toolpath
	protected MODE connectionMode = MODE.APPEND; // By default this connector is set for appending toolpaths
	protected LOCDATA locData;
	protected BezierCurve curve;
	protected int tessellation = defaultTessellation;
	protected Toolpath<Point3d> toolpath;
	protected double length;

	protected GenericConnector() {
	}
	
	protected GenericConnector(BezierCurve curve, INTERPOLATION_TYPE iType, int tessellation) {
		this.curve = curve;
		this.iType = iType;
		this.toolpath = new Toolpath<Point3d>(curve, tessellation);
		this.length = toolpath.length();
	}

	protected GenericConnector(BezierCurve curve, INTERPOLATION_TYPE iType) {
		this(curve, iType, defaultTessellation);
	}

	protected GenericConnector(BezierCurve curve, int tessellation) {
		this(curve, INTERPOLATION_TYPE.G1, tessellation);
	}

	protected GenericConnector(BezierCurve curve) {
		this(curve, INTERPOLATION_TYPE.G1, defaultTessellation);
	}

	public static int getDefaultTessellation() {
		return defaultTessellation;
	}

	public static void setDefaultTessellation(int defaultTessellation) {
		GenericConnector.defaultTessellation = defaultTessellation;
	}

	public INTERPOLATION_TYPE getiType() {
		return iType;
	}

	public void setiType(INTERPOLATION_TYPE iType) {
		this.iType = iType;
	}

	public Point3d getStartP() {
		return curve.getControlPoint(0);
	}

	public Point3d getEndP() {
		return curve.getControlPoint(curve.getNpu()-1);
	}

	public TERM_TYPE getStartConn() {
		return startConn;
	}

	public void setStartConn(TERM_TYPE startConn) {
		this.startConn = startConn;
	}

	public TERM_TYPE getEndConn() {
		return endConn;
	}

	public void setEndConn(TERM_TYPE endConn) {
		this.endConn = endConn;
	}

	public MODE getConnectionMode() {
		return connectionMode;
	}

	public void setConnectionMode(MODE connectionMode) {
		this.connectionMode = connectionMode;
	}

	public LOCDATA getLocData() {
		return locData;
	}

	public void setLocData(LOCDATA locData) {
		this.locData = locData;
	}

	public BezierCurve getCurve() {
		return curve;
	}

	public void setCurve(BezierCurve curve) {
		this.curve = curve;
	}

	public int getTessellation() {
		return tessellation;
	}

	public void setTessellation(int tessellation) {
		this.tessellation = tessellation;
	}

	public Toolpath<Point3d> getToolpath() {
		return toolpath;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public int compareTo(GenericConnector o) {
		if (this.length < o.length)
			return -1;
		if (this.length > o.length)
			return 1;
		return 0;
	}

}
