package org.ica.cosmo.machining;

import org.ica.cosmo.machining.ConnectorFactory.MODE;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;

public class EndConnector extends GenericConnector{
	public enum LOCDATA { // used to localize the connector in relation to tps
		PREV, FIRST, LAST, NEXT
	};
	
	protected TERM_TYPE startConn = TERM_TYPE.END; // By default connector start is the end of first toolpath
	protected TERM_TYPE endConn = TERM_TYPE.START; // By default connector end is the start of second toolpath
	protected MODE connectionMode = MODE.APPEND; // By default this connector is set for appending toolpaths
	
	protected LOCDATA locData;

	public EndConnector() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EndConnector(BezierCurve curve, int tessellation) {
		super(curve, tessellation);
		// TODO Auto-generated constructor stub
	}

	public EndConnector(BezierCurve curve, INTERPOLATION_TYPE iType, int tessellation) {
		super(curve, iType, tessellation);
		// TODO Auto-generated constructor stub
	}

	public EndConnector(BezierCurve curve, INTERPOLATION_TYPE iType) {
		super(curve, iType);
		// TODO Auto-generated constructor stub
	}

	public EndConnector(BezierCurve curve) {
		super(curve);
		// TODO Auto-generated constructor stub
	}

	public TERM_TYPE getStartConn() {
		return startConn;
	}

	public void setStartConn(TERM_TYPE startConn) {
		this.startConn = startConn;
	}

	public TERM_TYPE getEndConn() {
		return endConn;
	}

	public void setEndConn(TERM_TYPE endConn) {
		this.endConn = endConn;
	}

	public MODE getConnectionMode() {
		return connectionMode;
	}

	public void setConnectionMode(MODE connectionMode) {
		this.connectionMode = connectionMode;
	}



}
