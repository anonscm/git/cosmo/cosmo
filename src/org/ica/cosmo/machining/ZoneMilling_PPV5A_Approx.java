package org.ica.cosmo.machining;

import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.cbrt;
import static java.lang.Math.cos;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.curves.SurfaceIsoCurve;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.functions.UnivarFunction;
import org.lgmt.dgl.optimization.BrentOptimizer;
import org.lgmt.dgl.optimization.Newton1x1Solver;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;
import org.lgmt.jcam.part.CLSurface;
import org.lgmt.jcam.strategy.MillingContext;

/**
 * Approximation des trajectoires outil sur une zone.
 * 
 * <p>
 * Chaque trajectoire d'outil est approximée par l'intersection entre son plan
 * d'usinage et toutes les isoparamétriques en u et en v définies sur la zone.
 * Cette approximation est suffisante pour calculer la longueur de la
 * trajectoire et le temps de parcours de l'outil sur cette trajectoire.
 * 
 * <p>
 * Tous les calculs sont faits dans un repère de calcul pour lequel le plan
 * d'usinage est définie par x = cste. Les matrices {@link #mGX mGX} et
 * {@link #mXG mXG} permettent respectivement de passer du repère global (G) au
 * repère de calcul (X) et inversement.
 * 
 * @author redonnet
 *
 */
public class ZoneMilling_PPV5A_Approx extends AbstractZoneMilling {
	private APPROX_LEVEL approxLevel;

	/** liste des mailles de la zone */
	private ArrayList<UVMeshUnit> meshList;

	/** surface dans le repère de calcul */
	private Surface surfX;

	/** surface centre-outil dans le repère global */
	private Surface CLSurf;

	/** surface centre-outil dans le repère de calcul */
	private Surface CLSurfX;

	/** matrice de passage du repère global au repère de calcul */
	private Matrix4d mGX;

	/** matrice de passage du repère de calcul au repère global */
	private Matrix4d mXG;

	/**
	 * approximation de la surface centre-outil de la zone dans le repère de calcul
	 */
	private ApproxZone zoneApprox = null;

	/**
	 * approximation des trajectoires d'usinage recherchée dans le repère de calcul
	 */
	private LinkedList<ArrayList<SurfacePointsCurve>> toolpathApproxX;

	/** approximation des trajectoires d'usinage recherchée dans le repère global */
	private LinkedList<ArrayList<SurfacePointsCurve>> toolpathApproxG;

	/** les points extrêmes de la zone */
	private SurfacePoint pXmin, pXmax, pYmin, pYmax, pZmin, pZmax;

	/** les limites paramétriques de la zone */
//	private double umin, umax, vmin, vmax;

	/** la marge dans la diretion transverse utilisée en début d'usinage */
	private double margeMin; // la marge au début de l'usinage

	/** la marge dans la diretion transverse utilisée en fin d'usinage */
	private double margeMax; // la marge à la fin de l'usinage

	/** angle de la diretion d'usinage */
	private double vfAngle;

	/**
	 * Constructeur.
	 * 
	 * @param zone
	 * @param vfAngle
	 * @param clSurf  true if <i>zone</i> belongs to CL surface false if <i>zone</i>
	 *                belongs to the original surface
	 */
	public ZoneMilling_PPV5A_Approx(Zone zone, double vfAngle, boolean clSurf, APPROX_LEVEL accuracy) {
		super(zone);
		this.vfAngle = vfAngle;
		this.approxLevel = accuracy;
		meshList = new ArrayList<UVMeshUnit>();
		toolpathApproxX = new LinkedList<ArrayList<SurfacePointsCurve>>();
		toolpathApproxG = new LinkedList<ArrayList<SurfacePointsCurve>>();

		mGX = new Matrix4d();
		mGX.setIdentity();
		// Usinage à x=cste => vf // Y
		// vfangle = (X,vf) => rotation = Pi/2 - vfangle
		mGX.setRotation(new AxisAngle4d(0.0, 0.0, 1.0, Math.PI / 2.0 - vfAngle));
		mXG = new Matrix4d();
		mXG.invert(mGX);

		this.surfX = surface.transformClone(mGX);
		if (clSurf) {
			this.CLSurf = this.surface;
			this.CLSurfX = this.surfX;
		} else {
			this.CLSurf = new CLSurface(surface, cutter);
			this.CLSurfX = new CLSurface(surfX, cutter);
		}

		/*
		 * On (re)construit la liste des mailles de la zone à partir de la surface
		 * transformée
		 */
		for (UVMeshUnit m : zone.getMeshmap().values()) {
			meshList.add(new UVMeshUnit(CLSurfX, m.getIndex(), m.getUmin(), m.getUmax(), m.getVmin(), m.getVmax()));
		}

		/*
		 * On calcule xmin et xmax pour la zone (pour l'usinage et l'approximation en
		 * isoparamétriques), ymin et ymax (pour l'approximation en isoparamétriques) et
		 * zmin et zmax (pour la pénalité)
		 */
		pXmin = meshList.get(0).getCenter();
		pXmax = meshList.get(0).getCenter();
		pYmin = meshList.get(0).getCenter();
		pYmax = meshList.get(0).getCenter();
		pZmin = meshList.get(0).getCenter();
		pZmax = meshList.get(0).getCenter();
//		umin = meshList.get(0).getUmin();
//		umax = meshList.get(0).getUmax();
//		vmin = meshList.get(0).getVmin();
//		vmax = meshList.get(0).getVmax();
		for (UVMeshUnit m : meshList) {
			for (SurfacePoint p : m.getCorners()) {
				if (p.x < pXmin.x)
					pXmin = p;
				if (p.x > pXmax.x)
					pXmax = p;
				if (p.y < pYmin.y)
					pYmin = p;
				if (p.y > pYmax.y)
					pYmax = p;
				if (p.z < pZmin.z)
					pZmin = p;
				if (p.z > pZmax.z)
					pZmax = p;
//				if (p.u < umin)
//					umin = p.u;
//				if (p.u > umax)
//					umax = p.u;
//				if (p.v < vmin)
//					vmin = p.v;
//				if (p.v > vmax)
//					vmax = p.v;
			}
		}
		/*
		 * On définit les marges
		 */
		this.margeMin = calcMarge(pXmin);
		this.margeMax = calcMarge(pXmax);

		/*
		 * On définit une approximation de la surface par un réseau d'isoparamétriques
		 */
		this.zoneApprox = new ApproxZone(CLSurfX, zone);
	}

	public ZoneMilling_PPV5A_Approx(Zone zone, double vfAngle, boolean clSurf) {
		this(zone, vfAngle, clSurf, APPROX_LEVEL.COARSE);
	}

	/**
	 * Lance la simulation d'usinage
	 */
	@Override
	public void run() throws SodException {
		double sod = margeMin;

		double xPlan = pXmin.x + sod;
		while (xPlan <= pXmax.x - margeMax) {

			boolean newCurve = calcPlane(xPlan);

			if (newCurve) {
				SurfacePointsCurve spcX = toolpathApproxX.getLast().get(0);
				sod = calcSOD(spcX);
			}
			xPlan = xPlan + sod;
			System.out.println("sod = "+sod);
			if (sod < 1e-1) // 0.1*r
				throw new SodException();
		}

		if (toolpathApproxX.size() > 0)
			postproc();
	}

	private void postproc() {
		ArrayList<SurfacePointsCurve> connections = new ArrayList<SurfacePointsCurve>();

		// à ce moment il y a toujours un seul tableau de courbes par plan
		SurfacePointsCurve spc = toolpathApproxX.get(0).get(0);
		SurfacePoint end = spc.get(spc.getNbPoints() - 1);

		// on inverse l'ordre des courbes paires
		for (int i = 1; i < toolpathApproxX.size(); i++) {
			spc = toolpathApproxX.get(i).get(0);
			if ((i % 2) != 0) {
				spc.reverse();
			}
			ArrayList<SurfacePoint> rList = new ArrayList<SurfacePoint>();
			SurfacePoint start = spc.get(0);
			rList.add(start);
			rList.add(end);
			SurfacePointsCurve rCurve = new SurfacePointsCurve(rList);
			connections.add(rCurve);
			end = spc.get(spc.getNbPoints() - 1);
		}

		// Looking for concavities
		ListIterator<ArrayList<SurfacePointsCurve>> xPlanIter = toolpathApproxX.listIterator();
		while (xPlanIter.hasNext()) {
			ArrayList<SurfacePointsCurve> spcList = xPlanIter.next();
			ListIterator<SurfacePointsCurve> nCurveIter = spcList.listIterator();
			while (nCurveIter.hasNext()) {
				spc = nCurveIter.next();
				ListIterator<SurfacePoint> ptIter = spc.listIterator();
				SurfacePoint p0 = ptIter.next();
				while (ptIter.hasNext()) {
					SurfacePoint p1 = ptIter.next();
					if (p0.distance(p1) > cutter.getRadius() / 2.0) { // TODO: Meilleure condition pour détecter les
																		// concavités
						Vector3d vproj = Vectors3.crossV(new Vector3d(p0, p1), new Vector3d(1.0, 0.0, 0.0));
						Point3d pmid = new Point3d();
						pmid.interpolate(p0, p1, 0.5);
						try {
							SurfacePoint pProj = surfX.getPointProjection(pmid, 0.5 * (p0.u + p1.u),
									0.5 * (p0.v + p1.v), vproj);
							if (zone.includes(pProj.u, pProj.v)) {
								// si le point projeté est dans la zone, on l'ajoute à la courbe
								ptIter.previous();
								ptIter.add(pProj);
								ptIter.previous();
							} else {
								// si le point projeté n'est pas dans la zone, on découpe la courbe en 2
								ptIter.previous();
								nCurveIter.previous();
								nCurveIter.remove();
								int firstIndex = 0;
								int lastIndex = ptIter.previousIndex();
								if (lastIndex > firstIndex) {// avoid sole point curves
									SurfacePointsCurve firstBranch = new SurfacePointsCurve(spc, firstIndex, lastIndex);
									nCurveIter.add(firstBranch);
								}

								firstIndex = ptIter.nextIndex();
								lastIndex = spc.getNbPoints() - 1;
								if (lastIndex > firstIndex) {// avoid sole point curves
									SurfacePointsCurve lastBranch = new SurfacePointsCurve(spc, firstIndex, lastIndex);
									nCurveIter.add(lastBranch);
									nCurveIter.previous();
								}
								break;
							}
						} catch (Exception e) {
//							e.printStackTrace();
							// le point projeté n'est pas dans la zone, on découpe la courbe en 2
							ptIter.previous();
							nCurveIter.previous();
							nCurveIter.remove();
							int firstIndex = 0;
							int lastIndex = ptIter.previousIndex();
							if (lastIndex > firstIndex) {// avoid sole point curves
								SurfacePointsCurve firstBranch = new SurfacePointsCurve(spc, firstIndex, lastIndex);
								nCurveIter.add(firstBranch);
							}
							firstIndex = ptIter.nextIndex();
							lastIndex = spc.getNbPoints() - 1;
							if (lastIndex > firstIndex) {// avoid sole point curves
								SurfacePointsCurve lastBranch = new SurfacePointsCurve(spc, firstIndex, lastIndex);
								nCurveIter.add(lastBranch);
								nCurveIter.previous();
							}
							break;
						}
					}
					p0 = p1;
				}
			}
		}

		if (!connections.isEmpty())
			toolpathApproxX.add(connections);

		for (

		SurfacePointsCurve c : connections) {
//			this.emitter.sendSignal(new PointsCurveSignal("newCurve", c));
		}
	}

	/**
	 * Calcule le pas transversal pour une trajectoire outil. la direction d'usinage
	 * est selon y (plans a x=cste)
	 * 
	 * @param traj
	 * @return la valeur du pas transversal
	 */
	private double calcSOD(SurfacePointsCurve traj) {
		double sod = 2*(cutter.getRadius()-cutter.getTipRadius()); // le pas transversal à calculer
		double reff; // le rayon effectif
		double d; // la distance entre les centreReff

		double gamma;
		double sod_test;
		for (SurfacePoint p : traj.getPoints()) {
			reff = calcReffAt(p);
			// dans un premier temps, on néglige la courbure de la surface
			// pour calculer la distance entre les deux centres Reff
//			d = 2 * sqrt(2 * reff * sh - sh * sh);
			double u = p.u;
			double v = p.v;
//			double u = Math.min(Math.max(p.u,1e-2),1-1e-2);
//			double v = Math.min(Math.max(p.v,1e-2),1-1e-2);
			Vector3d n_old = CLSurfX.normal(u, v);
			Vector3d n = new Vector3d();
			Matrix4d T = new Matrix4d();
			Matrix4d R1 = new Matrix4d(); // first rotation to bring normal vector to vertical
			Matrix4d R2 = new Matrix4d(); // second rotation to create slope along principal direction
			T.setIdentity();
			R1.setIdentity();
			R2.setIdentity();
			T.setTranslation(new Vector3d(-p.x,-p.y,-p.z));
			Vector3d ax1 = new Vector3d();
			ax1 = Vectors3.crossV(n_old, new Vector3d(0.0,0.0,1.0));
			double ang1 = abs(Util.angle0(n_old, new Vector3d(0.0, 0.0, 1.0)));
			R1.setRotation(new AxisAngle4d(ax1, ang1));
			Vector3d ax2 = new Vector3d(1.0,0.0,0.0);
			double ang2 =5.0*Math.PI/180;
			R2.setRotation(new AxisAngle4d(ax2, ang2));
			Matrix4d Rot = new Matrix4d();
			Rot.mul(R2, R1);
			Matrix4d Tf = new Matrix4d();
			Tf.mul(Rot, T);
			Surface rotSurf = CLSurfX.transformClone(Tf);
			Point3d pt = rotSurf.eval(p.u, p.v);
			pt.add(new Vector3d(0.0,1e-3,0.0));
			SurfacePoint p1 = rotSurf.getNormalPointProjection(pt, u, v);
			if (p1 == null) {
				pt = rotSurf.eval(p.u, p.v);
				pt.add(new Vector3d(0.0,-1e-3,0.0));
				p1 = rotSurf.getNormalPointProjection(pt, u, v);
			}
			double u1 = p1.u;
			double v1 = p1.v;
			double angle = Math.atan2(v1-v, u1-u);
			double rho = 1.0/rotSurf.evalNormalCurvature(u, v, angle);
			d = sqrt((4*reff*reff + 4*rho*reff - 2*sh*rho - sh*sh)*(2*rho+sh)*sh)/(rho+sh);
//			Rot.transform(n_old, n);
			n = rotSurf.normal(u, v);
//			gamma = atan(sqrt(n.x * n.x + n.y * n.y) / n.z);

			// atan impaire => gamma peut être > 0 ou < 0, ce n'est pas grave puisqu'on
			// n'utilise que le cosinus
//			sod_test = d * cos(gamma);
			sod_test = d * 1.0 / sqrt(1 + Math.pow(n.x, 2) / Math.pow(n.z, 2));
			sod = sod_test < sod ? sod_test : sod;
//			System.out.println("rho = "+rho);
//			System.out.println("sod_test = "+sod_test);
		}
		return sod;
	}

	private double calcReffAt(SurfacePoint p) {
		
		double u = p.u;
		double v = p.v;
		Vector3d n_old = surfX.normal(u, v);
		Vector3d n = new Vector3d();
		
		Matrix4d R1 = new Matrix4d(); // first rotation to bring normal vector to vertical
		Matrix4d R2 = new Matrix4d(); // second rotation to create slope along principal direction
		R1.setIdentity();
		R2.setIdentity();
		Vector3d ax1 = new Vector3d();
		ax1 = Vectors3.crossV(n_old, new Vector3d(0.0,0.0,1.0));
		double ang1 = abs(Util.angle0(n_old, new Vector3d(0.0, 0.0, 1.0)));
		R1.setRotation(new AxisAngle4d(ax1, ang1));
		Vector3d ax2 = new Vector3d(1.0,0.0,0.0);
		double ang2 = 5.0*Math.PI/180;
		R2.setRotation(new AxisAngle4d(ax2, ang2));
		Matrix4d Rot = new Matrix4d();
		Rot.mul(R2, R1);
		Rot.transform(n_old, n);

		// Pente maxi
		double S = abs(Util.angle0(n, new Vector3d(0.0, 0.0, 1.0)));

		double R = cutter.getRadius();
		double r = cutter.getTipRadius();

		// direction de plus grande pente dans le plan XY (ramené sur [0,Pi] car étant
		// donné le changement de repère, la direction d'usinage est toujours selon Y

		Vector2d maxSlopeDirection = new Vector2d(n.x, n.y);
		if (n.y < 0)
			maxSlopeDirection.scale(-1.0);

		// alpha est l'angle entre la direction de plus grande pente et la
		// direction d'usinage.
		// Étant donné le changement de repère, la direction d'usinage est
		// toujours (0.0, 1.0)
		double alpha = maxSlopeDirection.angle(new Vector2d(0.0, 1.0));
		double reff = ((R - r) * Math.pow(Math.cos(alpha), 2))
				/ (Math.sin(S) * (1 - Math.pow(Math.sin(S), 2) * Math.pow(Math.sin(alpha), 2))) + r;
//		System.out.println("reff = "+reff);

		return reff;
	}

	/**
	 * Calcul de la trajectoire sur un plan x=cste (plan d'usinage)
	 * 
	 * @param xPlan
	 * @return true si ne trajectoire (deux points au moins) a pu être calculée,
	 *         false dans le cas contraire
	 */
	private boolean calcPlane(double xPlan) {
		ArrayList<SurfacePoint> pList = new ArrayList<SurfacePoint>();

		for (ArrayList<SurfacePointsCurve> list : zoneApprox.approxIsoU.values()) {
			for (SurfacePointsCurve pc : list) {
				pList.addAll(calcAllIsoInt(pc, xPlan, Surface.Param.U));
			}
		}

		for (ArrayList<SurfacePointsCurve> list : zoneApprox.approxIsoV.values()) {
			for (SurfacePointsCurve pc : list) {
				pList.addAll(calcAllIsoInt(pc, xPlan, Surface.Param.V));
			}
		}

		// Si on a au moins 2 points d'intersection, on crée une nouvelle courbe
		// d'approximation
		if (pList.size() > 1) {
			Collections.sort(pList, new SurfacePointComparator());
			SurfacePointsCurve trajX = new SurfacePointsCurve(pList);
			ArrayList<SurfacePointsCurve> spcList = new ArrayList<SurfacePointsCurve>();
			spcList.add(trajX);
			toolpathApproxX.add(spcList);
//			SurfacePointsCurve traj = trajX.transformClone(mXG);
//			this.emitter.sendSignal(new PointsCurveSignal("newCurve", trajX));
			return true;
		}
		return false;
		// TODO: verifier que les deux points sont bien connectés sans concavité
	}

	/**
	 * Calcule tous les points d'intersection entre un plan et une isoparamétrique.
	 * Utilise une isoparamétrique approximée pour déterminer le point de départ de
	 * la résolution.
	 * 
	 * @param pc    : une courbe isoparamétrique approximée
	 * @param xPlan
	 * @param param
	 * 
	 * @return un ArrayList<SurfacePoint> contenant tous les points d'intersection
	 *         entre un plan et une isoparamétrique
	 */
	private ArrayList<SurfacePoint> calcAllIsoInt(SurfacePointsCurve pc, double xPlan, Surface.Param param) {
		ArrayList<SurfacePoint> result = new ArrayList<SurfacePoint>();
		for (int i = 0; i < pc.getNbPoints() - 1; i++) {
			if ((xPlan > pc.getPoint(i).x && xPlan < pc.getPoint(i + 1).x)
					|| (xPlan < pc.getPoint(i).x && xPlan > pc.getPoint(i + 1).x)) {
				// Point de départ du calcul d'intersection : p0
				SurfacePoint p0 = new SurfacePoint();
				SurfacePoint pInf = pc.getPoint(i);
				SurfacePoint pSup = pc.getPoint(i + 1);
				p0.x = xPlan;
				double k = (p0.x - pInf.x) / (pSup.x - pInf.x);
				p0.y = (pSup.y - pInf.y) * k + pInf.y;
				p0.z = (pSup.z - pInf.z) * k + pInf.z;

				SurfacePoint pSol = null;

				if (param == Surface.Param.U) {
					double vInf = pInf.v;
					double vSup = pSup.v;
					double v0 = (vSup - vInf) * k + vInf;
					// calcule l'intersection entre l'iso en u correspondant à
					// la courbe approchée pc et le plan xPlan
					SurfaceIsoCurve isoU = new SurfaceIsoCurve(pInf.getSurface(), Surface.Param.U, pInf.u);
					pSol = calcInt(isoU, xPlan, v0);
				}
				if (param == Surface.Param.V) {
					double uInf = pInf.u;
					double uSup = pSup.u;
					double u0 = (uSup - uInf) * k + uInf;
					// calcule l'intersection entre l'iso en v correspondant à
					// la courbe approchée pc (même index) et le plan xPlan
					SurfaceIsoCurve isoV = new SurfaceIsoCurve(pInf.getSurface(), Surface.Param.V, pInf.v);
					pSol = calcInt(isoV, xPlan, u0);
				}
				if (pSol != null)
					// Parfois la résolution ne marche pas (pale) pour des pb
					// bizarres
					// => on bataille pas et on passe au point suivant
					result.add(pSol);
			}
		}
		return result;
	}

	/**
	 * Calcule l'intersection entre une isoparamétrique et un plan en utilisant le
	 * point de départ fourni
	 * 
	 * Résout cx(u) - xPlan = 0 avec cx(u) composante en x de la courbe
	 * isoparamétrique
	 * 
	 * variables : u, y, z resp var[0], var[1] et var[2]
	 * 
	 * @param c     l'isoparamétrique
	 * @param xPlan l'abscisse x du
	 * @param var   la valeur u du point de départ
	 * @return le point d'intersection calculé ou null si aucun point n'a pu être
	 *         calculé
	 */
	private SurfacePoint calcInt(final SurfaceIsoCurve c, final double xPlan, double var) {
		UnivarFunction f = new UnivarFunction(new UnivarEvaluable() {
			public double eval(double var) {
				return c.getV3f().getFx().eval(var) - xPlan;
			}
		});
		Newton1x1Solver solver = new Newton1x1Solver(f, var, 10, Util.PREC9);
		try {
			solver.run();
		} catch (Exception e) {
			// System.out.println("Solver erreur");
		}

		double usol = 0.0;
		double vsol = 0.0;

		double varsol = solver.getVar();

		SurfacePoint result = null;
		BrentOptimizer fallbackSolver = null;

		double varinf = 0.0, varsup = 1.0; // initialisation pour éviter
											// l'erreur de compil
		if (c.getParam_name() == Surface.Param.U) {
			varinf = surface.getUmin();
			varsup = surface.getUmax();
			usol = c.getValue();
			vsol = varsol;
//			varinf = (floor(var * meshTess * tessV) / (meshTess * tessV));
//			varsup = ((floor(var * meshTess * tessV) + 1.0) / (meshTess * tessV));
		}
		if (c.getParam_name() == Surface.Param.V) {
			varinf = surface.getVmin();
			varsup = surface.getVmax();
			usol = varsol;
			vsol = c.getValue();
//			varinf = (floor(var * meshTess * tessU) / (meshTess * tessU));
//			varsup = ((floor(var * meshTess * tessU) + 1.0) / (meshTess * tessU));
		}

		if (varsol < varinf || varsol > varsup || !c.isDefinedFor(varsol)) {
			fallbackSolver = new BrentOptimizer(f, varinf, varsup, Util.PREC9, 50);
			try {
				fallbackSolver.run();
			} catch (Exception e) {
			}
			varsol = fallbackSolver.getVar();
			if (c.getParam_name() == Surface.Param.U) {
				usol = c.getValue();
				vsol = varsol;
			}
			if (c.getParam_name() == Surface.Param.V) {
				usol = varsol;
				vsol = c.getValue();
			}
		}

		result = new SurfacePoint(c.getSurface(), usol, vsol);
		double err = result.x - xPlan;

		if (abs(err) > Util.PREC3) {
			return null;
		}

		return result;
	}

	private double calcMarge(SurfacePoint p) {
		double marge;
		double d; // la distance entre les centreReff
		double r = cutter.getTipRadius();

		/*
		 * gamma est l'angle entre la direction des centreReff et l'horizontale. Dans la
		 * mesure où on usine dans un plan x=cste, cet angle correspond à la projection
		 * de la normale dans ce plan, c'est à dire à atan(n.x/n.z)
		 */
		double gamma;
		// on néglige la courbure de la surface
		d = 2 * sqrt(2 * r * sh - sh * sh);
		double u = p.u;
		double v = p.v;
		Vector3d n = CLSurfX.normal(u, v);
		gamma = atan(n.x / (sqrt(n.y * n.y + n.z * n.z)));
		marge = d * cos(gamma);
		return marge;

	}

	public LinkedList<ArrayList<SurfacePointsCurve>> getToolpathApproxX() {
		return toolpathApproxX;
	}

	public LinkedList<ArrayList<SurfacePointsCurve>> getToolpathApprox() {
		for (ArrayList<SurfacePointsCurve> spcListX : toolpathApproxX) {
			ArrayList<SurfacePointsCurve> spcListG = new ArrayList<SurfacePointsCurve>();
			for (SurfacePointsCurve spcX : spcListX) {
				SurfacePointsCurve spcG = spcX.transformClone(mXG);
				spcListG.add(spcG);
			}
			toolpathApproxG.add(spcListG);
		}
		return toolpathApproxG;
	}

	public double getToolPathApproxLength() {
		double result = 0.0;
		for (ArrayList<SurfacePointsCurve> spcListX : toolpathApproxX) {
			for (SurfacePointsCurve spc : spcListX) {
				result = result + spc.length();
			}
		}
		return result;
	}

	/**
	 * Pour calculer le temps de parcours d'une trajectoire, trois cas sont à
	 * considérer :
	 * <ol>
	 * <li>l'accélération maximale est atteinte pour la vitesse de consigne
	 * programmée et atteinte</li>
	 * <li>l'accélération maximale n'est pas atteinte pour la vitesse de consigne
	 * programmée et atteinte</li>
	 * <li>la distance parcourue est trop courte pour atteindre la vitesse de
	 * consigne</li>
	 * </ol>
	 * 
	 * Soit jmax le jerk maxi, amax l'accélération maxi, v la vitesse de consigne et
	 * h la demi-longueur à parcourir, on calcule
	 * 
	 * <ol>
	 * <li>tA = amax/jmax, le temps pour atteindre l'accélération maximale (cas
	 * 1)</li>
	 * <li>tV = sqrt(v/jmax), le temps pour atteindre la vitesse de consigne (cas
	 * 2)</li>
	 * <li>tH = cbrt(h/jmax), le temps pour parcourir la distance h (cas 3)</li>
	 * </ol>
	 * 
	 * Le minimum de ces trois temps détermine le cas dans lequel on se trouve.
	 * Ensuite le temps nécessaire pour parcourir la demi-longueur est donné par :
	 * 
	 * <ol>
	 * <li>cas 1 : t = h/v+0.5*amax/jmax+0.5*v/amax</li>
	 * <li>cas 2 : t = h/v+sqrt(v/jmax)</li>
	 * <li>cas 3 : t = 2*cbrt(h/jmax)</li>
	 * </ol>
	 * 
	 * @return le temps de parcours d'une trajectoire
	 */
	public double getMillingTimeApprox() {
		double totalTime = 0.0;
		double jmax = MillingContext.getInstance().getJmax();
		double amax = MillingContext.getInstance().getAmax();
		double v = MillingContext.getInstance().getVf();

		double tA = amax / jmax;
		double tV, tH;
		double length, h;
		double t = 0.0;
		for (ArrayList<SurfacePointsCurve> spcListX : toolpathApproxX) {
			for (SurfacePointsCurve spc : spcListX) {
				length = spc.length();
				h = length / 2.0; // en mm
				h = h / 1000.0; // en m
				tV = sqrt(v / jmax);
				tH = cbrt(h / jmax);
				if (tA < tV && tA < tH) { // cas 1
					t = h / v + 0.5 * amax / jmax + 0.5 * v / amax;
				}
				if (tV < tA && tV < tH) { // cas2
					t = h / v + sqrt(v / jmax);
				}
				if (tH < tA && tH < tV) {// cas 3
					t = 2 * cbrt(h / jmax);
				}
				totalTime = totalTime + 2 * t;
			}
		}
		// On suppose que le temps de parcours de la pénalité correspond toujours au cas
		// 2
		totalTime = totalTime + this.getPenalty() / 1000.0 / v + sqrt(v / jmax);

		return totalTime;
	}

	/**
	 * Calcul de la pénalité d'usinage correspondant à l'entrée/sortie de la zone.
	 * 
	 * <p>
	 * La pénalité est égale à la différence en Z entre le point le plus haut et le
	 * point le plus bas de la zone, le tout augmenté de 10%.
	 * 
	 * @return la valeur de la pénalité
	 */
	public double getPenalty() {
		double result = pZmax.z - pZmin.z;
		result = result * 1.1;
		return result;
//		return 0.0;
	}

	public Surface getSurfX() {
		return surfX;
	}

	public Surface getCLSurf() {
		return CLSurf;
	}

	public Surface getCLSurfX() {
		return CLSurfX;
	}

	public ApproxZone getZoneApprox() {
		return zoneApprox;
	}

	/**
	 * Une approximation d'une zone par un réseau d'isoparamétriques approchées.
	 * 
	 * Chaque isoparamétrique appprochée est définie par un tableau de courbes
	 * consituées d'une suite de points. Chaque élément de ce tableau est un segment
	 * de la courbe isoparamétrique réelle. Chaque segment correspond à une portion
	 * contigüe du maillage pris en compte.
	 * 
	 * @author redonnet
	 * 
	 */
	public class ApproxZone {
		private Surface s;
		private Zone z;

		// WARNING: s reference is X / z reference is G

		// liste des isoparamétriques approchées en u
		// la clé de la map correspond à la valeur de l'isoparamétrique.
		private Map<Double, ArrayList<SurfacePointsCurve>> approxIsoU;
		// liste des isoparamétriques approchées en u
		// la clé de la map correspond à la valeur de l'isoparamétrique.
		private Map<Double, ArrayList<SurfacePointsCurve>> approxIsoV;

		public ApproxZone(Surface s, Zone z) {
			this.s = s;
			this.z = z;
			createApproxUV();
		}

		public Surface getS() {
			return s;
		}

		public Zone getZ() {
			return z;
		}

		/**
		 * Return the iso u approximation of the cutter location surface in the global
		 * frame
		 * 
		 * @return une approximation de la courbe iso en u
		 */
		public Map<Double, ArrayList<SurfacePointsCurve>> getApproxIsoU() {
			Map<Double, ArrayList<SurfacePointsCurve>> globalIsoU = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
			for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> entry : approxIsoU.entrySet()) {
				Double key = entry.getKey();
				ArrayList<SurfacePointsCurve> value = entry.getValue();
				ArrayList<SurfacePointsCurve> curvesArray = new ArrayList<SurfacePointsCurve>();
				for (SurfacePointsCurve c : value) {
					SurfacePointsCurve l = new SurfacePointsCurve(CLSurf);
					for (SurfacePoint p : c.getPoints()) {
						SurfacePoint result = new SurfacePoint(p);
						l.add(result);
					}
					curvesArray.add(l);
				}
				globalIsoU.put(key, curvesArray);
			}
			return globalIsoU;
		}

		public Map<Double, ArrayList<SurfacePointsCurve>> getApproxIsoV() {
			Map<Double, ArrayList<SurfacePointsCurve>> globalIsoV = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
			for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> entry : approxIsoV.entrySet()) {
				Double key = entry.getKey();
				ArrayList<SurfacePointsCurve> value = entry.getValue();
				ArrayList<SurfacePointsCurve> curvesArray = new ArrayList<SurfacePointsCurve>();
				for (SurfacePointsCurve c : value) {
					SurfacePointsCurve l = new SurfacePointsCurve(CLSurf);
					for (SurfacePoint p : c.getPoints()) {
						SurfacePoint result = new SurfacePoint(p);
						l.add(result);
					}
					curvesArray.add(l);
				}
				globalIsoV.put(key, curvesArray);
			}
			return globalIsoV;
		}

		private void createApproxUV() {
			// écart entre les points (objectif visé, en mm)
			// comme on est dans le repère X il se mesure sur y
			double d;

			d = approxLevel.getValeur();

			double deltaX = pXmax.x - pXmin.x;
			double deltaY = pYmax.y - pYmin.y;
			int nbIsoUx = (int) Math.ceil(deltaX / d);
			int nbIsoUy = (int) Math.ceil(deltaY / d);
			int nbIsoVx = (int) Math.ceil(deltaX / d);
			int nbIsoVy = (int) Math.ceil(deltaY / d);
			int nbIsoU = Math.max(nbIsoUx, nbIsoUy);
			int nbIsoV = Math.max(nbIsoVx, nbIsoVy);

			approxIsoU = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
			SurfacePointsCurve pc;
			for (int i = 0; i <= nbIsoU; i++) { // pour chaque iso en U
				ArrayList<SurfacePointsCurve> isoUlist = new ArrayList<SurfacePointsCurve>();
				double u = (double) i / (double) nbIsoU;
				pc = new SurfacePointsCurve(s);

				for (int j = 0; j <= nbIsoV; j++) {
					double v = (double) j / (double) nbIsoV;
					if (z.includes(u, v)) { // si (u,v) appartient à la zone, on regarde le point précédent
						double vprec = (double) (j - 1) / (double) nbIsoV;
						if (z.includes(u, vprec)) {
							// si le point précédent appartient à la zone, il a déjà dû être ajouté. On
							// ajoute simplement le nouveau point
							pc.addPoint(new SurfacePoint(s, u, v));
							// si la courbe a deux points, on signale l'existence d'un nouvelle courbe
//							if (pc.getNbPoints() == 2)
//								ZoneMilling_PPV3A_Approx.this.support.firePropertyChange("newCurve", null, pc);

						} else {
							// si le point précédent n'appartient pas à la zone, on stocke la courbe
							// précédente si elle n'est pas nulle et on crée une nouvelle courbe à partir du
							// nouveau point
							if (pc.getNbPoints() > 0) {
								isoUlist.add(pc);
							}
							pc = new SurfacePointsCurve(s);
							pc.addPoint(new SurfacePoint(s, u, v));
						}
					}
				}
				// On stocke la dernière courbe calculée si elle n'est pas nulle
				if (pc.getNbPoints() > 0) {
					isoUlist.add(pc);
				}
				// Si la liste des courbes calculées pour cette valeur de u n'est pas vide, on
				// la stocke
				if (isoUlist.size() > 0)
					approxIsoU.put(u, isoUlist);
			}

			approxIsoV = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
			for (int j = 0; j <= nbIsoV; j++) { // pour chaque iso en V
				ArrayList<SurfacePointsCurve> isoVlist = new ArrayList<SurfacePointsCurve>();
				double v = (double) j / (double) nbIsoV;
				pc = new SurfacePointsCurve(s);

				for (int i = 0; i <= nbIsoU; i++) {
					double u = (double) i / (double) nbIsoU;
					if (z.includes(u, v)) {
						double uprec = (double) (i - 1) / (double) nbIsoU;
						if (z.includes(uprec, v)) {
							pc.addPoint(new SurfacePoint(s, u, v));
//							if (pc.getNbPoints() == 2)
//								ZoneMilling_PPV3A_Approx.this.support.firePropertyChange("newCurve", null, pc);
						} else {
							if (pc.getNbPoints() > 0)
								isoVlist.add(pc);
							pc = new SurfacePointsCurve(s);
							pc.addPoint(new SurfacePoint(s, u, v));
						}
					}
				}
				if (pc.getNbPoints() > 0)
					isoVlist.add(pc);
				if (isoVlist.size() > 0)
					approxIsoV.put(v, isoVlist);
			}
		}
	}

	private class SurfacePointComparator implements Comparator<SurfacePoint> {
		public SurfacePointComparator() {
		}

		public int compare(SurfacePoint p0, SurfacePoint p1) {
			int result = Double.compare(p0.y, p1.y);

			return result;
		}
	}

}
