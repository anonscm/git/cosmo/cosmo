/*
** ToolPathSet.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the jCAM software package (jCAM stands for
** java Computer Aided Manufacturing). This software provides various
** professional tools about CNC manufacturing (cutter, toolpath,
** machine-tools, ...)
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import org.lgmt.dgl.commons.AbstractTreeNode.TreeNodeIterator;
import org.lgmt.dgl.commons.TreeNode;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.toolpath.Toolpath;
import org.lgmt.jcam.toolpath.ToolpathSet;

/**
 * A set of Toolpaths in 3-axes context.
 * 
 * NOTE: May be moved to the jCAM eventually. But this would imply that
 * G00Connector is moved to the jCAM too. This would make the jCAM dependant on
 * VTK...
 * 
 * @author redonnet
 * 
 */
public class ToolpathSet3A extends ToolpathSet {
	private static final long serialVersionUID = 4787014376530288703L;

	public ToolpathSet3A() {
		super();
	}

	/**
	 * Generates an optimal set of G00 connectors to link all the Toolpaths of this
	 * set.
	 * 
	 * Uses brute force. May be very long for a significant amount of toolpaths.
	 */
	public void generateG0Connectors() {
		/*
		 * Build list of toolpaths endings
		 */
		List<TpTerm> termList = new ArrayList<TpTerm>();
		for (Toolpath<? extends Point3d> tp : this) {
			TpTerm start = new TpTerm(tp, TERM_TYPE.START);
			TpTerm end = new TpTerm(tp, TERM_TYPE.END);
			start.setPeer(end);
			end.setPeer(start);
			termList.add(start);
			termList.add(end);
		}

		/*
		 * Build tree of TpTerms possible combinations (taking into account peers)
		 */
		List<TreeNode<TpTerm>> treeList = new ArrayList<TreeNode<TpTerm>>();
		for (TpTerm term : termList) {
			TreeNode<TpTerm> rootNode = new TreeNode<TpTerm>(term);
			treeList.add(rootNode);
		}
		for (TreeNode<TpTerm> root : treeList) {
			populate(root, termList);
		}

		/*
		 * Building list of valid paths going through all TpTerms (starting from leaf to
		 * root, and then reverse it)
		 */
		List<ArrayList<TreeNode<TpTerm>>> g00Paths = new ArrayList<ArrayList<TreeNode<TpTerm>>>();
		for (TreeNode<TpTerm> root : treeList) {
			@SuppressWarnings("rawtypes")
			TreeNodeIterator itr = root.iterator();
			while (itr.hasNext()) {
				@SuppressWarnings("unchecked")
				TreeNode<TpTerm> node = (TreeNode<TpTerm>) itr.next();
				if (!node.isLeaf())
					continue;
				ArrayList<TreeNode<TpTerm>> ancestors = new ArrayList<TreeNode<TpTerm>>(node.ancestors());
				ancestors.add(0, node);
				Collections.reverse(ancestors);
				g00Paths.add(ancestors);
			}
		}

		/*
		 * Selection the best path (shortest)
		 */
		ArrayList<TreeNode<TpTerm>> bestPath = null;
		double shortestLength = Double.MAX_VALUE;
		for (ArrayList<TreeNode<TpTerm>> path : g00Paths) {
			double length = evalPath(path);
			if (length < shortestLength) {
				bestPath = path;
				shortestLength = length;
			}
		}

		/*
		 * Reordering toolpath in this to match best path order
		 */
		for (int i = 0; i < this.size(); i = i + 1) {
			Toolpath<? extends Point3d> tp = bestPath.get(2 * i).getData().getTp();
			if (tp != this.get(i)) {
				int j = this.indexOf(tp);
				Collections.swap(this, i, j);
			}
		}

		/*
		 * Reversing toolpath in this that were stored in the inverse order of the best
		 * path
		 */
		for (int i = 0; i < this.size(); i = i + 1) {
			Point3d start = bestPath.get(2 * i).getData().getPoint();
			if (this.get(i).getFirst() != start) {
				this.get(i).reverse();
			}
		}

		/*
		 * Add G0 connectors at their place
		 */
		ListIterator<Toolpath<? extends Point3d>> it = this.listIterator();
		while (it.hasNext()) {
			Toolpath<? extends Point3d> tp = it.next();
			int i = this.indexOf(tp);
			if (i == this.size() - 1)
				break;
			Point3d exitPoint = new Point3d(this.get(i).getLast());
//			exitPoint.z = exitPoint.z + g00Arrival;
			Point3d entryPoint = new Point3d(this.get(i + 1).getFirst());
//			entryPoint.z = entryPoint.z + g00Arrival;
			G0Connector_save connector = new G0Connector_save(exitPoint, entryPoint);
			it.add(connector.getToolpath());
		}
	}

	/**
	 * Populate recursively a node using a list of TpTerms.
	 * 
	 * If the node is at the start of a toolpath populates the peer.</br>
	 * If the node is at the end of a toolpath, build the list of remaining
	 * available TpTerms and populate each of them
	 * 
	 * @param baseNode the node to populate
	 * @param termList the list of TpTerms to use
	 */
	private void populate(TreeNode<TpTerm> baseNode, final List<TpTerm> termList) {
		if (baseNode.isRoot() || baseNode.getData() != baseNode.getParent().getData().getPeer()) {
			TreeNode<TpTerm> peerNode = new TreeNode<TpTerm>(baseNode.getData().getPeer());
			baseNode.add(peerNode);
			populate(peerNode, termList);
		} else {
			Collection<? extends TreeNode<TpTerm>> ancestors = baseNode.ancestors();
			List<TreeNode<TpTerm>> children = new ArrayList<TreeNode<TpTerm>>();
			for (TpTerm term : termList) {
				if (term == baseNode.getData())
					continue;
				boolean isAncestor = false;
				for (TreeNode<TpTerm> node : ancestors) {
					if (node.getData() == term) {
						isAncestor = true;
						break;
					}
				}
				if (!isAncestor) {
					children.add(new TreeNode<TpTerm>(term));
				}
			}
			for (TreeNode<TpTerm> child : children) {
				baseNode.add(child);
				populate(child, termList);
			}
		}
	}

	/**
	 * Computes length of a list of Jumps
	 * 
	 * @param path
	 * @return
	 */
	private double evalPath(ArrayList<TreeNode<TpTerm>> path) {
		double value = 0.0;
		Point3d start, end;
		for (int i = 1; i < path.size() - 1; i = i + 2) {
			start = path.get(i).getData().getPoint();
			end = path.get(i + 1).getData().getPoint();
			value = value + start.distance(end);
		}

		return value;
	}

	public void generateArrivals(double g0Arrival) {
		for (Toolpath<? extends Point3d> tp : this) {
			
			Point3d p0 = tp.get(0);
			Point3d prevP = tp.get(1);
			Vector3d dir0 = new Vector3d(prevP, p0);
			dir0.normalize();
			dir0.scale(g0Arrival);
			Point3d p1 = Points.translateClone(p0, dir0);
			Point3d p2 = new Point3d(p1);
			p2.z = p2.z + g0Arrival;
			BezierCurve c = new BezierCurve(new Point3d[] { p0, p1, p2 });

			/*
			 * Estimation of number of interpolation points needed. Based on a rough
			 * estimation of the length of the Bézier cuurve.
			 */
			double l1 = c.polygonLength();
			double l2 = p0.distance(p2);
			int tess = (int) Math.floor((l1 + l2) / 1.5);
			if (tess < 6)
				tess = 6;

			Toolpath<Point3d> arrival = new Toolpath<Point3d>(c, tess);
			arrival.reverse();

//			tp.prepend((Toolpath<Point3d>)arrival);
			
			prependHelper(tp, arrival);
		}
	}

	// FIXME: wildcard capture problem.
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void appendHelper(Toolpath tp, Point3d p) {
		tp.append(p);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void prependHelper(Toolpath tp, Point3d p) {
		tp.prepend(p);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void appendHelper(Toolpath tp, Toolpath newTp) {
		tp.append(newTp);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void prependHelper(Toolpath tp, Toolpath newTp) {
		tp.prepend(newTp);
	}
	
}
