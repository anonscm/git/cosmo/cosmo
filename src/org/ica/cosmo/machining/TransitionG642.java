package org.ica.cosmo.machining;

import static java.lang.Math.abs;
import static org.lgmt.dgl.vecmath.Vectors3.addV;
import static org.lgmt.dgl.vecmath.Vectors3.scaleV;

import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.machine.Axis.AxisName;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

/**
 * Modélise une transition entre deux blocs G1 p0 - s - p1
 * 
 * <ul>
 * <li>p0 : premier point du premier block
 * <li>s : sommet
 * <li>p1 : deuxième point du deuxième bloc
 * </ul>
 * 
 * Le point de passage à la tolérance maxi est I
 * 
 */

public class TransitionG642 {
	protected MillingContext mc;
	protected Toolpath<Point3d> tp;
	protected int posIdx; // 1 <= posIdx <= tp.size()-2
	protected double vfP;
	protected Point3d s; // summit
	protected Vector3d u0; // incomming vector
	protected Vector3d u1; // outcomming vector
	protected double vf; // m/s
	protected double jmax; // m/s^3 , per axis
	protected double amax; // m/s^2 , per axis
	protected double g642tol; // mm , per axis
	protected Point3d pTol = null;
	protected Vector3d vTol = null;

	public TransitionG642(Toolpath<Point3d> tp, int posIdx) {
		this.mc = MillingContext.getInstance();
		this.tp = tp;
		this.posIdx = posIdx;
		this.vfP = mc.getVf();
		this.s = tp.get(posIdx);
		this.u0 = new Vector3d(s, tp.get(posIdx - 1));
		this.u1 = new Vector3d(s, tp.get(posIdx + 1));
		this.vf = mc.getVf() / 1000.0; // mm/s -> m/s
		this.jmax = mc.getJmax(); // m/s^3 , per axis
		this.amax = mc.getAmax(); // m/s^2 , per axis
		this.g642tol = mc.getG642tol(); // mm , per axis
		this.calcTol();
	}

	private void calcTol() {
		vTol = addV(scaleV(1.0 / u0.length(), u0), scaleV(1.0 / u1.length(), u1));
		double projX = abs(vTol.x);
		double projY = abs(vTol.y);
		double projZ = abs(vTol.z);
		double projMax = projX;
		AxisName limAxis = AxisName.X;
		if (projY > projMax) {
			projMax = projY;
			limAxis = AxisName.Y;
		}
		if (projZ > projMax) {
			projMax = projZ;
			limAxis = AxisName.Z;
		}
		double ratio;
		switch (limAxis) {
		case X:
			ratio = g642tol / projX;
			break;
		case Y:
			ratio = g642tol / projY;
			break;
		case Z:
			ratio = g642tol / projZ;
			break;
		default:
			ratio = 0.0; // TODO: throw Exception
		}
		vTol.scale(ratio);

		pTol = new Point3d(s);
		pTol.add(vTol);
	}

	public Vector3d getIncomingVector() {
		return u0;
	}

	public Vector3d getOutcomingVector() {
		return u1;
	}

	public Point3d getSummitVertex() {
		return s;
	}

	public Point3d getTolVertex() {
		return pTol;
	}

	public Vector3d getTolVector() {
		return vTol;
	}

	
}
