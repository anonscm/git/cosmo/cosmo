package org.ica.cosmo.machining;

import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

import vtk.vtkCellArray;
import vtk.vtkCellArrayIterator;
import vtk.vtkCenterOfMass;
import vtk.vtkDataArray;
import vtk.vtkHull;
import vtk.vtkIdList;
import vtk.vtkNativeLibrary;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataNormals;
import vtk.vtkTransform;
import vtk.vtkTransformFilter;

public class ConvexHull {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	private vtkPoints inputPoints;
	private vtkPolyData output;
	private vtkPolyData upperHull;
	private vtkPolyData upperHullProjection;
	private vtkPoints outputPoints;
	private vtkCellArray outputPolys;
	private Point3d center;

	private void getPoints(BezierSurface surface) {
		int npu = surface.getNpu();
		int npv = surface.getNpv();

		inputPoints = new vtkPoints();
		Point3d p;
		for (int i = 0; i < npu; i++) {
			for (int j = 0; j < npv; j++) {
				p = surface.getPoint(i, j);
				inputPoints.InsertNextPoint(p.x, p.y, p.z);
			}
		}
	}

	public ConvexHull(Surface surface) {
		this(surface, 1);
	}

	public ConvexHull(Surface surface, int recursionLevel) {
		if (surface instanceof BezierSurface)
			getPoints((BezierSurface) surface);
		else {
			System.err.println("Surface type not supported for moment.");
			return;
		}

		vtkPolyData polyData = new vtkPolyData();
		polyData.SetPoints(inputPoints);

		vtkHull hullFilter = new vtkHull();
		hullFilter.SetInputData(polyData);
		hullFilter.AddCubeFacePlanes();
		hullFilter.AddRecursiveSpherePlanes(recursionLevel);
		hullFilter.Update();
		output = hullFilter.GetOutput();
		outputPoints = output.GetPoints();
		outputPolys = output.GetPolys();

		// Compute the center of mass
		vtkCenterOfMass centerOfMassFilter = new vtkCenterOfMass();
		centerOfMassFilter.SetInputData(output);
		centerOfMassFilter.SetUseScalarsAsWeights(false);
		centerOfMassFilter.Update();
		center = new Point3d(centerOfMassFilter.GetCenter());

		// Compute normals
		vtkPolyDataNormals normalGenerator = new vtkPolyDataNormals();
		normalGenerator.SetInputData(output);
		normalGenerator.ComputePointNormalsOff();
		normalGenerator.ComputeCellNormalsOn();
		normalGenerator.Update();
		output = normalGenerator.GetOutput();
	}

	public vtkPolyData getOutput() {
		return output;
	}

	public vtkPoints getOutputPoints() {
		return outputPoints;
	}

	public vtkCellArray getOutputPolys() {
		return outputPolys;
	}

	public Point3d getCenter() {
		return center;
	}

	public vtkCellArray getUpperPolys() {
		vtkCellArray upperPolys = new vtkCellArray();
		Vector3d z = new Vector3d(0.0, 0.0, 1.0);

		Vector3d normal;
		vtkIdList idL = new vtkIdList();
		vtkCellArrayIterator iter = outputPolys.NewIterator();
		vtkDataArray data = output.GetCellData().GetArray("Normals");
		for (iter.GoToFirstCell(); !iter.IsDoneWithTraversal(); iter.GoToNextCell()) {
			outputPolys.GetNextCell(idL);
			double[] n = new double[3];
			n = data.GetTuple3(iter.GetCurrentCellId());
			normal = new Vector3d(n);
			if (Vectors3.dotV(normal, z) > 0.0) {
				upperPolys.InsertNextCell(idL);
			}
		}

		return upperPolys;
	}

	public vtkPolyData getUpperHull() {
		upperHull = new vtkPolyData();
		vtkPoints upperPoints = new vtkPoints();
		vtkCellArray upperPolys = new vtkCellArray();

		Vector3d z = new Vector3d(0.0, 0.0, 1.0);
		Vector3d normal;
		vtkIdList outputIdL = new vtkIdList();
		;
		vtkCellArrayIterator iter = outputPolys.NewIterator();
		vtkDataArray data = output.GetCellData().GetArray("Normals");
		for (iter.GoToFirstCell(); !iter.IsDoneWithTraversal(); iter.GoToNextCell()) {
			outputPolys.GetNextCell(outputIdL);
			double[] ncoords = data.GetTuple3(iter.GetCurrentCellId());
			normal = new Vector3d(ncoords);
			if (Vectors3.dotV(normal, z) > 0.0) {
				vtkIdList idList = new vtkIdList();
				for (int i = 0; i < outputIdL.GetNumberOfIds(); i++) {
					long n = outputIdL.GetId(i);
					long id = upperPoints.InsertNextPoint(output.GetPoint(n));
					idList.InsertNextId(id);
				}
				upperPolys.InsertNextCell(idList);
			}
		}
		upperHull.SetPoints(upperPoints);
		upperHull.SetPolys(upperPolys);

		return upperHull;
	}

	public vtkPolyData getUpperHullProjection() {
		upperHullProjection = new vtkPolyData();

		vtkTransform transform = new vtkTransform();
		transform.Scale(1.0, 1.0, 0.0);

		vtkTransformFilter transformFilter = new vtkTransformFilter();
		transformFilter.SetTransform(transform);
		transformFilter.SetInputData(upperHull);
		transformFilter.SetTransform(transform);
		transformFilter.Update();
		upperHullProjection = transformFilter.GetPolyDataOutput();

		return upperHullProjection;
	}

	public Point3d getSafeAbovePoint(double x, double y) {
		Point3d p = new Point3d();
		vtkIdList idL = new vtkIdList();
		vtkCellArray polys = upperHullProjection.GetPolys();
		vtkCellArrayIterator iter = polys.NewIterator();
		for (iter.GoToFirstCell(); !iter.IsDoneWithTraversal(); iter.GoToNextCell()) {
			polys.GetNextCell(idL);
			for (int i = 0; i < idL.GetNumberOfIds(); i++) {
				long n = idL.GetId(i);
			}

		}

		return p;
	}
}
