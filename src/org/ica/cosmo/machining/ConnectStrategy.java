package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public abstract class ConnectStrategy {
	protected final static ConnectorFactory factory = new ConnectorFactory();
	protected final EventManager emitter = new EventManager(this);

	protected TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath;
	protected List<Double> keys;
	protected boolean rounded;
	protected double overrun;
	protected List<Toolpath<Point3d>> tpList;
	protected ArrayList<ToolpathSequence> tpsList;
	
	protected ConnectStrategy(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath, double overrun) {
		super();
		this.prgToolpath = prgToolpath;
		this.overrun = overrun;
		this.rounded = true;
		keys = prgToolpath.keySet().stream().collect(Collectors.toList());
		tpList = new ArrayList<Toolpath<Point3d>>();
		tpsList = new ArrayList<ToolpathSequence>();
		
	}
	
	protected ConnectStrategy(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath) {
		this(prgToolpath, 2.0);
	}
	
	protected abstract void run();

	public List<Toolpath<Point3d>> getToolpaths() {
		return tpList;
	}

	public ArrayList<ToolpathSequence> getTpsList() {
		return tpsList;
	}

	public EventManager getEmitter() {
		return emitter;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}
	
}
