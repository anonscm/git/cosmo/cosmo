package org.ica.cosmo.machining;

import static java.lang.Math.cbrt;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.util.PolyLineSignal;
import org.ica.cosmo.util.ToolpathSignal;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.Signal;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

/**
 * Calcul des trajectoires outil sur une zone.
 * 
 * <p>
 * Dans cette classe tout est dans le repère global.
 * 
 * @author redonnet
 *
 */
public class ZoneMilling_PPV3A extends AbstractZoneMilling {
	private static Logger logger;
	static {
		if (LogManager.getLogManager().getLogger("cosmo") != null)
			logger = LogManager.getLogManager().getLogger("cosmo");
		else
			logger = Logger.getGlobal();
	}

	private APPROX_LEVEL approxLevel;

	/** matrice de passage du repère global au repère de calcul */
	private Matrix4d mGX;

	/** matrice de passage du repère de calcul au repère global */
	private Matrix4d mXG;

	/** contructeur de trajectoires brutes */
	private PPV3A_RawTrajBuilder rawTrajBuilder = null;

	/**
	 * Les trajectoires brutes dans le repère global
	 * 
	 * Tient compte des concavités. Ne contient aucune connexion en fin de
	 * trajectoire.
	 */
	private TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath;

	/**
	 * Angle de rotation à utiliser pour usiner cette zone dans une direction x=cste
	 */
	private double rotation;

	/**
	 * Corresponding points list
	 * 
	 * Useful to find back a surface point from which a given traj point is
	 * calculated
	 */
	Map<Point3d, SurfacePoint> pairsMap;

	/**
	 * Decide whether G0 connection are computed or not
	 */
	private boolean connect;

	/**
	 * Constructeur.
	 * 
	 * @param zone
	 * @param vfAngle
	 * @param accuracy
	 */
	public ZoneMilling_PPV3A(Zone zone, double vfAngle, APPROX_LEVEL accuracy, boolean connect) {
		super(zone);
		this.rotation = vfAngle;
		this.approxLevel = accuracy;
		if (this.approxLevel == APPROX_LEVEL.COARSE)
			logger.warning("Approx level is set to COARSE. Resulting toolpaths may not be suitable for machining");
		mGX = new Matrix4d();
		mGX.setIdentity();
		// Usinage à x=cste => vf // Y
		// vfangle = (X,vf) => rotation = Pi/2 - vfangle
		mGX.setRotation(new AxisAngle4d(0.0, 0.0, 1.0, Math.PI / 2 - vfAngle));

		this.rawTrajBuilder = new PPV3A_RawTrajBuilder(zone, mGX, approxLevel);

		prgToolpath = new TreeMap<Double, ArrayList<Toolpath<Point3d>>>();
		pairsMap = new HashMap<Point3d, SurfacePoint>();
		this.connect = connect;
	}

	public ZoneMilling_PPV3A(Zone zone, double vfAngle, APPROX_LEVEL accuracy) {
		this(zone, vfAngle, accuracy, true);
	}

	public ZoneMilling_PPV3A(Zone zone, APPROX_LEVEL accuracy) {
		this(zone, 0.0, accuracy, true);
	}

	/**
	 * Lance la simulation d'usinage
	 */
	@Override
	public void run() throws SodException {
		rawTrajBuilder.run();

		this.mXG = rawTrajBuilder.getmXG();

		TreeMap<Double, List<Toolpath<SurfacePoint>>> rawToolpathG = rawTrajBuilder.getRawToolpath();

		// Mapping surface points traj to tool programmed points
		for (Map.Entry<Double, List<Toolpath<SurfacePoint>>> e : rawToolpathG.entrySet()) {
			Double k = e.getKey();
			ArrayList<Toolpath<Point3d>> prgTpList = new ArrayList<Toolpath<Point3d>>();
			for (Toolpath<SurfacePoint> rawTp : e.getValue()) {
				emitter.sendSignal(new ToolpathSignal("AddRawToolpath", rawTp, this.zone.getId()));
				List<Point3d> prgPoints = new ArrayList<Point3d>();
				for (SurfacePoint p : rawTp) {
					Point3d pt = null;
					switch (prgPointType) {
					case CL:
						pt = getCLPoint(p);
						break;
					case CLT:
						pt = getCLTPoint(p);
						break;
					case CC:
						pt = new Point3d(p);
					default:
					}
					prgPoints.add(pt);
					pairsMap.put(pt, p);
				}
				Toolpath<Point3d> prgTp = new Toolpath<Point3d>(prgPoints);
				if (prgTp.length() < 1.0)
					continue;
				prgTpList.add(prgTp);

//				this.emitter.sendSignal(new ToolpathSignal("AddPrgToolpath", prgTp, zone.getId()));
			}
			if (prgTpList.size() > 0) // may be zero if prgTp too short
				prgToolpath.put(k, prgTpList);
		}

		if (connect) {
			// ConnectStrategyLined cs = new ConnectStrategyLined(prgToolpath, 2.0);
			ConnectStrategyProximity cs = new ConnectStrategyProximity(prgToolpath);
			// ConnectStrategyOptimal cs = new ConnectStrategyOptimal(prgToolpath);
			cs.addListener(this);
			cs.run();
			for (Toolpath<Point3d> tp : cs.getToolpaths()) {
				this.addG1ends(tp);
				toolpathSet.add(tp);
				this.emitter.sendSignal(new ToolpathSignal("AddPrgToolpath", tp, zone.getId()));
			}
//			g0connect();
		} else {
			for (ArrayList<Toolpath<Point3d>> tpList : prgToolpath.values()) {
				for (Toolpath<Point3d> tp : tpList) {
					toolpathSet.add(tp);
				}
			}
		}
		for (Toolpath<? extends Point3d> tp : toolpathSet.getG0Toolpaths())
			this.emitter.sendSignal(new ToolpathSignal("AddPrgToolpath", tp, zone.getId()));
	}

	private Point3d getCLPoint(SurfacePoint p) {
		Point3d ct = new Point3d(p);
		Vector3d normal = surface.normal(p.u, p.v);
		if (normal.x > 0.0)
			logger.warning("A summit is reached. Unexpected results may occur");
		if (normal.angle(Vector3d.Z) < Util.PREC3)
			logger.warning("Normal is vertical. Projection may be wrong");
		normal.scale(cutter.getTipRadius());
		ct.add(normal);
		Point3d clpt = new Point3d(ct);
		Vector3d proj = new Vector3d(normal);
		proj.z = 0.0;
		proj.normalize();
		proj.scale(cutter.getRadius() - cutter.getTipRadius());
		clpt.add(proj);
		return clpt;
	}

	private Point3d getCLTPoint(SurfacePoint p) {
		// Create link for debug
//		ArrayList<Point3d> link = new ArrayList<Point3d>();
//		link.add(p);
//		Point3d ct = new Point3d(p);
//		Vector3d normal = surface.normal(p.u, p.v);
//		if (normal.x > 0.0)
//			logger.warning("A summit is reached. Unexpected results may occur");
//		if (normal.angle(Vector3d.Z) < Util.PREC3)
//			logger.warning("Normal is vertical. Projection may be wrong");
//		normal.scale(cutter.getTipRadius());
//		ct.add(normal);
//		link.add(new Point3d(ct));
//		Point3d clpt = new Point3d(ct);
//		Vector3d proj = new Vector3d(normal);
//		proj.z = 0.0;
//		proj.normalize();
//		proj.scale(cutter.getRadius() - cutter.getTipRadius());
//		clpt.add(proj);
//		link.add(new Point3d(clpt));
//		emitter.sendSignal(new PolyLineSignal("AddPolyLine", link, zone.getId()));

		Point3d cltPt = new Point3d(getCLPoint(p));
		cltPt.z = cltPt.z - cutter.getTipRadius();
		return cltPt;
	}

	/**
	 * Appends smooths in and out trajectories to a toolpath.
	 * 
	 * These trajectories are build from Bézier curves defined by point 4 points.
	 * 
	 * @param tp
	 */
	private void addG1ends(Toolpath<Point3d> tp) {
		double dim = 3.0;
		// arrival
		Point3d p0 = tp.getFirst(); // point considéré pour tp
		Point3d nextP = tp.get(1);
		Vector3d dir = new Vector3d(nextP, p0);
		dir.normalize();
		dir.scale(dim);

		Point3d p1 = Points.translateClone(p0, dir);
		Point3d p2 = Points.translateClone(p1, dir);
		p2.z = p2.z + dim;

		Point3d p3 = new Point3d(p2);
		p3.z = p3.z + dim;

		BezierCurve c = new BezierCurve(new Point3d[] { p3, p2, p1, p0 });
		/*
		 * Estimation of number of interpolation points needed. Based on a rough
		 * estimation of the length of the Bézier cuurve.
		 */
		double l1 = c.polygonLength();
		double l2 = p0.distance(p3);
		int tess = (int) Math.floor((l1 + l2) / 1.5);
		Toolpath<Point3d> tpStart = new Toolpath<Point3d>(c, tess);
		tp.prepend(tpStart);

		// exit
		p0 = new Point3d(tp.getLast()); // point considéré pour tp
		Point3d prevP = tp.get(tp.size() - 2);
		dir = new Vector3d(prevP, p0);
		dir.normalize();
		dir.scale(dim);

		p1 = Points.translateClone(p0, dir);
		p2 = Points.translateClone(p1, dir);
		p2.z = p2.z + dim;

		p3 = new Point3d(p2);
		p3.z = p3.z + dim;
		c = new BezierCurve(new Point3d[] { p0, p1, p2, p3 });
		/*
		 * Estimation of number of interpolation points needed. Based on a rough
		 * estimation of the length of the Bézier cuurve.
		 */
		l1 = c.polygonLength();
		l2 = p0.distance(p3);
		tess = (int) Math.floor((l1 + l2) / 1.5);

		Toolpath<Point3d> tpEnd = new Toolpath<Point3d>(c, tess);
		tp.append(tpEnd);

	}

	/**
	 * Make g0 connections between toolpaths when toolpathSet contains several
	 * toolpaths.
	 * 
	 */
	private void g0connect() {
		if (toolpathSet.size() > 1)
			toolpathSet.generateG0Connectors();
	}

	private void remove(TreeMap<Double, ArrayList<Toolpath<Point3d>>> map, Double key, Toolpath<Point3d> tp) {
		map.get(key).remove(tp);
		if (map.get(key).size() == 0) {
			map.remove(key);
		}
	}

	/** debug purpose only */
	public PPV3A_RawTrajBuilder getRawTrajBuilder() {
		return rawTrajBuilder;
	}

	public ArrayList<UVMeshUnit> getMeshList() {
		return rawTrajBuilder.getMeshList();
	}

	public double getRotation() {
		return rotation;
	}

	public Matrix4d getmGX() {
		return mGX;
	}

	public Matrix4d getmXG() {
		return mXG;
	}

	/**
	 * Debug purpose only
	 */
	public Map<Double, ArrayList<Toolpath<Point3d>>> getPrgToolpath() {
		return prgToolpath;
	}

	public Map<Point3d, SurfacePoint> getPairsMap() {
		return pairsMap;
	}

	/**
	 * Pour calculer le temps de parcours d'une trajectoire, trois cas sont à
	 * considérer :
	 * <ol>
	 * <li>l'accélération maximale est atteinte pour la vitesse de consigne
	 * programmée et atteinte</li>
	 * <li>l'accélération maximale n'est pas atteinte pour la vitesse de consigne
	 * programmée et atteinte</li>
	 * <li>la distance parcourue est trop courte pour atteindre la vitesse de
	 * consigne</li>
	 * </ol>
	 * 
	 * Soit jmax le jerk maxi, amax l'accélération maxi, v la vitesse de consigne et
	 * h la demi-longueur à parcourir, on calcule
	 * 
	 * <ol>
	 * <li>tA = amax/jmax, le temps pour atteindre l'accélération maximale (cas
	 * 1)</li>
	 * <li>tV = sqrt(v/jmax), le temps pour atteindre la vitesse de consigne (cas
	 * 2)</li>
	 * <li>tH = cbrt(h/jmax), le temps pour parcourir la distance h (cas 3)</li>
	 * </ol>
	 * 
	 * Le minimum de ces trois temps détermine le cas dans lequel on se trouve.
	 * Ensuite le temps nécessaire pour parcourir la demi-longueur est donné par :
	 * 
	 * <ol>
	 * <li>cas 1 : t = h/v+0.5*amax/jmax+0.5*v/amax</li>
	 * <li>cas 2 : t = h/v+sqrt(v/jmax)</li>
	 * <li>cas 3 : t = 2*cbrt(h/jmax)</li>
	 * </ol>
	 * 
	 * @return le temps de parcours d'une trajectoire
	 */
	public double getMillingTimeApprox() {
		double totalTime = 0.0;
		double jmax = MillingContext.getInstance().getJmax();
		double amax = MillingContext.getInstance().getAmax();
		double v = MillingContext.getInstance().getVf();

		double tA = amax / jmax;
		double tV, tH;
		double length, h;
		double t = 0.0;
		for (Map.Entry<Double, ArrayList<Toolpath<Point3d>>> e : prgToolpath.entrySet()) {
			for (Toolpath<Point3d> tp : e.getValue()) {
				length = tp.length();
				h = length / 2.0; // en mm
				h = h / 1000.0; // en m
				tV = sqrt(v / jmax);
				tH = cbrt(h / jmax);
				if (tA < tV && tA < tH) { // cas 1
					t = h / v + 0.5 * amax / jmax + 0.5 * v / amax;
				}
				if (tV < tA && tV < tH) { // cas2
					t = h / v + sqrt(v / jmax);
				}
				if (tH < tA && tH < tV) {// cas 3
					t = 2 * cbrt(h / jmax);
				}
				totalTime = totalTime + 2 * t;
			}
		}
		// On suppose que le temps de parcours de la pénalité correspond toujours au cas
		// 2
		totalTime = totalTime + this.getPenalty() / 1000.0 / v + sqrt(v / jmax);

		return totalTime;
	}

	public double getToolpathLength() {
		double length = 0.0;

		for (Toolpath<? extends Point3d> tp : toolpathSet) {
			length = length + tp.length();
		}

		return length;
	}

	public double getMillingTime() {
		double totalTime = 0.0;

		for (Toolpath<? extends Point3d> tp : toolpathSet) {
			totalTime = totalTime + tp.millingTime();
		}
		return totalTime;
	}

	/**
	 * Calcul de la pénalité d'usinage correspondant à l'entrée/sortie de la zone.
	 * 
	 * <p>
	 * La pénalité est égale à la différence en Z entre le point le plus haut et le
	 * point le plus bas de la zone, le tout augmenté de 10%.
	 * 
	 * @return la valeur de la pénalité
	 */
	public double getPenalty() {
		double dz = 0.0;

		for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> entry : zone.getListIsoU().entrySet()) {
			for (SurfacePointsCurve spc : entry.getValue()) {
				double tmp = spc.getZRange();
				if (tmp > dz)
					dz = tmp;
			}
		}
		for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> entry : zone.getListIsoV().entrySet()) {
			for (SurfacePointsCurve spc : entry.getValue()) {
				double tmp = spc.getZRange();
				if (tmp > dz)
					dz = tmp;
			}
		}

		double result = dz * 1.1;
		return result;
	}

	@Override
	public void onReceivedSignal(Signal signal) {
		super.onReceivedSignal(signal);
		if (signal.getSource() instanceof ConnectStrategy)
			if (signal instanceof ToolpathSignal && signal.getName() == "AddToolpath") {
				ToolpathSignal s = (ToolpathSignal) signal;
				ToolpathSignal newSignal = new ToolpathSignal("AddFinalToolpath", s.getToolpath(), this.zone.getId());
//				this.emitter.sendSignal(newSignal);
			}
	}
}
