package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import org.ica.cosmo.machining.ConnectorFactory.LOCDATA;
import org.ica.cosmo.machining.ConnectorFactory.MODE;
import org.ica.cosmo.util.ToolpathSignal;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ConnectStrategyProxyNext extends ConnectStrategy {

	public ConnectStrategyProxyNext(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath) {
		super(prgToolpath);
	}

	@Override
	protected void run() {
		ArrayList<ToolpathSequence> tpsList = new ArrayList<ToolpathSequence>();
		ToolpathSequence tps = new ToolpathSequence();

		while (!prgToolpath.isEmpty()) {
			Double k = prgToolpath.firstKey();

			// Starting from the longest traj
			ArrayList<Toolpath<Point3d>> tpListK = prgToolpath.get(k);
			double length = 0.0;
			int longest = 0;
			for (int j = 0; j < tpListK.size(); j++) {
				Toolpath<Point3d> tp = tpListK.get(j);
				double tpl = tp.length(); 
				if (tpl > length)
					length = tpl;
					longest = j;
			}
			
			Toolpath<Point3d> tp = tpListK.get(longest);
			tps.add(tp);
			this.emitter.sendSignal(new ToolpathSignal("AddToolpath", tp));
			prgToolpath.get(k).remove(longest);
			
			if (prgToolpath.get(k).size() == 0) {
				prgToolpath.remove(k);
			}
			
			Double lastK = k; // last k in the tps
			Double firstK = k; // first k in the tps
			Double nextK = prgToolpath.higherKey(lastK);
			Double prevK = prgToolpath.lowerKey(firstK);

			while (true) {
				ArrayList<GenericConnector> candidates = new ArrayList<GenericConnector>();
				if (prgToolpath.get(firstK) != null && firstK != lastK) // at the begining firstK == lastK
				for (Toolpath<Point3d> tpTest : prgToolpath.get(firstK)) {
					GenericConnector gc = factory.makeShortestConnector(tps, tpTest, LOCDATA.FIRST);
					if(gc.getToolpath() != null)
						candidates.add(gc);
				}
			if (prgToolpath.get(lastK) != null)
				for (Toolpath<Point3d> tpTest : prgToolpath.get(lastK)) {
					GenericConnector gc = factory.makeShortestConnector(tps, tpTest, LOCDATA.LAST);
					if(gc.getToolpath() != null)
						candidates.add(gc);
				}

				if (prevK != null)
					for (Toolpath<Point3d> tpTest : prgToolpath.get(prevK)) {
						GenericConnector gc = factory.makeShortestConnector(tps, tpTest, LOCDATA.PREV);
						if(gc != null)
							candidates.add(gc);
					}
				if (nextK != null)
					for (Toolpath<Point3d> tpTest : prgToolpath.get(nextK)) {
						GenericConnector gc = factory.makeShortestConnector(tps, tpTest, LOCDATA.NEXT);
						if(gc != null)
							candidates.add(gc);
					}

				
				if (candidates.size() == 0) { // no more candidates. A new tps must be started
					tpsList.add(tps);
					tps = new ToolpathSequence();
					break;
				}
				GenericConnector winnerC = Collections.min(candidates);

				if (winnerC.connectionMode == MODE.APPEND) {
					tps.addLast(winnerC.getToolpath());
					tps.addLast(tp);
				}
				if (winnerC.connectionMode == MODE.PREPEND) {
					tps.addFirst(winnerC.getToolpath());
					tps.addFirst(tp);
				}
				this.emitter.sendSignal(new ToolpathSignal("AddToolpath", tp));
				this.emitter.sendSignal(new ToolpathSignal("AddToolpath", winnerC.getToolpath()));

				switch (winnerC.getLocData()) {
				case PREV:
					k = prevK;
					firstK = k;
					prevK = prgToolpath.lowerKey(k);
					break;
				case FIRST:
					k = firstK;
					prevK = prgToolpath.lowerKey(k);
					break;
				case LAST:
					k = lastK;
					nextK = prgToolpath.higherKey(k);
					break;
				case NEXT:
					k = nextK;
					lastK = k;
					nextK = prgToolpath.higherKey(k);
					break;
				default:
				}
				prgToolpath.get(k).remove(tp);
				if (prgToolpath.get(k).size() == 0)
					prgToolpath.remove(k);
			}
		}

			
	}

	public EventManager getEmitter() {
		return emitter;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}
}
