package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkHull;
import vtk.vtkNativeLibrary;
import vtk.vtkOBBTree;
import vtk.vtkPoints;

/**
 * A class to handle G0 toolpaths connecting G1 toolpaths together.
 * 
 * @author redonnet
 *
 */
public class G0Connector_save {
	/**
	 * As G0Connectors relies on VTK to handle convex hull, the VTK librairies are
	 * loaded in case no VTK-related work (i.e. visualization) is started yet
	 */
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	private static Logger logger = LogManager.getLogManager().getLogger("cosmo");
	private static Hull hull = null;
	private static double zMax = Double.NEGATIVE_INFINITY;
	private Point3d startP;
	private Point3d endP;
	private Toolpath<Point3d> connector;

	public G0Connector_save(Point3d startP, Point3d endP) {
		this.startP = startP;
		this.endP = endP;
		if (G0Connector_save.hull == null) {
			if (zMax < startP.z || zMax < endP.z)
				logger.warning("Warning: no hull nor zMax provided. May lead to unexpected results");
			logger.info("Warning: no hull provided. Using zMax safety limit");
			buildNoHullConnector();
		} else {
			buildConnector();
		}
	}

	private void buildNoHullConnector() {
		if (zMax < startP.z || zMax < endP.z) {
			double z = Math.max(startP.z, endP.z);
			G0Connector_save.zMax = z + startP.distance(endP) / 10.0;
		}
		connector = new Toolpath<Point3d>(startP, new Point3d(startP.x, startP.y, zMax));
		connector.append(new Point3d(endP.x, endP.y, zMax));
		connector.append(endP);
	}

	private void buildConnector() {
		double h = hull.getDeltaZ() / 10.0;
		double d = startP.distance(endP) / 10.0;
		vtkOBBTree tree = new vtkOBBTree();
		tree.SetDataSet(hull.GetOutput());
		tree.BuildLocator();
		boolean startOut = false, endOut = false;

		ArrayList<Point3d> bezierPoints = new ArrayList<Point3d>();
		double[] pStart = { startP.x, startP.y, startP.z };
		double[] pStartMax = { startP.x, startP.y, zMax };
		double[] pEnd = { endP.x, endP.y, endP.z };
		double[] pEndMax = { endP.x, endP.y, zMax };
		vtkPoints intersectPoints = new vtkPoints();
		tree.IntersectWithLine(pStart, pStartMax, intersectPoints, null);
		Point3d pt0 = null, pt3 = null;
		if (intersectPoints.GetNumberOfPoints() > 0) {
			pt0 = new Point3d(intersectPoints.GetPoint(0));
		} else {// point may be outside the hull
			startOut = true;
			pt0 = new Point3d(startP);
		}
		tree.IntersectWithLine(pEnd, pEndMax, intersectPoints, null);
		if (intersectPoints.GetNumberOfPoints() > 0) {
			pt3 = new Point3d(intersectPoints.GetPoint(0));
		} else { // point may be outside the hull
			endOut = true;
			pt3 = new Point3d(endP);
		}

		Point3d pt1 = new Point3d(pt0);
		pt1.z = pt1.z + h + d;
		Point3d pt2 = new Point3d(pt3);
		pt2.z = pt2.z + h + d;

		bezierPoints.add(pt0);
		bezierPoints.add(pt1);
		bezierPoints.add(pt2);
		bezierPoints.add(pt3);
		BezierCurve jumpCurve = new BezierCurve(bezierPoints);

		connector = new Toolpath<Point3d>(jumpCurve, 10);
		if (!startOut)
			connector.prepend(startP);
		if (!endOut)
			connector.append(endP);
		connector.setICode(INTERPOLATION_TYPE.G0);
	}

	public Toolpath<Point3d> getToolpath() {
		return connector;
	}

	public static vtkHull getHull() {
		return hull;
	}

	public static void setHull(Hull hull) {
		G0Connector_save.hull = hull;
		G0Connector_save.zMax = hull.getZMax();
	}

	public static double getzMax() {
		return zMax;
	}

	public static void setzMax(double zMax) {
		G0Connector_save.zMax = zMax;
	}
}
