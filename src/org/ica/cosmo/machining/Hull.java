package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.List;

import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;

import vtk.vtkHull;
import vtk.vtkNativeLibrary;
import vtk.vtkPoints;
import vtk.vtkPolyData;

/**
 * Using VTK to define hull.
 * 
 * Consider only the geometric properties. See VTKHull for display.
 * 
 * @author redonnet
 *
 */
public class Hull extends vtkHull {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	private double zMax = Double.NEGATIVE_INFINITY;
	private double deltaZ = 0.0;
	private int recursionDepth;

	public Hull(List<? extends Point3d> pointsList) {
		build(pointsList);
	}

	/**
	 * Creates a hull with a given recursion depth.
	 * 
	 * WARNING: increasing recursion depth is quickly time consuming.
	 * 
	 * @param recursionDepth
	 */
	public Hull(Surface surface, int recursionDepth) {
		if (!(surface instanceof BezierSurface)) {
			throw new IllegalArgumentException("Hull can handle only Bezier surfaces for moment");
		}
		BezierSurface sbez = (BezierSurface) surface;
		int npu = sbez.getNpu();
		int npv = sbez.getNpv();

		this.recursionDepth = recursionDepth;

		List<Point3d> pList = new ArrayList<Point3d>();
		for (int i = 0; i < npu; i++) {
			for (int j = 0; j < npv; j++) {
				pList.add(sbez.getPoint(i, j));
			}
		}
		build(pList);
	}

	public Hull(Surface surface) {
		this(surface, 1);
	}

	private void build(List<? extends Point3d> pointsList) {
		vtkPoints points = new vtkPoints();
		double zMin = Double.MAX_VALUE;
		for (Point3d p : pointsList) {
			points.InsertNextPoint(p.x, p.y, p.z);
			if (p.z > zMax)
				zMax = p.z;
			if (p.z < zMin)
				zMin = p.z;
		}
		this.deltaZ = this.zMax - zMin;
		vtkPolyData hullPolyData = new vtkPolyData();
		hullPolyData.SetPoints(points);
		SetInputData(hullPolyData);
		AddCubeFacePlanes();
		AddCubeVertexPlanes();
		AddCubeEdgePlanes();
		AddRecursiveSpherePlanes(recursionDepth);
		Update();
	}

	public double getZMax() {
		return zMax;
	}

	public double getDeltaZ() {
		return deltaZ;
	}

	public int getRecursionDepth() {
		return recursionDepth;
	}

	/**
	 * Set recursion depth for hull construction
	 * 
	 * WARNING: increasing value is quickly time consuming. Use 1 if unsure.
	 * 
	 * @param recursionDepth
	 */
	public void setRecursionDepth(int recursionDepth) {
		this.recursionDepth = recursionDepth;
	}

}
