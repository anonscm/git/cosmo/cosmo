package org.ica.cosmo.machining;

import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.ica.support.Signal;
import org.lgmt.dgl.commons.FrameSet;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.PRG_TYPE;
import org.lgmt.jcam.toolpath.ToolpathSet;

/**
 * Une classe abstraite pour manipuler l'usinage par zone.
 * 
 * <p>
 * Les trajectoires d'outil finales sont stockées dans {@link #toolpathSet
 * toolpathSet}.
 * 
 * @author redonnet
 *
 */
public abstract class AbstractZoneMilling implements IListener {
	/** le gestionnaire de listeners pour cette opération d'usinage */
	protected final EventManager emitter = new EventManager(this);

	/** Le système de repères par défaut */
	protected FrameSet fs = FrameSet.getInstance();

	/** Le contexte d'usinage {@link MillingContext MillingContext} */
	protected MillingContext mc = MillingContext.getInstance();

	/** L'outil */
	protected Cutter cutter;

	/** La hauteur de crête */
	protected double sh;

	/** Le pas longitudinal */
	protected double pasLongitudinal;

	/** La zone à usiner */
	protected Zone zone;

	/** La surface contenant la zone à usiner */
	protected Surface surface;

	/** Carte des zones sur la surface contenant la zone à usiner */
	protected SurfaceMap surfaceMap;

	/** Ensemble des trajectoires à programmer (au sens de la jCAM) */
	protected ToolpathSet3A toolpathSet;

	/** Autorise le dépassement en bord de surface */
	protected boolean overrunAllowed = true;

	/** Dépassement minimal requis en bord de zone */
	protected double minOverrun;

	/** Dépassement maximal autorisé en bord de zone */
	protected double maxOverrun;

	/** Distance d'approche en vitesse travail (en mm) */
	protected double g0Arrival = 5;

	/** Le type de point programmés pour cette zone */
	protected PRG_TYPE prgPointType = mc.getPrgType();

	/**
	 * Constructeur
	 * 
	 * @param zone la zone à usiner
	 */
	public AbstractZoneMilling(Zone zone) {
		this.cutter = mc.getCutter();
		this.sh = mc.getScallopHeight();
		this.pasLongitudinal = mc.getPasLongitudinal();
		this.zone = zone;
		this.surfaceMap = zone.getSurfaceMap();
		this.surface = surfaceMap.getSurface();
		this.toolpathSet = new ToolpathSet3A();
		this.maxOverrun = cutter.getRadius();
	}

	public Zone getZone() {
		return zone;
	}

	public boolean isOverrunAllowed() {
		return overrunAllowed;
	}

	public void setOverrunAllowed(boolean overrunAllowed) {
		this.overrunAllowed = overrunAllowed;
	}

	public double getMinOverrun() {
		return minOverrun;
	}

	public void setMinOverrun(double minOverrun) {
		this.minOverrun = minOverrun;
	}

	public double getMaxOverrun() {
		return maxOverrun;
	}

	public void setMaxOverrun(double maxOverrun) {
		this.maxOverrun = maxOverrun;
	}

	public double getG0Arrival() {
		return g0Arrival;
	}

	public void setG0Arrival(double g0Arrival) {
		this.g0Arrival = g0Arrival;
	}

	public PRG_TYPE getPrgPoint() {
		return prgPointType;
	}

	public void setPrgPoint(PRG_TYPE prgPointType) {
		this.prgPointType = prgPointType;
	}

	/**
	 * Retourne les trajectoires d'outil au format jCAM
	 * 
	 * @return un <b>ToolpathSet</b> compatible jCAM
	 */
	public ToolpathSet getToolpathSet() {
		return toolpathSet;
	}

	/**
	 * Lance la planification de trajectoires.
	 * 
	 * L'implémentation dépend de la stratégie utilisée.
	 * 
	 * @throws SodException
	 */
	public abstract void run() throws SodException;

	public EventManager getEmitter() {
		return emitter;
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	/**
	 * Propagate received signals when their propagation attribute is true
	 */
	@Override
	public void onReceivedSignal(Signal signal) {
		if (signal.getPropagation() == true)
			this.emitter.sendSignal(signal);
	}

}
