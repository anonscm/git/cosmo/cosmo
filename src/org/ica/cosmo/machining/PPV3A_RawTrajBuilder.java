package org.ica.cosmo.machining;

import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.util.ToolpathSignal;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.ica.support.IObservable;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.toolpath.Toolpath;

/**
 * Calcul des trajectoires outil brutes sur une zone.
 * 
 * <p>
 * Les trajectoires sont calculées dans des plans parallèles à YZ (X = cste)
 * 
 * <p>
 * Chaque trajectoire d'outil est d'abord calculée par l'intersection entre son
 * plan d'usinage et toutes les isoparamétriques en u et en v définies sur la
 * zone. Les points d'interpolation sont ensuite redistribués le long de la
 * trajectoire avec le pas longitudinal défini dans
 * {@link org.lgmt.jcam.strategy.MillingContext}.
 * 
 * @author redonnet
 *
 */
public class PPV3A_RawTrajBuilder extends AbstractRawTrajBuilder implements IObservable {
	private static Logger logger = LogManager.getLogManager().getLogger("org.ica.cosmo");

	private APPROX_LEVEL approxLevel;

	/** liste des mailles de la zone */
	private ArrayList<UVMeshUnit> meshList;

	/** surface dans le repère de calcul */
	private Surface surfX;

	/** matrice de passage du repère global au repère de calcul */
	private Matrix4d mGX;

	/** matrice de passage du repère de calcul au repère global */
	private Matrix4d mXG;
	
	/**
	 * Une trajectoire brute
	 * 
	 * Ne tient pas compte des concavités.
	 */
	private ArrayList<SurfacePoint> rawTrajX;

	/**
	 * Les trajectoires brutes
	 * 
	 * Tient compte des concavités. Ne contient aucune connexion en fin de
	 * trajectoire.
	 */
	private Map<Double, List<Toolpath<SurfacePoint>>> rawToolpathX;

	/** les points extrêmes de la zone, selon l'axe Y */
	private SurfacePoint pYmin, pYmax;

	/** la marge dans la direction transverse utilisée en début d'usinage */
	private double margeMin;

	/** la marge dans la direction transverse utilisée en fin d'usinage */
	private double margeMax;

	/** liste des isoparametriques de la zone dans le repère de travail */
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoU;
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoV;

	/** max number of delta in UVMeshUnit indexes to establish connexity */
	private int maxConnectUnits;

	/**
	 * Constructeur.
	 * 
	 * @param zoneIsoUV
	 * @param mGX
	 * @param vfAngle
	 */
	public PPV3A_RawTrajBuilder(Zone z, Matrix4d mGX, APPROX_LEVEL accuracy) {
		super(z);
		this.approxLevel = accuracy;
		if (this.approxLevel == APPROX_LEVEL.COARSE)
			System.err.println(
					"Warning Approx level is set to COARSE. Resulting toolpaths may not be suitable for machining");
		this.mGX = mGX;
		
		
		rawToolpathX = new TreeMap<Double, List<Toolpath<SurfacePoint>>>();

		this.surfX = zone.getSurfaceMap().getSurface();

		// Calcul des isos de la zone
		listIsoU = zone.getListIsoU();
		listIsoV = zone.getListIsoV();

		/*
		 * On calcule l'écart maxi en nombre d'UVMeshUnit pour pouvoir déclarer deux
		 * points connexes
		 */
		maxConnectUnits = (int) Math.floor(cutter.getRadius() / approxLevel.getValeur());
		maxConnectUnits = maxConnectUnits + 1;

		/*
		 * On récupère la liste des mailles de la zone
		 */
		meshList = new ArrayList<UVMeshUnit>(zone.getMeshmap().values());

		/*
		 * On calcule les points extrêmes de la zone en Y
		 */
		pYmin = meshList.get(0).getCenter();
		pYmax = meshList.get(0).getCenter();
		for (UVMeshUnit m : meshList) {
			for (SurfacePoint p : m.getCorners()) {
				if (p.y < pYmin.y)
					pYmin = p;
				if (p.y > pYmax.y)
					pYmax = p;
			}
		}

		/*
		 * On définit les marges
		 */
		this.margeMin = calcMarge(pYmin);
		this.margeMax = calcMarge(pYmax);
	}

	public PPV3A_RawTrajBuilder(Zone z, APPROX_LEVEL accuracy) {
		this(z,new Matrix4d(),accuracy);
	}
	
	public PPV3A_RawTrajBuilder(Zone z) {
		this(z, APPROX_LEVEL.FINE);
	}

	/**
	 * Lance la simulation d'usinage
	 */
	@Override
	public void run() throws SodException {
		double sod = margeMin;

		double yPlan = pYmin.y + sod;
		while (yPlan <= pYmax.y - margeMax) {

			boolean newCurve = calcPlane(yPlan);

			if (newCurve) {
				sod = calcSOD(rawTrajX);
			}
			yPlan = yPlan + sod;
			if (sod < 0.1 * sh)
				throw new SodException(new String("" + sod));
		}

	}

	/**
	 * Calcule le pas transversal pour une trajectoire outil. La direction d'usinage
	 * est selon X (plans a y=cste)
	 * 
	 * @param pList
	 * @return la valeur du pas transversal
	 */
	private double calcSOD(ArrayList<SurfacePoint> pList) {
		double sod = cutter.getRadius(); // le pas transversal à calculer
		double reff; // le rayon effectif
		double d; // la distance entre les centreReff

		double sod_test;
		for (SurfacePoint p : pList) {
			reff = calcReffAt(p);
			// dans un premier temps, on néglige la courbure de la surface
			// pour calculer la distance entre les deux centres Reff
			d = 2 * sqrt(2 * reff * sh - sh * sh);
			double u = p.u;
			double v = p.v;
			Vector3d n = surfX.normal(u, v);

			// Dans le plan vertical, on a d incliné et sod horizontal et l'inclinaison est
			// donnée par la projection de n (n.x,n.z) dans ce plan
			sod_test = d * n.z * n.z / sqrt((n.y * n.y + n.z * n.z));

			sod = sod_test < sod ? sod_test : sod;
		}
		return sod;
	}

	/**
	 * Calcule du rayon effectif en un point donné
	 * 
	 * NOTA : La direction d'usinage est toujours selon X (y=cste).
	 * 
	 * @param p
	 * @return
	 */
	private double calcReffAt(SurfacePoint p) {
		double u = p.u;
		double v = p.v;

		Vector3d n = surfX.normal(u, v);

		// Pente maxi
		double S = abs(Util.angle0(n, new Vector3d(0.0, 0.0, 1.0)));

		double R = cutter.getRadius();
		double r = cutter.getTipRadius();

		// direction de plus grande pente dans le plan XY (ramené sur [0,Pi] car étant
		// donné le changement de repère, la direction d'usinage est toujours selon Y

		Vector2d maxSlopeDirection = new Vector2d(n.x, n.y);
		if (n.x < 0)
			maxSlopeDirection.scale(-1.0);

		// alpha est l'angle entre la direction de plus grande pente et la
		// direction d'usinage.
		// La direction d'usinage est toujours (1.0, 0.0)
		double alpha = maxSlopeDirection.angle(new Vector2d(1.0, 0.0));
		double reff = ((R - r) * Math.pow(Math.cos(alpha), 2))
				/ (Math.sin(S) * (1 - Math.pow(Math.sin(S), 2) * Math.pow(Math.sin(alpha), 2))) + r;

		return reff;
	}

	/**
	 * Calcul de la trajectoire sur un plan y=cste (plan d'usinage)
	 * 
	 * @param yPlan
	 * @return true si ne trajectoire (deux points au moins) a pu être calculée,
	 *         false dans le cas contraire
	 */
	private boolean calcPlane(double yPlan) {
		this.rawTrajX = new ArrayList<SurfacePoint>();

		for (ArrayList<SurfacePointsCurve> list : listIsoU.values()) {
			for (SurfacePointsCurve pc : list) {
				rawTrajX.addAll(calcAllIsoInt(pc, yPlan, Surface.Param.U));
			}
		}

		for (ArrayList<SurfacePointsCurve> list : listIsoV.values()) {
			for (SurfacePointsCurve pc : list) {
				rawTrajX.addAll(calcAllIsoInt(pc, yPlan, Surface.Param.V));
			}
		}

		// Si on a au moins 2 points d'intersection, on crée une nouvelle courbe
		if (rawTrajX.size() > 1) {
			Collections.sort(rawTrajX, new SurfacePointComparator());

			List<Toolpath<SurfacePoint>> tpList = buildTraj(rawTrajX);

			if (tpList != null)
				rawToolpathX.put(yPlan, tpList);

			rawToolpathX.put(yPlan, tpList);

//			this.pcs.firePropertyChange("newCurve", null, tpList);
			for(Toolpath<SurfacePoint> tp: tpList)
				this.emitter.sendSignal(new ToolpathSignal("AddRawToolpath", tp, zone.getId()));

			return true;
		}
		return false;
	}

	/**
	 * Calcule tous les points d'intersection entre un plan et une isoparamétrique.
	 * Utilise une isoparamétrique approximée pour déterminer le point de départ de
	 * la résolution.
	 * 
	 * @param pc    : une courbe isoparamétrique approximée
	 * @param yPlan
	 * @param param
	 * 
	 * @return un ArrayList<SurfacePoint> contenant tous les points d'intersection
	 *         entre un plan et une isoparamétrique
	 */
	private ArrayList<SurfacePoint> calcAllIsoInt(SurfacePointsCurve pc, double yPlan, Surface.Param param) {
		ArrayList<SurfacePoint> result = new ArrayList<SurfacePoint>();
		for (int i = 0; i < pc.getNbPoints() - 1; i++) {
			// on cherche les points de l'iso qui encadrent yPlan
			SurfacePoint pInf = null;
			SurfacePoint pSup = null;
			if ((yPlan > pc.getPoint(i).y && yPlan < pc.getPoint(i + 1).y)) { // intersection "montante"
				pInf = pc.getPoint(i);
				pSup = pc.getPoint(i + 1);
			}
			if ((yPlan < pc.getPoint(i).y && yPlan > pc.getPoint(i + 1).y)) { // intersection "descendante"
				pInf = pc.getPoint(i + 1);
				pSup = pc.getPoint(i);
			}
			if (pInf == null) // pas d'intersection
				continue;

			// On calcule l'intersection.
			// Étant donné les faibles courbures, une simple interpolation linéaire est
			// suffisante.
			SurfacePoint p0 = new SurfacePoint();
			p0.setSurface(surfX);
			p0.y = yPlan;
			double k = (p0.y - pInf.y) / (pSup.y - pInf.y);
			p0.x = (pSup.x - pInf.x) * k + pInf.x;
			p0.z = (pSup.z - pInf.z) * k + pInf.z;
			p0.u = (pSup.u - pInf.u) * k + pInf.u;
			p0.v = (pSup.v - pInf.v) * k + pInf.v;

			// On teste si l'interpolation linéaire est suffisante et on corrige si
			// nécessaire
			SurfacePoint pTest = new SurfacePoint(surfX, p0.u, p0.v);
			if (abs(pTest.y - yPlan) > Util.PREC3) {
				double err = abs(pTest.y - yPlan);
				Vector3d n = surfX.normal(p0.u, p0.v);
				n.y = 0;
				pTest = surfX.getPointProjection(p0, p0.u, p0.v, n);
				if (pTest != null) { // projection mail fail on surface border
					if (zone.includes(pTest.u, pTest.v)) { // projection may be outside the zone
						p0 = pTest;
					}
				} else {
					//FIXME
					logger.finest(String.format("Isoparametric intersection may be too approximative : error is %.3f microns (unable to correct)", err*1000.0));
				}
			}

			if (p0 != null) {
				result.add(p0);
			}
		}
		return result;
	}

	private double calcMarge(SurfacePoint p) {
		double marge;
		double d2; // moitié de la distance entre les centreReff
		double r = cutter.getTipRadius();

		/*
		 * gamma est l'angle entre la direction des centreReff et l'horizontale. Dans la
		 * mesure où on usine dans un plan y=cste, cet angle correspond à la projection
		 * de la normale dans ce plan, c'est à dire à atan(n.y/n.z)
		 */
		double gamma;
		// on néglige la courbure de la surface
		// on se place dans le cas le plus défavorable, avec Reff = r
		d2 = sqrt(2 * r * sh - sh * sh);
		double u = p.u;
		double v = p.v;
		Vector3d n = surfX.normal(u, v);
		gamma = atan(n.y / (sqrt(n.x * n.x + n.z * n.z)));
		marge = d2 * cos(gamma);
		return marge;
	}

	private List<Toolpath<SurfacePoint>> buildTraj(ArrayList<SurfacePoint> pList) {
		List<Toolpath<SurfacePoint>> tpList = new ArrayList<Toolpath<SurfacePoint>>();
		ListIterator<SurfacePoint> pListIt = pList.listIterator();
		SurfacePoint p0, p1;
		Toolpath<SurfacePoint> tp = null;
		while (pListIt.hasNext()) {
			p0 = pListIt.next();
			if (pListIt.hasNext()) {
				// il reste au moins deux points
				p1 = pListIt.next();
				// on essaie de démarrer une trajectoire
				while (startTp(p0, p1) == null) {
					p0 = p1;
					if (pListIt.hasNext())
						p1 = pListIt.next();
					else
						break;
				}
				tp = startTp(p0, p1);
				if (tp != null) { // traj démarrée => poursuite
					while (pListIt.hasNext()) {
						p0 = p1;
						p1 = pListIt.next();
						if (isNextPointConnex(p0, p1))
							tp.append(new SurfacePoint(p1));
						else
							break;
					}
					makeLenghtwiseToolpath(tp);
					tpList.add(tp);
					tp = null;
					pListIt.previous();
				}
			}
		}
		if (tpList.size() > 0)
			return tpList;
		return null;
	}

	private Toolpath<SurfacePoint> startTp(SurfacePoint p0, SurfacePoint p1) {
		if (!isNextPointConnex(p0, p1))
			return null;

		return new Toolpath<SurfacePoint>(new SurfacePoint(p0), new SurfacePoint(p1));
	}

	private boolean isNextPointConnex(SurfacePoint current, SurfacePoint next) {
		UVMeshUnit uvm0 = getMeshUnit(current.u, current.v);
		UVMeshUnit uvm1 = getMeshUnit(next.u, next.v);

		if (uvm0.getZone() == uvm1.getZone() && uvm0.isConnexTo(uvm1, this.maxConnectUnits)) {
			return true;
		}
		return false;
	}

	private void makeLenghtwise2PointsToolpath(Toolpath<SurfacePoint> tp) {
		double d = tp.getFirst().distance(tp.getLast());
		int nbInserts = (int) Math.floor(d / this.lengthwiseStep);
		for (int i = 1; i <= nbInserts; i++) {
			double k = 1.0 / ((double) (nbInserts+1));
			SurfacePoint middlePoint = getInterpolatedPoint(tp.getFirst(), tp.getLast(), i * k);
			tp.insert(i, middlePoint);
		}
		return;
	}

	private void redistributeLastPoints(Toolpath<SurfacePoint> tp, int n) {
		if (n < 2 || n > tp.getNbPoints()) {
			System.err.println("Cannot redistribute " + n + " points on a " + tp.getNbPoints() + "-long toolpath");
			return;
		}
		int baseI = tp.getNbPoints() - n;
		SurfacePoint baseP = tp.get(baseI);
		SurfacePoint lastP = tp.get(tp.getNbPoints() - 1);
		int i = baseI + 1;
		while (i < tp.getNbPoints() - 1) { // getNbPoints() is reduced by 1 at each iteration
			tp.remove(i);
		}
		i = baseI + 1;
		double d = baseP.distance(lastP);
		if (d > this.lengthwiseStep) {
			int nbInserts = (int) Math.floor(d / this.lengthwiseStep);
			for (int j = 1; j <= nbInserts; j++) {
				double k = 1.0 / ((double) (nbInserts + 1));
				SurfacePoint middlePoint = getInterpolatedPoint(baseP, lastP, j * k);
				tp.insert(baseI + j, middlePoint);
			}
		}
	}

	private void makeLenghtwiseToolpath(Toolpath<SurfacePoint> tp) {
//		ObservableToolpath otp = new ObservableToolpath(tp);
//		emitter.sendSignal(new ObservableToolpathSignal("NewOTP", otp, zone.getId()));

		if (tp.size() == 2) {
			this.makeLenghtwise2PointsToolpath(tp);
			return;
		}
		if (tp.size() == 3) {
			tp.remove(1);
			this.makeLenghtwise2PointsToolpath(tp);
			return;
		}

//		if (tp.getNbPoints() < 4) {
//			this.redistributeLastPoints(tp, tp.size());
//		}
		SurfacePoint baseP;
		SurfacePoint nextP;
		int i = 0;

		while (i < tp.getNbPoints()) {
			baseP = tp.get(i);
			nextP = getLenghtwisePoint(tp, i);
			if (nextP == null)
				break;
			double actualStep = baseP.distance(nextP);

			while (tp.get(i + 1).distance(baseP) < actualStep) {
				tp.remove(i + 1);
			}
			i++;
			tp.insert(i, nextP);
			baseP = nextP;
		}

		/* Endlines management */
		int n = 2;
		i = tp.getNbPoints()-3;
		SurfacePoint lastP = tp.getLast();
		while (tp.get(i).distance(lastP) < 2 * this.lengthwiseStep) {
				i--;
				if(i<0)
					break;
				n++;
			}
		this.redistributeLastPoints(tp, n);

		// Uncomment to investigate chord errors
//		int counter = 0;
//		for (int j = 0; j < tp.getNbPoints() - 2; j++) {
//			SurfacePoint p0 = tp.get(j);
//			SurfacePoint p1 = tp.get(j + 1);
//			double chordError = this.surfX.evalChordError(p0, p1);
//			if(chordError > 2*Util.PREC3) {
//				//System.out.println("chord error = "+chordError);
//				counter++;
//			}
//			double percent = (double)counter / (double)tp.getNbPoints() * 100.0;
//			System.out.printf("errors : %.2f %% \n", percent);
//		}
	}

	private SurfacePoint getLenghtwisePoint(Toolpath<SurfacePoint> tp, int i) {
		int lastI = i;
		double supDist = 0.0;
		double infDist = 0.0;
		SurfacePoint lastP = tp.get(i);
		SurfacePoint nextP = tp.get(i + 1);
		while (supDist < this.lengthwiseStep) {
			infDist = supDist;
			lastP = tp.get(lastI);
			nextP = tp.get(lastI + 1);
			double d = lastP.distance(nextP);
			supDist = supDist + d;
			lastI++;
			if (lastI == tp.size() - 1)
				return null;
		}
		double k = (this.lengthwiseStep - infDist) / (supDist - infDist);
		SurfacePoint newPt = getInterpolatedPoint(lastP, nextP, k);

		return newPt;
	}

	/* Return an interpolated point belonging to the traj (same y) */
	private SurfacePoint getInterpolatedPoint(SurfacePoint p0, SurfacePoint p1, double k) {
		Point3d interP = new Point3d(p0);
		Vector3d vec = new Vector3d(p0, p1);
		vec.scale(k);
		interP.add(vec);

		double u0 = (p1.u - p0.u) * k + p0.u;
		double v0 = (p1.v - p0.v) * k + p0.v;

		Vector3d n = surfX.normal(u0, v0);
		n.x = 0.0;
		SurfacePoint newPt = surfX.getPointProjection(interP, u0, v0, n);
		if (newPt != null)
			return newPt;
		else
			/* fallback to rough approximation if point projection fails */
			return new SurfacePoint(surfX, u0, v0);
	}

	private UVMeshUnit getMeshUnit(double u, double v) {
		for (UVMeshUnit meshUnit : meshList) {
			if (meshUnit.includes(u, v))
				return meshUnit;
		}
		return null;
	}

	public Map<Double, List<Toolpath<SurfacePoint>>> getRawToolpathX() {
		return rawToolpathX;
	}

	public TreeMap<Double, List<Toolpath<SurfacePoint>>> getRawToolpath() {
		TreeMap<Double, List<Toolpath<SurfacePoint>>> rawToolpath = new TreeMap<Double, List<Toolpath<SurfacePoint>>>();
		for (Map.Entry<Double, List<Toolpath<SurfacePoint>>> e : rawToolpathX.entrySet()) {
			Double k = e.getKey();
			List<Toolpath<SurfacePoint>> v = e.getValue();
			ArrayList<Toolpath<SurfacePoint>> tpList = new ArrayList<Toolpath<SurfacePoint>>();
			for (Toolpath<SurfacePoint> tp : v) {
				tpList.add(tp);
			}
			rawToolpath.put(k, tpList);
		}
		return rawToolpath;
	}

	public double getRawToolpathLength() {
		double result = 0.0;
		for (Map.Entry<Double, List<Toolpath<SurfacePoint>>> e : rawToolpathX.entrySet()) {
			for (Toolpath<SurfacePoint> tp : e.getValue()) {
				result = result + tp.length();
			}
		}
		return result;
	}

	public Surface getSurfX() {
		return surfX;
	}

	public ArrayList<UVMeshUnit> getMeshList() {
		return meshList;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getIsoUx() {
		return listIsoU;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getIsoVx() {
		return listIsoV;
	}

	public Matrix4d getmGX() {
		return mGX;
	}

	public Matrix4d getmXG() {
		return mXG;
	}

	@Override
	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	@Override
	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	@Override
	public EventManager getEventManager() {
		return emitter;
	}

	/**
	 * Comparateur de SurfacePoint pour trier les points selon leur coordonnée x
	 */
	private class SurfacePointComparator implements Comparator<SurfacePoint> {
		public SurfacePointComparator() {
		}

		public int compare(SurfacePoint p0, SurfacePoint p1) {
			int result = Double.compare(p0.x, p1.x);

			return result;
		}
	}
}
