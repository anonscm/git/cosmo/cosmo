package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import org.ica.cosmo.machining.Connector.LOCDATA;
import org.ica.cosmo.util.ToolpathSignal;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ConnectStrategyProximity extends ConnectStrategy {
	private static double defaultSafetyFactor = 20;

	public ConnectStrategyProximity(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath, double overrun) {
		super(prgToolpath, overrun);
	}

	public ConnectStrategyProximity(TreeMap<Double, ArrayList<Toolpath<Point3d>>> prgToolpath) {
		super(prgToolpath);
	}

	@Override
	protected void run() {
		ArrayList<ToolpathSequence> tpsList = new ArrayList<ToolpathSequence>();
		ToolpathSequence tps = new ToolpathSequence();

		while (!prgToolpath.isEmpty()) {
			Double k = prgToolpath.firstKey();

			ArrayList<Toolpath<Point3d>> tpListK = prgToolpath.get(k);
			double length = 0.0;
			int longest = 0;
			for (int j=0 ; j<tpListK.size() ; j++) {
				Toolpath<Point3d> tp = tpListK.get(j);
				if(tp.length()>length)
					longest = j;
			}
			Toolpath<Point3d> tp = tpListK.get(longest);
			tps.add(tp);
			this.emitter.sendSignal(new ToolpathSignal("AddToolpath", tp));
			prgToolpath.get(k).remove(0);
			if (prgToolpath.get(k).size() == 0) {
				prgToolpath.remove(k);
			}
			Double lastK = k; // last k in the tps
			Double firstK = k; // first k in the tps
			Double nextK = prgToolpath.higherKey(lastK);
			Double prevK = prgToolpath.lowerKey(firstK);

			while (true) {
				ArrayList<Connector> candidates = new ArrayList<Connector>();
//				if (prgToolpath.get(firstK) != null && firstK != lastK) // at the begining firstK == lastK
//					for (Toolpath<Point3d> tpTest : prgToolpath.get(firstK)) {
//						Connector c = new Connector(tps, tpTest, LOCDATA.FIRST);
//						if(c.getConnectorToolpath() != null)
//							candidates.add(c);
//					}
//				if (prgToolpath.get(lastK) != null)
//					for (Toolpath<Point3d> tpTest : prgToolpath.get(lastK)) {
//						Connector c = new Connector(tps, tpTest, LOCDATA.LAST);
//						if(c.getConnectorToolpath() != null)
//							candidates.add(c);
//					}
				if (prevK != null)
					for (Toolpath<Point3d> tpTest : prgToolpath.get(prevK)) {
						Connector c = new Connector(tps, tpTest, LOCDATA.PREV);
						if(c.getConnectorToolpath() != null)
							candidates.add(c);
					}
				if (nextK != null)
					for (Toolpath<Point3d> tpTest : prgToolpath.get(nextK)) {
						Connector c = new Connector(tps, tpTest, LOCDATA.NEXT);
						if(c.getConnectorToolpath() != null)
							candidates.add(c);
					}

				if (candidates.size() == 0) { // no more candidates. A new tps must be started
					tpsList.add(tps);
					tps = new ToolpathSequence();
					break;
				}
				Connector winnerC = Collections.min(candidates);

				if (winnerC.getMode() == Connector.MODE.APPEND) {
					tps.addLast(winnerC.getConnectorToolpath());
					tps.addLast(winnerC.getTp1());
				}
				if (winnerC.getMode() == Connector.MODE.PREPEND) {
					tps.addFirst(winnerC.getConnectorToolpath());
					tps.addFirst(winnerC.getTp1());
				}
				this.emitter.sendSignal(new ToolpathSignal("AddToolpath", winnerC.getTp1()));
				this.emitter.sendSignal(new ToolpathSignal("AddToolpath", winnerC.getConnectorToolpath()));

				switch (winnerC.getLocData()) {
				case PREV:
					k = prevK;
					firstK = k;
					prevK = prgToolpath.lowerKey(k);
					break;
				case FIRST:
					k = firstK;
					prevK = prgToolpath.lowerKey(k);
					break;
				case LAST:
					k = lastK;
					nextK = prgToolpath.higherKey(k);
					break;
				case NEXT:
					k = nextK;
					lastK = k;
					nextK = prgToolpath.higherKey(k);
					break;
				default:
				}
				prgToolpath.get(k).remove(winnerC.getTp1());
				if (prgToolpath.get(k).size() == 0)
					prgToolpath.remove(k);
			}
		}
		
		for(ToolpathSequence tpsFinal: tpsList)
			this.tpList.add(tpsFinal.convert2Toolpath());
	}

	public static double getDefaultSafetyFactor() {
		return defaultSafetyFactor;
	}

	public static void setDefaultSafetyFactor(double defaultSafetyFactor) {
		ConnectStrategyProximity.defaultSafetyFactor = defaultSafetyFactor;
	}

}
