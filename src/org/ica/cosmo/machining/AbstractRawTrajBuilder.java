package org.ica.cosmo.machining;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.ica.cosmo.zoning.Zone;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.commons.FrameSet;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;

/**
 * Une classe abstraite pour manipuler l'usinage par zone.
 * 
 * <p>
 * Les trajectoires d'outil sont stockées dans {@link #trajPoints trajPoints }
 * sous forme de listes de points. {@link #toolpathSet toolpathSet } permet de
 * les récupérer sous forme de trajectores au sens de la jCAM.
 * 
 * @author redonnet
 *
 */
public abstract class AbstractRawTrajBuilder {
	/** le gestionnaire de listeners pour cette opération d'usinage */
	protected final EventManager emitter = new EventManager(this);
	/** Le système de repères par défaut */
	protected FrameSet fs = FrameSet.getInstance();
	/** Le contexte d'usinage {@link MillingContext MillingContext} */
	protected MillingContext mc = MillingContext.getInstance();
	/** L'outil */
	protected Cutter cutter;
	/** La hauteur de crête */
	protected double sh;
	/** La zone à usiner dans le repère global */
	protected Zone zone;
	/** Le pas longitudinal à utiliser */
	protected double lengthwiseStep;
	
	/**
	 * Constructeur
	 * 
	 * @param zone la zone à usiner
	 */
	public AbstractRawTrajBuilder(Zone zone) {
		this.zone = zone;
		this.cutter = mc.getCutter();
		this.sh = mc.getScallopHeight();
		this.lengthwiseStep = mc.getPasLongitudinal();
	}

	/**
	 * Lance la planification de trajectoires.
	 * 
	 * L'implémentation dépend de la stratégie utilisée.
	 * 
	 * @throws SodException
	 */
	public abstract void run() throws SodException;
	
	public EventManager getEmitter() {
		return emitter;
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

}
