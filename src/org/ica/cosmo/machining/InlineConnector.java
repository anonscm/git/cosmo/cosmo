package org.ica.cosmo.machining;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;


/**
 * Connector between toolpaths in the same parallel plane
 */
public class InlineConnector extends GenericConnector {

	public InlineConnector() {
		// TODO Auto-generated constructor stub
	}

	public InlineConnector(BezierCurve curve, INTERPOLATION_TYPE iType, int tessellation) {
		super(curve, iType, tessellation);
	}

	public InlineConnector(BezierCurve curve, INTERPOLATION_TYPE iType) {
		super(curve, iType);
	}

	public InlineConnector(BezierCurve curve, int tessellation) {
		super(curve, tessellation);
	}

	public InlineConnector(BezierCurve curve) {
		super(curve);
	}

}
