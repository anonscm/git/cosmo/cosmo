package org.ica.cosmo.machining;

import java.util.ArrayList;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkNativeLibrary;
import vtk.vtkOBBTree;
import vtk.vtkPoints;

public class OffHullConnector extends GenericConnector {
	/**
	 * As OffHullConnector relies on VTK to handle convex hull, the VTK librairies are
	 * loaded in case no VTK-related work (i.e. visualization) is started yet
	 */
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	
	private static Hull hull = null;
	private static double zMax = Double.NEGATIVE_INFINITY;
	
	public static void setHull(Hull hull) {
		OffHullConnector.hull = hull;
		OffHullConnector.zMax = hull.getZMax();
	}

	public static boolean isHullDefined() {
		if(hull != null)
			return true;
		return false;
	}
	
	public OffHullConnector(Point3d startP, Point3d endP, INTERPOLATION_TYPE iType) {
		
		if(OffHullConnector.hull == null)
			System.err.println("Hull not set. Cannot define off-hull connector");
		
		double h = hull.getDeltaZ() / 10.0;
		double d = startP.distance(endP) / 10.0;
		vtkOBBTree tree = new vtkOBBTree();
		tree.SetDataSet(hull.GetOutput());
		tree.BuildLocator();
		boolean startOut = false, endOut = false;

		ArrayList<Point3d> bezierPoints = new ArrayList<Point3d>();
		double[] pStart = { startP.x, startP.y, startP.z };
		double[] pStartMax = { startP.x, startP.y, zMax };
		double[] pEnd = { endP.x, endP.y, endP.z };
		double[] pEndMax = { endP.x, endP.y, zMax };
		vtkPoints intersectPoints = new vtkPoints();
		tree.IntersectWithLine(pStart, pStartMax, intersectPoints, null);
		Point3d pt0 = null, pt3 = null;
		if (intersectPoints.GetNumberOfPoints() > 0) {
			pt0 = new Point3d(intersectPoints.GetPoint(0));
		} else {// point may be outside the hull
			startOut = true;
			pt0 = new Point3d(startP);
		}
		tree.IntersectWithLine(pEnd, pEndMax, intersectPoints, null);
		if (intersectPoints.GetNumberOfPoints() > 0) {
			pt3 = new Point3d(intersectPoints.GetPoint(0));
		} else { // point may be outside the hull
			endOut = true;
			pt3 = new Point3d(endP);
		}

		Point3d pt1 = new Point3d(pt0);
		pt1.z = pt1.z + h + d;
		Point3d pt2 = new Point3d(pt3);
		pt2.z = pt2.z + h + d;
		bezierPoints.add(pt0);
		bezierPoints.add(pt1);
		bezierPoints.add(pt2);
		bezierPoints.add(pt3);
		curve = new BezierCurve(bezierPoints);

		toolpath = new Toolpath<Point3d>(curve, tessellation);
		if (!startOut)
			toolpath.prepend(startP);
		if (!endOut)
			toolpath.append(endP);
		toolpath.setICode(this.iType);
		this.length = toolpath.length();
	}
	
	public OffHullConnector(Point3d startP, Point3d endP) {
		this(startP, endP, INTERPOLATION_TYPE.G0);
	}
	
}
