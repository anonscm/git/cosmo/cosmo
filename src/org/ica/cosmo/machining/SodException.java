package org.ica.cosmo.machining;

public class SodException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6317795518969541174L;

	public SodException() {
	    super();
	  }

	  public SodException(String s) {
	    super(s);
	  }
}
