package org.ica.cosmo.machining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

public class ConnectorFactory {
	private static double r = MillingContext.getInstance().getCutter().getRadius();
	private static double closeLimitFactor = 1.0;
	private static double closeLimit = closeLimitFactor * r;
	private static double offHullLimitFactor = 1.0;
	private static double offHullLimit = offHullLimitFactor * r;

	public static void setCloseLimitFactor(double closeLimitFactor) {
		ConnectorFactory.closeLimitFactor = closeLimitFactor;
		ConnectorFactory.closeLimit = closeLimitFactor * r;
	}

	public static void setOffHullLimitFactor(double offHullLimitFactor) {
		ConnectorFactory.offHullLimitFactor = offHullLimitFactor;
		ConnectorFactory.offHullLimit = offHullLimitFactor * r;
	}

	public enum MODE {
		APPEND, PREPEND
	};
	
	public enum LOCDATA { // used to localize the connector in relation to already connected toolpaths (tps)
		PREV, FIRST, LAST, NEXT
	};

	public ConnectorFactory() {
	}

	/**
	 * Creates a connector between tp0 and tp1.
	 * 
	 * The connector will join the last point of tp0 with the first point of tp1
	 * 
	 * @param tp0
	 * @param tp1
	 * @return
	 */
	private GenericConnector genericConnector(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1, INTERPOLATION_TYPE iType,
			double height, double scale) {
		Point3d p0 = tp0.getLast();
		Point3d p5 = tp1.getFirst();
		Point3d prevP = tp0.get(tp0.size() - 2);
		Point3d nextP = tp1.get(1);
		double distance = p0.distance(p5);

		Vector3d dir0 = new Vector3d(prevP, p0);
		dir0.normalize();
		dir0.scale(scale);

		Point3d p1 = Points.translateClone(p0, dir0);
		Point3d p2 = new Point3d(p1);
		p2.z = p2.z + height;

		Vector3d dir1 = new Vector3d(nextP, p5);
		dir1.normalize();
		dir1.scale(scale);

		Point3d p4 = Points.translateClone(p5, dir1);
		Point3d p3 = new Point3d(p4);
		p3.z = p3.z + height;

		BezierCurve c = new BezierCurve(new Point3d[] { p0, p1, p2, p3, p4, p5 });

		/*
		 * Estimating the tessellation we need. Based on a rough estimation of the
		 * length of the Bézier curve.
		 */
		double l1 = c.polygonLength();
		int tess = (int) Math.floor((l1 + distance) / 1.5);
		if (tess < 6)
			tess = 6;

		return new GenericConnector(c, INTERPOLATION_TYPE.G1, tess);
	}

	/**
	 * Creates a connector between tp0 and tp1.
	 * 
	 * The connector will join the last point of tp0 with the first point of tp1
	 * 
	 * @param tp0
	 * @param tp1
	 * @return
	 */
	public GenericConnector makeInlineConnector(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		double distance = tp0.getLast().distance(tp1.getFirst());

		return genericConnector(tp0, tp1, INTERPOLATION_TYPE.G1, r, 0.25 * distance);
	}

	/**
	 * Creates a connector between tp0 and tp1.
	 * 
	 * The connector will join the last point of tp0 with the first point of tp1
	 * 
	 * @param tp0
	 * @param tp1
	 * @return
	 */
	public GenericConnector makeEndConnector(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		Point3d startP = tp0.getLast();
		Point3d endP = tp1.getFirst();
		double distance = startP.distance(endP);

		if (distance < closeLimit) {// MODE classic end points
			return genericConnector(tp0, tp1, INTERPOLATION_TYPE.G1, r, r);
		} else {// MODE SAFE
			if (distance < offHullLimit) {
				return genericConnector(tp0, tp1, INTERPOLATION_TYPE.G0, r, r);
			} else {
//				if(OffHullConnector.isHullDefined())
//					return new OffHullConnector(startP, endP);
//				else
				return null;
			}
		}
	}	
	
	/**
	 * Calcule le connecteur le plus cours
	 * 
	 * @param tp0
	 * @param tp1
	 * 
	 * @return
	 */
	public GenericConnector makeShortestConnector(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		Toolpath<Point3d> tpTest1 = new Toolpath<Point3d>(tp1);
		tpTest1.reverse();

		List<GenericConnector> candidates = new ArrayList<GenericConnector>();

		GenericConnector gc1 = genericConnector(tp0, tp1, INTERPOLATION_TYPE.G0, r, r);
		if (gc1 != null)
			candidates.add(gc1);

		GenericConnector gc2 = genericConnector(tp0, tpTest1, INTERPOLATION_TYPE.G0, r, r);
		if (gc2 != null) {
			gc2.setEndConn(TERM_TYPE.END);
			candidates.add(gc2);
		}

		Toolpath<Point3d> tpTest0 = new Toolpath<Point3d>(tp0);
		tpTest0.reverse();

		GenericConnector gc3 = genericConnector(tpTest0, tp1, INTERPOLATION_TYPE.G0, r, r);
		if (gc3 != null) {
			gc3.setStartConn(TERM_TYPE.START);
			candidates.add(gc3);
		}

		GenericConnector gc4 = genericConnector(tpTest0, tpTest1, INTERPOLATION_TYPE.G0, r, r);
		if (gc4 != null) {
			gc4.setStartConn(TERM_TYPE.START);
			gc4.setEndConn(TERM_TYPE.END);
			candidates.add(gc4);
		}

		if (candidates.size() > 0)
			return Collections.min(candidates);

		return null;
	}

	/**
	 * Connecting single tp with an existing tps, the tps cannot be reversed.
	 * 
	 * @param tps
	 * @param tp1
	 * @return
	 */
	public GenericConnector makeShortestConnector(ToolpathSequence tps, Toolpath<Point3d> tp1) {
		Toolpath<Point3d> tp0;
		Toolpath<Point3d> tp1rev = new Toolpath<Point3d>(tp1);
		tp1rev.reverse();
		
		List<GenericConnector> candidates = new ArrayList<GenericConnector>();

		tp0 = tps.getLast();
		GenericConnector gc1 = genericConnector(tp0, tp1, INTERPOLATION_TYPE.G0, r, r);
		if (gc1 != null)
			candidates.add(gc1);
		
		GenericConnector gc2 = genericConnector(tp0, tp1rev, INTERPOLATION_TYPE.G0, r, r);
		if (gc2 != null) {
			gc2.setEndConn(TERM_TYPE.END);
			candidates.add(gc2);
		}

		tp0 = tps.getFirst();
		GenericConnector gc3 = genericConnector(tp1, tp0, INTERPOLATION_TYPE.G0, r, r);
		if (gc3 != null) {
			gc3.setEndConn(TERM_TYPE.START);
			gc3.setStartConn(TERM_TYPE.END);
			gc3.setConnectionMode(MODE.PREPEND);
			candidates.add(gc3);
		}

		GenericConnector gc4 = genericConnector(tp1rev, tp0, INTERPOLATION_TYPE.G0, r, r);
		if (gc4 != null) {
			gc4.setEndConn(TERM_TYPE.START);
			gc4.setStartConn(TERM_TYPE.START);
			gc4.setConnectionMode(MODE.PREPEND);

			candidates.add(gc3);
		}

		GenericConnector winnerC = null;
		winnerC = Collections.min(candidates);
		if (winnerC.connectionMode == MODE.PREPEND)
			tp1.reverse();

		return winnerC;

	}
	
	public GenericConnector makeShortestConnector(ToolpathSequence tps, Toolpath<Point3d> tp1, LOCDATA locdata) {
		GenericConnector  gc = this.makeShortestConnector(tps, tp1);
		if(gc != null)
			gc.locData = locdata;
		
		return gc;
	}

}
