package org.ica.cosmo.machining;

import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

public class Connector implements Comparable<Connector> {
	static double modulus = MillingContext.getInstance().getCutter().getRadius();
	private ToolpathSequence tps;
	private Toolpath<Point3d> tp0; // tp0 belongs to tps
	private Toolpath<Point3d> tp1;
	private TERM_TYPE term1;

	private Toolpath<Point3d> connectorTp;
	private double distance;
	private double length;

	public enum MODE {
		APPEND, PREPEND
	};

	private MODE mode;

	public enum LOCDATA { // used to localize the connector in relation to tps
		PREV, FIRST, LAST, NEXT
	};

	private LOCDATA locData;

	public Connector(Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		super();
		this.tp0 = tp0;
		this.tp1 = tp1;

		findClosest();
		calcConnector();
	}

	public Connector(ToolpathSequence tps, Toolpath<Point3d> tp, LOCDATA locData) {
		this.tps = tps;
		this.tp1 = tp;
		this.locData = locData;

		findClosest();

		switch (mode) {
		case APPEND:
			this.tp0 = tps.getLast();
			break;
		case PREPEND:
			this.tp0 = tps.getFirst();
			break;
		default:
			// Should never happen
		}

		calcConnector();
	}

	private void findClosest() {
		distance = Double.MAX_VALUE;
		this.tp0 = tps.getLast();
		Point3d p0 = tp0.getLast();
		Point3d p1 = tp1.getFirst();
		if (p0.distance(p1) < distance) {
			mode = MODE.APPEND;
			distance = p0.distance(p1);
			term1 = TERM_TYPE.START;
		}
		p1 = tp1.getLast();
		if (p0.distance(p1) < distance) {
			mode = MODE.APPEND;
			distance = p0.distance(p1);
			term1 = TERM_TYPE.END;
		}

		this.tp0 = tps.getFirst();
		p0 = tp0.getFirst();
		p1 = tp1.getFirst();
		if (p0.distance(p1) < distance) {
			mode = MODE.PREPEND;
			distance = p0.distance(p1);
			term1 = TERM_TYPE.START;
		}
		p1 = tp1.getLast();
		if (p0.distance(p1) < distance) {
			mode = MODE.PREPEND;
			distance = p0.distance(p1);
			term1 = TERM_TYPE.END;
		}
	}

	/*
	 * Le connecteur est toujours calculé de tps vers tp.
	 * 
	 * Si on est en mode PREPEND, il est inversé à la fin.
	 */
	private void calcConnector() {
		// p0 : point de départ
		// p5 : point d'arrivée
		Point3d p0 = null, p5 = null, prevP = null, nextP = null;

		switch (mode) {
		case APPEND:
			p0 = tp0.get(tp0.size() - 1);
			prevP = tp0.get(tp0.size() - 2);
			break;
		case PREPEND:
			p0 = tp0.get(0);
			prevP = tp0.get(1);
			break;
		default:
		}

		if (term1 == TERM_TYPE.END && mode == MODE.APPEND)
			tp1.reverse();
		if (term1 == TERM_TYPE.START && mode == MODE.PREPEND)
			tp1.reverse();

		switch (mode) {
		case APPEND:
			p5 = tp1.get(0);
			nextP = tp1.get(1);
			break;
		case PREPEND:
			p5 = tp1.get(tp1.size() - 1);
			nextP = tp1.get(tp1.size() - 2);
			break;
		default:
		}

		double scaleFactor;
		if(distance < 2*modulus) {
			// MODE classic end points
			scaleFactor = 0.8*modulus;
		} else {
			// MODE SAFE
			if(distance < 4*modulus) {
				scaleFactor = distance;
			} else {
				this.connectorTp = null;
				return;
			}
		}

		Vector3d dir0 = new Vector3d(prevP, p0);
		dir0.normalize();
		dir0.scale(scaleFactor);

		Point3d p1 = Points.translateClone(p0, dir0);
		Point3d p2 = new Point3d(p1);
		p2.z = p2.z + scaleFactor;

		Vector3d dir1 = new Vector3d(nextP, p5);
		dir1.normalize();
		dir1.scale(scaleFactor);

		Point3d p4 = Points.translateClone(p5, dir1);
		Point3d p3 = new Point3d(p4);
		p3.z = p3.z + scaleFactor;

		BezierCurve c = new BezierCurve(new Point3d[] { p0, p1, p2, p3, p4, p5 });

		/*
		 * Estimating number of interpolation points needed. Based on a rough
		 * estimation of the length of the Bézier curve.
		 */
		double l1 = c.polygonLength();
		double l2 = p0.distance(p5);
		int tess = (int) Math.floor((l1 + l2) / 1.5);
		if (tess < 6 )
			tess = 6;

		this.connectorTp = new Toolpath<Point3d>(c, tess);
		this.length = connectorTp.length();
		if (mode == MODE.PREPEND)
			this.connectorTp.reverse();
	}

	public double getDistance() {
		return distance;
	}

	public Toolpath<Point3d> getTp1() {
		return tp1;
	}

	public Toolpath<Point3d> getConnectorToolpath() {
		return connectorTp;
	}

	public MODE getMode() {
		return mode;
	}

	public LOCDATA getLocData() {
		return locData;
	}

	@Override
	public int compareTo(Connector o) {
		if (this.length < o.length)
			return -1;
		if (this.length > o.length)
			return 1;
		return 0;
	}

}
