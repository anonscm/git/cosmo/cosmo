package org.ica.cosmo.util;

import java.util.ArrayList;

import org.ica.support.Signal;
import org.lgmt.dgl.curves.PointsCurve;
import org.lgmt.dgl.vecmath.Point3d;

public class PolyLineSignal extends Signal {

	public PolyLineSignal(String name, ArrayList<Point3d> pointsList, Integer index) {
		super(name);
		this.data = pointsList;
		this.context = index;
	}

	public PolyLineSignal(String name, ArrayList<Point3d> pointsList) {
		super(name);
		this.data = pointsList;
		this.context = null;
	}
	
	public PolyLineSignal(String name, PointsCurve<Point3d> curve, Integer index) {
		super(name);
		this.data = curve.getPoints();
		this.context = index;
	}

	public PolyLineSignal(String name, PointsCurve<Point3d> curve) {
		super(name);
		this.data = curve.getPoints();
		this.context = null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Point3d> getData() {
		return (ArrayList<Point3d>) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Point3d> getPointsList() {
		return (ArrayList<Point3d>) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
