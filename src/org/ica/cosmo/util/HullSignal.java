package org.ica.cosmo.util;

import org.ica.cosmo.machining.Hull;
import org.ica.support.Signal;

public class HullSignal extends Signal {

	public HullSignal(String name, Hull hull, Integer index) {
		super(name);
		this.data = hull;
		this.context = index;
	}

	public HullSignal(String name, Hull hull) {
		super(name);
		this.data = hull;
	}
	
	@Override
	public Hull getData() {
		return (Hull) this.data;
	}
	
	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public Hull getHull() {
		return (Hull) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
