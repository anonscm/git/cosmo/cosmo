package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.lgmt.dgl.curves.Curve;

public class CurveSignal extends Signal {

	public CurveSignal(String name, Curve curve, Integer index) {
		super(name);
		this.data = curve;
		this.context = index;
	}

	public CurveSignal(String name, Curve curve) {
		super(name);
		this.data = curve;
		this.context = null;
	}

	@Override
	public Curve getData() {
		return (Curve) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public Curve getCurve() {
		return (Curve) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
