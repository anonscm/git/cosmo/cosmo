package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.ica.vtkviewer.model.ObservableToolpath;

public class ObservableToolpathSignal extends Signal {

	public ObservableToolpathSignal(String name, ObservableToolpath tp, Integer index) {
		super(name);
		this.data = tp;
		this.context = index;
	}

	public ObservableToolpathSignal(String name, ObservableToolpath tp) {
		super(name);
		this.data = tp;
		this.context = null;
	}

	@Override
	public ObservableToolpath getData() {
		return (ObservableToolpath) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public ObservableToolpath getObservableToolpath() {
		return (ObservableToolpath) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
