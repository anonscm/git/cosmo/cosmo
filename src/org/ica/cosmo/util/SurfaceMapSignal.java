package org.ica.cosmo.util;

import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.support.Signal;

public class SurfaceMapSignal extends Signal {

	public SurfaceMapSignal(String name, SurfaceMap surfaceMap, Integer index) {
		super(name);
		this.data = surfaceMap;
		this.context = index;
	}

	public SurfaceMapSignal(String name, SurfaceMap surfaceMap) {
		super(name);
		this.data = surfaceMap;
		this.context = null;
	}

	@Override
	public SurfaceMap getData() {
		return (SurfaceMap) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public SurfaceMap getSurfaceMap() {
		return (SurfaceMap) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
