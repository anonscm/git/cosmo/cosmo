package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.toolpath.Toolpath;

public class ToolpathSignal extends Signal {

	public ToolpathSignal(String name, Toolpath<? extends Point3d> toolpath, Integer index) {
		super(name);
		this.data = toolpath;
		this.context = index;
	}

	public ToolpathSignal(String name, Toolpath<? extends Point3d> toolpath) {
		super(name);
		this.data = toolpath;
		this.context = null;
	}

	@Override
	public Toolpath<? extends Point3d> getData() {
		return (Toolpath<?>) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public Toolpath<? extends Point3d> getToolpath() {
		return (Toolpath<?>) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
