package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.ica.vtkviewer.model.ObservablePoints;

public class ObservablePointsListSignal extends Signal {

	public ObservablePointsListSignal(String name, ObservablePoints pointsList, Integer index) {
		super(name);
		this.data = pointsList;
		this.context = index;
	}

	public ObservablePointsListSignal(String name, ObservablePoints pointsList) {
		super(name);
		this.data = pointsList;
		this.context = null;
	}

	@Override
	public ObservablePoints getData() {
		return (ObservablePoints) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public ObservablePoints getObservablePointsList() {
		return (ObservablePoints) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
