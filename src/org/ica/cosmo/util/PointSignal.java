package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.lgmt.dgl.vecmath.Point3d;

public class PointSignal extends Signal {

	public PointSignal(String name, Point3d point, Integer index) {
		super(name);
		this.data = point;
		this.context = index;
	}

	public PointSignal(String name, Point3d point) {
		super(name);
		this.data = point;
		this.context = null;
	}

	@Override
	public Point3d getData() {
		return (Point3d) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public Point3d getPoint() {
		return (Point3d) data;
	}

	public Integer getIndex() {
		return (Integer) context;
	}

}
