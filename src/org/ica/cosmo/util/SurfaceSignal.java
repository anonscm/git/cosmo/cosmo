package org.ica.cosmo.util;

import org.ica.support.Signal;
import org.lgmt.dgl.surfaces.Surface;

public class SurfaceSignal extends Signal {

	public SurfaceSignal(String name, Surface surface, Integer index) {
		super(name);
		this.data = surface;
		this.context = index;
	}

	public SurfaceSignal(String name, Surface surface) {
		super(name);
		this.data = surface;
	}

	@Override
	public Surface getData() {
		return (Surface) this.data;
	}

	@Override
	public Integer getContext() {
		return (Integer) this.context;
	}

	/*
	 * convenient getters
	 */
	public Surface getSurface() {
		return (Surface) this.data;
	}

	public Integer getIndex() {
		return (Integer) this.context;
	}

}
