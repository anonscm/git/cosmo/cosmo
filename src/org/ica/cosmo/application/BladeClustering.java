package org.ica.cosmo.application;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.tools.FileManager;
import org.ica.cosmo.zoning.SurfaceMap;
import org.lgmt.dgl.surfaces.Surface;

import vtk.vtkNativeLibrary;

public class BladeClustering {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	private static Logger logger = LogManager.getLogManager().getLogger("org.ica.cosmo");

	/** surface */
	private Surface surface;
	private SurfaceMap map;
	private Clustering c;

	/** Méthode de clustering à utiliser */
	private ALGO_TYPE algo = ALGO_TYPE.KMEANS;

	/** métrique */
	private METRIC metric = METRIC.SLOPE;

	/** niveau d'approximation tessellation */
	private APPROX_LEVEL apxl;

	/** nombre de zones */
	private int nK;

	private boolean save;

	public BladeClustering(Surface surface, int nK, APPROX_LEVEL apxl, boolean save) {
		super();

		this.surface = surface;
		this.nK = nK;
		this.apxl = apxl;
		this.save = save;
	}

	public BladeClustering(Surface surface, int nK, boolean save) {
		this(surface, nK, APPROX_LEVEL.MEDIUM, save);
	}

	public BladeClustering(Surface surface, int nK, APPROX_LEVEL apxl) {
		this(surface, nK, apxl, false);
	}

	/**
	 * Calcule la map (clustering).
	 */
	public void run() {
		logger.info("Clustering in progress, using " + apxl.name() + " level");
		long start_time = System.currentTimeMillis();
		map = new SurfaceMap(surface, apxl);
		this.c = new Clustering(nK, map, algo, metric, false);
		c.run();
		try {
			map = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.nK = c.getNbClusters();

		if (save) {
			FileManager fileManager = FileManager.getInstance();
			fileManager.saveMap(map, new String("blade"));
		}

		long end_time = System.currentTimeMillis();
		long clustering_time = end_time - start_time;
		logger.info("tesselation = (" + map.getTessU() + "," + map.getTessV() + ")");
		logger.info("clustering time = " + clustering_time + " ms");
	}

	public SurfaceMap getMap() {
		return map;
	}

}
