N16477 X-126.563 Y-73.313 Z-9.688 		; dernier point usiné
N16478 Z-7.688 					; pré-retrait 2mm en vitesse travail
N16479 G0 Z138.086 				; retrait en rapide hors brut niveau plan de sécurité (20mm du brut)
N16480 ROT 					; desactivation de la rotation du repère
N16481; Finition_Zone2
 MSG("Finition_Zone2")
N16482 TRAFOOF 					; désactivation de la transformation TRAORI (RTCP : Rotate Tool Center Point)
N16483 G54 					; définition du repère Origine Programme)
N16484 G0 SUPA Z-5 D0 ; dégagement en rapide butée Z-5 sans jauge Longueur Outil
N16485 M23 ; déclampage des axes rotatifs 
N16486 G0 A-28.512 C251.352 ; rotation en rapide des axes A et C 
N16487 D1 ; activation jauge longueur outil
N16488 TRAORI ; activation la fonction RTCP 
N16489 G54 ; définition du repère Origine Programme)

N16490 ROT X-28.512 Y0 Z-108.648 ; aligne le repère sur la nouvelle orientation pièce
N16491 G0 X-134.231 Y110.883 ; déplacement linéaire rapide sur le point d'entrée
N16492 ROT
N16493 G0 X242.263 Y59.911 Z155 A3=0.45228 B3=-0.15263 C3=0.87872
N16494 M22 ; clampage des axes rotatifs

N16495 ROT X-28.512 Y0 Z-108.648
N16496 G0 X-134.23 Y110.883 Z97.484
N16497 G1 Z95.484
N16498 X-134.258 Y110.858 Z95.156