; %_COSMO_BLADE
N1 ;
N2 G54 ; origine pièce
N3 CYCLE832(0.02,102001) ; (Tolérance, code = (COMPCAD, FFWOF SOFT, G642, TRAFOOF, ,Finition))
N4 G642 ; lissage de la trajectoire
N5 SOFT ; lissage de la vitesse
 MSG("Phase1")
N7 G0 SUPA Z-5 D0 ; retrait en rapide à Zmax-5 sans compter la longueur d'outil
N8 M23 ; désactivation poinçonnage, grignotage (si macro standard)
N9 G0 A0 C0

N10 T="FRAISE_D16_R4"
N11 M6 ; changement d'outil 
N12 G0 SUPA Z-5 D0
N13 D1 ; correcteur d'outil 1
N14 G54 ; origine pièce
N15 M8 ; arrosage
N16; Finition_Zone1
 MSG("Finition_Zone1")
N17 S18000 M3 ; rotation broche = 18000 tr/min
N18 M23 ; déclampage des axes rotatifs 
N19 G0 A-35.474 C169.131
N20 TRAORI
N21 G54

N22 ROT X-35.474 Y0 Z169.131
N23 G0 X-78.371 Y-125.445
N24 ROT
N25 G0 X85.184 Y28.023 Z155 A3=-0.10943 B3=-0.56993 C3=0.81438
N26 M22 ; clampage des axes rotatifs

N27 ROT X-35.474 Y0 Z169.131
N28 G0 X-78.371 Y-125.445 Z9.084
N29 G1 Z7.084 F3600
N30 X-78.262 Y-125.544 Z6.788
N31 X-78.134 Y-125.659 Z6.505
