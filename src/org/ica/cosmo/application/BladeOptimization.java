package org.ica.cosmo.application;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.optimization.ZoneOptimizationPPV3p2A;
import org.ica.cosmo.tools.FileManager;
import org.ica.cosmo.util.SurfaceMapSignal;
import org.ica.cosmo.util.SurfaceSignal;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.ica.support.Signal;
import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.vecmath.Matrix4d;

public class BladeOptimization implements IListener {
	static {
		String nomadHome = System.getenv("NOMAD_HOME");
		if (nomadHome == null)
			nomadHome = new String("/opt/Nomad"); // default location
		try {
			System.load(nomadHome + "/examples/interfaces/jNomad/libjNomad.so");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load.\n" + e);
			System.exit(1);
		}
	}

	@SuppressWarnings("unused")
	private static Logger logger = LogManager.getLogManager().getLogger("cosmo");

	private final EventManager emitter = new EventManager(this);

	private int zoneId;
	private Zone z;
	private boolean save;
	private ZoneOptimizationPPV3p2A zOpt;
	private SurfaceMap initMap;
	private SurfaceMap resultMap;
	private Matrix4d transform;
	private OptimResult result;

	public BladeOptimization(SurfaceMap map, int zoneId, boolean save) {
		super();
		this.zoneId = zoneId;
		this.z = map.getZone(zoneId);
		this.save = save;
		zOpt = new ZoneOptimizationPPV3p2A(z);
		zOpt.addListener(this);
	}

	public void run() {
		zOpt.init();
		this.initMap = zOpt.getInitMap();

		zOpt.run();

		Frame f = zOpt.getWorkingFrame();
		Matrix4d orientation = zOpt.getOrientation();
		this.transform = zOpt.getResultTransform();
		resultMap = zOpt.getResultMap();

		this.result = new OptimResult(resultMap, f, orientation, transform);

		if (save) {
			FileManager fileManager = FileManager.getInstance();
			fileManager.saveAnyObject(result, new String("optimResultZone" + zoneId), true);
		}
	}

	public OptimResult getResult() {
		return result;
	}

	public SurfaceMap getInitMap() {
		return initMap;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	@Override
	public void onReceivedSignal(Signal s) {
		if (s instanceof SurfaceMapSignal && s.getName() == "AddSurfaceMap") {
			SurfaceMapSignal sMap = (SurfaceMapSignal) s;
			emitter.sendSignal(new SurfaceMapSignal("AddSurfaceMap", sMap.getSurfaceMap(), sMap.getIndex()));
		}
		if (s instanceof SurfaceSignal && s.getName() == "AddSurface") {
			SurfaceSignal sSurf = (SurfaceSignal) s;
			emitter.sendSignal(new SurfaceSignal("AddSurface", sSurf.getSurface(), sSurf.getIndex()));
		}
	}
}
