package org.ica.cosmo.application;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.G0Connector_save;
import org.ica.cosmo.machining.Hull;
import org.ica.cosmo.machining.SodException;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.tools.FileManager;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.EventManager;
import org.ica.support.IListener;

public class BladeMilling{
	private static Logger logger = LogManager.getLogManager().getLogger("org.ica.cosmo");
	protected final EventManager emitter = new EventManager(this);
	private Zone zone;
	private boolean save;
	private ZoneMilling_PPV3A zMilling;

	public BladeMilling(Zone zone, boolean save) {
		super();
		this.zone = zone;
		this.save = save;
		APPROX_LEVEL apxl = APPROX_LEVEL.FINE;
		logger.fine("Building convex hull");
		Hull hullK = new Hull(zone.getSurfaceMap().getSurface(), 3);
		G0Connector_save.setHull(hullK);
		zMilling = new ZoneMilling_PPV3A(zone, apxl);
		zMilling.setOverrunAllowed(true);
		zMilling.setMinOverrun(2.0);
		zMilling.setMaxOverrun(10.0);
	}

	public void run() {
		try {
			zMilling.run();
		} catch (SodException e) {
			e.printStackTrace();
		}
		if (save) {
			FileManager fileManager = FileManager.getInstance();
			fileManager.saveToolpathSet(zMilling.getToolpathSet(), new String("toolpathZone" + zone.getId()));
		}
	}

	public ZoneMilling_PPV3A getzMilling() {
		return zMilling;
	}

	public void addListener(IListener l) {
		emitter.addListener(l);
		zMilling.addListener(l);
	}

	public void removeListener(IListener l) {
		emitter.removeListener(l);
		zMilling.removeListener(l);
	}

}
