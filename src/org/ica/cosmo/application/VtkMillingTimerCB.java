package org.ica.cosmo.application;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.support.Signal;
import org.ica.vtkviewer.VtkTimerCallback;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.VtkCutter;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.PRG_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;
import org.lgmt.jcam.toolpath.ToolpathSet;

import vtk.vtkActor;
import vtk.vtkRenderWindowInteractor;

public class VtkMillingTimerCB extends VtkTimerCallback {
	private static Logger logger = LogManager.getLogManager().getLogger("cosmo");
	private MillingContext mc = MillingContext.getInstance();
	private VtkCutter vtkCutter;
	private ToolpathSet toolpathSet;
	private int tpIndex = 0;
	private Toolpath<? extends Point3d> tp;

	public VtkMillingTimerCB(vtkRenderWindowInteractor interactor, ToolpathSet toolpathSet, VtkCutter vtkCutter) {
		super(interactor);
		this.toolpathSet = toolpathSet;
		this.vtkCutter = vtkCutter;
		this.tp = toolpathSet.get(tpIndex);
		gotoTpStart(tp);
	}

	public VtkMillingTimerCB(VtkViewer viewer, ToolpathSet toolpathSet, VtkCutter vtkCutter) {
		this(viewer.getPanel3D().getRenderWindowInteractor(), toolpathSet, vtkCutter);
	}

	private void gotoTpStart(Toolpath<? extends Point3d> tp) {
		Point3d p = new Point3d(tp.getFirst());
		if (mc.getPrgType() == PRG_TYPE.CLT) {
			p.z = p.z + mc.getCutter().getTipRadius();
		}
		vtkCutter.setPosition(p);
	}

	@Override
	public void run() {
		if (status == STATUS.RUN) {
			this.timerCount++;
//			this.timerCount = this.timerCount+10;
			if (timerCount < tp.getNbPoints()) {
				for (vtkActor a : vtkCutter.getActors()) {
					Point3d position = getPosition(timerCount);
					a.SetPosition(position.x, position.y, position.z);
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else {
				if (tpIndex < toolpathSet.size() - 1) {
					tpIndex++;
					tp = toolpathSet.get(tpIndex);
					gotoTpStart(tp);
					timerCount = 0;
				} else {
					this.done();
				}
			}
			interactor.GetRenderWindow().Render();
		}
	}

	public void done() {
		this.status = STATUS.STOP;
		this.emitter.sendSignal(new Signal("Done"));
	}

	public void setVtkCutter(VtkCutter vtkCutter) {
		this.vtkCutter = vtkCutter;
	}

	private Point3d getPosition(int timerCount) {
		Point3d p = new Point3d(tp.get(timerCount));
		if (mc.getPrgType() == PRG_TYPE.CLT) {
			p.z = p.z + mc.getCutter().getTipRadius();
		}
		if (mc.getPrgType() == PRG_TYPE.CC) {
			logger.log(Level.WARNING, "Positionning cutter at CC point not avaible yet");
		}
		return p;
	}

}