package org.ica.cosmo.application;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ica.cosmo.machining.Hull;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.tools.FileManager;
import org.ica.cosmo.util.CurveSignal;
import org.ica.cosmo.util.HullSignal;
import org.ica.cosmo.util.ObservableToolpathSignal;
import org.ica.cosmo.util.PolyLineSignal;
import org.ica.cosmo.util.SurfaceMapSignal;
import org.ica.cosmo.util.SurfaceSignal;
import org.ica.cosmo.util.ToolpathSignal;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.IListener;
import org.ica.support.LoggingSetup;
import org.ica.support.Signal;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkCurve;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkObservablePoints;
import org.ica.vtkviewer.model.VtkPolyline;
import org.ica.vtkviewer.model.VtkSurface;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.ica.vtkviewer.model.VtkToolpath;
import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.curves.PointsCurve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.TBAnglesXYZi;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.io.IGESReader;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.PRG_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkNativeLibrary;

public class Blade3p2Axes implements IListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	/** surface */
	private Surface surface;
	private SurfaceMap map;
	private MillingContext mc = MillingContext.getInstance();

	enum OPTION {
		FILE, CALC
	};

	private FileManager fileManager;
	private OPTION clusteringOption = OPTION.FILE;
	private OPTION optimizingOption = OPTION.CALC;
	private OPTION millingOption = OPTION.CALC;
	boolean saveClustering = true;
	boolean saveOptimization = true;
	boolean saveMilling = true;

	private int nK = 4;

	private List<Integer> kList = Arrays.asList(2);

	private VtkModel model; // global model
	private VtkViewer viewer; // root frame viewer

	// for each Map, Integer key is zone id
	private Map<Integer, OptimResult> optimK = new HashMap<Integer, OptimResult>();
	private Map<Integer, ZoneMilling_PPV3A> millingK = new HashMap<Integer, ZoneMilling_PPV3A>();
	private Map<Integer, VtkModel> modelsK = new HashMap<Integer, VtkModel>();
	private Map<Integer, VtkViewer> viewersK = new HashMap<Integer, VtkViewer>();

	private static final Logger logger = LoggingSetup.setupLogger("org.ica.cosmo", Level.ALL, Level.ALL);

	/** main program */
	public static void main(String[] args) {
		new Blade3p2Axes(args);
	}

	public Blade3p2Axes(String[] args) {
		super();
		long start = System.currentTimeMillis();

		FileManager.setResourcesPath(new String("src/org/ica/cosmo/application/ressources/"));
		fileManager = FileManager.getInstance();

		initModel();

		mc.setCutter(new Cutter(8.0, 4.0, 110.0, 4));
		mc.setScallopHeight(0.1);
		mc.setPrgType(PRG_TYPE.CLT);

		mc.setSpindleSpeed(18000); // S = 18 m/min
		mc.setVf(3600); // Vf = 3600 mm/min

		logger.config(String.format("Milling conditions: Cutter (R,r,Z) = (%.0f, %.0f, %d),  f = %.2f mm/tooth, Vc = %.0f m/min",
				mc.getCutter().getRadius(), mc.getCutter().getTipRadius(), mc.getCutter().getNbTeeth(), mc.getF(), mc.getVc()));

		// Display in global frame
		model = new VtkModel();
		VtkSurface vtkSurface = new VtkSurface(surface);
		model.add(vtkSurface);
//		viewer = new VtkViewer(model);
//		viewer.run();

		doCLustering();

		vtkSurface.setOpacity(0.0);
//		model.add(new VtkSurfaceMap(map));

		doOptimization();

		doMilling();

//		doSimulation();

		writeGCode();

		long end = System.currentTimeMillis();

		long duration = (end - start) / 1000;
		int sec = (int) duration % 60;
		int min = (int) (duration / 60) % 60;
		System.out.println("total computation time = " + min + " min " + sec + " s");

		double millingTime = 0.0;
		for (Integer k : kList) {
			for (Toolpath<? extends Point3d> tp : millingK.get(k).getToolpathSet()) {
				millingTime = millingTime + tp.millingTime();
			}
		}

		sec = (int) millingTime % 60;
		min = (int) (millingTime / 60) % 60;
		System.out.println("total milling time (estimated) = " + min + " min " + sec + " s");
		logger.info("End.");
	}

	public void initModel() {
		File file = new File("/home/redonnet/Recherche/CoSMO/Publis/CAD2024/Data/blade.igs");
		IGESReader reader = new IGESReader(file);
		reader.read();
		surface = reader.getSurfaces(0);
		logger.info("Surface loaded: " + surface.toString());
	}

	public void doCLustering() {
		if (clusteringOption == OPTION.FILE) {
			logger.info("Loading Blade map");
			try {
				this.map = fileManager.loadMap(new File("blade.map"));
			} catch (FileNotFoundException e) {
				logger.warning("File blade.map not found. Falling back to clusters calculation");
				this.clusteringOption = OPTION.CALC;
			}
		}
		if (clusteringOption == OPTION.CALC) {
			logger.info("Clustering in progress");
			BladeClustering clustering = new BladeClustering(surface, nK, saveClustering);
			clustering.run();
			this.map = clustering.getMap();
		}

		nK = map.getZonesCount();

		/*
		 * Prepare visualization for each zone
		 */
		for (Integer k : kList) {
			VtkModel m = new VtkModel();
			modelsK.put(k, m);
			VtkViewer v = new VtkViewer(modelsK.get(k), new String("Optimizing zone " + k));
			v.run();
			v.setBackground(Color4d.bluegray, Color4d.lightbluegray);
			viewersK.put(k, v);
		}
	}

	public void doOptimization() {
		optimK = new HashMap<Integer, OptimResult>();
		if (optimizingOption == OPTION.CALC)
			for (Integer k : kList) {
				if (k >= nK) {
					logger.severe("Zone " + k + " do not exists");
					continue;
				} else
					logger.info("Optimizing Zone " + k);
				BladeOptimization optim = new BladeOptimization(map, k, saveOptimization);
				optim.addListener(this);
				optim.run();
				// Logger seems to be no longer active after running optimization ?!??
				// logger = LogManager.getLogManager().getLogger("cosmo");
				// or setupLogger(); do not fix the problem
				optimK.put(k, optim.getResult());
			}
		if (optimizingOption == OPTION.FILE) {
			logger.info("Loading optimized maps");
			for (Integer k : kList) {
				if (k >= nK) {
					logger.severe("Zone " + k + " do not exists");
					continue;
				} else
					logger.info("Loading Zone " + k + " optimization");
				Object o = fileManager.loadAnyObject(new String("optimResultZone" + k), OptimResult.class);
				OptimResult result = (OptimResult) o;
				optimK.put(k, result);

				modelsK.get(k).add(new VtkSurfaceMap(result.getMap()));
			}

		}

		for (Integer k : kList) {
			OptimResult result = optimK.get(k);
			Matrix4d m = getTrickyMatrix();
			SurfaceMap sMap = result.getMap().transformClone(m);
			Matrix4d o = new Matrix4d(result.getOrientation());
			o.mul(m);
			Matrix4d tf = new Matrix4d(result.getTransform());
			tf.mul(m);
			Frame f = new Frame(result.getWorkingFrame(), m);
			OptimResult or = new OptimResult(sMap, f, o, tf);
			optimK.put(k, or);
			VtkModel model = this.modelsK.get(k);
			model.remove(model.getByType(VtkSurfaceMap.class).get(0));
			model.add(new VtkSurfaceMap(sMap));
		}

		/*
		 * VERIFS
		 */
//		for (Integer k : kList) {
//			OptimResult result = optimK.get(k);
//
//			Matrix4d mRef = result.getWorkingFrame().getTransform();
//
//			FrameSet fs = FrameSet.getInstance();
//			Frame rootf = fs.getGlobalFrame();
//			Frame f = new Frame(rootf, mRef);
//			VtkFrame vtkF = new VtkFrame(f, "wf0", 40);
//			model.add(vtkF);
//
//			Matrix4d m = result.getTransform();
//			Vector3d t = m.getTranslation();
//			Matrix3d r = m.getRotation();
//
//			m.invert();
//
//			Surface s0 = surface.transformClone(m); // surface dans le repère de la zone
//			Matrix4d m0 = new Matrix4d();
//			m0.setTranslation(t);
////			model.add(new VtkSurface(s0));
//			EulerAngles ea = new EulerAngles(r);
//			Matrix4d m1 = new Matrix4d();
//			m1.setTranslation(t);
//			m1.setRotation(ea.getRotationMatrix());
//			Surface s2 = s0.transformClone(m1);
//			model.add(new VtkSurface(s2));
//			TBAnglesXYZi tba = new TBAnglesXYZi(r);
//			Matrix4d m2 = new Matrix4d();
//			m2.setTranslation(t);
//			m2.setRotation(tba.getRotationMatrix());
//			Surface s3 = s0.transformClone(m2);
//			model.add(new VtkSurface(s3));
//		}

	}

	/**
	 * Debugging purpose. Set angle to 0 for normal behavior.
	 */
	private Matrix4d getTrickyMatrix() {
		double angle = Math.toRadians(30);
		double c = Math.cos(angle);
		double s = Math.sin(angle);
		Matrix4d m = new Matrix4d(
				new double[] { c, -s, 0.0, 0.0, s, c, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0 });
		return m;
	}

	public void doMilling() {
		millingK = new HashMap<Integer, ZoneMilling_PPV3A>();
		if (millingOption == OPTION.CALC) {
			for (Integer k : kList) {
				if (k >= nK) {
					logger.severe("Zone " + k + " do not exists");
					continue;
				} else
					logger.info("Milling Zone " + k);
				Zone zone = optimK.get(k).getMap().getZone(k);
				BladeMilling milling = new BladeMilling(zone, saveMilling);
				milling.addListener(this);
				milling.run();
				millingK.put(k, milling.getzMilling());
			}
		}
		if (millingOption == OPTION.FILE) {
			logger.severe("Unsupported");
		}
	}

	public void doSimulation() {
		for (Integer k : kList) {
			BladeSimulation sim = new BladeSimulation(modelsK.get(k), viewersK.get(k),
					millingK.get(k).getToolpathSet());
			sim.run();
		}
	}

	public void writeGCode() {
		GCodeWriter.setResourcesPath(FileManager.getResourcesPath());
		String name = new String("blade");
		logger.info("Writing GCode to file " + name + ".gcode");
		GCodeWriter writer = new GCodeWriter(name);
		List<String> header = new ArrayList<String>();
		header.add("WORKPIECE(,\" \",, \"BOX\", 64, 135, -30, -80, 0, 0, 180, 180)");
		header.add("G54");
		header.add("CYCLE832(0.02,1,1)");
		header.add("G642");
		header.add("SOFT");
		header.add("CUT2D");
		header.add("G0 SUPA Z-5 D0");
		header.add("M23");
		header.add("CYCLE800(1,\"TC8\",0,57,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,-1,0,1)"); // Required !!!
		header.add("T=\"FRAISE_D16_R4\"");
		header.add("M6");
		header.add("G0 SUPA Z-5 D0");
		header.add("D1");
		header.add("G54");
		header.add("G17");
		header.add("S18000 F3600");
		writer.appendLines(header);

		for (Integer k : kList) {
			OptimResult result = optimK.get(k);

			Matrix4d transform = result.getWorkingFrame().getTransform();

			Vector3d t = transform.getTranslation();
			Matrix3d m = new Matrix3d(transform.getRotation());

			TBAnglesXYZi tba = new TBAnglesXYZi(m);
			writer.appendLine(String.format(
					"MSG(\"---------------------------- Usinage de la zone %d ----------------------------\")", k));
			String str800 = new String("CYCLE800(");
			// writer.appendLine(String.format("TRANS X%.3f Y%.3f Z%.3f", t.x, t.y, t.z));
//			// TODO: voir PM 10600 pour programmer directement les angles d'Euler
//			this.appendLine(String.format("%s X%.3f Y%.3f Z%.3f\n", code, ea.psi, ea.theta, ea.phi));
//			writer.appendLine(String.format("AROT Z%.3f", Math.toDegrees(ea.psi)));
//			writer.appendLine(String.format("AROT X%.3f", Math.toDegrees(ea.theta)));
//			writer.appendLine(String.format("AROT Z%.3f", Math.toDegrees(ea.phi)));
			str800 = str800 + "1,"; // dégagement stadard selon Z
			str800 = str800 + "\"TC8\","; // nom du bloc de paramètre d'orientation (?)
			str800 = str800 + "0,"; // nouveau plan orienté
			// Rotations X(01), Y(10) Z(11) => 111001 (bin) = 57 (décimal). NOTA: binaire :
			// axe3/axe2/axe1
			str800 = str800 + "57,";
			// Point de ref avant l'orientation : origine du nouveau repềre
			str800 = str800 + String.format("%.3f,%.3f,%.3f,", t.x, t.y, t.z);
			str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.alpha)); // rotation autour de X
			str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.beta)); // rotation autour de Y
			str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.gamma)); // rotation autour de Z
			str800 = str800 + "0,0,0,"; // origine après l'orientation
			str800 = str800 + "-1,"; // orientation : OUI, plus petite valeur de l'axe rotatif
			str800 = str800 + "0,"; // dégagement incrémental dans le sens de l'outil
			str800 = str800 + "1)"; // ???

			writer.appendLine(str800);
			writer.appendLine("M22 M8 M3 D1");

			ZoneMilling_PPV3A milling = millingK.get(k);

			Point3d p = milling.getToolpathSet().get(0).getFirst();
			writer.appendLine(String.format("G0 X%.3f Y%.3f Z%.3f", p.x, p.y, p.z));
			for (Toolpath<? extends Point3d> tp : milling.getToolpathSet()) {
				if (tp.getICode() == INTERPOLATION_TYPE.G0)
					writer.appendLine("G0");
				if (tp.getICode() == INTERPOLATION_TYPE.G1)
					writer.appendLine("G1");
				Iterator<? extends Point3d> it = tp.iterator();
				while (it.hasNext()) {
					p = it.next();
					writer.appendLine(p);
				}
			}

			writer.appendLine("M23 M9 M5");
			writer.appendLine("G0 SUPA Z-5 D0"); // dégagement en rapide
//			writer.appendLine("ROT");
			writer.appendLine("G54");
//			writer.appendLine("G0 A0 C0");
		}

		List<String> footer = new ArrayList<String>();
//		footer.add("G0 SUPA Z-5 D0");
		footer.add("M30");
		writer.appendLines(footer);

		writer.write();
	}

	@Override
	public void onReceivedSignal(Signal signal) {
		if (signal instanceof SurfaceSignal && signal.getName() == "AddSurface") {
			SurfaceSignal sSignal = (SurfaceSignal) signal;
			Surface s = sSignal.getSurface();
			Integer k = sSignal.getIndex();
			VtkModel model = this.modelsK.get(k);
			model.add(new VtkSurface(s));
		}
		if (signal instanceof HullSignal && signal.getName() == "NewHull") {
			HullSignal s = (HullSignal) signal;
			Hull h = s.getHull();
			Integer k = s.getIndex();
			VtkModel model = this.modelsK.get(k);
//			model.add(new VtkHull(h));
		}
		if (signal instanceof SurfaceMapSignal && signal.getName() == "AddSurfaceMap") {
			SurfaceMapSignal smSignal = (SurfaceMapSignal) signal;
			SurfaceMap map = smSignal.getSurfaceMap();
			Integer k = smSignal.getIndex();
			VtkModel model = this.modelsK.get(k);
			if (model.getNbOf(VtkSurfaceMap.class) == 1) { // single map model: decreasing opacity
				model.getByType(VtkSurfaceMap.class).get(0).setOpacity(0.1);
			}
			if (model.getNbOf(VtkSurfaceMap.class) > 1) // ongoing optimization
				model.remove(model.getByType(VtkSurfaceMap.class).get(1));
			model.add(new VtkSurfaceMap(map));
		}
		if (signal instanceof ToolpathSignal) {
			ToolpathSignal tpSignal = (ToolpathSignal) signal;
			Toolpath<? extends Point3d> tp = tpSignal.getToolpath();
			Integer k = tpSignal.getIndex();
			VtkModel model = this.modelsK.get(k);
			if (signal.getName() == "AddRawToolpath") {
				VtkToolpath vtp = new VtkToolpath(tp, 1.0f);
				vtp.setPointsVisibility(true);
				model.add(vtp);
			}
			if (signal.getName() == "AddPrgToolpath")
				model.add(new VtkToolpath(tp, 2.0f));
			if (signal.getName() == "AddFinalToolpath")
				model.add(new VtkToolpath(tp, 3.0f));
		}
		/* debug purpose only */
		if (signal instanceof CurveSignal && signal.getName() == "NewCurve") {
			CurveSignal cSignal = (CurveSignal) signal;
			if (cSignal.getCurve() instanceof BezierCurve) {
				BezierCurve cbez = (BezierCurve) cSignal.getCurve();
				VtkModel model = this.modelsK.get(2);
				VtkCurve vtkC = new VtkCurve(cbez);
				vtkC.setPolygonVisibility(false);
				model.add(vtkC);
			}
		}
		if (signal instanceof PolyLineSignal) {
			PolyLineSignal s = (PolyLineSignal) signal;
			if (s.getName() == "AddCurve") {
				PointsCurve<Point3d> c = new PointsCurve<Point3d>(s.getPointsList());
				int k = s.getContext();
				VtkModel model = this.modelsK.get(k);
				VtkPolyline vtkC = new VtkPolyline(c, 2.0f, Color4d.orange);
				vtkC.setVisiblePoints(true);
				model.add(vtkC);
			}
		}
		if (signal instanceof ObservableToolpathSignal) {
			ObservableToolpathSignal s = (ObservableToolpathSignal) signal;
			Integer k = s.getIndex();
			VtkModel model = this.modelsK.get(k);
			if (s.getName() == "ResetPL") {
				while (model.getNbOf(VtkObservablePoints.class) > 0) {
					model.remove(model.getByType(VtkObservablePoints.class).get(0));
				}
			}
			if (s.getName() == "NewOTP") {
				s.getObservableToolpath().addListener(this);
				model.add(new VtkObservablePoints(s.getObservableToolpath(), 6.0f, Color4d.magenta));
			}
		}

	}

}
