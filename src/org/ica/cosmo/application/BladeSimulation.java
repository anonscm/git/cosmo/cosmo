package org.ica.cosmo.application;

import java.beans.PropertyChangeSupport;

import org.ica.support.IListener;
import org.ica.support.Signal;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkCutter;
import org.ica.vtkviewer.model.VtkModel;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;
import org.lgmt.jcam.toolpath.ToolpathSet;

public class BladeSimulation implements IListener {
	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private MillingContext mc = MillingContext.getInstance();
	private VtkModel model;
	private VtkViewer viewer;
	private ToolpathSet toolpathSet;
	private VtkCutter vtkCutter;
	
	private int currentTP = 0;
	
	public BladeSimulation(VtkModel model, VtkViewer viewer, ToolpathSet toolpathSet) {
		super();
		this.model = model;
		this.viewer = viewer;
		pcs.addPropertyChangeListener(viewer);
		
		this.toolpathSet = toolpathSet;

		vtkCutter = new VtkCutter(mc.getCutter(), Color4d.teal, 1.0);

		this.model.add(vtkCutter);

	}

	public void run() {
		VtkMillingTimerCB cb = new VtkMillingTimerCB(viewer, toolpathSet, vtkCutter);
		
		cb.addListener(this);
		
		viewer.attachCallback(cb);
		
		
	}
	
	@Override
	public void onReceivedSignal(Signal signal) {
		
	}

}
