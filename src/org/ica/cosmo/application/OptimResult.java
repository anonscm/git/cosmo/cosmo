package org.ica.cosmo.application;

import org.ica.cosmo.zoning.SurfaceMap;
import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.vecmath.Matrix4d;

public class OptimResult implements java.io.Serializable{
	private static final long serialVersionUID = 816162072647990268L;
	
	private SurfaceMap map;
	private Frame workingFrame;
	private Matrix4d orientation;
	private Matrix4d transform;
	
	public OptimResult(SurfaceMap map, Frame f, Matrix4d orientation, Matrix4d globalTransform) {
		super();
		this.map = map;
		this.workingFrame = f;
		this.orientation = orientation;
		this.transform = globalTransform;
	}

	public SurfaceMap getMap() {
		return map;
	}

	public Frame getWorkingFrame() {
		return workingFrame;
	}

	public Matrix4d getOrientation() {
		return orientation;
	}

	public Matrix4d getTransform() {
		return transform;
	}
	
}