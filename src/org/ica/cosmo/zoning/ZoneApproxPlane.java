package org.ica.cosmo.zoning;

import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.tan;

import java.util.ArrayList;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

/**
 * Rectangle plan qui approxime au mieux la zone.
 * 
 * Ce plan est defini par les vecteurs propres eigenVectorI et eigenVectorII et
 * les longueurs sqrt(12*eigenValueI) selon eigenVectorI et
 * sqrt(12*eigenValueII) selon eigenVectorII.
 *
 * Le plan est défini par ses 4 angles p00, p01, p11 et p10. Il est créé centré
 * à l'origine et aligné selon X, puis transformé pour s'aligner avec la zone.
 * 
 * <pre>
 * p00 --- p01
 *  | (0,0) |   -->X
 * p10 --- p11
 * </pre>
 */
public class ZoneApproxPlane {
	/** Paramètres d'usinage */
	private MillingContext mc = MillingContext.getInstance();

	private Zone zone;
	private ArrayList<Point3d> rect;
	private Matrix4d m;
	private Toolpath<Point3d> tp = null;

	private double millingTime = 0.0;
	private double millingLength = 0.0;

	public ZoneApproxPlane(Zone z) {
		super();
		this.zone = z;
		rect = zone.getApproxPlane();
		Vector3d eI = new Vector3d(zone.getEigenVectorI());
		Vector3d eII = new Vector3d(zone.getEigenVectorII());
		Vector3d eIII = new Vector3d(zone.getEigenVectorIII());

		m = new Matrix4d();
		Matrix3d rot = new Matrix3d();
		rot.setColumn(0, eI);
		rot.setColumn(1, eII);
		rot.setColumn(2, eIII);
		m.setRotation(rot);
		Vector3d t1 = new Vector3d(zone.getMedianPoint());
		m.setTranslation(t1);

		// Reflexions pour l'affichage
		// n'impacte pas le calcul
		Matrix4d refMat = new Matrix4d();
		refMat.setIdentity();
		refMat.m11 = -1.0;
		m.mul(refMat);
	}

	public Zone getZone() {
		return zone;
	}

	public double getMillingTime() {
		return millingTime;
	}

	public double getMillingLength() {
		return millingLength;
	}

	public ArrayList<Point3d> getRect() {
		return rect;
	}

	public Toolpath<Point3d> getToolpath() {
		return tp;
	}

	public Matrix4d getM() {
		return m;
	}

	public void calc(double angle) {
		Vector3d n = zone.getEigenVectorIII();
		// MEMO: eigenVectorIII always oriented Z+

		Vector3d eI = zone.getEigenVectorI();

		double w = sqrt(12.0 * zone.getEigenValueI());
		double h = sqrt(12.0 * zone.getEigenValueII());
		double w2 = w / 2.0;
		double h2 = h / 2.0;

		double z = -(n.x * cos(angle) + n.y * sin(angle)) / n.z;
		Vector3d fp = new Vector3d(cos(angle), sin(angle), z);

		double alpha = abs(Util.angle0(eI, fp));
		double p = getSOD(angle, n);

		// distance entre traj dans les directions principales
		double dx, dy;
		dx = p / sin(alpha);
		dy = p / cos(alpha);

		// liste des points de la traj
		ArrayList<Point3d> pList = new ArrayList<Point3d>();
		pList.add(new Point3d(-w2, h2, 0.0));

		// liste des points de départ d'un aller-retour
		ArrayList<Point3d> spl = new ArrayList<Point3d>();
		double x = -w2;
		double y = h2 - dy / 2;
		while (y > -h2) {
			spl.add(new Point3d(x, y, 0));
			y = y - dy;
		}
		x = -w2 - (p + (h2 + y) * cos(alpha)) / sin(alpha) + dx;
		while (x < w2) {
			spl.add(new Point3d(x, -h2, 0));
			x = x + dx;
		}
		boolean retour = false;
		for (Point3d pt : spl) {
			y = pt.y;
			// x = last x, y = new y
			if (!retour && abs(x + w2) < Util.PREC12 && abs(y + h2) < Util.PREC12) // corner
				pList.add(new Point3d(-w2, -h2, 0));
			x = pt.x;
			pList.add(pt);
			if (retour) {
				retour = !retour;
				continue;
			}

			double dw = (h2 - y) / tan(alpha);
			double dh = (w2 - x) * tan(alpha);
			if (x + dw < w2) {
				// lim en y
				pList.add(new Point3d(x + dw, h2, 0));
				if (x + dw + dx < w2)
					pList.add(new Point3d(x + dw + dx, h2, 0));
				else { // corner
					pList.add(new Point3d(w2, h2, 0));
					pList.add(new Point3d(w2, h2 - (x + dw + dx - w2) * tan(alpha), 0));
				}
			} else {
				// lim en x
				pList.add(new Point3d(w2, y + dh, 0));
				if (y + dh - dy > -h2)
					pList.add(new Point3d(w2, y + dh - dy, 0));
				else
					pList.add(new Point3d(w2, -h2, 0));
			}
			retour = !retour;
		}
		if (!retour)
			pList.add(new Point3d(w2, -h2, 0));

		if (pList.size() >= 2) {
			Toolpath<Point3d> tpZero = new Toolpath<Point3d>(pList);

			// No Transform (debug)
//			this.tp = tpZero;
//			Matrix4d invM = new Matrix4d(m);
//			invM.invert();
//			for (Point3d invP : rect)
//				invM.transform(invP);

			// Transform
			this.tp = tpZero.transformClone(m);

			millingTime = tp.millingTime();
			millingLength = tp.length();
		}
	}

	private double getSOD(double theta, Vector3d n) {
		// calcul du Reff (dans le plan normal à l'avance Vf)
		double R = mc.getCutter().getRadius();
		double r = mc.getCutter().getTipRadius();

		double S = Util.angle0(n, new Vector3d(0.0, 0.0, 1.0)); // slope
		if (S < 0)
			System.err.println("Contre-dépouille");
		Vector3d vf = new Vector3d(cos(theta), sin(theta), 0.0);
		Vector3d projSlope = new Vector3d(n.x, n.y, 0.0);

		double alpha = abs(Util.angle2(vf, projSlope));

		double reff = ((R - r) * pow(cos(alpha), 2)) / (sin(S) * (1 - pow(sin(S), 2) * pow(sin(alpha), 2))) + r;

		// calcul de la distance entre traj (dans le plan normal à l'avance Vf)
		double sh = mc.getScallopHeight();
		double d = 2 * sqrt(2 * reff * sh - pow(sh, 2));

		// calcul du pas sod (dans un plan horizontal, normal à la projection de Vf dans
		// (X,Y))
		Vector3d nxvf = Vectors3.crossV(vf, n);
		Vector3d nxvfProj = new Vector3d(nxvf.x, nxvf.y, 0.0);
		double gamma = Util.angle0(nxvf, nxvfProj);

		double sod = d * cos(gamma);
		return sod;
	}
}