/*
** UVMeshUnitImpl.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the CoSMO software package (CoSMO stands for
** Complex Surfaces Machining Optimization). This software aims to deal
** with optimization of toolpath planning strategy when machining complex
** surfaces.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.ica.cosmo.zoning;

import static java.lang.Math.abs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Locale;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors2;

/**
 * Une classe pour représenter une maille élémentaire en (u,v)
 * 
 * <p>
 * <b>calcOptimalDirection()</b> retourne la direction de plus grande pente
 * (angle par rapport à X)
 * </p>
 * <p>
 * Contient les limites paramétriques de cette maille et la surface à laquelle
 * elle appartient. Destinée à être observée par Zone et SurfaceMap.
 * </p>
 * <p>
 * Les composantes géométriques de la maille (limites paramétriques, coins, etc)
 * ne sont pas destinées à être modifiées.
 * </p>
 * 
 * @author redonnet
 *
 */
public class UVMeshUnit {
	protected final PropertyChangeSupport support = new PropertyChangeSupport(this);
	protected Surface surface;
	protected Index2 index;
	protected double umin;
	protected double umax;
	protected double vmin;
	protected double vmax;
	protected SurfacePoint center; /* centre de la maille */
	protected Vector3d normal; /* normale au centre de la maille */
	private Zone zone; // Should only be modified by changeZone() method

	/**
	 * Constructeur
	 * 
	 * @param surface la surface à laquelle appartient cette maille
	 * @param index   l'{@link Index2} de cette maille
	 * @param umin
	 * @param umax
	 * @param vmin
	 * @param vmax
	 */
	public UVMeshUnit(Surface surface, Index2 index, double umin, double umax, double vmin, double vmax) {
		this.surface = surface;
		this.index = index;
		this.umin = umin;
		this.umax = umax;
		this.vmin = vmin;
		this.vmax = vmax;
		this.center = getCenter();
		this.normal = center.getSurface().normal(center.u, center.v);
	}

	/**
	 * Constructeur
	 * 
	 * @param surface la surface à laquelle appartient cette maille
	 * @param i
	 * @param j
	 * @param umin
	 * @param umax
	 * @param vmin
	 * @param vmax
	 */
	public UVMeshUnit(Surface surface, int i, int j, double umin, double umax, double vmin, double vmax) {
		this(surface, new Index2(i, j), umin, umax, vmin, vmax);
	}

	/**
	 * Clone this onto another surface
	 * 
	 * @param surface
	 */
	public UVMeshUnit cloneOntoSurface(Surface s) {
		UVMeshUnit m = new UVMeshUnit(s, this.index, this.umin, this.umax, this.vmin, this.vmax);
		m.center = m.getCenter();
		m.normal = m.center.getSurface().normal(center.u, center.v);
		return m;
	}

	/**
	 * Retourne la direction de plus grande pente de cette maille
	 * 
	 * La direction de plus grande pente est angle entre la projection de la normale
	 * dans le plan (X,Y) et l'axe X.
	 * 
	 * @return la direction de plus grande pente, en radians, dans l'intervalle
	 *         [-Pi/2,Pi/2]
	 */
	public double getMaxSlopeAngle() {
		Vector2d maxSlopeDir = new Vector2d(-normal.x, -normal.y);
		if (maxSlopeDir.x < 0.0)
			maxSlopeDir.scale(-1.0);

		double maxSlopeAngle = Vectors2.angleV(new Vector2d(1.0, 0.0), maxSlopeDir);

		if (maxSlopeDir.y < 0.0)
			maxSlopeAngle = -Vectors2.angleV(new Vector2d(1.0, 0.0), maxSlopeDir);

		return maxSlopeAngle;
	}

	/**
	 * Retourne la direction optimale d'usinage de cette maille
	 * 
	 * Same as getMaxSlopeAngle(). Kept for historical purpose.
	 * 
	 * @return la direction optimale d'usinage, en radians, dans l'intervalle
	 *         [-Pi,Pi]
	 */
	public double getOptimalDir() {

		return getMaxSlopeAngle();
	}

	/**
	 * Vérifie si un couple (u,v) appartient à cette maille
	 * 
	 * @param u
	 * @param v
	 * @return VRAI si (u,v) appartient à la maille, FAUX dans le cas contraire
	 */
	public boolean includes(double u, double v) {
		if (abs(u - umax) < Util.PREC12)
			u = umax;
		if (abs(u - umin) < Util.PREC12)
			u = umin;
		if (abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (abs(v - vmin) < Util.PREC12)
			v = vmin;

		if (u >= umin && u <= umax && v >= vmin && v <= vmax)
			return true;
		return false;
	}

	/**
	 * Teste si cette maille est connexe de la maille passée en paramètre.
	 * 
	 * Retourne également <b>true</b> si les deux mailles sont identiques.
	 * 
	 * @param uvm
	 * @return
	 */
	public boolean isConnexTo(UVMeshUnit uvm) {
		if (abs(this.index.i - uvm.index.i) <= 1 && abs(this.index.j - uvm.index.j) <= 1)
			return true;
		return false;
	}

	/**
	 * Teste si cette maille est connexe de la maille passée en paramètre
	 * 
	 * Retourne également <b>true</b> si les deux mailles sont identiques.
	 * 
	 * @param uvm
	 * @return
	 */
	public boolean isConnexTo(UVMeshUnit uvm, int maxStep) {
		if (abs(this.index.i - uvm.index.i) <= maxStep && abs(this.index.j - uvm.index.j) <= maxStep)
			return true;
		return false;
	}

	/**
	 * Pour affichage.
	 */
	public String toString() {
		return String.format(Locale.US,
				"(" + index.i + "," + index.j + ") : [(" + String.format("%.3f", umin) + ";"
						+ String.format("%.3f", vmin) + "), " + "(" + String.format("%.3f", umax) + ";"
						+ String.format("%.3f", vmax) + ")]");
	}

	/**
	 * 
	 * @return une approximation de la surface de la maille
	 */
	public double getArea() {
		double area = 0.0;
		Point3d p0 = surface.eval(umin, vmin);
		Point3d p1 = surface.eval(umin, vmax);
		Point3d p2 = surface.eval(umax, vmax);
		Point3d p3 = surface.eval(umax, vmin);

		Vector3d v1 = new Vector3d(p0, p1);
		Vector3d v2 = new Vector3d(p0, p3);
		Vector3d v = new Vector3d();
		v.cross(v1, v2);
		area += v.length() / 2;

		Vector3d v3 = new Vector3d(p2, p3);
		Vector3d v4 = new Vector3d(p2, p1);
		v.cross(v3, v4);
		area += v.length() / 2;

		return area;
	}

	/**
	 * 
	 * @return le centre de la maille
	 */
	public SurfacePoint getCenter() {
		double u = (umin + umax) / 2.0;
		double v = (vmin + vmax) / 2.0;

		return new SurfacePoint(surface, u, v);
	}

	/**
	 * Retourne la normale au centre de la maille
	 * 
	 * @return la normale au centre de la maille
	 */
	public Vector3d getNormal() {
		return normal;
	}

	/**
	 * Retourne les angles de la maille dans un tableau de SurfacePoint
	 * 
	 * <p>
	 * L'ordre du tableau est le suivant :
	 * <ol>
	 * <li>S(umin,vmin)</li>
	 * <li>S(umin,vmax)</li>
	 * <li>S(umax,vmax)</li>
	 * <li>S(umax,vmin)</li>
	 * </ol>
	 * 
	 * @return les points aux angles de la maille
	 */
	public SurfacePoint[] getCorners() {
		SurfacePoint[] result = new SurfacePoint[4];
		double u, v;
		u = umin;
		v = vmin;
		result[0] = new SurfacePoint(surface, u, v);
		u = umin;
		v = vmax;
		result[1] = new SurfacePoint(surface, u, v);
		u = umax;
		v = vmax;
		result[2] = new SurfacePoint(surface, u, v);
		u = umax;
		v = vmin;
		result[3] = new SurfacePoint(surface, u, v);

		return result;
	}

	public ArrayList<SurfacePoint> getCornersAsList() {
		ArrayList<SurfacePoint> result = new ArrayList<SurfacePoint>(4);
		SurfacePoint[] array = this.getCorners();
		for (int i = 0; i < 4; i++)
			result.add(array[i]);

		return result;
	}

	/**
	 * Retourne la pente de la maille par rapport à l'horizontale.
	 * 
	 * En 3 axes, la pente varie entre 0 (usinage horizontal) et Pi/2 (usinage en
	 * Z).
	 * 
	 * La pente de la maille par rapport à la surface est égale à l'angle entre la
	 * normale à la maille et l'axe Z.
	 * 
	 * @return l'angle
	 */
	public double getSlope() {
		Vector3d n = center.getSurface().normal(center.u, center.v);
		Vector3d z = new Vector3d(0.0, 0.0, 1.0);

		return z.angle(n);
	}

	/**
	 * 
	 * @return la valeur mini du paramètre u
	 */
	public double getUmin() {
		return umin;
	}

	/**
	 * 
	 * @return la valeur maxi du paramètre u
	 */
	public double getUmax() {
		return umax;
	}

	/**
	 * 
	 * @return la valeur mini du paramètre v
	 */
	public double getVmin() {
		return vmin;
	}

	/**
	 * 
	 * @return la valeur maxi du paramètre v
	 */
	public double getVmax() {
		return vmax;
	}

	/**
	 * 
	 * @return l'index de la maille élémentaire
	 */
	public Index2 getIndex() {
		return index;
	}

	/**
	 * 
	 * @return la zone à la quelle appartient la maille
	 */
	public Zone getZone() {
		return zone;
	}

	/**
	 * Send a signal to listeners to propagate the new zone event
	 * 
	 * @param oldZone
	 * @param newZone
	 */
	public void changeZone(Zone oldZone, Zone newZone) {
		this.zone = newZone;
		this.removePropertyChangeListener(oldZone);
		this.addPropertyChangeListener(newZone);
		this.support.firePropertyChange("changeZone", oldZone, newZone);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.support.addPropertyChangeListener(listener);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.support.removePropertyChangeListener(listener);
	}
}
