/*
** SurfaceMap.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the CoSMO software package (CoSMO stands for
** Complex Surfaces Machining Optimization). This software aims to deal
** with optimization of toolpath planning strategy when machining complex
** surfaces.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.ica.cosmo.zoning;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.ica.cosmo.machining.APPROX_LEVEL;
import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Matrix4d;

/**
 * Une classe pour manipuler les cartes de zones sur une surface.
 * 
 * Un maillage élémentaire en u,v est créé sur toute la surface paramétrique.
 * Les mailles sont crées en utilisant des UVMeshUnit. Permet d'initialiser les
 * zones en créant soit une seule zone pour toutes les mailles
 * ({@link #init1Zone init1Zone}), soit en créant un nombre fixé de zones
 * ({@link #initNZone initNZone}), soit en créant une zone par maille
 * élémentaire ({@link #initMultiZone initMultiZone}).
 * 
 * <p>
 * Par convention, l'identification des mailles avec des {@link Index2} utilise
 * l'indice i pour la dimension u et l'indice j pour la dimension v
 * 
 * <p>
 * La surfaceMap écoute les signaux émis par les mailles
 * 
 * <p>
 * La surfaceMap écoute les signaux émis par les zones qui la composent
 * 
 * <p>
 * La surfaceMap émet un signal quand le nombre de zones change
 * 
 * @author redonnet
 *
 */
public class SurfaceMap implements PropertyChangeListener, Transformable<SurfaceMap> {
	/**
	 * Observable used to signal a change in the number of zones <br>
	 * 
	 * To signal a UVMeshUnit changing zone, use UVMeshUnit Observable
	 * 
	 */
	protected final PropertyChangeSupport support = new PropertyChangeSupport(this);
	protected Surface surface; // la surface à mailler
	protected int tessU; // nombre de mailles en u
	protected int tessV; // nombre de mailles en v
	protected UVMeshUnit[][] meshGrid; // le maillage complet de la surface

	/**
	 * Liste des isoparamétriques en u. La clé de la map correspond à la valeur de
	 * l'isoparamétrique. On utilise des listes de courbes pour chaque valeur
	 * paramétrique pour faciliter la division de chaque isoparamétrique par zone.
	 */
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoU;

	/**
	 * Liste des isoparamétriques approchées en u. La clé de la map correspond à la
	 * valeur de l'isoparamétrique. On utilise des listes de courbes pour chaque
	 * valeur paramétrique pour faciliter la division de chaque isoparamétrique par
	 * zone.
	 */
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoV;

	/**
	 * Tableau d'entiers de la taille du maillage dans lequel chaque valeur
	 * correspond à l'identifiant de la zone
	 */
	protected Integer[][] zMapIDs;

	/**
	 * Liste des zones avec leur identifiant
	 */
	protected ConcurrentNavigableMap<Integer, Zone> zones;

	/**
	 * Créée une SurfaceMap à partir d'une surface et d'une taille de grille en u et
	 * en v.
	 * 
	 * L'infrastructure est créée, mais aucune zone n'est effectivement construite.
	 * 
	 * @param surface
	 */
	public SurfaceMap(Surface surface, int tessU, int tessV) {
		this.surface = surface;
		this.tessU = tessU;
		this.tessV = tessV;

		createMeshGrid();
	}

	/**
	 * Créée une SurfaceMap à partir d'une surface et d'une taille de maille visée.
	 * 
	 * L'infrastructure est créée, mais aucune zone n'est effectivement construite.
	 * 
	 * @param surface
	 */
	public SurfaceMap(Surface surface, APPROX_LEVEL approxLevel) {
		this.surface = surface;

		// écart entre les points (objectif visé, en mm)
		double d = approxLevel.getValeur();

		// Le nombre d'iso est fixé à partir de d et de la direction x,y,z, dans
		// laquelle la zone est la plus étendue.
		double umin = surface.getUmin();
		double umax = surface.getUmax();
		double vmin = surface.getVmin();
		double vmax = surface.getVmax();

		int n = 10;
		SurfacePoint pinf, psup;

		// Calcul du nombre d'iso : Dans chaque direction (u,v), on estime la longueur
		// de la surface par la moyenne des longueurs de quelques isoparamétriques
		// approchées. Ensuite, pour avoir le nombre d'iso du maillage, on divise cette
		// longueur par la distance voulue pour une maille.
		double uinf, usup, v;
		double uLengthMoy = 0.0; // longueur moyenne des isos en u
		for (int i = 0; i < n; i++) {
			uinf = (double) i / (double) n * (umax - umin) + umin;
			usup = (double) (i + 1) / (double) n * (umax - umin) + umin;
			double uLength = 0; // longueur d'une iso
			for (int j = 0; j <= n; j++) {
				v = (double) j / (double) n * (vmax - vmin) + vmin;
				pinf = new SurfacePoint(surface, uinf, v);
				psup = new SurfacePoint(surface, usup, v);
				uLength = uLength + pinf.distance(psup);
			}
			uLengthMoy = uLengthMoy + uLength;
		}
		uLengthMoy = uLengthMoy / (double) n;
		int nbIsoU = (int) Math.ceil(uLengthMoy / d);

		double vinf, vsup, u;
		double vlengthmoy = 0.0; // longueur moyenne des isos en v
		for (int i = 0; i <= n; i++) {
			u = (double) i / (double) n * (umax - umin) + umin;
			double vlength = 0; // longueur d'une iso
			for (int j = 0; j < n; j++) {
				vinf = (double) j / (double) n * (vmax - vmin) + vmin;
				vsup = (double) (j + 1) / (double) n * (vmax - vmin) + vmin;
				pinf = new SurfacePoint(surface, u, vinf);
				psup = new SurfacePoint(surface, u, vsup);
				vlength = vlength + pinf.distance(psup);
			}
			vlengthmoy = vlengthmoy + vlength;
		}
		vlengthmoy = vlengthmoy / (double)n;
		int nbIsoV = (int) Math.ceil(vlengthmoy / d);
		
		// Workaround to avoid an odd values for tess.
		// KMeans is buggy for odd values of tess
		tessU = nbIsoU % 2 == 0 ? nbIsoU : nbIsoU - 1;
		tessV = nbIsoV % 2 == 0 ? nbIsoV : nbIsoV - 1;

		createMeshGrid();
	}

	public SurfaceMap(SurfaceMap map) {
		this(map.getSurface(), map.tessU, map.tessV);
		this.zMapIDs = map.zMapIDs;

		for (Integer i : map.zones.keySet()) {
			int id = this.addZone();
			for (UVMeshUnit uvm : map.zones.get(i).meshMap.values()) {
				this.getZone(id).add(uvm.cloneOntoSurface(this.surface));
			}
		}

	}

	private void createMeshGrid() {
		meshGrid = new UVMeshUnit[tessU][tessV];

		double umin = surface.getUmin();
		double umax = surface.getUmax();
		double vmin = surface.getVmin();
		double vmax = surface.getVmax();

		double su = (double) (umax - umin) / (double) tessU;
		double sv = (double) (vmax - vmin) / (double) tessV;

		double ulb, uub, vlb, vub;
		// Mapping de la surface en pts carreaux paramétriques
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				ulb = (double) i * su;
				uub = (double) (i + 1) * su;
				vlb = (double) j * sv;
				vub = (double) (j + 1) * sv;

				UVMeshUnit m = new UVMeshUnit(surface, new Index2(i, j), ulb, uub, vlb, vub);
				m.addPropertyChangeListener(this);
				meshGrid[i][j] = m;
			}
		}

		buildIsosLists();

		this.zones = new ConcurrentSkipListMap<Integer, Zone>();
		this.zMapIDs = new Integer[tessU][tessV];
	}

	private void buildIsosLists() {
		this.listIsoU = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
		double u, v;
		SurfacePointsCurve spc;
		ArrayList<SurfacePointsCurve> spcList;
		for (int i = 0; i < tessU; i++) { // pour chaque u
			spc = new SurfacePointsCurve(surface);
			u = meshGrid[i][0].umin;
			for (int j = 0; j < tessV; j++) {
				v = meshGrid[i][j].vmin;
				spc.addPoint(new SurfacePoint(surface, u, v));
			}
			v = meshGrid[i][tessV - 1].vmax;
			spc.addPoint(new SurfacePoint(surface, u, v));
			spcList = new ArrayList<SurfacePointsCurve>();
			spcList.add(spc);
			listIsoU.put(u, spcList);
		}
		spc = new SurfacePointsCurve(surface);
		u = meshGrid[tessU - 1][0].umax;
		for (int j = 0; j < tessV; j++) {
			v = meshGrid[tessU - 1][j].vmin;
			spc.addPoint(new SurfacePoint(surface, u, v));
		}
		v = meshGrid[tessU - 1][tessV - 1].vmax;
		spc.addPoint(new SurfacePoint(surface, u, v));
		spcList = new ArrayList<SurfacePointsCurve>();
		spcList.add(spc);
		listIsoU.put(u, spcList);

		this.listIsoV = new HashMap<Double, ArrayList<SurfacePointsCurve>>();
		for (int j = 0; j < tessV; j++) { // pour chaque v
			spc = new SurfacePointsCurve(surface);
			v = meshGrid[0][j].vmin;
			for (int i = 0; i < tessU; i++) {
				u = meshGrid[i][j].umin;
				spc.addPoint(new SurfacePoint(surface, u, v));
			}
			u = meshGrid[tessU - 1][j].umax;
			spc.addPoint(new SurfacePoint(surface, u, v));
			spcList = new ArrayList<SurfacePointsCurve>();
			spcList.add(spc);
			listIsoV.put(v, spcList);
		}
		spc = new SurfacePointsCurve(surface);
		v = meshGrid[0][tessV - 1].vmax;
		for (int i = 0; i < tessU; i++) {
			u = meshGrid[i][tessV - 1].umin;
			spc.addPoint(new SurfacePoint(surface, u, v));
		}
		u = meshGrid[tessU - 1][tessV - 1].umax;
		spc.addPoint(new SurfacePoint(surface, u, v));
		spcList = new ArrayList<SurfacePointsCurve>();
		spcList.add(spc);
		listIsoV.put(v, spcList);
	}

	/**
	 * Initialisation par défaut : multizone (voir {@link #initMultiZone
	 * initMultiZone})
	 * 
	 * Déprécié. Conservé pour des raisons de compatibilité. Utiliser plutôt
	 * {@link #init1Zone init1Zone}, {@link #initNZone initNZone} ou
	 * {@link #initMultiZone initMultiZone}
	 */
	@Deprecated
	public void init() {
		this.initMultiZone();
	}

	/**
	 * Initialise une SurfaceMap en créant une zone pour chaque maille élémentaire
	 */
	public void initMultiZone() {
		int id;
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				id = i * tessV + j;
				Zone z = new Zone(this, meshGrid[i][j]);
				z.addPropertyChangeListener(this); // Utile pour gérer les zones qui se vident
				zMapIDs[i][j] = id;
				zones.put(id, z);
				this.support.firePropertyChange("nbZones", id, id + 1);
			}
		}
	}

	/**
	 * Initialise une SurfaceMap en créant le nombre de zones passé en paramètre
	 */
	public void initNZone(int n) {
		int id;
		for (int i = 0; i < n; i++) {
			Zone z = new Zone(this);
			zones.put(i, z);
			this.support.firePropertyChange("nbZones", i, i + 1);
		}
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				double centerU = (this.surface.getUmin() + this.surface.getUmax()) / 2.0;
				double centerV = (this.surface.getVmin() + this.surface.getVmax()) / 2.0;
				double u = meshGrid[i][j].getCenter().u;
				double v = meshGrid[i][j].getCenter().v;
				double angle = Math.atan2(v - centerV, u - centerU) + Math.PI;
				id = (int) ((n * angle) / (2 * Math.PI)); // division entière
				meshGrid[i][j].changeZone(null, zones.get(id));
			}
		}
	}

	/**
	 * Initialise une SurfaceMap en créant une seule zone contenant toutes les
	 * mailles élémentaires
	 */
	public void init1Zone() {
		Zone z = new Zone(this);
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				zMapIDs[i][j] = 0;
				meshGrid[i][j].changeZone(null, z);
			}
		}
		zones.put(0, z);
		this.support.firePropertyChange("nbZones", 0, 1);
	}

	/**
	 * Initialise une SurfaceMap en affectant toutes les mailles à la zone passée en
	 * paramètre
	 */
	public void setAllMeshTo(Zone z) {
		z.addPropertyChangeListener(this);
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				zMapIDs[i][j] = 0;
				meshGrid[i][j].changeZone(null, z);
			}
		}
		zones.put(0, z);
		this.support.firePropertyChange("nbZones", 0, 1);
	}

	/**
	 * Ajoute une zone vide
	 * 
	 * @return l'id de la zone créée
	 */
	public int addZone() {
		int id = zones.size();
		Zone z = new Zone(this);
		zones.put(id, z);
		this.support.firePropertyChange("nbZones", id, id + 1);
		return id;
	}

	/**
	 * Supprime une zone.
	 * 
	 * Les autres zones sont réindexées et la table {@link zMapIDs} est mise à jour
	 * en conséquence.
	 * 
	 * @param id l'identifiant de la zone à supprimer
	 */
	private void removeZone(int id) {
		for (Entry<Integer, Zone> e : zones.tailMap(id, false).entrySet()) {
			Integer iz = e.getKey();
			Zone z = e.getValue();
			for (int i = 0; i < tessU; i++) {
				for (int j = 0; j < tessV; j++) {
					if (zMapIDs[i][j] == z.id) {
						zMapIDs[i][j] = zMapIDs[i][j] - 1;
					}
				}
			}
			z.id = z.id - 1;
			zones.replace(iz - 1, z);
		}
		zones.remove(zones.lastKey(), zones.get(zones.lastKey()));
	}

	/**
	 * Réinitialise this
	 */
	public void reset() {
		this.zMapIDs = new Integer[tessU][tessV];
		this.zones = new ConcurrentSkipListMap<Integer, Zone>();
	}

	/**
	 * 
	 * @return le tableau des identifiants de zone
	 */
	public Integer[][] getZMap() {
		return zMapIDs;
	}

	/**
	 * 
	 * @return la carte des zones
	 */
	public Map<Integer, Zone> getZones() {
		return zones;
	}

	/**
	 * Retourne la zone identifiée par l'index passé en paramètre
	 * 
	 * @param i
	 * @return la zone identifiée par l'index passé en paramètre
	 */
	public Zone getZone(Integer i) {
		return zones.get(i);
	}

	/**
	 * Retourne le numéro de la zone identifiée par l'index passé en paramètre
	 * 
	 * @param idx
	 * @return le numéro de la zone identifiée par l'index passé en paramètre
	 */
	public Integer getZoneId(Index2 idx) {
		return getZoneId(idx.i, idx.j);
	}

	/**
	 * Retourne le numéro de la zone identifiée par les indices passés en paramètres
	 * 
	 * @param i
	 * @param j
	 * @return le numéro de la zone identifiée par les indices passés en paramètres
	 */
	public Integer getZoneId(int i, int j) {
		return zMapIDs[i][j];
	}

	/**
	 * Retourne le numéro de la zone contenant le couple (u,v) passé en paramètre.
	 * 
	 * @param u
	 * @param v
	 * @return le numéro de la zone contenant le couple (u,v) passé en paramètre.
	 */
	public Integer getZoneId(double u, double v) {
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				if (meshGrid[i][j].includes(u, v))
					return zMapIDs[i][j];
			}
		}
		return null;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getListIsoU() {
		return listIsoU;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getListIsoV() {
		return listIsoV;
	}

	/**
	 * Change une maille de zone.
	 * 
	 * @param meshUnit
	 * @param newZone
	 */
	public void changeMeshUnitZone(UVMeshUnit meshUnit, Zone newZone) {
		int i = meshUnit.index.i;
		int j = meshUnit.index.j;
		Zone oldZone = meshUnit.getZone();
		meshGrid[i][j].changeZone(oldZone, newZone);
	}

	/**
	 * Retourne la maille contenant le couple (u,v) passé en paramètres
	 * 
	 * @param u
	 * @param v
	 * @return la maille contenant le couple (u,v) passé en paramètres
	 */
	public UVMeshUnit getMeshUnit(double u, double v) {
		for (UVMeshUnit meshLine[] : meshGrid)
			for (UVMeshUnit meshUnit : meshLine) {
				if (meshUnit.includes(u, v))
					return meshUnit;
			}
		return null;
	}

	/**
	 * Retourne la maille indexée par le couple (i,j) passé en paramètres
	 * 
	 * @param i
	 * @param j
	 * @return la maille indexée par le couple (i,j) passé en paramètres
	 */
	public UVMeshUnit getMeshUnit(int i, int j) {
		return meshGrid[i][j];
	}

	/**
	 * Retourne la maille identifié par l'index passé en paramètres
	 * 
	 * @param index
	 * @return la maille identifié par index
	 */
	public UVMeshUnit getMeshUnit(Index2 index) {
		return meshGrid[index.i][index.j];
	}

	/**
	 * 
	 * @return le nombre de zones
	 */
	public int getZonesCount() {
		return zones.size();
	}

	/**
	 * 
	 * @return la surface
	 */
	public Surface getSurface() {
		return surface;
	}

	/**
	 * 
	 * @return la tesselation en u
	 */
	public int getTessU() {
		return tessU;
	}

	/**
	 * 
	 * @return la tesselation en v
	 */
	public int getTessV() {
		return tessV;
	}

	/**
	 * 
	 * @return le maillage de base
	 */
	public UVMeshUnit[][] getMeshGrid() {
		return meshGrid;
	}

	/**
	 * Affiche une cartographie des zones.
	 * 
	 * <p>
	 * DEBUG purpose
	 * </p>
	 */
	public void printZMap() {
		for (int i = 0; i < tessU; i++) {
			System.out.printf("[");
			for (int j = 0; j < tessV; j++) {
				System.out.printf("[%d]", zMapIDs[i][j]);
			}
			System.out.println("]");
		}
	}

	public void clean() {
		for (Map.Entry<Integer, Zone> entry : zones.entrySet()) {
			Integer id = (Integer) entry.getKey();
			Zone z = (Zone) entry.getValue();
			if (z.isEmpty()) {
				int n = zones.size();
				zones.remove(id);
				this.support.firePropertyChange("nbZones", n, n - 1);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getSource().getClass() == Zone.class) {
			Zone z = (Zone) event.getSource();
			if (event.getPropertyName() == "emptyZone") {
				removeZone(z.id);
			}
		}
		if (event.getSource().getClass() == UVMeshUnit.class) {
			UVMeshUnit m = (UVMeshUnit) event.getSource();
			if (event.getPropertyName() == "changeZone") {
				Zone z = (Zone) event.getNewValue();
				zMapIDs[m.index.i][m.index.j] = z.id;
			}
		}
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.support.addPropertyChangeListener(listener);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.support.removePropertyChangeListener(listener);
	}

	/**
	 * Transform is not safe for SurfaceMap.
	 * 
	 * TransformClone should be considered instead.
	 * 
	 * @param m
	 */
	@Override
	public void transform(Matrix4d m) {
		System.err.println("Transform is not safe for SurfaceMap. TransformClone should be considered instead.");
	}

	@Override
	public SurfaceMap transformClone(Matrix4d m) {
		Surface s = this.surface.transformClone(m);
		SurfaceMap map = new SurfaceMap(s, tessU, tessV);
		map.zMapIDs = this.zMapIDs;

		for (Integer i : this.zones.keySet()) {
			int id = map.addZone();
			for (UVMeshUnit uvm : this.zones.get(i).meshMap.values()) {
				map.getZone(id).add(uvm.cloneOntoSurface(s));
			}
		}

		return map;
	}
}
