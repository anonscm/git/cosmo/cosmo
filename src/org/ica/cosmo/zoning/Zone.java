/*
** Zone.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the CoSMO software package (CoSMO stands for
** Complex Surfaces Machining Optimization). This software aims to deal
** with optimization of toolpath planning strategy when machining complex
** surfaces.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.ica.cosmo.zoning;

import static java.lang.Math.sqrt;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.ica.cosmo.clustering.DataPoint;
import org.ica.cosmo.machining.AveragePDE;
import org.ica.cosmo.machining.PrefDirEvaluator;
import org.ica.cosmo.machining.SinglePointPDE;
import org.ica.support.Support;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.shapes.Rectangle;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

/**
 * Une classe pour représenter une zone.
 * 
 * Une zone est constituée d'un ensemble de mailles élémentaires connexes.
 * Chaque zone doit également être associée à une {@link SurfaceMap} qui gère le
 * mapping des zones sur la surface.
 * 
 * <p>
 * Par convention, l'identification des mailles avec des {@link Index2} utilise
 * l'indice i pour la dimension u et l'indice j pour la dimension v
 * 
 * <p>
 * Une zone écoute les signaux émis par les mailles qui la composent
 * 
 * @author redonnet
 *
 */
public class Zone implements PropertyChangeListener {
	protected final PropertyChangeSupport support = new PropertyChangeSupport(this);
	/** identifiant de la zone */
	protected Integer id;
	/** liste des mailles de la zone */
	protected HashMap<Index2, UVMeshUnit> meshMap;
	protected final SurfaceMap map;
	private PrefDirEvaluator prefDirEvaluator = null;
	/** Internal flag to recalculate eigen decomposition only when needed */
	private boolean eigenSync = false;
	/** First eigenvector of the zone. Always in the most extended direction */
	protected Vector3d eigenVectorI;
	/** Second eigenvector. Normal to eigenVectorI in the best fitting plan */
	protected Vector3d eigenVectorII;
	/**
	 * Third eigenvector of the zone. Normal to the plane best fitting the zone.
	 * Always oriented Z+
	 */
	protected Vector3d eigenVectorIII;
	/** First eigenvalue of the zone. Corresponds to first eigen vector */
	protected double eigenValueI;
	/** Second eigenvalue of the zone. Corresponds to second eigen vector */
	protected double eigenValueII;
	/** Third eigenvalue of the zone. Corresponds to third eigen vector */
	protected double eigenValueIII;
	/** median value of mesh coordinates */
	protected Point3d medianPoint;
	/**
	 * Liste des isoparamétriques en u. La clé de la map correspond à la valeur de
	 * l'isoparamétrique.
	 */
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoU;
	private boolean syncListIsoU = false;
	/**
	 * Liste des isoparamétriques en v. La clé de la map correspond à la valeur de
	 * l'isoparamétrique.
	 */
	private Map<Double, ArrayList<SurfacePointsCurve>> listIsoV;
	private boolean syncListIsoV = false;

	/**
	 * Rectangle approximant au mieux la zone
	 */
	private Rectangle approxRectangle;

	/**
	 * Matrice de position/orientation du rectangle approximant au mieux la zone.
	 * Définie par rapport à un rectangle horizontal centré
	 */
	private Matrix4d approxPlaneTransform;

	/**
	 * Crée une nouvelle zone vide
	 * 
	 * @param map
	 */
	public Zone(SurfaceMap map) {
		this.map = map;
		this.id = map.getZonesCount();
		this.meshMap = new HashMap<Index2, UVMeshUnit>();
		this.addPropertyChangeListener(map); // Utile pour gérer les zones qui se vident
		this.listIsoU = new TreeMap<Double, ArrayList<SurfacePointsCurve>>();
		this.listIsoV = new TreeMap<Double, ArrayList<SurfacePointsCurve>>();
	}

	/**
	 * Créée une nouvelle zone à partir d'une seule maille
	 * 
	 * @param map
	 * @param meshUnit
	 */
	public Zone(SurfaceMap map, UVMeshUnit meshUnit) {
		this(map);
		add(meshUnit);
	}

	private void updateListIsoU() {
		for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> e : map.getListIsoU().entrySet()) {
			Double u = e.getKey();
			ArrayList<SurfacePointsCurve> zIsoU = new ArrayList<SurfacePointsCurve>();
			for (SurfacePointsCurve spc : e.getValue()) {
				ArrayList<SurfacePoint> pList = new ArrayList<SurfacePoint>();
				for (SurfacePoint p : spc.getPoints()) {
					if (this.includes(p.u, p.v)) {
						pList.add(new SurfacePoint(p));
						continue;
					} else if (pList.size() >= 2) {
						zIsoU.add(new SurfacePointsCurve(pList));
						pList = new ArrayList<SurfacePoint>();
					}
				}
				if (pList.size() >= 2) {
					zIsoU.add(new SurfacePointsCurve(pList));
				}
			}
			this.listIsoU.put(u, zIsoU);
		}
	}

	private void updateListIsoV() {
		for (Map.Entry<Double, ArrayList<SurfacePointsCurve>> e : map.getListIsoV().entrySet()) {
			Double v = e.getKey();
			ArrayList<SurfacePointsCurve> zIsoV = new ArrayList<SurfacePointsCurve>();
			for (SurfacePointsCurve spc : e.getValue()) {
				Iterator<SurfacePoint> it = spc.getPoints().iterator();
				ArrayList<SurfacePoint> pList = new ArrayList<SurfacePoint>();
				while (it.hasNext()) {
					SurfacePoint p = it.next();
					if (this.includes(p.u, p.v))
						pList.add(new SurfacePoint(p));
					else if (pList.size() >= 2) {
						zIsoV.add(new SurfacePointsCurve(pList));
						pList = new ArrayList<SurfacePoint>();
					}
					if (!it.hasNext()) // dernier point
						if (pList.size() >= 2)
							zIsoV.add(new SurfacePointsCurve(pList));
				}
			}
			this.listIsoV.put(v, zIsoV);
		}
	}

	/**
	 * 
	 * @return les mailles de la zone
	 */
	public HashMap<Index2, UVMeshUnit> getMeshmap() {
		return meshMap;
	}

	/**
	 * Teste si la zone contient la maille identifiée par l'index passé en
	 * paramètre.
	 * 
	 * @param index
	 * @return <b>true</b> si la zone contient la maille identifiée par l'index
	 *         passé en paramètre, <b>false</b> dans le cas contraire
	 */
	public boolean includes(Index2 index) {
		boolean result = false;
		if (index.i >= 0 && index.i < map.tessU && index.j >= 0 && index.j < map.tessV)
			if (map.zMapIDs[index.i][index.j] == this.id)
				result = true;
		return result;
	}

	/**
	 * Teste si la zone contient la maille à laquelles appartiennent les coordonnées
	 * (u,v) passées en paramètres.
	 * 
	 * @param u
	 * @param v
	 * @return <b>true</b> si la zone contient le point (u,v) passé en paramètre,
	 *         <b>false</b> dans le cas contraire
	 */
	public boolean includes(double u, double v) {
		boolean result = false;
		for (UVMeshUnit uvm : meshMap.values())
			if (uvm.includes(u, v)) {
				result = true;
				break;
			}
		return result;
	}

	public double distanceFrom(double u, double v) {
		if (this.includes(u, v))
			return 0.0;
		double dist = Double.MAX_VALUE;
		Surface s = map.getSurface();
		SurfacePoint pRef = new SurfacePoint(s, u, v);
		for (UVMeshUnit uvm : meshMap.values())
			for (SurfacePoint p : uvm.getCornersAsList()) {
				double d = pRef.distance(p);
				if (d < dist)
					dist = d;
			}
		return dist;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getListIsoU() {
		if (!syncListIsoU) {
			updateListIsoU();
			syncListIsoU = true;
		}
		return listIsoU;
	}

	public Map<Double, ArrayList<SurfacePointsCurve>> getListIsoV() {
		if (!syncListIsoV) {
			updateListIsoV();
			syncListIsoV = true;
		}
		return listIsoV;
	}

	/**
	 * Retourne la maille qui contient les coordonnées (u,v) passées en paramètres
	 * 
	 * Retourne <b>null</b> si aucune maille de la zone ne contient ces coordonnées.
	 * 
	 * @param u
	 * @param v
	 * @return
	 */
	public UVMeshUnit getUVMeshUnit(double u, double v) {
		UVMeshUnit result = null;
		for (UVMeshUnit uvm : meshMap.values()) {
			if (uvm.includes(u, v))
				result = uvm;
		}
		return result;
	}

	public UVMeshUnit getUVMeshUnit(Index2 index) {
		return meshMap.get(index);
	}

	/**
	 * Retourne l'identifiant de la zone
	 * 
	 * @return l'identifiant de la zone
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Supprime une maille de la zone
	 * 
	 * <p>
	 * ATTENTION : Aucune vérification n'est faite sur la connexité de la zone.
	 * </p>
	 * 
	 * @param meshUnit
	 */
	protected void remove(UVMeshUnit meshUnit) {
		meshMap.remove(meshUnit.index);
		meshUnit.removePropertyChangeListener(this);
		eigenSync = false;
		syncListIsoU = false;
		syncListIsoV = false;
	}

	/**
	 * Ajoute une maille à la zone
	 * 
	 * <p>
	 * ATTENTION : Aucune vérification n'est faite sur la connexité de la zone.
	 * </p>
	 * 
	 * @param meshUnit
	 */
	protected void add(UVMeshUnit meshUnit) {
		meshMap.put(meshUnit.index, meshUnit);
		meshUnit.addPropertyChangeListener(this);
		eigenSync = false;
		syncListIsoU = false;
		syncListIsoV = false;
	}

	/**
	 * Définit l'évaluateur qui va permettre de déterminer la direction optimale
	 * d'usinage pour cette zone.
	 * 
	 * @param evaluator
	 */
	public void setEvaluator(PrefDirEvaluator evaluator) {
		this.prefDirEvaluator = evaluator;
	}

	/**
	 * Evaluates the preferred machining direction using the current
	 * prefDirEvaluator.
	 * 
	 * If no prefDirEvaluator have been set, the default PrefDirEvaluator, which is
	 * {@link AveragePDE}, is used
	 * 
	 * @return the current preferred machining direction
	 */
	public double evalPrefDir() {
		if (prefDirEvaluator == null)
			this.prefDirEvaluator = new AveragePDE(this);
		return prefDirEvaluator.eval();
	}

	/**
	 * Evaluates the preferred machining direction using the SinglePointPDE on
	 * Datapoint d
	 * 
	 * @return the current preferred machining direction
	 */
	public double evalPrefDir(DataPoint d) {
		prefDirEvaluator = new SinglePointPDE(this, d);
		return prefDirEvaluator.eval();
	}

	/**
	 * Evaluates the preferred machining direction using the current
	 * prefDirEvaluator and the given initial guess.
	 * 
	 * No specific implementation for moment. Returns {@link #evalPrefDir()}
	 * 
	 * @return the current preferred machining direction
	 */
	public double evalPrefDir(Double initialGuess) {
		return evalPrefDir();
	}

	/**
	 * Calculates the center of the zone.
	 * 
	 * @return the center of the zone
	 */
	public SurfacePoint getCenter() {
		double umean = 0.0;
		double vmean = 0.0;

		for (UVMeshUnit m : meshMap.values()) {
			umean += m.getCenter().u;
			vmean += m.getCenter().v;
		}
		umean = umean / meshMap.size();
		vmean = vmean / meshMap.size();

		return new SurfacePoint(this.getSurfaceMap().surface, umean, vmean);
	}

	/**
	 * Vérifie la connexité entre this et la zone passée en paramètres
	 * 
	 * @param z la zone à tester
	 * 
	 * @return <b>true</b> si z est connexe à <b>this</b>. <b>false</b> dans le cas
	 *         contraire
	 */
	public boolean isNext(Zone z) {
		if (z.isEmpty() || this.isEmpty())
			return false;

		for (UVMeshUnit m : meshMap.values()) {
			for (UVMeshUnit mz : z.meshMap.values()) {
				if (m.index.isNextTo(mz.index))
					return true;
			}
		}

		return false;
	}

	/**
	 * Teste si une zone est vide.
	 * 
	 * @return <b>true</b> si la zone est vide, <b>false</b> dans le cas contraire
	 */
	public boolean isEmpty() {
		if (this.meshMap.size() == 0)
			return true;
		return false;
	}

	/**
	 * Retourne la SurfaceMap à laquelle appartient cette zone
	 * 
	 * @return la SurfaceMap à laquelle appartient cette zone
	 */
	public SurfaceMap getSurfaceMap() {
		return map;
	}

	/**
	 * Returns rows of the zone.
	 * 
	 * @return an HashMap where key is the index of the row and value is a SortedMap
	 *         of the {@link UVMeshUnit} of this row belonging to this
	 */
	public HashMap<Integer, SortedMap<Index2, UVMeshUnit>> getRows() {
		TreeMap<Index2, UVMeshUnit> smm = new TreeMap<Index2, UVMeshUnit>(new Index2.iComparator()); // sorted MeshMap
		smm.putAll(this.getMeshmap());

		Index2 idx = smm.firstKey();
		int ideb = idx.i;
		HashMap<Integer, SortedMap<Index2, UVMeshUnit>> list = new HashMap<Integer, SortedMap<Index2, UVMeshUnit>>();
		while (ideb <= smm.lastKey().i) {
			// on récupère l'ensemble des mailles ayant le même indice ideb
			// index i is always along u
			Index2 lastKey = smm.floorKey(new Index2(idx.i, this.getSurfaceMap().getTessU() + 1));
			list.put(ideb, smm.subMap(idx, true, lastKey, true));
			ideb++;
			idx = smm.ceilingKey(new Index2(ideb, 0));
		}
		return list;
	}

	/**
	 * Returns columns of the zone.
	 * 
	 * @return an HashMap where key is the index of the column and value is a
	 *         SortedMap of the {@link UVMeshUnit} of this column belonging to this
	 */
	public HashMap<Integer, SortedMap<Index2, UVMeshUnit>> getColumns() {
		TreeMap<Index2, UVMeshUnit> smm = new TreeMap<Index2, UVMeshUnit>(new Index2.jComparator()); // sorted MeshMap
		smm.putAll(this.getMeshmap());

		Index2 idx = smm.firstKey();
		int jdeb = idx.j;
		HashMap<Integer, SortedMap<Index2, UVMeshUnit>> list = new HashMap<Integer, SortedMap<Index2, UVMeshUnit>>();
		while (jdeb <= smm.lastKey().j) {
			// on récupère l'ensemble des mailles ayant le même indice jdeb
			// index j is always along v
			Index2 lastKey = smm.floorKey(new Index2(this.getSurfaceMap().getTessV() + 1, idx.j));
			list.put(jdeb, smm.subMap(idx, true, lastKey, true));
			jdeb++;
			idx = smm.ceilingKey(new Index2(0, jdeb));
		}
		return list;
	}

	/**
	 * Calcule la matrice de covariance et effectue sa décomposition en valeurs
	 * propres / vecteurs propres.
	 * 
	 * Met le flag eigenSync à <b>true</b>
	 */
	private void calc_covMat() {
		RealMatrix data = new Array2DRowRealMatrix(meshMap.size(), 3);
		int im = 0;
		double sumX = 0.0, sumY = 0.0, sumZ = 0.0;
		for (UVMeshUnit m : meshMap.values()) {
			sumX = sumX + m.getCenter().x;
			sumY = sumY + m.getCenter().y;
			sumZ = sumZ + m.getCenter().z;
			data.setEntry(im, 0, m.getCenter().x);
			data.setEntry(im, 1, m.getCenter().y);
			data.setEntry(im, 2, m.getCenter().z);
			im++;
		}

		this.medianPoint = new Point3d(sumX / (double) meshMap.size(), sumY / (double) meshMap.size(),
				sumZ / (double) meshMap.size());

		Covariance cov = new Covariance(data);
		RealMatrix covMat = cov.getCovarianceMatrix();

		EigenDecomposition eigdc = new EigenDecomposition(covMat);
		ArrayList<Double> values = new ArrayList<Double>();
		ArrayList<Vector3d> vectors = new ArrayList<Vector3d>();
		for (int i = 0; i < 3; i++) {
			values.add(i, eigdc.getRealEigenvalue(i));
			vectors.add(i, new Vector3d(eigdc.getEigenvector(i).toArray()));
		}
		eigenValueI = values.get(0);
		eigenValueII = values.get(1);
		eigenValueIII = values.get(2);
		eigenVectorI = vectors.get(0);
		eigenVectorII = vectors.get(1);
		eigenVectorIII = vectors.get(2);

		if (eigenVectorI.x < 0) {
			// System.err.println("Warning: unhandled case, eigenVectorI.x < 0 :");
			eigenVectorI.scale(-1.0);
		}

		// test if the provided base (eI, eII, eIII) is direct
		Vector3d testDirect = Vectors3.crossV(eigenVectorI, eigenVectorII);
		if (eigenVectorIII.angle(testDirect) > Math.PI / 2.0)
			eigenVectorIII.scale(-1.0);

		if (eigenVectorII.y < 0) {
			eigenVectorII.scale(-1.0);
			eigenVectorIII.scale(-1.0);
		}

		if (eigenVectorIII.z < 0) {
			eigenVectorII.scale(-1.0);
			eigenVectorIII.scale(-1.0);
		}

		// on oriente le repère pour que le X soit toujours montant
//		if (eigenVectorI.z < 0) {
//			eigenVectorI.scale(-1.0);
//			eigenVectorII.scale(-1.0);
//		}

		calcApproxPlaneTransform();

		eigenSync = true;
	}

	public Vector3d getEigenVectorI() {
		if (eigenSync == false)
			calc_covMat();
		return eigenVectorI;
	}

	public Vector3d getEigenVectorII() {
		if (eigenSync == false)
			calc_covMat();
		return eigenVectorII;
	}

	public Vector3d getEigenVectorIII() {
		if (eigenSync == false)
			calc_covMat();
		return eigenVectorIII;
	}

	public double getEigenValueI() {
		if (eigenSync == false)
			calc_covMat();
		return eigenValueI;
	}

	public double getEigenValueII() {
		if (eigenSync == false)
			calc_covMat();
		return eigenValueII;
	}

	public double getEigenValueIII() {
		if (eigenSync == false)
			calc_covMat();
		return eigenValueIII;
	}

	public Point3d getMedianPoint() {
		if (eigenSync == false)
			calc_covMat();
		return medianPoint;
	}

	private void calcApproxPlaneTransform() {
		Matrix4d m = new Matrix4d();
		Matrix3d rotation = new Matrix3d();
		rotation.setColumn(0, eigenVectorI);
		rotation.setColumn(1, eigenVectorII);
		rotation.setColumn(2, eigenVectorIII);
		m.setRotation(rotation);
		Vector3d translation = new Vector3d(medianPoint);
		m.setTranslation(translation);

		this.approxPlaneTransform = m;

		this.approxRectangle = new Rectangle(medianPoint, eigenVectorIII, eigenVectorI, 2 * sqrt(3 * eigenValueI),
				2 * sqrt(3 * eigenValueII));
	}

	public Rectangle getApproxRectangle() {
		if (eigenSync == false)
			calc_covMat();
		return approxRectangle;
	}

	/**
	 * Retourne le rectangle plan qui approxime au mieux la zone.
	 * 
	 * Ce plan est defini par les vecteurs propres eigenVectorI et eigenVectorII et
	 * les longueurs sqrt(12*eigenValueI) selon eigenVectorI et
	 * sqrt(12*eigenValueII) selon eigenVectorII.
	 *
	 * Le plan est défini par ses 4 angles p00, p01, p11 et p10. Il est créé centré
	 * à l'origine et aligné selon X, puis transformé pour s'aligner avec la zone.
	 * 
	 * <pre>
	 * p00 --- p01
	 *  | (0,0) |   -->X
	 * p10 --- p11
	 * </pre>
	 * 
	 * @return
	 */
	public ArrayList<Point3d> getApproxPlane() {
		if (eigenSync == false)
			calc_covMat();
		Vector3d normal = new Vector3d(eigenVectorIII);
		normal.normalize();

		// les demi-longueurs sont sqrt(12*eigenValueX) / 2 = sqrt(3*eigenValueX)
		Point3d p00 = new Point3d(-sqrt(3.0 * eigenValueI), sqrt(3.0 * eigenValueII), 0);
		Point3d p01 = new Point3d(sqrt(3.0 * eigenValueI), sqrt(3.0 * eigenValueII), 0);
		Point3d p10 = new Point3d(-sqrt(3.0 * eigenValueI), -sqrt(3.0 * eigenValueII), 0);
		Point3d p11 = new Point3d(sqrt(3.0 * eigenValueI), -sqrt(3.0 * eigenValueII), 0);

		this.approxPlaneTransform.transform(p00);
		this.approxPlaneTransform.transform(p01);
		this.approxPlaneTransform.transform(p10);
		this.approxPlaneTransform.transform(p11);

		ArrayList<Point3d> corners = new ArrayList<Point3d>();
		corners.add(p00);
		corners.add(p01);
		corners.add(p11);
		corners.add(p10);

		return corners;
	}

	public Matrix4d getApproxPlaneTransform() {
		if (eigenSync == false)
			this.calc_covMat();

		return (Matrix4d) approxPlaneTransform.clone();
	}

	/**
	 * Retourne la liste des mailles au bord de la zone.
	 * 
	 * Est considérée comme maille au bord de la zone, toute maille de la zone qui
	 * est en contact avec une maille n'appartenant pas à la zone.
	 * 
	 * Le test est effectué par rapport aux limites de la surface et aux limites
	 * entre les zones.
	 * 
	 * @return liste indexée des mailles au bord de la zone.
	 */
	public HashMap<Index2, UVMeshUnit> getBorder() {
		HashMap<Index2, UVMeshUnit> border = new HashMap<Index2, UVMeshUnit>();
		for (UVMeshUnit uvm : meshMap.values()) {
			int i = uvm.getIndex().i;
			int j = uvm.getIndex().j;
			if (!this.includes(new Index2(i + 1, j)) || !this.includes(new Index2(i + 1, j + 1))
					|| !this.includes(new Index2(i, j + 1)) || !this.includes(new Index2(i - 1, j + 1))
					|| !this.includes(new Index2(i - 1, j)) || !this.includes(new Index2(i - 1, j - 1))
					|| !this.includes(new Index2(i, j - 1)) || !this.includes(new Index2(i + 1, j - 1))) {
				border.put(new Index2(i, j), uvm);
				continue;
			}
		}
		return border;
	}

	public List<SurfacePoint> getPoints() {
		List<SurfacePoint> list = new ArrayList<SurfacePoint>();

		return list;
	}

	/**
	 * Gère les événnements signalés par les objets observés.
	 * 
	 * Pour l'instant :
	 * <ul>
	 * <li>changement de zone d'une maille (event "changeZone") émis par les
	 * mailles</li>
	 * </ul>
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		UVMeshUnit meshUnit = (UVMeshUnit) event.getSource();
		if (event.getPropertyName() == "changeZone") {
			if (event.getNewValue() != null) {
				Zone newZone = (Zone) event.getNewValue();
				if (newZone.equals(this))
					this.add(meshUnit);
			}
			if (event.getOldValue() != null) {
				Zone oldZone = (Zone) event.getOldValue();
				if (oldZone.equals(this)) {
					this.remove(meshUnit);
					if (this.meshMap.size() == 0) {
						this.support.firePropertyChange("emptyZone", null, null);
					}
				}
			}
		}
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.support.addPropertyChangeListener(listener);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.support.removePropertyChangeListener(listener);
	}

	public void print() {
		System.out.println("Zone " + id);
		for (UVMeshUnit m : meshMap.values())
			System.out.println(m.toString());
	}

}