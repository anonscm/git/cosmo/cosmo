package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique SN (liu 2018)
 * 
 * @author redonnet
 *
 */
public class SNFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new SNData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new SNData(m));
	}
	
}