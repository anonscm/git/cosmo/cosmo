package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique Covar3D
 * 
 * @author redonnet
 *
 */
public class Covar3DFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new Covar3DData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new Covar3DData(m));
	}

}