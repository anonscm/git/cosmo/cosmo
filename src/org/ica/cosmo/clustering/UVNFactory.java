package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique UVN (Roman 2005)
 * 
 * @author redonnet
 *
 */
public class UVNFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new UVNData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new UVNData(m));
	}
}
