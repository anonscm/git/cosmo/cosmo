package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.ica.cosmo.zoning.UVMeshUnit;

//import Jama.Matrix;

/**
 * Un point de données pour la métrique de Mahalanobis, distance=values^T x
 * CovMat^-1 x values
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : pente</li>
 * <li>values[3] : angle entre la direction de plus grande pente et l'axe des X
 * </li>
 * </ul>
 * 
 * @author redonnet
 *
 */
public class MahalanobisData extends DataPoint {
	static {
		size = 4;
		isize = 4;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public MahalanobisData() {
		super();
	}

	public MahalanobisData(double[] values) {
		super(values);
	}

	public MahalanobisData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getSlope();
		values[3] = m.getOptimalDir();
	}

	/**
	 * @return a clone of the data
	 */
	public MahalanobisData clone() {
		MahalanobisData copy = new MahalanobisData(values);
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double distance(DataPoint o) {
		Cluster cls = o.getCluster();
		double dist, sum = 0.0;

//		if (cls == null) {
//			for (int i = 0; i < size; i++)
//				sum += Math.pow(values[i] - o.values[i], 2);
//		} else if (cls.getCovmat().isZero()) {
//			for (int i = 0; i < size; i++)
//				sum += Math.pow(values[i] - o.values[i], 2);
//		} else {
			// using the covariance matrix of each zone
//			GVector x = new GVector(values);
//			x.sub(new GVector(o.values));
//			GMatrix invCovMat = cls.getCovmat();
//			invCovMat.invert();
//			GVector tmp = new GVector(x.getSize());
//			tmp.mul(invCovMat, x);
//			sum = x.dot(tmp);
			// using the global covariance matrix
			double x_array[][] = {Arrays.copyOf(values, size)};
			for (int i = 0; i < size; i++)
				x_array[0][i] = x_array[0][i]-o.values[i];
			RealMatrix X = new Array2DRowRealMatrix(x_array);
//			Matrix test = X.times(X.transpose());
//			sum = test.get(0, 0);
			sum = X.multiply(MatrixUtils.inverse(gcov)).multiply(X.transpose()).getEntry(0, 0);
//		}
		dist = Math.sqrt(sum);
		return dist;
	}
}