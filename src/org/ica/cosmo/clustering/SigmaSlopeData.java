package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Un point de données pour la métrique SigmaSlope qui utilise les écarts types
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : pente de la surface en (u,v)</li>
 * <li>values[3] : angle entre la direction de plus grande pente et l'axe des X
 * </li>
 * </ul>
 * 
 * la distance est calculée comme la somme des values divisées par les écarts
 * types
 * 
 * @author Mahfoud
 *
 */
public class SigmaSlopeData extends DataPoint {
	static {
		size = 4;
		isize = 4;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public SigmaSlopeData() {
		super();
	}

	public SigmaSlopeData(double[] values) {
		super(values);
	}

	public SigmaSlopeData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getSlope();
		values[3] = m.getOptimalDir();
	}

	@Override
	public DataPoint clone() {
		SigmaSlopeData copy = new SigmaSlopeData(getValues());
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double distance(DataPoint o) {
		Cluster cluster = o.getCluster();
		double dist, sum = 0;
		// standard deviations of each zone
//		if (cluster == null) {
//			for (int i = 0; i < size; i++)
//				sum += Math.pow(values[i] - o.values[i], 2);
//		} else {
//			// récupérer les écarts type de la zone
//			double[] sigmacluster = { Math.sqrt(cluster.getCovmat().getElement(0, 0)), 
//					Math.sqrt(cluster.getCovmat().getElement(1, 1)),
//					Math.sqrt(cluster.getCovmat().getElement(2, 2)),
//					Math.sqrt(cluster.getCovmat().getElement(3, 3)) };
//
//			if (sigmacluster[0] * sigmacluster[1] * sigmacluster[2] * sigmacluster[3] == 0.0) {
//				for (int i = 0; i < size; i++)
//					sum += Math.pow(values[i] - o.values[i], 2);
//			} else {
//				for (int i = 0; i < size; i++)
//					sum += Math.pow(values[i] - o.values[i], 2) / sigmacluster[i];
//			}
//		}
		// global standard deviation
		for (int i = 0; i < size; i++)
			sum += Math.pow(values[i] - o.values[i], 2) / Math.sqrt(gcov.getEntry(i, i));
		if (cluster == null) {
			for (int i = 0; i < size; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else {
			// récupérer les écarts type de la zone
			double[] sigmacluster = { Math.sqrt(cluster.getCovmat().getElement(0, 0)),
					Math.sqrt(cluster.getCovmat().getElement(1, 1)), Math.sqrt(cluster.getCovmat().getElement(2, 2)),
					Math.sqrt(cluster.getCovmat().getElement(3, 3)) };

			if (sigmacluster[0] * sigmacluster[1] * sigmacluster[2] * sigmacluster[3] == 0.0) {
				for (int i = 0; i < size; i++)
					sum += Math.pow(values[i] - o.values[i], 2);
			} else {
				for (int i = 0; i < size; i++)
					sum += Math.pow(values[i] - o.values[i], 2) / sigmacluster[i];
			}
		}
		dist = Math.sqrt(sum);
		return dist;
	}

}
