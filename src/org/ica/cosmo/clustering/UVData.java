package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Un point de données pour la métrique UV
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * </ul>
 * 
 * @author redonnet
 *
 */
public class UVData extends DataPoint {
	static {
		size = 2;
		isize = 2;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public UVData() {
		super();
	}

	public UVData(double[] values) {
		super(values);
	}

	public UVData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
	}

	@Override
	public DataPoint clone() {
		UVData copy = new UVData(getValues());
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double getMaxSlopeAngle() {
		double u = getValues()[0];
		double v = getValues()[1];
		Vector3d n = surface.normal(u, v);
		Vector2d maxSlope;
		if (n.x < 0)
			maxSlope = new Vector2d(-n.x, -n.y);
		else
			maxSlope = new Vector2d(n.x, n.y);

		double maxSlopeAngle = maxSlope.angle(new Vector2d(1.0, 0.0));
		if (maxSlope.y < 0)
			maxSlopeAngle = -maxSlopeAngle;
		return maxSlopeAngle;
	}

}
