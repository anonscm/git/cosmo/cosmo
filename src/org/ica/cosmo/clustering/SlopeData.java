package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Un point de données pour la métrique Slope (distance euclidienne)
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : pente de la surface en (u,v)</li>
 * <li>values[3] : angle entre la direction de plus grande pente et l'axe des X
 * </li>
 * </ul>
 * 
 * @author redonnet
 *
 */
public class SlopeData extends DataPoint {
	static {
		size = 4;
		isize = 4;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public SlopeData() {
		super();
	}

	public SlopeData(double[] values) {
		super(values);
	}

	public SlopeData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getSlope();
		values[3] = m.getOptimalDir();
	}

	@Override
	public DataPoint clone() {
		SlopeData copy = new SlopeData(getValues());
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

}
