package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique Slope (see {@link METRIC} and {@link SlopeData})
 * 
 * @author redonnet
 *
 */
public class SlopeFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new SlopeData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new SlopeData(m));
	}

}
