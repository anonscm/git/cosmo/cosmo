package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;

public class UVNData extends DataPoint {
	static {
		size = 5;
		isize = 5;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public UVNData() {
		super();
	}

	public UVNData(double[] values) {
		super(values);
	}

	public UVNData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = this.surface.normal(values[0], values[1]).x;
		values[3] = this.surface.normal(values[0], values[1]).y;
		values[4] = this.surface.normal(values[0], values[1]).z;
	}

	/**
	 * @return a clone of the data
	 */
	public UVNData clone() {
		UVNData copy = new UVNData(values);
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}
}
