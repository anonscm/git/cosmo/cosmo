package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.HashMap;

import org.ica.cosmo.zoning.Index2;

/**
 * An abstraction of clustering algorithm.
 *
 * <p>
 * The actual clustering process should be defined in the method {@link run} of
 * implementations.
 */
public abstract class AbstractAlgo {
	/** list of data */
	protected HashMap<Index2,DataPoint> dataSet = new HashMap<Index2,DataPoint>();

	/** list of clusters */
	protected ArrayList<Cluster> clusters = new ArrayList<>();

	/**
	 * Common constructor operations for clustering algorithms.
	 * 
	 * @param dataSet  a set of {@link DataPoint Datapoints} to perform clustering
	 * @param clusters the initial clusters
	 */
	public AbstractAlgo(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super();
		this.dataSet = dataSet;
		this.clusters = clusters;
	}

	/**
	 * Search the cluster, the DataPoint data should belong to.
	 * 
	 * <p>
	 * Among all clusters, select the cluster whose the centroid is the closest from
	 * data (in the sense of DataPoint metric).
	 * 
	 * @param data a DataPoint
	 * @return the selected cluster
	 */
	protected Cluster searchCluster(DataPoint data) {
		Cluster bestCluster = null;
		double minimum = Double.POSITIVE_INFINITY;
		double distance = 0.0;
		DataPoint clone = data.clone();
		for (Cluster cluster : clusters) {
			clone.setCluster(cluster);
			distance = clone.distance(cluster.getCentroid());
			if (distance < minimum) {
				minimum = distance;
				bestCluster = cluster;
			}
		}
		return bestCluster;
	}

	/**
	 * @return the clusters
	 */
	public ArrayList<Cluster> getClusters() {
		return clusters;
	}

	/**
	 * Abstract method to implement for running the actual clustering process.
	 */
	public abstract void run();
}