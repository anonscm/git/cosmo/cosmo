package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique NormalVec qui utilise les coordonnées u,v et
 * les projections du vecteur normal nx et ny (see {@link METRIC} and {@link NormalVecData})
 * 
 * @author redonnet
 *
 */
public class NormalVecFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new NormalVecData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new NormalVecData(m));
	}

}