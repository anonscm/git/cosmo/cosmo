package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.GVector;

/**
 * A Cluster of {@link DataPoint data points}
 * 
 * @author redonnet et al.
 *
 */
public class Cluster {
	/** number of clusters
	 *  @deprecated incompatible with Multithread */
	private static int nb;

	/** data associated to the cluster */
	private ArrayList<DataPoint> dataSet;

	/** computed data that represent the center of the cluster */
	private DataPoint centroid;

	/** id of the cluster */
	private int id;

	/**
	 * minimal distance between one of the data and the centroid
	 */
	private double minDist;

	/**
	 * maximal distance between one of the data and the centroid
	 */
	private double maxDist;

	/**
	 * average of the distances between the data and the centroid
	 */
	private double moyDist;

	/**
	 * covariance matrix of the cluster
	 */
	private GMatrix covMat;

	/**
	 * mean data vector (same as centroid but updated at each change of the
	 * cluster). Necessary to update the covariance matrix
	 */
	private GVector mean;

	/**
	 * Creates a cluster using the given centroid
	 * 
	 * <p>
	 * No data is put in {@link #dataSet}
	 * 
	 * @param centroid the centroid of the new cluster
	 * @deprecated Use {@link #Cluster(DataPoint, int)} instead
	 */
	public Cluster(DataPoint centroid) {
		dataSet = new ArrayList<DataPoint>();
		id = nb++;
		this.centroid = centroid;
		this.covMat = new GMatrix(DataPoint.getIsize(), DataPoint.getIsize());
		this.mean = new GVector(DataPoint.getIsize());
	}

	/**
	 * Creates a cluster using the given centroid
	 * 
	 * <p>
	 * No data is put in {@link #dataSet}
	 * 
	 * @param centroid the centroid of the new cluster
	 */
	public Cluster(DataPoint centroid, int id) {
		dataSet = new ArrayList<DataPoint>();
		this.id = id;
		this.centroid = centroid;
		this.covMat = new GMatrix(DataPoint.getIsize(), DataPoint.getIsize());
		this.mean = new GVector(DataPoint.getIsize());
	}

	/**
	 * Creates a Cluster from an existing one
	 * 
	 * <p>
	 * If cluster contains data, they are copied to new cluster.
	 * 
	 * @param cluster the cluster to be copied.
	 */
	public Cluster(Cluster cluster) {
		if (cluster != null) {
			this.dataSet = new ArrayList<DataPoint>();
			if (cluster.getDataSet() != null) {
				for (DataPoint dp : cluster.getDataSet()) {
					this.dataSet.add(dp);
				}
				id = nb++;
				this.centroid = cluster.getCentroid().clone();
				this.covMat = new GMatrix(cluster.getCovmat());
				this.mean = new GVector(cluster.mean);
			}
		}
	}

	/**
	 * Add data to the cluster
	 * 
	 * Also makes the DataPoint data aware it is now associated to this cluster
	 * 
	 * @param data the Datapoint to attach to this
	 */
	public void add(DataPoint data) {
		dataSet.add(data);
		data.setCluster(this);
		// mise à jour de la matrice de covariance
		double n = dataSet.size() - 1.0;
		GVector xnew = new GVector(data.getVector(DataPoint.getIsize()));
		GMatrix tmp = new GMatrix(DataPoint.getIsize(), DataPoint.getIsize());
		tmp.mul(mean, mean);
		covMat.add(tmp);
		covMat.scale(n);
		tmp.mul(xnew, xnew);
		covMat.add(tmp);
		covMat.scale(1.0 / (n + 1));
		GVector tmp1 = new GVector(mean);
		tmp1.scale(n);
		tmp1.add(xnew);
		tmp1.scale(1.0 / (n + 1));
		tmp.mul(tmp1, tmp1);
		covMat.sub(tmp);
		// mise à jour du vecteur moyen
		mean.scale(n);
		mean.add(xnew);
		mean.scale(1.0 / (n + 1));
	}

	/**
	 * Remove data to from the cluster
	 * 
	 * Also makes the DataPoint data aware it is no longer associated to this
	 * cluster
	 * 
	 * @param data the Datapoint to remove from this
	 */
	public void remove(DataPoint data) {
		dataSet.remove(data);
		data.setCluster(null);
		// mise à jour de la matrice de covariance
		double n = dataSet.size();
		GVector xnew = new GVector(data.getVector(DataPoint.getIsize()));
		GMatrix tmp = new GMatrix(DataPoint.getIsize(), DataPoint.getIsize());
		tmp.mul(mean, mean);
		covMat.add(tmp);
		covMat.scale(n + 1.0);
		tmp.mul(xnew, xnew);
		covMat.sub(tmp);
		covMat.scale(1.0 / n);
		GVector tmp1 = new GVector(mean);
		tmp1.scale(n + 1.0);
		tmp1.sub(xnew);
		tmp1.scale(1.0 / n);
		tmp.mul(tmp1, tmp1);
		covMat.sub(tmp);
		// mise à jour du vecteur moyen
		mean.scale(n + 1);
		mean.sub(xnew);
		mean.scale(1.0 / n);
	}

	/**
	 * Recompute the center of the cluster
	 */
	public void centralize() {
		int nbElt = dataSet.size();
		if (nbElt > 0) {
			int dim = DataPoint.size;
			int[] tabI = { 0 };
			for (int i = 0; i < dim; i++) {
				tabI[0] = i;
				double sum = dataSet.stream().parallel().mapToDouble(d -> d.getValue(tabI[0])).sum();
				double average = sum / (double) nbElt;
				centroid.setValue(i, average);
				centroid.setCluster(this);
			}
		}
	}

	/**
	 * Compute the stats of this
	 * 
	 * Stats calculated are:
	 * <ul>
	 * <li>Minimal distance from any data to the centroid,
	 * <li>maximal distance from any data to the centroid,
	 * <li>average of the distances
	 */
	public void computeStats() {
		double somDist = 0;
		minDist = Double.POSITIVE_INFINITY;
		maxDist = Double.NEGATIVE_INFINITY;
		for (DataPoint data : dataSet) {
			double dist = data.distance(centroid);
			if (dist < minDist)
				minDist = dist;
			if (dist > maxDist)
				maxDist = dist;
			somDist += dist;
		}
		if (dataSet.size() > 0)
			this.moyDist = somDist / dataSet.size();
	}

	/** @return the id of the cluster, its nb of data, the stats and the data */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("cluster " + id + ", nb elts =  " + dataSet.size() + "\n");
		sb.append("--> centroid = ").append(centroid).append("\n data = \n");
		if (dataSet.size() < 50)
			for (DataPoint data : dataSet)
				sb.append(data.toString()).append("\n");
		sb.append("--> dist min=").append(String.format(Locale.ENGLISH, "%.2f", minDist));
		sb.append("; dist max=").append(String.format(Locale.ENGLISH, "%.2f", maxDist));
		sb.append("; average dist=").append(String.format(Locale.ENGLISH, "%.2f", getMoyDist()));
		sb.append("	\n----\n");
		return sb.toString();
	}

	/**
	 * @return the dataSet
	 */
	public ArrayList<DataPoint> getDataSet() {
		return dataSet;
	}

	/**
	 * @return the centroid of this
	 */
	public DataPoint getCentroid() {
		return centroid;
	}

	/**
	 * @return the id of this
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the moyDist
	 */
	public double getMoyDist() {
		return moyDist;
	}

	/**
	 * @return the covariance matrix
	 */
	public GMatrix getCovmat() {
		return covMat;
	}

	/**
	 * Calculates the distance between two clusters.
	 * 
	 * Calculates the distance between this and the given cluster
	 * 
	 * @param cluster the cluster to calculate the distance to
	 * @return the distance between the two clusters
	 */
	public double distance(Cluster cluster) {
		double distance = 0.0;
		int dim = DataPoint.size;
		double n1 = this.dataSet.size();
		double n2 = cluster.dataSet.size();
		double sum1 = 0.0;
		for (DataPoint data1 : this.dataSet) {
			for (DataPoint data2 : this.dataSet) {
				double dist = 0.0;
				for (int i = 0; i < dim; i++) {
					dist = dist + Math.pow(data1.getValue(i) - data2.getValue(i), 2);
				}
				sum1 = sum1 + Math.sqrt(dist);
			}
		}
		double sum2 = 0.0;
		for (DataPoint data1 : cluster.dataSet) {
			for (DataPoint data2 : cluster.dataSet) {
				double dist = 0.0;
				for (int i = 0; i < dim; i++) {
					dist = dist + Math.pow(data1.getValue(i) - data2.getValue(i), 2);
				}
				sum2 = sum2 + Math.sqrt(dist);
			}
		}
		double sum3 = 0.0;
		for (DataPoint data1 : this.dataSet) {
			for (DataPoint data2 : cluster.dataSet) {
				double dist = 0.0;
				for (int i = 0; i < dim; i++) {
					dist = dist + Math.pow(data1.getValue(i) - data2.getValue(i), 2);
				}
				sum3 = sum3 + Math.sqrt(dist);
			}
		}
		distance = n1 * n2 / (n1 + n2)
				* (2 / (n1 * n2) * sum3 - 1 / Math.pow(n1, 2) * sum1 - 1 / Math.pow(n2, 2) * sum2);
		return distance;
	}

	/**
	 * Merge two clusters.
	 * 
	 * <p>
	 * Merging this with the given cluster. Needed for {@link HierarchicalAlgo
	 * hierarchical clustering}. All data are removed from given cluster.
	 * 
	 * @param cluster the cluster to merge with this
	 */
	public void merge(Cluster cluster) {
		if (this == cluster) {
			System.out.println("Clusters Are Equal");
		}
		for (DataPoint data : cluster.getDataSet()) {
			this.add(data);
		}
		Iterator<DataPoint> iter = cluster.getDataSet().iterator();
		while (iter.hasNext()) {
			iter.next();
			iter.remove();
		}
	}

	/**
	 * Set id of this cluster.
	 * 
	 * <p>
	 * Used by {@link HierarchicalAlgo hierarchical clustering} in particular
	 * 
	 * @param n the new id of this.
	 */
	public void setId(int n) {
		this.id = n;
	}

	/**
	 * @return the number of clusters
	 * @deprecated
	 */
	public static int getNb() {
		return nb;
	}

	/**
	 * Set the number of clusters
	 * 
	 * @param nb
	 * @deprecated
	 */
	public static void setNb(int nb) {
		Cluster.nb = nb;
	}

}