package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique UV (see {@link METRIC}
 * and {@link UVData})
 * 
 * @author redonnet
 *
 */
public class UVFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new UVData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new UVData(m));
	}

}
