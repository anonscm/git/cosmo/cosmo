package org.ica.cosmo.clustering;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.min;
import static java.lang.Math.sin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.Callable;

import org.apache.commons.math3.linear.RealMatrix;
import org.ica.cosmo.zoning.Index2;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.ica.cosmo.zoning.Zone;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;

public class Clustering implements Callable<SurfaceMap> {
	/** generator used for random init clustering */
	private static Random r = new Random();

	/** algorithme de clustering à utliser */
	private AbstractAlgo algo;

	/** métrique à utiliser */
	@SuppressWarnings("unused")
	private METRIC metric; // unused for now, may be used later

	/** cartographie utilisée */
	private SurfaceMap map;

	/** Tessellation values */
	private int tessU;
	private int tessV;

	/** nombre de zones */
	private int k;

	/** flag pour la recherche de composantes connexes */
	private boolean ccsFlag;

	/** list of the data */
	private HashMap<Index2, DataPoint> dataSet = new HashMap<Index2, DataPoint>();

	/** list of the clusters */
	private ArrayList<Cluster> clusters = new ArrayList<>();

	/** arrays for labeling */
	private Integer[][] labels;
	private Integer[][] zMapIDs;

	/**
	 * The global covariance matrix
	 * 
	 * Used for Mahalanobis and standard deviation based metrics
	 * 
	 */
	private RealMatrix gCov = null;

	/** the dataFactory to use. Depends on METRIC */
	private DataFactory dataFactory;

	/** temps de calcul (en ms) */
	private long computationTime = 0;

	public enum INIT {
		ANGULAR, PRESET, RANDOM
	};

	private INIT init_method;

	/**
	 * Private constructor for common initializations
	 * 
	 * @param surfaceMap
	 * @param algo_type
	 * @param metric
	 * @param ccsFlag
	 */
	private Clustering(SurfaceMap surfaceMap, METRIC metric, boolean ccsFlag) {
		this.clusters.clear();
		this.map = surfaceMap;
		this.tessU = map.getTessU();
		this.tessV = map.getTessV();
		this.metric = metric;
		this.ccsFlag = ccsFlag;
		dataFactory = DataFactory.getFactory(metric);
		for (int i = 0; i < tessU; i++)
			for (int j = 0; j < tessV; j++) {
				UVMeshUnit m = map.getMeshGrid()[i][j];
				DataPoint data = dataFactory.createData(m);
				dataSet.put(m.getIndex(), data);
			}

		// On ne calcule la matrice de covariance globale que pour les métriques qui en
		// ont besoin.
		if (metric == METRIC.SIGMASLOPE)
			calcGCov();
	}

	public Clustering(int k, SurfaceMap surfaceMap, ALGO_TYPE algo_type, METRIC metric, boolean ccsFlag) {
		this(surfaceMap, metric, ccsFlag);
		init_method = INIT.ANGULAR;
		init(k, algo_type);
	}

	public Clustering(ArrayList<Vector2d> centers, SurfaceMap surfaceMap, ALGO_TYPE algo_type, METRIC metric,
			boolean ccsFlag) {
		this(surfaceMap, metric, ccsFlag);
		init_method = INIT.PRESET;
		init(centers, algo_type);
	}

	public Clustering(int k, SurfaceMap surfaceMap, ALGO_TYPE algo_type, METRIC metric) {
		this(k, surfaceMap, algo_type, metric, true);
	}

	public Clustering(int k, SurfaceMap surfaceMap, METRIC metric) {
		this(k, surfaceMap, ALGO_TYPE.KMEANS, metric, true);
	}

	private void init(int k, ALGO_TYPE type) {
		this.k = k;
		switch (init_method) {
		case ANGULAR:
			createAngularClusters();
			break;
		case RANDOM:
			createRandomClusters();
			break;
		default:
			// Should never occur
			break;
		}

		this.algo = type.create(dataSet, clusters);
	}

	private void init(ArrayList<Vector2d> initCenters, ALGO_TYPE type) {
		this.k = initCenters.size();
		if (init_method == INIT.PRESET) // Should never be False
			createPresetClusters(initCenters);

		this.algo = type.create(dataSet, clusters);
	}

	public void run() {
		long start_time = System.currentTimeMillis();
		algo.run();
		this.clusters = algo.getClusters();
		if (ccsFlag)
			ccl();
		long end_time = System.currentTimeMillis();
		this.computationTime = end_time - start_time;
	}

	/**
	 * Never used. Kept for history.
	 */
	private void createRandomClusters() {
		Index2 randomKey = new Index2(r.nextInt(tessU), r.nextInt(tessV));
		DataPoint centroid = dataSet.get(randomKey);
		Cluster firstCluster = new Cluster(centroid, 0);
		clusters.add(firstCluster);
		int nbClusters = k;
		for (int c = 1; c < nbClusters; c++) {
			DataPoint farData = null;
			double maxDist = Double.NEGATIVE_INFINITY;
			for (DataPoint data : dataSet.values()) {
				double minDist = Double.POSITIVE_INFINITY;
				for (Cluster cluster : clusters) {
					DataPoint clone = data.clone();
					clone.setCluster(cluster);
					double dist = clone.distance(cluster.getCentroid());
					if (minDist > dist)
						minDist = dist;
				}
				if (maxDist < minDist) {
					maxDist = minDist;
					farData = data;
				}
			}

			Cluster cluster = new Cluster(farData.clone(), c);
			clusters.add(cluster);
		}
	}

	private void createAngularClusters() {
		Surface s = map.getSurface();
		double centerU = (s.getUmin() + s.getUmax()) / 2.0;
		double centerV = (s.getVmin() + s.getVmax()) / 2.0;
		double angularStep = 2 * PI / (double) k;
		// écart en (u,v) par rappport au centre
		double deltaUV = min((s.getUmax() - s.getUmin()) / 4.0, (s.getVmax() - s.getVmin()) / 4.0);
		for (int i = 0; i < k; i++) {
			double angle = (double) i * angularStep + angularStep / 2.0 + PI;
			double u = centerU + deltaUV * cos(angle);
			double v = centerV + deltaUV * sin(angle);
			DataPoint centroid = dataFactory.createData(map.getMeshUnit(u, v));
			Cluster cluster = new Cluster(centroid, i);
			clusters.add(cluster);
		}
	}

	private void createPresetClusters(ArrayList<Vector2d> centers) {
		for (int i = 0; i < centers.size(); i++) {
			Vector2d center = centers.get(i);
			DataPoint centroid = dataFactory.createData(map.getMeshUnit(center.x, center.y));
			Cluster cluster = new Cluster(centroid, i);
			clusters.add(cluster);
		}
	}

	/**
	 * WARNING: Because of deep recursion, StackOverflowError may occurs for high
	 * tesselations
	 * 
	 * catch block verbatim:
	 * 
	 * <pre>
	 * "Run into StackOverflowError while labeling zones !"
	 * "This is due to too deep recursions"
	 * "Try to reduce map tessellation or increase stack size"
	 * "To know the actual java stack size, type (in a console):"
	 * "    java -XX:+PrintFlagsFinal -version | grep -iE 'HeapSize|PermSize|ThreadStackSize'"
	 * "To increase the stack size use the -Xss<size> option of the VM"
	 * "For example -Xss4m set the stack size to 4 Mo"
	 * </pre>
	 * 
	 */
	private void ccl() {
		zMapIDs = new Integer[tessU][tessV];
		for (int i = 0; i < tessU; i++) {
			for (int j = 0; j < tessV; j++) {
				UVMeshUnit m = map.getMeshGrid()[i][j];
				DataPoint data = dataSet.get(m.getIndex());
				zMapIDs[i][j] = data.getCluster().getId();
			}
		}

		labels = new Integer[tessU][tessV];
		Arrays.stream(labels).forEach(a -> Arrays.fill(a, null));

		int l = 0; // first label

		try {
			label(0, 0, l);

			for (int i = 0; i < tessU; i++)
				for (int j = 0; j < tessV; j++)
					if (labels[i][j] == null) {
						l = l + 1;
						label(i, j, l);
					}
		} catch (StackOverflowError e) {
			System.err.println("Run into StackOverflowError while labeling zones !");
			System.err.println("This is due to too deep recursions");
			System.err.println("Try to reduce map tessellation or increase stack size");
			System.err.println("To know the actual java stack size, type (in a console):");
			System.err.println("    java -XX:+PrintFlagsFinal -version | grep ThreadStackSize");
			System.err.println("To increase the stack size use the -Xss<size> option of the VM");
			System.err.println("For example -Xss4m set the stack size to 4 Mo");
		}

		for (int i = 0; i < tessU; i++)
			for (int j = 0; j < tessV; j++) {
				if (labels[i][j] != zMapIDs[i][j]) {
					UVMeshUnit m = map.getMeshGrid()[i][j];
					DataPoint data = dataSet.get(m.getIndex());
					clusters.get(data.getCluster().getId()).remove(data);
					if (k <= labels[i][j]) {
						DataPoint centroid = data.clone();
						// Cluster cl = new Cluster(centroid);
						// cl.setId(k);
						Cluster cl = new Cluster(centroid, k);
						cl.add(data);
						clusters.add(cl);
						k = k + 1;
					} else {
						clusters.get(labels[i][j]).add(data);
					}
				}
			}
		clusters.forEach(c -> c.centralize());
	}

	private void label(int i, int j, int l) {
		labels[i][j] = l;
		if (i - 1 >= 0)
			if (zMapIDs[i - 1][j] == zMapIDs[i][j] && labels[i - 1][j] == null) {
				label(i - 1, j, l);
			}
		if (j - 1 >= 0)
			if (zMapIDs[i][j - 1] == zMapIDs[i][j] && labels[i][j - 1] == null) {
				label(i, j - 1, l);
			}
		if (i + 1 < tessU)
			if (zMapIDs[i + 1][j] == zMapIDs[i][j] && labels[i + 1][j] == null) {
				label(i + 1, j, l);
			}
		if (j + 1 < tessV)
			if (zMapIDs[i][j + 1] == zMapIDs[i][j] && labels[i][j + 1] == null) {
				label(i, j + 1, l);
			}
		return;
	}

	public int getNbClusters() {
		return k;
	}

	/**
	 * 
	 * @return the clustering process computation time (in seconds)
	 */
	public double getComputationTime() {
		return (double) computationTime / 1000.0;
	}

	public DataPoint[] getCentroids() {
		clusters.forEach(c -> c.centralize());
		DataPoint[] centroids = new DataPoint[k];
		Arrays.setAll(centroids, i -> clusters.get(i).getCentroid());
		return centroids;
	}

	public DataPoint getCentroid(int k) {
		DataPoint[] centroids = getCentroids();
		return centroids[k];
	}

	public Cluster getCluster(int i) {
		return clusters.get(i);
	}

	public void setCcsFlag(boolean ccsFlag) {
		this.ccsFlag = ccsFlag;
	}

	public void setInit_method(INIT init_method) {
		this.init_method = init_method;
	}

	public RealMatrix getgCov() {
		if (gCov == null)
			calcGCov();
		return gCov;
	}

	private void calcGCov() {
		this.gCov = DataPoint.getCovMatrix(dataSet);
	}

	@Override
	public SurfaceMap call() throws Exception {
		map.reset();
		clusters.forEach(i -> map.addZone());
		double[] values;
		for (DataPoint data : dataSet.values()) {
			values = data.getValues();
			UVMeshUnit m = map.getMeshUnit(values[0], values[1]);
			m.changeZone(null, map.getZone(data.cluster.getId()));
			map.getZMap()[m.getIndex().i][m.getIndex().j] = data.getCluster().getId();
		}
		sanityCheck();

		return map;
	}

	/**
	 * Remove single UVMeshUnit zones
	 * 
	 * Attach lonely UVMeshUnit to zone which next-to-it UVMeshUnit normal are
	 * closer to its one.
	 */
	private void sanityCheck() {
		double angle = Math.PI;
		for (Zone z1 : map.getZones().values()) {
			if (z1.getMeshmap().size() == 1) {
				Entry<Index2, UVMeshUnit> e = z1.getMeshmap().entrySet().iterator().next();
				Index2 idx1 = e.getKey();
				UVMeshUnit m1 = e.getValue();
				Vector3d n1 = z1.getSurfaceMap().getMeshUnit(idx1).getNormal();
				Zone bestNewZone = null;
				for (Zone z : map.getZones().values()) {
					for (Index2 idx : z.getMeshmap().keySet()) {
						if (idx.isNextTo(idx1)) {
							Vector3d n = z.getSurfaceMap().getMeshUnit(idx).getNormal();
							if (n.angle(n1) < angle) {
								bestNewZone = z;
							}
						}
					}
				}
				m1.changeZone(z1, bestNewZone);
			}
		}
	}
}
