package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;

public class SNData extends DataPoint {
	static {
		size = 8;
		isize = 8;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}
	public SNData() {
		super();
	}

	public SNData(double[] values) {
		super(values);
	}

	public SNData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();
		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getCenter().x;
		values[3] = m.getCenter().y;
		values[4] = m.getCenter().z;
		values[5] = this.surface.normal(values[0], values[1]).x;
		values[6] = this.surface.normal(values[0], values[1]).y;
		values[7] = this.surface.normal(values[0], values[1]).z;
	}

	/**
	 * @return a clone of the data
	 */
	public SNData clone() {
		SNData copy = new SNData(values);
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}
	
	@Override
	public double distance(DataPoint o) {
		double dist, sum = 0;
//		double norm = Math.pow(o.values[2], 2) + Math.pow(o.values[3], 2) + Math.pow(o.values[4], 2);
//		for (int i = 2; i < 5; i++)
//			sum += omega[i] * Math.pow(values[i] - o.values[i], 2)/norm;
		for (int i = 2; i < size; i++)
			sum += omega[i] * Math.pow(values[i] - o.values[i], 2);
		dist = Math.sqrt(sum);
		return dist;
	}


}