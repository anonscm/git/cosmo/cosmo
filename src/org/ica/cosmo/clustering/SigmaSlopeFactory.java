package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique SigmaSlope qui utilise
 * les écarts types (see {@link METRIC} and {@link SigmaSlopeData})
 * 
 * @author redonnet
 *
 */
public class SigmaSlopeFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new SigmaSlopeData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new SigmaSlopeData(m));
	}

}