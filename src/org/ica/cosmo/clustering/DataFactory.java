package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique abstraite pour les points de données.
 * 
 * 
 * @author redonnet
 *
 */
public abstract class DataFactory {
	public static DataFactory getFactory(METRIC metric) {
		return metric.getInstance();
	}

	/**
	 * Crée un point à partir des données passées en paramètre.
	 * 
	 * <p>
	 * Doit être implémentée dans les fabriques concrètes.
	 * </p>
	 * <p>
	 * ATTENTION : Aucune vérification sur la taille du tableau n'est faite.
	 * </p>
	 * 
	 * @return un point de données
	 */
	public abstract DataPoint createData(double[] data);

	/**
	 * Crée un point à partir d'une maille élémentaire.
	 * 
	 * <p>
	 * Doit être implémentée dans les fabriques concrètes.
	 * </p>
	 * 
	 * @return un point de données
	 */
	public abstract DataPoint createData(UVMeshUnit m);
}
