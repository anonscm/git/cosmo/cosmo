package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique Covar
 * 
 * @author redonnet
 *
 */
public class CovarFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new CovarData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new CovarData(m));
	}
	
}
