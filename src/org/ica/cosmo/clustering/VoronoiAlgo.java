package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.HashMap;

import org.ica.cosmo.zoning.Index2;

public class VoronoiAlgo extends AbstractAlgo{
	public VoronoiAlgo(HashMap<Index2, DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super(dataSet, clusters);
	}
	
	public void run() {
		for (DataPoint data : dataSet.values()) {
			Cluster bestCluster = searchCluster(data);
			if (data.getCluster() != bestCluster) {
				bestCluster.add(data);
			}
		}
	}
}
