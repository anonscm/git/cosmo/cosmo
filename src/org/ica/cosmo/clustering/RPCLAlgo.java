package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.ica.cosmo.zoning.Index2;

/**
 * Algorithm of Rival Penalized Competitive Learning (RPCL) :<br>
 * 
 * At each iteration, a data point is randomly selected and the its closest
 * centroid, the winner, is moved to its direction while the second, or rival is
 * penalized.
 */
public class RPCLAlgo extends AbstractAlgo {
	private static Random r = new Random();
	private int k;
	private int dataSize;

	public RPCLAlgo(HashMap<Index2, DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super(dataSet, clusters);
		this.k = clusters.size();
		this.dataSize = DataPoint.getSize();
	}

	public void run() {
		int loop = 0;
		// winner and rival learning rates and clusters
		double alpha_c = 0.2;
		double alpha_r = 0.2;
		Cluster winner_cl, rival_cl;
		int change_count = 1;
		// cumulated number of occurrences
		int n_occ[] = new int[k];
		Arrays.setAll(n_occ, i -> 1);
		// RPCL loop
		List<DataPoint> pointsList = new ArrayList<DataPoint>(dataSet.values());
		
		while (loop < 300 & alpha_c > 0.001 & alpha_r > 0.001 & change_count > 0) {
			loop = loop + 1;
			System.out.println("itération = " + loop);
			DataPoint point = pointsList.get(r.nextInt(pointsList.size()));
			winner_cl = searchWinner(point, n_occ);
			rival_cl = searchRival(point, winner_cl, n_occ);
			n_occ[winner_cl.getId()] = n_occ[winner_cl.getId()] + 1;
			// updating centers
			for (int i = 0; i < dataSize; i++) {
				winner_cl.getCentroid().setValue(i, alpha_c * (point.getValue(i) - winner_cl.getCentroid().getValue(i))
						+ winner_cl.getCentroid().getValue(i));
				rival_cl.getCentroid().setValue(i, rival_cl.getCentroid().getValue(i)
						- alpha_r * (point.getValue(i) - rival_cl.getCentroid().getValue(i)));
			}
			change_count = 0;
			// affectation of each DataPoint to a cluster
			for (DataPoint data : dataSet.values()) {
				Cluster bestCluster = searchCluster(data);
				if (bestCluster != data.getCluster()) {
					change_count = change_count + 1;
				}
				if (loop > 1) {
					data.getCluster().remove(data);
				}
				bestCluster.add(data);
			}
			System.out.println("change = " + change_count);
			// mise à jour des alphas
			if (loop > 1) {
				alpha_c = 0.2 * Math.pow(0.99, loop) * change_count / dataSet.size() * 15;
				alpha_r = 0.2 * Math.pow(0.99, loop) * change_count / dataSet.size() * 15;
			}
		}
		clusters.forEach(c -> c.centralize());
	}

	/**
	 * search the winner cluster for a DataPoint
	 * 
	 * @param data  the DataPoint to search its rival cluster
	 * @param n_occ the number of occurrences for each centroid
	 * @return the winner cluster for data
	 */
	protected Cluster searchWinner(DataPoint data, int[] n_occ) {
		Cluster winner = null;
		double minimum = Double.POSITIVE_INFINITY;
		double distance = 0.0;
		DataPoint clone = data.clone();
		double gamma;
		int sum_occ = 0;
		for (int i = 0; i < k; i++)
			sum_occ = sum_occ + n_occ[i];
		for (Cluster cluster : clusters) {
			gamma = (double) n_occ[cluster.getId()] / (double) sum_occ;
			clone.setCluster(cluster);
			distance = gamma * clone.distance(cluster.getCentroid());
			if (distance < minimum) {
				minimum = distance;
				winner = cluster;
			}
		}
		return winner;
	}

	/**
	 * search the rival cluster for a DataPoint knowing his best cluster
	 * 
	 * rpcl
	 * 
	 * @param data  the DataPoint to search its rival cluster
	 * @param best  the best cluster for data
	 * @param n_occ the number of occurrences for each centroid
	 * @return the rival cluster for data
	 */
	protected Cluster searchRival(DataPoint data, Cluster best, int[] n_occ) {
		Cluster rival = null;
		double minimum = Double.POSITIVE_INFINITY;
		double distance = 0.0;
		DataPoint clone = data.clone();
		@SuppressWarnings("unchecked")
		ArrayList<Cluster> rival_list = (ArrayList<Cluster>) clusters.clone();
		rival_list.remove(best);
//		System.out.println("nombre de clusters" + clusters.size() + " sans le winner" + rival_list.size());
		double gamma;
		int sum_occ = 0;
		for (int i = 0; i < k; i++)
			sum_occ = sum_occ + n_occ[i];
		for (Cluster cluster : rival_list) {
			gamma = (double) n_occ[cluster.getId()] / (double) sum_occ;
			clone.setCluster(cluster);
			distance = gamma * clone.distance(cluster.getCentroid());
			if (distance < minimum) {
				minimum = distance;
				rival = cluster;
			}
		}
		return rival;
	}

}
