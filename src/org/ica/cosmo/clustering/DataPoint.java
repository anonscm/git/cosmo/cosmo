package org.ica.cosmo.clustering;

import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.management.RuntimeErrorException;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.GVector;

/**
 * Un point de données abstrait pour le clustering
 * 
 * Chaque point de données est constitué de données dites intrinsèques et
 * éventuellement de données dites mixtes. Les données intrinsèques sont
 * relatives au point lui-même, uniquement (u, v, pente de la surface, etc). Les
 * données mixtes sont définies par le point et sa relation au reste de la zone
 * ((variance, covariance, etc).
 * 
 * Par convention, les données intrinsèques sont toujours les premières valeurs
 * du datapoint.
 * 
 * @author redonnet
 *
 */
public abstract class DataPoint implements Cloneable {
	/** number of values in the DataPoint */
	protected static int size;

	/** number of intrinsic values in the DataPoint */
	protected static int isize;

	/** the coefficients of the metric */
	protected static double[] omega;

	/** the surface this DataPoint belongs to */
	protected Surface surface;

	/** the values */
	protected double[] values;

	/** global covariance matrix */
	protected static RealMatrix gcov;

	/** cluster associated to the data */
	protected Cluster cluster = null;

	/**
	 * Initialize size (no data)
	 */
	public DataPoint() {
		this.values = new double[size];
	}

	/**
	 * Initialize point with given data
	 * 
	 * @param tab the list of values
	 */
	public DataPoint(double... tab) {
		this();
		values = Arrays.copyOf(tab, tab.length);
	}

	/**
	 * Change a value
	 * 
	 * @param i indice of the value to be changed
	 * @param x new value
	 */
	public void setValue(int i, double x) {
		values[i] = x;
	}

	/**
	 * @return the i<sup>th</sup> value of the data
	 */
	public double getValue(int i) {
		return values[i];
	}

	/**
	 * @return the values
	 */
	public double[] getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	protected void setValues(double[] values) {
		this.values = values;
	}

	/**
	 * Set the global covariance matrix
	 * 
	 * @param gCov covariance matrix
	 */
	public void setGcov(RealMatrix gCov) {
		this.gcov = gCov;
	}

	/**
	 * Compute the euclidian distance between the current value and another one
	 * 
	 * @param o the Other value to be compared with
	 * @return the distance between the current data and the other
	 */
	public double distance(DataPoint o) {
		double dist, sum = 0;
		for (int i = 0; i < size; i++)
			sum += omega[i] * Math.pow(values[i] - o.values[i], 2);
//		for (int i = 0; i < 3; i++)
//			sum += omega[i] * Math.pow(values[i] - o.values[i], 2);
//		sum += omega[3] * Math.pow(Math.sin(values[3]+Math.PI/2.0) - Math.sin(o.values[3]+Math.PI/2.0), 2);
		dist = Math.sqrt(sum);
		return dist;
	}

	/**
	 * @return a String containing the list of the values
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		String sep = ", ";
		for (int i = 0; i < size - 1; i++)
			sb.append(String.format(Locale.ENGLISH, "%.2f", getValues()[i])).append(sep);
		sb.append(String.format(Locale.ENGLISH, "%.2f", getValues()[size - 1])).append(")");
		return sb.toString();
	}

	/**
	 * Attach this datapoint to the given cluster
	 * 
	 * @param cluster the cluster to attache this datapoint to
	 */
	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}

	/**
	 * @return the cluster this datapoint belongs to
	 */
	public Cluster getCluster() {
		return cluster;
	}

	/**
	 * Return this datapoint values in a GVector.
	 * 
	 * @return a GVector containing this datapoint values
	 */
	public GVector getGVector() {
		return (new GVector(values));
	}

	/**
	 * Return the vector of the size<sup>th</sup> first components
	 * 
	 * @param size the number of components to return
	 * @return the resulting vector
	 */
	public GVector getVector(int size) {
		if (size > values.length)
			throw new RuntimeErrorException(null);
		double[] tmp = new double[size];
		for (int i = 0; i < size; i++)
			tmp[i] = values[i];
		return new GVector(tmp);
	}

	/**
	 * @return a clone of this datapoint
	 */
	public abstract DataPoint clone();

	/**
	 * <p>
	 * Override when u is not the first value of the datapoint
	 * 
	 * @return the u parameter for the current datapoint
	 * 
	 */
	public double getU() {
		return values[0];
	}

	/**
	 * <p>
	 * Override when v is not the second value of the datapoint
	 * 
	 * @return the v parameter for the current datapoint
	 */
	public double getV() {
		return values[1];

	}

	/**
	 * Returns the angle between the steepest slope direction and the X axis
	 * 
	 * <p>
	 * Override when this angle is not the fourth value of the datapoint
	 * 
	 * @return the angle of the steepest slope direction
	 */
	public double getMaxSlopeAngle() {
		return values[3];
	}

	public Surface getSurface() {
		return surface;
	}

	/*
	 * Static methods
	 */
	public static RealMatrix getCovMatrix(Collection<DataPoint> list) {
		RealMatrix covMat = null;

		int n = list.size();
		int s = DataPoint.isize;
		DataPoint[] a = new DataPoint[n];
		list.toArray(a);

		double[] moy = new double[s];
		double[][] vcov = new double[s][s]; // variance et covariance
		for (int i = 0; i < n; i++) {
			double p[] = a[i].values;
			for (int j = 0; j < s; j++) {
				moy[j] = moy[j] + p[j];
				for (int k = 0; k < j + 1; k++) {
					vcov[j][k] = vcov[j][k] + p[j] * p[k];
				}
			}
		}
		for (int j = 0; j < s; j++) {
			moy[j] = moy[j] / (double) n;
		}
		for (int j = 0; j < s; j++) {
			for (int k = 0; k < j + 1; k++) {
				vcov[j][k] = vcov[j][k] / (double) (n) - moy[j] * moy[k];
				vcov[k][j] = vcov[j][k];
			}
		}
		covMat = new Array2DRowRealMatrix(vcov);

		return covMat;
	}

	public static RealMatrix getCovMatrix(Map<?, DataPoint> list) {
		gcov = getCovMatrix(list.values());
		return getCovMatrix(list.values());
	}

	public static int getSize() {
		return size;
	}

	public static int getIsize() {
		return isize;
	}

	public static double[] getOmega() {
		return omega;
	}

	public static void setOmega(double[] omega) {
		DataPoint.omega = omega;
	}
}
