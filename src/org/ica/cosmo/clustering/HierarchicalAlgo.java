package org.ica.cosmo.clustering;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.ica.cosmo.zoning.Index2;

public class HierarchicalAlgo extends AbstractAlgo {
	public HierarchicalAlgo(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super(dataSet, clusters);
	}

	/**
	 * algorithm of Hierarchical Clustering
	 */
	public void run() {
		NumberFormat f = new DecimalFormat("#0.00");
		int loop = 0;
		ArrayList<Cluster> partition = new ArrayList<>();
		ArrayList<Cluster> old_partition = new ArrayList<>();
		int id=0;
		for (DataPoint data : dataSet.values()) {
//			Cluster cluster = new Cluster(data);
			Cluster cluster = new Cluster(data, id);
			id++;
			cluster.add(data);
			partition.add(cluster);
		}
		int n = partition.size();
		int nd = n; // number of data
		double[][] dist_tab = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				dist_tab[i][j] = partition.get(i).distance(partition.get(j));
			}
		}
		double max_dist = 0.0;
		double minimum_old = 0.0;
		while (n > 2) {
			loop = loop + 1;
			n = n - 1;

			int i0 = 0;
			int j0 = 0;
			double minimum = Double.POSITIVE_INFINITY;
			double distance = 0.0;
			// trouver la paire de classes avec la distance minimale
			for (int i = 0; i < nd - 1; i++) {
				for (int j = i + 1; j < nd; j++) {
					distance = dist_tab[i][j];
					if (distance < minimum) {
						i0 = i;
						j0 = j;
						minimum = distance;
					}
				}
			}
			int n1 = partition.get(i0).getDataSet().size();
			int n2 = partition.get(j0).getDataSet().size();
			partition.get(i0).merge(partition.get(j0));
			partition.set(j0, null);
			// mise a jour des distances
			for (int i = 0; i < i0; i++) {
				if (partition.get(i) != null) {
					int n3 = partition.get(i).getDataSet().size();
					dist_tab[i][i0] = (double) (n1 + n3) / (n1 + n2 + n3) * dist_tab[i][i0]
							+ (double) (n2 + n3) / (n1 + n2 + n3) * dist_tab[i][j0]
							+ (double) n3 / (n1 + n2 + n3) * dist_tab[i0][j0];
				}
			}
			for (int j = i0; j < j0; j++) {
				if (partition.get(j) != null) {
					int n3 = partition.get(j).getDataSet().size();
					dist_tab[i0][j] = (double) (n1 + n3) / (n1 + n2 + n3) * dist_tab[i0][j]
							+ (double) (n2 + n3) / (n1 + n2 + n3) * dist_tab[j][j0]
							+ (double) n3 / (n1 + n2 + n3) * dist_tab[i0][j0];
				}
			}
			for (int j = j0; j < nd; j++) {
				if (partition.get(j) != null) {
					int n3 = partition.get(j).getDataSet().size();
					dist_tab[i0][j] = (double) (n1 + n3) / (n1 + n2 + n3) * dist_tab[i0][j]
							+ (double) (n2 + n3) / (n1 + n2 + n3) * dist_tab[j0][j]
							+ (double) n3 / (n1 + n2 + n3) * dist_tab[i0][j0];
				}
			}
			for (int i = 0; i < j0; i++) {
				dist_tab[i][j0] = Double.POSITIVE_INFINITY;
			}
			for (int j = j0; j < nd; j++) {
				dist_tab[j0][j] = Double.POSITIVE_INFINITY;
			}
			// comparer le saut de distance
			if (minimum - minimum_old > max_dist) {
//				this.clusters = old_partition.stream().map(Cluster::new).collect(toCollection(ArrayList::new));
				this.clusters.clear();
				for (Cluster cl : old_partition) {
					if (cl != null) {
						this.clusters.add(new Cluster(cl));
					} else {
						this.clusters.add(null);
					}
				}
			}
			max_dist = minimum - minimum_old;
			minimum_old = minimum;
//			old_partition = partition.stream().map(Cluster::new).collect(toCollection(ArrayList::new));
			old_partition.clear();
			for (Cluster cl : partition) {
				if (cl != null) {
					old_partition.add(new Cluster(cl));
				} else {
					old_partition.add(null);
				}
			}
//			System.out.println("iteration " + loop + ", saut de distance " + f.format(max_dist));
		}
		// remove null clusters
		Iterator<Cluster> iter = this.clusters.iterator();
		while (iter.hasNext()) {
			Cluster cl = iter.next();
			if (cl == null) {
				iter.remove();
			}
		}
		int noc = 0;
		for (Cluster cl : clusters) {
			cl.setId(noc);
			noc = noc + 1;
			for (DataPoint data : cl.getDataSet()) {
				data.setCluster(cl);
			}
		}
//		System.out.println("real dataset size = " + dataSet.size());
//		System.out.println("fake one = " + clusters.stream().parallel().mapToInt(c -> c.getDataSet().size()).sum());
//		System.out.println("clusters number = " + clusters.size());
	}

}
