package org.ica.cosmo.clustering;

import java.util.function.Supplier;

/**
 * Une énumération des métriques
 * 
 * <p>
 * Définit les diverses métriques utilisables, leur taille et les fabriques
 * concrètes associées.
 * </p>
 */
public enum METRIC {
	/** 
	 * Une métrique basée sur les paramètres u et v uniquement
	 */
	UV(UVFactory::new, 2),
	/**
	 * Une métrique basée sur les paramètres (u,v) et les directions de plus grandes pentes
	 */
	SLOPE(SlopeFactory::new, 4),
	/** 
	 * Une métrique basée sur les paramètres (u,v), les directions de plus grandes pentes et
	 * la covariance entre u et v
	 */
	COVAR(CovarFactory::new, 6),
	/**
	 * une métrique basée sur les parmètres (u,v,s,theta) et leurs écarts types
	 */
	SIGMASLOPE(SigmaSlopeFactory::new, 4),
	/**
	 * une métrique basée sur les paramètres (u,v,s,theta) et l'inverse de leur matrice de covariance
	 */
	MAHALANOBIS(MahalanobisFactory::new, 4),
	/**
	 * une métrique basée sur les paramètres (u,v,nx,ny) avec nx et ny les projections sur l'axe x et
	 * y du vecteur normal n(u,v)
	 */
	NORMALVEC(NormalVecFactory::new, 4),
	/**
	 * une métrique basée sur les directions principales (ACP) en 3D et les paramètres (Sx,Sy,Sz,s,theta) 
	 */
	COVAR3D(Covar3DFactory::new, 9),
	/**
	 * une métrique basée sur les paramètres (u,v,nx,ny,nz) article roman 2005
	 */
	UVN(UVNFactory::new, 5),
	/**
	 * une métrique basée sur les paramètres (Sx,Sy,Sz,nx,ny,nz) article liu 2018
	 */
	SN(SNFactory::new,8);

	private Supplier<DataFactory> instantiator;
	
	private int size;

	public DataFactory getInstance() {
		return instantiator.get();
	}

	public int getSize() {
		return this.size;
	}

	private METRIC(Supplier<DataFactory> instantiator, int size) {
		this.instantiator = instantiator;
		this.size = size;
	}
}