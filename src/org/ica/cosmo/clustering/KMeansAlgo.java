package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.HashMap;

import org.ica.cosmo.zoning.Index2;

/**
 * Algorithm of K-means :<br>
 * 
 * Set each data in the best cluster and do a loop:
 * <ul>
 * <li>-compute the new clusters centers</li>
 * <li>-moving of data in best clusters if necessary</li>
 * </ul>
 */
public class KMeansAlgo extends AbstractAlgo {
	public KMeansAlgo(HashMap<Index2, DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super(dataSet, clusters);
	}

	public void run() {
		boolean moving = true;
		int loop = 0;
		while (moving && loop < 50) {
			loop = loop + 1;
			moving = false;
			for (DataPoint data : dataSet.values()) {
				Cluster bestCluster = searchCluster(data);
				if (data.getCluster() != bestCluster) {
					moving = true;
					if (loop > 1) {
						data.getCluster().remove(data);
					}
					bestCluster.add(data);
				}
			}
			clusters.forEach(c -> c.centralize());
		}
	}
}
