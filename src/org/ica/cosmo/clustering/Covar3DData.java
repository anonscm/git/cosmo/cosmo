package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.functions.MultivarEvaluable;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.optimization.NewtonSolver;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Un point de données pour la métrique Covar3D basée sur l'analyse en
 * composantes principales en 3D
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : pente</li>
 * <li>values[3] : angle entre la direction de plus grande pente et l'axe des X
 * </li>
 * <li>values[4] : Sx(u,v) coordonée x dans l'espace réel 3D</li>
 * <li>values[5] : Sy(u,v) coordonée y dans l'espace réel 3D</li>
 * <li>values[6] : Sz(u,v) coordonée z dans l'espace réel 3D</li>
 * <li>values[7] : angle caractérisant l'élargissement de la zone</li>
 * <li>values[8] : angle caractérisant l'alignement de la zone avec la direction
 * optimale d'usinage</li>
 * </ul>
 * 
 * @author redonnet
 *
 */
public class Covar3DData extends DataPoint {
	static {
		size = 9;
		isize = 7;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public Covar3DData() {
		super();
	}

	public Covar3DData(double[] values) {
		super(values);
	}

	public Covar3DData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getSlope();
		values[3] = m.getOptimalDir();
		values[4] = surface.evalX(m.getCenter().u, m.getCenter().v);
		values[5] = surface.evalY(m.getCenter().u, m.getCenter().v);
		values[6] = surface.evalZ(m.getCenter().u, m.getCenter().v);
	}

	/**
	 * @return a clone of the data
	 */
	public Covar3DData clone() {
		Covar3DData copy = new Covar3DData(values);
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double distance(DataPoint o) {
		o.setValue(7, 0.0);
		o.setValue(8, 0.0);
		double x = values[4];
		double y = values[5];
		double z = values[6];
		Cluster cls = o.getCluster();
		double dist, sum = 0.0;

		if (cls == null) {
			for (int i = 0; i < 4; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else if (cls.getCovmat().isZero()) {
			for (int i = 0; i < 4; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else {
			double cx = cls.getCentroid().getValue(4);
			double cy = cls.getCentroid().getValue(5);
			double cz = cls.getCentroid().getValue(6);
			// vecteur centroid -> this
			Vector3d dir_P = new Vector3d(x - cx, y - cy, z - cz);

			// calcul de matrice des variances et covariance du cluster
			double var_x = cls.getCovmat().getElement(4, 4);
			double var_y = cls.getCovmat().getElement(5, 5);
			double var_z = cls.getCovmat().getElement(6, 6);
			double cov_xy = cls.getCovmat().getElement(4, 5);
			double cov_xz = cls.getCovmat().getElement(4, 6);
			double cov_yz = cls.getCovmat().getElement(5, 6);

			// calcul de la directions propres X_I
			double[][] C_array = {{var_x, cov_xy, cov_xz},{cov_xy, var_y, cov_yz},{cov_xz, cov_yz, var_z}};
			
			// Apache Commons code
			RealMatrix C = new Array2DRowRealMatrix(C_array);
			EigenDecomposition eigdc = new EigenDecomposition(C);
			RealMatrix eigval = eigdc.getD();
			RealMatrix eigvec = eigdc.getV();
			double z1 = eigval.getEntry(0, 0);
			double z2 = eigval.getEntry(1, 1);
			double z3 = eigval.getEntry(2, 2);
			
			double lambdaI = z1;
			double lambdaII = Double.max(z2, z3);
			int il = 0;
			if (z2 > lambdaI) {
				lambdaI = z2;
				lambdaII = Double.max(z1, z3);
				il = 1;
			}
			if (z3 > lambdaI) {
				lambdaI = z3;
				lambdaII = Double.max(z1, z2);
				il = 2;
			}
			// Apache Commons code
			double SxI = eigvec.getEntry(0, il);
			double SyI = eigvec.getEntry(1, il);
			double SzI = eigvec.getEntry(2, il);

			Vector3d XI = new Vector3d(SxI, SyI, SzI);

			// angle caractérisant l'élargissement de la zone
			values[7] = Math.sin(dir_P.angle(XI));

			// direction optimale d'usinage au centre de la zone
			double cu = cls.getCentroid().getValue(0);
			double cv = cls.getCentroid().getValue(1);
			Vector3d normal = surface.normal(cu, cv);
			Vector2d optDir = new Vector2d(normal.x, normal.y);
			double n = cls.getDataSet().size(); // nombre de point dans le cluster
			// coordonnées paramétriques du nouveau centre au cas ou le DataPoint est ajouté
			// au cluster
			double cx_new = (n * cx + x) / (n + 1);
			double cy_new = (n * cy + y) / (n + 1);
			double cz_new = (n * cz + z) / (n + 1);
			// matrice de covariance en uv du cluster au cas où le DataPoint lui est ajouté
			double varx_new = (n * (var_x + cx * cx) + x * x) / (n + 1) - Math.pow(cx_new, 2);
			double vary_new = (n * (var_y + cy * cy) + y * y) / (n + 1) - Math.pow(cy_new, 2);
			double varz_new = (n * (var_z + cz * cz) + z * z) / (n + 1) - Math.pow(cz_new, 2);
			double covxy_new = (n * (cov_xy + cx * cy) + x * y) / (n + 1) - cx_new * cy_new;
			double covxz_new = (n * (cov_xz + cx * cz) + x * z) / (n + 1) - cx_new * cz_new;
			double covyz_new = (n * (cov_yz + cy * cz) + y * z) / (n + 1) - cy_new * cz_new;

			double alpha_new = -(varx_new + vary_new + varz_new);
			double beta_new = varx_new * vary_new + varx_new * varz_new + vary_new * varz_new - Math.pow(covxy_new, 2)
					- Math.pow(covxz_new, 2) - Math.pow(covyz_new, 2);
			double gamma_new = varx_new * Math.pow(covyz_new, 2) + vary_new * Math.pow(covxz_new, 2)
					+ varz_new * Math.pow(covxy_new, 2) - varx_new * vary_new * varz_new
					- 2 * covxy_new * covxz_new * covyz_new;
			double p_new = beta_new - Math.pow(alpha_new, 2) / 3.0;
			double q_new = 2.0 * Math.pow(alpha_new, 3) / 27.0 - alpha_new * beta_new / 3.0 + gamma_new;
			double z3_new = 2.0 * Math.sqrt(-p_new / 3.0) * Math.cos(
					1.0 / 3.0 * Math.acos(3.0 * q_new / (2.0 * p_new) * Math.sqrt(-3.0 / p_new)) + 4.0 * Math.PI / 3.0)
					- alpha_new / 3.0;
			double z2_new = 2.0 * Math.sqrt(-p_new / 3.0) * Math.cos(
					1.0 / 3.0 * Math.acos(3.0 * q_new / (2.0 * p_new) * Math.sqrt(-3.0 / p_new)) + 2.0 * Math.PI / 3.0)
					- alpha_new / 3.0;
			double z1_new = 2.0 * Math.sqrt(-p_new / 3.0)
					* Math.cos(1.0 / 3.0 * Math.acos(3.0 * q_new / (2.0 * p_new) * Math.sqrt(-3.0 / p_new)))
					- alpha_new / 3.0;
			double lambdaI_new = z1_new;
			if (z2_new > lambdaI_new)
				lambdaI_new = z2_new;
			if (z3_new > lambdaI_new)
				lambdaI_new = z3_new;
//			double SxI_new = covxz_new * covyz_new - covxy_new * (varz_new - lambdaI_new);
//			double SyI_new = (varx_new - lambdaI_new) * (varz_new - lambdaI_new) - Math.pow(covxz_new, 2);
//			double SzI_new = 1 / (varz_new - lambdaI_new)
//					* (covxz_new * (covxy_new * (varz_new - lambdaI_new) - covxz_new * covyz_new) + covyz_new
//							* (Math.pow(covxz_new, 2) - (varx_new - lambdaI_new) * (varz_new - lambdaI_new)));
			List<MultivarFunction> functions_new = new ArrayList<MultivarFunction>();
			final double v1_new = varx_new;
			final double v2_new = vary_new;
			final double v3_new = varz_new;
			final double c1_new = covxy_new;
			final double c2_new = covxz_new;
			final double c3_new = covyz_new;
			final double l1_new = lambdaI_new;
			functions_new.add(new MultivarFunction(3, new MultivarEvaluable() {
				@Override
				public double eval(GVector var) {
					return (v1_new - l1_new) * var.getElement(0) + c1_new * var.getElement(1)
							+ c2_new * var.getElement(2);
				}
			}));
			functions_new.add(new MultivarFunction(3, new MultivarEvaluable() {
				@Override
				public double eval(GVector var) {
					return c1_new * var.getElement(0) + (v2_new - l1_new) * var.getElement(1)
							+ c3_new * var.getElement(2);
				}
			}));
			functions_new.add(new MultivarFunction(3, new MultivarEvaluable() {
				@Override
				public double eval(GVector var) {
					return c2_new * var.getElement(0) + c3_new * var.getElement(1)
							+ (v3_new - l1_new) * var.getElement(2);
				}
			}));
			NewtonSolver solver_new = new NewtonSolver(functions_new, 3, 10);
			solver_new.init(new GVector(new double[] { 1.0, 1.0, 1.0 }));
			try {
				solver_new.run();
			} catch (MaxIterException e) {
				throw e;
			}
			double SxI_new = solver_new.getVar().getElement(0);
			double SyI_new = solver_new.getVar().getElement(1);
			double SzI_new = solver_new.getVar().getElement(2);
			Vector3d XI_new = new Vector3d(SxI_new, SyI_new, SzI_new);
			Vector2d XI_new_proj = new Vector2d(XI_new.x, XI_new.y);
//			Vector3d normal_new = surface.normal(cu_new, cv_new);
//			Vector2d optDir_new = new Vector2d(normal_new.x, normal_new.y);
			// angle caractérisant l'alignement de la zone avec la direction d'usinage
			// optimale
			values[8] = Math.sin(XI_new_proj.angle(optDir));
			for (int i = 0; i < 4; i++)
				sum += omega[i] * Math.pow(values[i] - o.values[i], 2);
//			for (int i = 0; i < 3; i++)
//				sum += omega[i] * Math.pow(values[i] - o.values[i], 2);
//			sum += omega[3] * Math.pow(Math.sin(values[3]+Math.PI/2.0) - Math.sin(o.values[3]+Math.PI/2.0), 2);
			sum = sum + (omega[7] * Math.pow(values[7], 2) + omega[8] * Math.pow(values[8], 2))
					* (1 - Math.pow(lambdaII / lambdaI, 2));
			// (lambdaI/lambdaII)^2 or 1-(lambdaII/lambdaI)^2 choose the best ...
		}
		dist = Math.sqrt(sum);
		if (Double.isNaN(dist))
			System.out.println("NAAAAAN");
		return dist;
	}

}
