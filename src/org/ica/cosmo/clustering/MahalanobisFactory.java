package org.ica.cosmo.clustering;

import org.ica.cosmo.zoning.UVMeshUnit;

/**
 * Une fabrique pour les points de données de la métrique de Mahalanobis
 * 
 * @author redonnet
 *
 */
public class MahalanobisFactory extends DataFactory {

	@Override
	public DataPoint createData(double[] data) {
		return new MahalanobisData(data);
	}

	@Override
	public DataPoint createData(UVMeshUnit m) {
		return (new MahalanobisData(m));
	}

}
