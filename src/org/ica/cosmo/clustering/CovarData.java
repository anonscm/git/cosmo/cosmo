package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Un point de données pour la métrique Covar basée sur l'analyse en composantes
 * principales en 2D
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : pente</li>
 * <li>values[3] : angle entre la direction de plus grande pente et l'axe des X
 * </li>
 * <li>values[4] : angle caractérisant l'élargissement de la zone</li>
 * <li>values[5] : angle caractérisant l'alignement de la zone avec la direction
 * optimale d'usinage</li>
 * </ul>
 * 
 * @author redonnet
 *
 */
public class CovarData extends DataPoint {
	static {
		size = 6;
		isize = 4;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public CovarData() {
		super();
	}

	public CovarData(double[] values) {
		super(values);
	}

	public CovarData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = m.getSlope();
		values[3] = m.getOptimalDir();
	}

	/**
	 * @return a clone of the data
	 */
	public CovarData clone() {
		CovarData copy = new CovarData(values);
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double distance(DataPoint o) {
		o.setValue(4, 0.0);
		o.setValue(5, 0.0);
		double u = values[0];
		double v = values[1];
		Cluster cls = o.getCluster();
		double dist, sum = 0.0;

		if (cls == null) {
			for (int i = 0; i < isize; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else if (cls.getCovmat().isZero()) {
			for (int i = 0; i < isize; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else {
			double cu = cls.getCentroid().getValue(0);
			double cv = cls.getCentroid().getValue(1);
			// vecteur centroid -> this
			Vector2d dir_P = new Vector2d(u - cu, v - cv);

			// calcul de matrice des variances et covariance du cluster
			double var_u = cls.getCovmat().getElement(0, 0);
			double var_v = cls.getCovmat().getElement(1, 1);
			double cov = cls.getCovmat().getElement(0, 1);

			// calcul de la directions propres X_I
			double lambdaI = (var_u + var_v + Math.sqrt(Math.pow(var_u - var_v, 2) + 4.0 * cov * cov)) / 2.0;
			Vector2d XI = new Vector2d(cov, lambdaI - var_u);
			// angle caractérisant l'élargissement de la zone
			values[4] = Math.sin(dir_P.angle(XI));

			// direction optimale d'usinage au centre de la zone
			Vector3d normal = surface.normal(cu, cv);
			Vector2d optDir = new Vector2d(normal.x, normal.y);
			double n = cls.getDataSet().size(); // nombre de point dans le cluster
			// coordonnées paramétriques du nouveau centre au cas ou le DataPoint est ajouté
			// au cluster
			double cu_new = (n * cu + u) / (n + 1);
			double cv_new = (n * cv + v) / (n + 1);
			// matrice de covariance en uv du cluster au cas où le DataPoint lui est ajouté
			double varu_new = (n * (var_u + cu * cu) + u * u) / (n + 1) - Math.pow(cu_new, 2);
			double varv_new = (n * (var_v + cv * cv) + v * v) / (n + 1) - Math.pow(cv_new, 2);
			double cov_new = (n * (cov + cu * cv) + u * v) / (n + 1) - cu_new * cv_new;
			double lambdaI_new = (varu_new + varv_new
					+ Math.sqrt(Math.pow(varu_new - varv_new, 2) + 4.0 * cov_new * cov_new)) / 2.0;
			Vector2d XI_new = new Vector2d(cov_new, lambdaI_new - varu_new);
//			Vector3d normal_new = surface.normal(cu_new, cv_new);
//			Vector2d optDir_new = new Vector2d(normal_new.x, normal_new.y);
			// angle caractérisant l'alignement de la zone avec la direction d'usinage
			// optimale
			values[5] = Math.sin(XI_new.angle(optDir));
			for (int i = 0; i < size; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
//			sum = sum + 0.6*Math.pow(values[4], 2) + 0.6*Math.pow(values[5], 2);
		}
		dist = Math.sqrt(sum);
		return dist;
	}

}
