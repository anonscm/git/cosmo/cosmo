package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.HashMap;

import org.ica.cosmo.zoning.Index2;

/**
 * Une énumeration des algorithmes de clustering
 * 
 * @author redonnet
 *
 */

public enum ALGO_TYPE {
	/**
	 * L'algorithme K-Means
	 */
	KMEANS {
		@Override
		public KMeansAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
			return new KMeansAlgo(dataSet, clusters);
		}
	},
	/**
	 * L'algorithme fuzzy C-means (FCM)
	 */
	CMEANS {
		@Override
		public CMeansAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
			return new CMeansAlgo(dataSet, clusters);
		}
	},
	/**
	 * L'algorithme RPCL (Rival Penalized Competitive Learning)
	 */
	RPCL {
		@Override
		public RPCLAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
			return new RPCLAlgo(dataSet, clusters);
		}
	},
	/**
	 * L'algorithme Hierarchical clustering
	 */
	HIERARCHICAL {
		@Override
		public HierarchicalAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
			return new HierarchicalAlgo(dataSet, clusters);
		}
	},
	Voronoi {
		@Override
		public VoronoiAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters) {
			return new VoronoiAlgo(dataSet, clusters);
		}
	};

	/**
	 * Une méthode abstraite qui retourne l'algorithme créé à partir des
	 * informations passées en paramètres.
	 * 
	 * @param dataSet  a set of {@link DataPoint datapoints} to perform clustering
	 * @param clusters the initial clusters
	 * @return a concrete implementation of {@link AbstractAlgo }
	 */
	public abstract AbstractAlgo create(HashMap<Index2,DataPoint> dataSet, ArrayList<Cluster> clusters);
}
