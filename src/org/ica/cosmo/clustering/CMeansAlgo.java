package org.ica.cosmo.clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.ica.cosmo.zoning.Index2;

public class CMeansAlgo extends AbstractAlgo {

	/** membership matrix */
	double[][] mu = new double[this.dataSet.size()][this.clusters.size()];

	/** fuzzifier (m>1) */
	double m = 2.0;

	/** fuzzy centers */
	DataPoint[] centers = new DataPoint[this.clusters.size()];

	public CMeansAlgo(HashMap<Index2, DataPoint> dataSet, ArrayList<Cluster> clusters) {
		super(dataSet, clusters);
	}

	public void run() {
		int loop = 0;
		// centers initialization
		clusters.forEach(c -> centers[c.getId()] = c.getCentroid());
		// getting indexes array
		Index2[] idx = dataSet.keySet().toArray(new Index2[0]);
		for (int id = 0; id < idx.length; id++) {
			Arrays.setAll(mu[id], i -> 0.0);
		}
		while (loop < 30) {
			loop = loop + 1;
			// updating memberships
			for (int id = 0; id < idx.length; id++) {
				// distance from cluster k
				double[] di = new double[clusters.size()];
				double sum_i = 0.0;
				for (int k = 0; k < clusters.size(); k++) {
					di[k] = Double.max(0.00001, dataSet.get(idx[id]).distance(clusters.get(k).getCentroid()));
					sum_i = sum_i + 1.0 / Math.pow(di[k], 2.0 / (m - 1));
				}
				for (int j = 0; j < clusters.size(); j++) {
					mu[id][j] = 1.0 / (Math.pow(di[j], 2.0 / (m - 1)) * sum_i);
				}
			}
			// updating centers
			double val, sum1, sum2;
			for (int j = 0; j < clusters.size(); j++) {
				for (int s = 0; s < DataPoint.isize; s++) {
					val = 0.0;
					sum1 = 0.0;
					sum2 = 0.0;
					for (int id = 0; id < idx.length; id++) {
						val = dataSet.get(idx[id]).getValue(s);
						sum1 = sum1 + Math.pow(mu[id][j], m) * val;
						sum2 = sum2 + Math.pow(mu[id][j], m);
					}
					clusters.get(j).getCentroid().setValue(s, sum1 / sum2);
				}
			}
		}
		for (int id = 0; id < idx.length; id++) {
			int j0 = 0;
			double max = 0.0;
			for (int j = 0; j < clusters.size(); j++) {
				if (mu[id][j] > max) {
					max = mu[id][j];
					j0 = j;
				}
			}
			Cluster bestCluster = clusters.get(j0);
			bestCluster.add(dataSet.get(idx[id]));
		}
		clusters.forEach(c -> c.centralize()); // to comment?
	}

}
