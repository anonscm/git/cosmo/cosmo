package org.ica.cosmo.clustering;

import java.util.Arrays;

import org.ica.cosmo.zoning.UVMeshUnit;
import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.GVector;

/**
 * Un point de données pour la métrique NormalVec
 * 
 * <ul>
 * <li>values[0] : u</li>
 * <li>values[1] : v</li>
 * <li>values[2] : nx, la projection du vecteur normal n(u,v) selon l'axe des
 * X</li>
 * <li>values[3] : ny, la projection du vecteur normal n(u,v) selon l'axe des Y
 * </li>
 * </ul>
 * 
 * @author Mahfoud
 *
 */
public class NormalVecData extends DataPoint {
	static {
		size = 4;
		isize = 4;
		omega = new double[size];
		Arrays.setAll(omega, i -> 1.0);
	}

	public NormalVecData() {
		super();
	}

	public NormalVecData(double[] values) {
		super(values);
	}

	public NormalVecData(UVMeshUnit m) {
		super();

		this.surface = m.getCenter().getSurface();

		values[0] = m.getCenter().u;
		values[1] = m.getCenter().v;
		values[2] = surface.normal(m.getCenter().u, m.getCenter().v).x;
		values[3] = surface.normal(m.getCenter().u, m.getCenter().v).y;
	}

	@Override
	public DataPoint clone() {
		NormalVecData copy = new NormalVecData(getValues());
		copy.values = Arrays.copyOf(values, size);
		copy.surface = this.surface;
		return copy;
	}

	@Override
	public double distance(DataPoint o) {
		Cluster cls = o.getCluster();
		double dist, sum = 0.0;

		if (cls == null) {
			for (int i = 0; i < size; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else if (cls.getCovmat().isZero()) {
			for (int i = 0; i < size; i++)
				sum += Math.pow(values[i] - o.values[i], 2);
		} else {
			GVector x = new GVector(values, 2);
			x.sub(new GVector(o.values, 2));
			double[] array = { cls.getCovmat().getElement(2, 2), cls.getCovmat().getElement(2, 3),
					cls.getCovmat().getElement(3, 2), cls.getCovmat().getElement(3, 3) };
			GMatrix invCovMat = new GMatrix(2, 2, array);
			invCovMat.invert();
			GVector tmp = new GVector(x.getSize());
			tmp.mul(invCovMat, x);
			sum = x.dot(tmp);
		}
		dist = Math.sqrt(sum);
		return dist;
	}

}
