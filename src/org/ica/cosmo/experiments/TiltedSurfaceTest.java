package org.ica.cosmo.experiments;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.ica.cosmo.demos.SurfaceBuilder;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.UVMeshUnit;
import org.lgmt.dgl.surfaces.Surface;

public class TiltedSurfaceTest {
	/** surface */
	private Surface surface;

	/** tessellation */
	private int tessU = 80;
	private int tessV = 80;

	/** main program */
	public static void main(String[] args) {
		double[] alpha = { 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80,
				85.0, 90.0 };
		for (int k = 0; k < alpha.length; k++) {
			new TiltedSurfaceTest(alpha[k]);
		}
	}

	public TiltedSurfaceTest(double alpha) {
		super();
		NumberFormat f = new DecimalFormat("#0.00");
		// create the surface
		initModel(alpha);
		SurfaceMap map = new SurfaceMap(surface, tessU, tessV);
		map.init1Zone();
		// calculation of parameters
		double mean_slope = 0.0;
		double var_x = 0.0;
		double var_y = 0.0;
		double var_z = 0.0;
		double cov_xy = 0.0;
		double cov_xz = 0.0;
		double cov_yz = 0.0;
		double moy_x = 0.0;
		double moy_y = 0.0;
		double moy_z = 0.0;
		double n = tessU * tessV;
		for (UVMeshUnit[] meshline : map.getMeshGrid()) {
			for (UVMeshUnit mesh : meshline) {
				mean_slope = mean_slope + mesh.getSlope() / n;
				double u = mesh.getCenter().u;
				double v = mesh.getCenter().v;
				double Sx = surface.evalX(u, v);
				double Sy = surface.evalY(u, v);
				double Sz = surface.evalZ(u, v);
				moy_x = moy_x + Sx;
				moy_y = moy_y + Sy;
				moy_z = moy_z + Sz;
				cov_xy = cov_xy + Sx * Sy;
				cov_xz = cov_xz + Sx * Sz;
				cov_yz = cov_yz + Sy * Sz;
				var_x = var_x + Math.pow(Sx, 2);
				var_y = var_y + Math.pow(Sy, 2);
				var_z = var_z + Math.pow(Sz, 2);
			}
		}
		moy_x = moy_x / (Double) n;
		moy_y = moy_y / (Double) n;
		moy_z = moy_z / (Double) n;
		cov_xy = cov_xy / (Double) n;
		cov_xz = cov_xz / (Double) n;
		cov_yz = cov_yz / (Double) n;
		var_x = var_x / (Double) n;
		var_y = var_y / (Double) n;
		var_z = var_z / (Double) n;
		var_x = var_x - Math.pow(moy_x, 2.0);
		var_y = var_y - Math.pow(moy_y, 2.0);
		var_z = var_z - Math.pow(moy_z, 2.0);
		cov_xy = cov_xy - moy_x * moy_y;
		cov_xz = cov_xz - moy_x * moy_z;
		cov_yz = cov_yz - moy_y * moy_z;
		double[][] C_array = { { var_x, cov_xy, cov_xz }, { cov_xy, var_y, cov_yz }, { cov_xz, cov_yz, var_z } };
		RealMatrix C = new Array2DRowRealMatrix(C_array);
		EigenDecomposition eigdc = new EigenDecomposition(C);
		RealMatrix eigval = eigdc.getD();
		RealMatrix eigvec = eigdc.getV();

		class Sortbyeigvalue implements Comparator<Integer> {
			public int compare(Integer i1, Integer i2) {
				return (int) (eigval.getEntry(i1, i1) - eigval.getEntry(i2, i2));
			}
		}

		Integer[] il = { 0, 1, 2 };
		Arrays.sort(il, new Sortbyeigvalue());
		double lambdaI = eigval.getEntry(2, 2);
		double lambdaII = eigval.getEntry(1, 1);
		System.out.println(lambdaI + " " + lambdaII);

		double SxI = eigvec.getEntry(0, 2);
		double SyI = eigvec.getEntry(1, 2);
		double SzI = eigvec.getEntry(2, 2);

		double ratio = Math.sqrt(Math.sqrt(lambdaI / lambdaII));

//		MillingContext.getInstance().setCutter(new Cutter(5.0, 2.0, 100.0));
//		MillingContext.getInstance().setScallopHeight(0.01);
//		
//
//		ArrayList<Double> angles = new ArrayList<Double>();
////		angles.add(Math.PI / 2); // principal direction
////		if (theta != Math.PI / 2) {
////			angles.add(theta); // slope direction
////			angles.add((2 * theta + Math.PI) / 4);
////		}
//		for (int index = 0; index < angles.size(); index++) {
//			double totalLength = 0.0;
//			double totalTime = 0.0;
//			double vfAngle = angles.get(index);
//			ZoneMilling_PPV3A_Approx zMilling = new ZoneMilling_PPV3A_Approx(map.getZone(0), vfAngle, true);
//			zMilling.run();
////			totalLength = totalLength + zMilling.getToolPathApproxLength() + zMilling.getPenalty();
//			totalLength = totalLength + zMilling.getToolPathApproxLength();
//			totalTime = totalTime + zMilling.getMillingTimeApprox();
//			System.out.println(f.format(totalTime));
////			System.out.println("scale=" + f.format(scale) + ", theta=" + f.format(theta) + ", slope=" + f.format(slope)
////					+ ", outil=" + f.format(rayon) + ", dir_usi=" + f.format(vfAngle) + ", long_usi="
////					+ f.format(totalLength) + ", time_usi=" + f.format(totalTime));
//		}
	}

	public void initModel(double alpha) {
		this.surface = SurfaceBuilder.getTiltedTile(alpha);
	}

}
