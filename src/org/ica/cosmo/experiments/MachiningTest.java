package org.ica.cosmo.experiments;

import java.io.File;
import java.util.ArrayList;

import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.AveragePDE;
import org.ica.cosmo.machining.SodException;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.tools.FileManager;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkPolyline;
import org.ica.vtkviewer.model.VtkSurface;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;

import vtk.vtkNativeLibrary;

public class MachiningTest {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	
	/** surface */
	private Surface surface;
	
	/** nombre de zones */
	private int k = 3;


	/** main program */
	public static void main(String[] args) {
		new MachiningTest();
	}

	public MachiningTest() {
		super();
		MillingContext.getInstance().setCutter(new Cutter(5.0, 2.0, 100.0));
		MillingContext.getInstance().setScallopHeight(0.01);

		String filename = new String("choi80x80KmeansSlope4.map");
		File f = new File(FileManager.getInstance().getResourcesPath()+"/"+filename);
		SurfaceMap map = FileManager.getInstance().loadMap(f);
		this.k = map.getZonesCount();
		this.surface = map.getSurface();

		VtkModel model = new VtkModel();
		model.add(new VtkSurface(surface));
		VtkViewer viewer = new VtkViewer(model, "Machining Demo");
		viewer.run();

		ArrayList<ZoneMilling_PPV3A> zma = new ArrayList<ZoneMilling_PPV3A>(k);
		double totalLength = 0.0;
		double totalTime = 0.0;
		long start_time = System.currentTimeMillis();
		for (int zj = 0; zj < k; zj++) {
			System.out.println("Milling zone " + zj);
			Zone z = map.getZone(zj);
			/*
			 * Uncomment one of the two lines below to choose how the machining direction is
			 * calculated for a given zone. </br>
			 * AveragePDE : average of maximum slope direction all over the zone (default)
			 * SinglePointPDE : maximum slope direction of a given point (typically the centroid)
			 */
			z.setEvaluator(new AveragePDE(z));
			// z.setEvaluator(new SinglePointPDE(z, c.getCentroid(zj)));
			double angle = z.evalPrefDir();
			System.out.println("angle = " + angle);
			APPROX_LEVEL apxl = APPROX_LEVEL.ULTRA;
			ZoneMilling_PPV3A zMilling = new ZoneMilling_PPV3A(z, angle, true, apxl);
			zma.add(zMilling);
			try {
				zMilling.run();
			} catch (SodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			totalLength = totalLength + zMilling.getToolPathApproxLength();// + zMilling.getPenalty();
			totalTime = totalTime + zMilling.getMillingTimeApprox();
			System.out.println("longueur d'usinage de la zone = " + zMilling.getToolPathApproxLength());
			System.out.println("durée d'usinage de la zone = " + zMilling.getMillingTimeApprox());

		}
		long end_time = System.currentTimeMillis();
		long sim_duration = end_time - start_time;

		System.out.println("durée de simulation de l'usinage = " + sim_duration + " ms");
		System.out.println("longueur de trajectoire d'usinage = " + totalLength);
		System.out.println("durée de l'usinage = " + totalTime + " s");

		for (ZoneMilling_PPV3A zMilling : zma) {
			for (SurfacePointsCurve spc : zMilling.getRawToolpath())
					model.add(new VtkPolyline(spc, 2.0f, new Color4d(1.0, 1.0, 0.0)));
		}
	}
}
