package org.ica.cosmo.experiments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.ica.cosmo.application.GCodeWriter;
import org.ica.cosmo.tools.FileManager;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkToolpath;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.TBAnglesXYZi;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkNativeLibrary;

public class TestCycle800 {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	static {
		Locale.setDefault(Locale.US);
	}

	private Toolpath<Point3d> getToolpath(){
		List<Point3d> pts = new ArrayList<Point3d>();
		pts.add(new Point3d(0, 0, 0));
		pts.add(new Point3d(20, 0, 0));
		pts.add(new Point3d(20, -10, 0));
		pts.add(new Point3d(-20, -10, 0));
		pts.add(new Point3d(-20, 10, 0));
		pts.add(new Point3d(0, 10, 0));
		pts.add(new Point3d(0, 0, 0));
				
		return new Toolpath<Point3d>(pts);
	}
	
	public TestCycle800() {
		VtkModel model = new VtkModel();
		Toolpath<Point3d> tp0 = getToolpath();
		model.add(new VtkToolpath(tp0));
		VtkViewer viewer = new VtkViewer(model);
		viewer.run();
		
		Vector3d t1 = new Vector3d(0,10,20);
		AxisAngle4d r1 = new AxisAngle4d(Vector3d.Z, Math.toRadians(90));
		AxisAngle4d r2 = new AxisAngle4d(Vector3d.X, Math.toRadians(45));
		Matrix4d m1 = new Matrix4d(r2);
		m1.mul(new Matrix4d(r1));
		m1.setTranslation(t1);
		
		Toolpath<Point3d> tp1 = getToolpath();
		tp1.transform(m1);
		model.add(new VtkToolpath(tp1));
		
		writeGCode(m1, tp0, tp1);
		
		
	}

	private void writeToolpath(Toolpath<Point3d> tp, GCodeWriter writer) {
		Point3d p = tp.getFirst();
		writer.appendLine(String.format("G0 X%.3f Y%.3f Z%.3f", p.x, p.y, p.z));
		if (tp.getICode() == INTERPOLATION_TYPE.G0)
			writer.appendLine("G0");
		if (tp.getICode() == INTERPOLATION_TYPE.G1)
			writer.appendLine("G1");
		Iterator<? extends Point3d> it = tp.iterator();
		while (it.hasNext()) {
			p = it.next();
			writer.appendLine(p);
		}
	}

	
	public void writeGCode(Matrix4d m, Toolpath<Point3d> tp0, Toolpath<Point3d> tp1) {
		GCodeWriter.setResourcesPath(FileManager.getResourcesPath());
		String name = new String("cycle800_03");
		GCodeWriter writer = new GCodeWriter(name);
		List<String> header = new ArrayList<String>();
		header.add("G54");
		header.add("CYCLE832(0.02,1,1)");
		header.add("G642");
		header.add("SOFT");
		header.add("CUT2D");		
		header.add("G0 SUPA Z-5 D0");
		header.add("M23");
		header.add("CYCLE800(1,\"TC8\",0,57,0.000,0.000,0.000,0.000,0.000,0.000,0,0,0,-1,0,1)");
		header.add("T=\"FRAISE_D16_R4\"");
		header.add("M6");
		header.add("G0 SUPA Z-5 D0");
		header.add("D1");
		header.add("G54");
		header.add("G17");
		header.add("S18000 F3600");
		writer.appendLines(header);

		writer.appendLine(String.format("MSG(\"--- Toolpath 0 ---\")"));
		writeToolpath(tp0, writer);
		
		writer.appendLine(String.format("MSG(\"--- Toolpath 1 ---\")"));
		writeToolpath(tp1, writer);
		
		writer.appendLine(String.format("MSG(\"--- Toolpath 0 transformed ---\")"));
		Vector3d t = new Vector3d(m.getTranslation());
		TBAnglesXYZi tba = new TBAnglesXYZi(m.getRotation());
		String str800 = new String("CYCLE800(");
		// writer.appendLine(String.format("TRANS X%.3f Y%.3f Z%.3f", t.x, t.y, t.z));
//		// TODO: voir PM 10600 pour programmer directement les angles d'Euler
//		this.appendLine(String.format("%s X%.3f Y%.3f Z%.3f\n", code, ea.psi, ea.theta, ea.phi));
//		writer.appendLine(String.format("AROT Z%.3f", Math.toDegrees(ea.psi)));
//		writer.appendLine(String.format("AROT X%.3f", Math.toDegrees(ea.theta)));
//		writer.appendLine(String.format("AROT Z%.3f", Math.toDegrees(ea.phi)));
		str800 = str800 + "1,"; // dégagement stadard selon Z
		str800 = str800 + "\"TC8\","; // nom du bloc de paramètre d'orientation (?)
		str800 = str800 + "100000,"; // nouveau plan orienté
		// Rotations X(01), Y(10) Z(11) => 111001 (bin) = 57 (décimal). NOTA: binaire : axe3/axe2/axe1
		str800 = str800 + "57,";
		// Point de ref avant l'orientation : origine du nouveau repềre
		str800 = str800 + String.format("%.3f,%.3f,%.3f,", t.x, t.y, t.z);
		str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.alpha)); // rotation autour de X
		str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.beta)); // rotation autour de Y
		str800 = str800 + String.format("%.3f,", Math.toDegrees(tba.gamma)); // rotation autour de Z
		str800 = str800 + "0,0,0,"; // origine après l'orientation
		str800 = str800 + "-1,"; // orientation : OUI, plus petite valeur de l'axe rotatif
		str800 = str800 + "0,"; // dégagement incrémental dans le sens de l'outil
		str800 = str800 + "1)"; // ???
		writer.appendLine(str800);
		writer.appendLine("M22 M8 M3 D1");
		writeToolpath(tp0, writer);

		writer.appendLine("M23 M9 M5");
		writer.appendLine("G0 SUPA Z-5 D0"); // dégagement en rapide
//		writer.appendLine("ROT");
		writer.appendLine("G54");
//		writer.appendLine("G0 A0 C0");
		
		List<String> footer = new ArrayList<String>();
//		footer.add("G0 SUPA Z-5 D0");
		footer.add("M30");
		writer.appendLines(footer);

		writer.write();

	}
	
	public static void main(String[] args) {
		FileManager.setResourcesPath(new String("/tmp"));

		new TestCycle800();
	}

}
