package org.ica.cosmo.experiments;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.demos.SurfaceBuilder;
import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.AveragePDE;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkPolyline;
import org.ica.vtkviewer.model.VtkSurface;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;

import vtk.vtkNativeLibrary;

/**
 * Multi-treaded full run with VTK display
 * 
 * @author redonnet
 *
 */
public class MTfullRunVTK {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}
	private int nbCores = Runtime.getRuntime().availableProcessors();

	private final ExecutorService executor = Executors.newFixedThreadPool(nbCores);
	private final CompletionService<RunContext> sCompletion = new ExecutorCompletionService<RunContext>(executor);
	private final CompletionService<RunContext> kCompletion = new ExecutorCompletionService<RunContext>(executor);
	private final CompletionService<RunContext> mCompletion = new ExecutorCompletionService<RunContext>(executor);

	private Surface surface;
	private int tessU = 200; // WARNING: high tessellation probably needs large stack size
	private int tessV = 200;
	private int nbk = 10;

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		new MTfullRunVTK();
	}

	public MTfullRunVTK() throws InterruptedException, ExecutionException {
		System.out.println("Running on " + nbCores + " cores");

		MillingContext.getInstance().setCutter(new Cutter(5.0, 2.0, 100.0));
		MillingContext.getInstance().setScallopHeight(0.01);
		MillingContext.getInstance().setVf(3.0/60.0); // m/min => en m/s

		this.surface = SurfaceBuilder.getChoi();

		int nbSetupFutures = 0;
		SetupProcess setup;
		for (int i = 0; i < nbk - 2; i++) {
			int k = i + 2;
			setup = new SetupProcess(surface, k, tessU, tessV);
			sCompletion.submit(setup);
			nbSetupFutures++;
		}

		int nbKMeansFutures = 0;
		KMeansProcess kMeansProcess;
		while (nbSetupFutures > 0) {
			Future<RunContext> setupFuture = sCompletion.take();
			nbSetupFutures--;
			RunContext context = setupFuture.get();
			kMeansProcess = new KMeansProcess(context);
			kCompletion.submit(kMeansProcess);
			nbKMeansFutures++;
		}

		int nbMillingProcess = 0;
		MillingProcess kMillingProcess;
		while (nbKMeansFutures > 0) {
			Future<RunContext> kMeansFuture = kCompletion.take();
			nbKMeansFutures--;
			RunContext context = kMeansFuture.get();
			kMillingProcess = new MillingProcess(context);
			mCompletion.submit(kMillingProcess);
			nbMillingProcess++;
		}

		double objective = Double.MAX_VALUE;
		int kopt = 0;
		while (nbMillingProcess > 0) {
			Future<RunContext> millingFuture = mCompletion.take();
			nbMillingProcess--;
			RunContext context = millingFuture.get();
			int k0 = context.getInitialK();
			int k = context.getK();
			double length = 0.0;
			double duration = 0.0;
			for (ZoneMilling_PPV3A zma : context.getZmaList()) {
				length = length + zma.getToolPathApproxLength() + zma.getPenalty();
				duration = duration + zma.getMillingTimeApprox();
			}
			System.out.println("k = " + k0 + " (final "+k+") :");
			System.out.printf("\t Milling time = %.3f s\n", duration);
			System.out.printf("\t Milling length = %.3f mm\n", length);
			if (duration < objective) {
				kopt = k0;
				objective = duration;
			}
		}
		System.out.println("K opt = " + kopt);
		executor.shutdown();
	}

	/**
	 * A simple class to handle all data requested for a single run
	 * 
	 * @author redonnet
	 *
	 */
	private class RunContext {
		private int k0; // initial value of k
		private int k;
		private SurfaceMap map;
		private ArrayList<ZoneMilling_PPV3A> zmaList;
		private VtkModel zModel; // clustering
		private VtkModel sModel; // machining

		public RunContext(Surface surface, int k, int tessU, int tessV) {
			super();
			this.k = k;
			this.k0 = k;
			map = new SurfaceMap(surface, tessU, tessV);
			map.initNZone(k);

			zmaList = new ArrayList<ZoneMilling_PPV3A>();

			zModel = new VtkModel();
			zModel.add(new VtkSurfaceMap(map));

			sModel = new VtkModel();
			sModel.add(new VtkSurface(surface));
		}

		public int getK() {
			return k;
		}
		
		public int getInitialK() {
			return k0;
		}

		public SurfaceMap getMap() {
			return map;
		}

		public ArrayList<ZoneMilling_PPV3A> getZmaList() {
			return zmaList;
		}

		public VtkModel getzModel() {
			return zModel;
		}

		@SuppressWarnings("unused")
		public VtkModel getsModel() {
			return sModel;
		}
	}

	/**
	 * Setup GUI and init map
	 * 
	 * @author redonnet
	 *
	 */
	private class SetupProcess implements Callable<RunContext> {
		private RunContext context;
		private VtkViewer zViewer;
		private VtkViewer sViewer;

		public SetupProcess(Surface s, int k, int tessU, int tessV) {
			context = new RunContext(s, k, tessU, tessV);
		}

		@Override
		public RunContext call() throws Exception {
			int k = context.getK();

			String zTitle = new String("Clustering, K = " + k);
			zViewer = new VtkViewer(context.getzModel(), zTitle);
			zViewer.run();

			String sTitle = new String("Machining, K = " + k);
			sViewer = new VtkViewer(context.sModel, sTitle);
			sViewer.run();

			return this.context;
		}
	}

	private class KMeansProcess implements Callable<RunContext> {
		private RunContext context;

		public KMeansProcess(RunContext context) {
			this.context = context;
		}

		@Override
		public RunContext call() throws Exception {
			ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;
			METRIC metric = METRIC.SLOPE;

			int k = context.getK();
			SurfaceMap map = context.getMap();

			Clustering c = new Clustering(k, map, algo_type, metric, true);
			c.run();

			try {
				map = c.call();
			} catch (Exception e) {
				e.printStackTrace();
			}

			context.k = map.getZonesCount();

			return context;
		}
	}

	private class MillingProcess implements Callable<RunContext> {
		private RunContext context;

		public MillingProcess(RunContext context) {
			this.context = context;
		}

		public RunContext call() throws Exception {
			int k = context.getK();
			SurfaceMap map = context.getMap();
			List<ZoneMilling_PPV3A> zmaList = context.getZmaList();

			for (int zId = 0; zId < k; zId++) {
				Zone z = map.getZone(zId);
				z.setEvaluator(new AveragePDE(z));
				double vfAngle = z.evalPrefDir();
				APPROX_LEVEL apxl = APPROX_LEVEL.ULTRA;
				ZoneMilling_PPV3A zMilling = new ZoneMilling_PPV3A(z, vfAngle, true, apxl);
				zMilling.run();
				zmaList.add(zMilling);
			}
			for (ZoneMilling_PPV3A zMilling : zmaList) {
				for (SurfacePointsCurve spc : zMilling.getRawToolpath())
						context.sModel.add(new VtkPolyline(spc, 2.0f, new Color4d(1.0, 1.0, 0.0)));
			}
			return context;
		}
	}
}
