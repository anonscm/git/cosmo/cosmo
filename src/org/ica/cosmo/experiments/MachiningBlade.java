package org.ica.cosmo.experiments;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.AveragePDE;
import org.ica.cosmo.machining.G00Connector;
import org.ica.cosmo.machining.Hull;
import org.ica.cosmo.machining.SodException;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkSurface;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.ica.vtkviewer.model.VtkToolpath;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.io.IGESReader;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.INTERPOLATION_TYPE;
import org.lgmt.jcam.toolpath.PRG_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkLookupTable;
import vtk.vtkNativeLibrary;

public class MachiningBlade implements PropertyChangeListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	/** contexte d'usinage */
	private static MillingContext mc = MillingContext.getInstance();

	/** méthode de clustering à utiliser */
	private ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;

	/** métrique */
	private METRIC metric = METRIC.SLOPE;

	/** surface */
	private Surface surface;

	/** nombre de zones */
	private int nK = 4;

	/** niveau d'approximation */
	private APPROX_LEVEL apxl = APPROX_LEVEL.MEDIUM;

	private VtkModel model;

	/** main program */
	public static void main(String[] args) {
		new MachiningBlade();
	}

	public MachiningBlade() {
		super();
		mc.setCutter(new Cutter(5.0, 2.0, 100.0));
		mc.setScallopHeight(0.1);
		mc.setPrgType(PRG_TYPE.CL);

		File file = new File("/home/redonnet/Recherche/IGESData/sBez.igs");
		IGESReader reader = new IGESReader(file);
		reader.read();
		surface = reader.getSurfaces(0);
		System.out.println("Surface loaded: " + surface.toString());

		SurfaceMap map = new SurfaceMap(surface, apxl);
		Clustering c = new Clustering(nK, map, algo_type, metric, false);
		c.run();
		try {
			map = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		nK = c.getCentroids().length;

//		VtkModel modelC = new VtkModel();
//		VtkViewer viewerC = new VtkViewer(modelC, "Clustering");
//		viewerC.run();
//		viewerC.setBackground(new Color4d(1.0, 1.0, 1.0, 1.0));
//		modelC.add(new VtkSurfaceMap(map));

		model = new VtkModel();
		VtkViewer viewerG = new VtkViewer(model, "Model G");
		viewerG.run();
		VtkSurface vtkSurf = new VtkSurface(surface);
		vtkSurf.setNormalsLength(5.0);
		model.add(vtkSurf);
		VtkSurfaceMap vtkSurfaceMap = new VtkSurfaceMap(map); 
		vtkLookupTable lut = vtkSurfaceMap.getLut();
		
		Hull hull = new Hull(surface, 3);
		G00Connector.setHull(hull);
//		VtkHull vtkHull = new VtkHull(hull);
//		model.add(vtkHull);
		
		for (int k = 0; k < nK; k++) {
			Zone zone = map.getZone(k);
			
			System.out.println("Milling zone " + k);
			zone.setEvaluator(new AveragePDE(zone));
			// vfangle = (X,vf)
			double vfAngle = zone.evalPrefDir();
			// Usinage à x=cste => vf // Y => rotation = Pi/2 - vfAngle
			double rotation = Math.PI / 2.0 - vfAngle;
			System.out.printf("angle = %.3f° \n", Math.toDegrees(rotation));
			ZoneMilling_PPV3A zMilling = new ZoneMilling_PPV3A(zone, rotation, apxl);
			zMilling.setG00Arrival(10.0);
			zMilling.addPropertyChangeListener(this);

			try {
				zMilling.run();
			} catch (SodException e1) {
				e1.printStackTrace();
			}
			
			// Show raw trajectories
//			for(List<Toolpath<SurfacePoint>> tpList : zMilling.getRawTrajBuilder().getRawToolpath().values()) {
//				for(Toolpath<SurfacePoint> tp: tpList) {
//					VtkToolpath vtkTp = new VtkToolpath(tp, 2.0f, new Color4d(1.0, 1.0, 1.0, 1.0));
//					vtkTp.setPointsVisibility(true);
//					model.add(vtkTp);
//				}
//			}
		}

		
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "AddObject") {
			Object obj = event.getNewValue();
			if (obj instanceof Toolpath<?>) {
				@SuppressWarnings("unchecked")
				Toolpath<Point3d> tp = (Toolpath<Point3d>) event.getNewValue();
				Color4d color = new Color4d(1.0, 1.0, 0.0, 1.0);
				if(tp.getICode() == INTERPOLATION_TYPE.G00)
					color = new Color4d(1.0, 0.5, 0.0, 1.0);
				VtkToolpath vtkTp = new VtkToolpath(tp, 2.0f, color);
				vtkTp.setPointsVisibility(false);
				model.add(vtkTp);
			}
		}
	}
}
