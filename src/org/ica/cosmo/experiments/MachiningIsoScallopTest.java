package org.ica.cosmo.experiments;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.demos.SurfaceBuilder;
import org.ica.cosmo.machining.ZoneMilling_IsoScallop3A;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.ica.vtkviewer.model.VtkToolpath;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkNativeLibrary;

public class MachiningIsoScallopTest implements PropertyChangeListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	/** méthode de clustering à utiliser */
	private ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;

	/** métrique */
	private METRIC metric = METRIC.SLOPE;

	/** surface */
	private Surface surface;

	/** tessellation */
	private int tessU = 80;
	private int tessV = 80;

	/** nombre de zones */
	private int k = 3;

	VtkModel model;
	
	/** main program */
	public static void main(String[] args) {
		new MachiningIsoScallopTest();
	}

	public MachiningIsoScallopTest() {
		super();
		MillingContext.getInstance().setCutter(new Cutter(5.0, 2.0, 100.0));
		MillingContext.getInstance().setScallopHeight(0.1);
		MillingContext.getInstance().setPasLongitudinal(1.0);
		this.surface = SurfaceBuilder.getTile();
		

		SurfaceMap map = new SurfaceMap(surface, tessU, tessV);
		Clustering c = new Clustering(k, map, algo_type, metric, true);
		c.run();
		try {
			map = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		k = c.getCentroids().length;
		
		model = new VtkModel();
		model.add(new VtkSurfaceMap(map));
		VtkViewer viewer = new VtkViewer(model, "Machining Demo");
		viewer.run();

		ArrayList<ZoneMilling_IsoScallop3A> zma = new ArrayList<ZoneMilling_IsoScallop3A>(k);
//		double totalLength = 0.0;
//		double totalTime = 0.0;
//		long start_time = System.currentTimeMillis();
		for (int zj = 0; zj < k; zj++) {
			System.out.println("Milling zone " + zj);
			Zone z = map.getZone(zj);
			SurfacePoint center = z.getCenter(); 
			ZoneMilling_IsoScallop3A zMilling = new ZoneMilling_IsoScallop3A(z, center, true);
			zMilling.addPropertyChangeListener(this);
			zma.add(zMilling);
			zMilling.run();

//			totalTime = totalTime + zMilling.getMillingTimeApprox();
//			totalLength = totalLength + zMilling.getToolPathApproxLength();
//			System.out.println("longueur d'usinage de la zone = " + zMilling.getToolPathApproxLength());
//			System.out.println("durée d'usinage de la zone = " + zMilling.getMillingTimeApprox() + " s");

		}
//		long end_time = System.currentTimeMillis();
//		long sim_duration = end_time - start_time;
//
//		System.out.println("durée de simulation de l'usinage = " + sim_duration + " ms");
//		System.out.println("longueur de trajectoire d'usinage = " + totalLength);
//		System.out.println("durée de l'usinage = " + totalTime + " s");

	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName() == "NewToolpath") {
			Toolpath<? extends Point3d> traj = (Toolpath<?>)event.getNewValue();
			model.add(new VtkToolpath(traj, 4.0f, new Color4d(1.0, 1.0, 0.0)));
		}
	}

}
