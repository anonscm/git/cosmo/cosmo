package org.ica.cosmo.demos;

import java.util.ArrayList;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.Clustering.INIT;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.AveragePDE;
import org.ica.cosmo.machining.SodException;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.util.ToolpathSignal;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.support.IListener;
import org.ica.support.Signal;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.Color4d;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkSurface;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.ica.vtkviewer.model.VtkToolpath;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;
import org.lgmt.jcam.toolpath.PRG_TYPE;
import org.lgmt.jcam.toolpath.Toolpath;

import vtk.vtkNativeLibrary;

public class MachiningDemo implements IListener {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	/** méthode de clustering à utiliser */
	private ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;

	/** métrique */
	private METRIC metric = METRIC.SLOPE;

	/** surface */
	private Surface surface;

	/** nombre de zones */
	private int nK = 3;

	private APPROX_LEVEL apxl = APPROX_LEVEL.FINE;

	/** main program */
	public static void main(String[] args) {
		new MachiningDemo();
	}

	private VtkModel model;

	public MachiningDemo() {
		super();
		MillingContext.getInstance().setCutter(new Cutter(3.0, 1.0, 50.0));
		MillingContext.getInstance().setScallopHeight(0.01);
		MillingContext.getInstance().setPrgType(PRG_TYPE.CC);
		MillingContext.getInstance().setVf(5000); // Vf = 5 m/min

		this.surface = SurfaceBuilder.getChoi();

		model = new VtkModel();
		model.add(new VtkSurface(surface));
		VtkViewer viewer = new VtkViewer(model, "Machining Demo");
		viewer.run();

		SurfaceMap map = new SurfaceMap(surface, apxl);
		Clustering c = new Clustering(nK, map, algo_type, metric, true);
		c.setInit_method(INIT.ANGULAR);
		c.run();
		try {
			map = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		nK = c.getCentroids().length;

		VtkModel modelC = new VtkModel();
		VtkViewer viewerC = new VtkViewer(modelC, "Clustering");
		viewerC.run();
		viewerC.setBackground(new Color4d(1.0, 1.0, 1.0, 1.0));
		modelC.add(new VtkSurfaceMap(map));

		ArrayList<ZoneMilling_PPV3A> zma = new ArrayList<ZoneMilling_PPV3A>(nK);
		double totalLength = 0.0;
		double totalTime = 0.0;
		long start_time = System.currentTimeMillis();
		for (int k = 0; k < nK; k++) {
			System.out.println("Milling zone " + k);
			Zone z = map.getZone(k);
			/*
			 * Uncomment one of the two lines below to choose how the machining direction is
			 * calculated for a given zone. </br> AveragePDE : average of maximum slope
			 * direction all over the zone (default) SinglePointPDE : maximum slope
			 * direction of a given point (typically the centroid)
			 */
			z.setEvaluator(new AveragePDE(z));
			// z.setEvaluator(new SinglePointPDE(z, c.getCentroid(zj)));
			double angle = z.evalPrefDir();
			System.out.println("angle = " + Math.toDegrees(angle) + "°");
			APPROX_LEVEL apxl = APPROX_LEVEL.ULTRA;
			ZoneMilling_PPV3A zMilling = new ZoneMilling_PPV3A(z, angle, apxl);

			zMilling.setOverrunAllowed(true);
			zMilling.setMaxOverrun(2.0);
			zma.add(zMilling);
			zMilling.addListener(this);
			try {
				zMilling.run();
			} catch (SodException e) {
				e.printStackTrace();
			}

			totalLength = totalLength + zMilling.getToolpathLength();// + zMilling.getPenalty();
			totalTime = totalTime + zMilling.getMillingTimeApprox();
			System.out.println("longueur d'usinage de la zone = " + zMilling.getToolpathLength());
			System.out.println("durée d'usinage de la zone = " + zMilling.getMillingTimeApprox());

			for (Toolpath<? extends Point3d> tp : zMilling.getToolpathSet())
				model.add(new VtkToolpath(tp, 2.0f, new Color4d(0.0, 0.7, 0.0)));

		}
		long end_time = System.currentTimeMillis();
		long sim_duration = end_time - start_time;

		System.out.println("durée de simulation de l'usinage = " + sim_duration + " ms");
		System.out.println("longueur de trajectoire d'usinage = " + totalLength);
		System.out.println("durée de l'usinage = " + totalTime + " s");
	}

	@Override
	public void onReceivedSignal(Signal s) {
		if (s instanceof ToolpathSignal && s.getName() == "AddToolpath") {
			Toolpath<? extends Point3d> tp = ((ToolpathSignal) s).getToolpath();
			model.add(new VtkToolpath(tp, 2.0f, new Color4d(1.0, 0.0, 0.0, 1.0)));
		}
		if (s instanceof ToolpathSignal && s.getName() == "AddRawToolpath") {
			Toolpath<? extends Point3d> tp = ((ToolpathSignal) s).getToolpath();
			model.add(new VtkToolpath(tp, 1.0f, new Color4d(1.0, 0.0, 0.0, 1.0)));
		}
	}

}
