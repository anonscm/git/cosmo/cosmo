package org.ica.cosmo.demos;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.functions.BivarVec3Function;
import org.lgmt.dgl.surfaces.AlgebraicSurface;
import org.lgmt.dgl.surfaces.BezierSurface;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Tuple3d;
import org.lgmt.dgl.vecmath.Vector3d;

public class SurfaceBuilder {

	public static Surface getTile() {
		// vertical Tile
//		Point3d[][] points = new Point3d[3][3];
//		points[0][0] = new Point3d(0.0, 0.0, 0.0);
//		points[0][1] = new Point3d(20.0, 0.0, 10.0);
//		points[0][2] = new Point3d(40.0, 0.0, 0.0);
//		points[1][0] = new Point3d(0.0, 40.0, 5.0);
//		points[1][1] = new Point3d(20.0, 40.0, 15.0);
//		points[1][2] = new Point3d(40.0, 40.0, 5.0);
//		points[2][0] = new Point3d(0.0, 80.0, 20.0);
//		points[2][1] = new Point3d(20.0, 80.0, 35.0);
//		points[2][2] = new Point3d(40.0, 80.0, 20.0);

		// horizontal Tile
		Point3d[][] points = new Point3d[3][3];
		points[0][0] = new Point3d(0.0, 0.0, 0.0);
		points[0][1] = new Point3d(0.0, 20.0, 10.0);
		points[0][2] = new Point3d(0.0, 40.0, 0.0);
		points[1][0] = new Point3d(40.0, 0.0, 5.0);
		points[1][1] = new Point3d(40.0, 20.0, 15.0);
		points[1][2] = new Point3d(40.0, 40.0, 5.0);
		points[2][0] = new Point3d(80.0, 0.0, 20.0);
		points[2][1] = new Point3d(80.0, 20.0, 35.0);
		points[2][2] = new Point3d(80.0, 40.0, 20.0);

		return (new BezierSurface(points));
	}

	public static Surface getChoi() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 38.1);
		points[0][1] = new Point3d(0.0, 25.4, 30.48);
		points[0][2] = new Point3d(0.0, 50.8, 30.48);
		points[0][3] = new Point3d(0.0, 76.2, 38.1);
		points[1][0] = new Point3d(17.78, 0.0, 30.48);
		points[1][1] = new Point3d(17.78, 25.4, 22.86);
		points[1][2] = new Point3d(17.78, 50.8, 22.86);
		points[1][3] = new Point3d(17.78, 76.2, 30.48);
		points[2][0] = new Point3d(35.56, 0.0, 38.1);
		points[2][1] = new Point3d(35.56, 25.4, 30.48);
		points[2][2] = new Point3d(35.56, 50.8, 30.48);
		points[2][3] = new Point3d(35.56, 76.2, 38.1);
		points[3][0] = new Point3d(50.8, 0.0, 30.48);
		points[3][1] = new Point3d(50.8, 25.4, 22.86);
		points[3][2] = new Point3d(50.8, 50.8, 22.86);
		points[3][3] = new Point3d(50.8, 76.2, 30.48);

		return (new BezierSurface(points));
	}

	public static Surface getRoman() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, -47.0);
		points[0][1] = new Point3d(0.0, 75.0, -52.0);
		points[0][2] = new Point3d(0.0, 150.0, -42.0);
		points[0][3] = new Point3d(0.0, 225.0, -5.0);
		points[1][0] = new Point3d(50.0, 0.0, -35.0);
		points[1][1] = new Point3d(50.0, 75.0, -99.0);
		points[1][2] = new Point3d(50.0, 150.0, -56.0);
		points[1][3] = new Point3d(50.0, 225.0, 0.0);
		points[2][0] = new Point3d(100.0, 0.0, -65.0);
		points[2][1] = new Point3d(100.0, 75.0, -79.0);
		points[2][2] = new Point3d(100.0, 150.0, -28.0);
		points[2][3] = new Point3d(100.0, 225.0, -37.0);
		points[3][0] = new Point3d(150.0, 0.0, -17.0);
		points[3][1] = new Point3d(150.0, 50.0, -49.0);
		points[3][2] = new Point3d(150.0, 150.0, -50.0);
		points[3][3] = new Point3d(150.0, 225.0, -53.0);

		return (new BezierSurface(points));
	}

	public static Surface getPlan(double slope, double scale, double theta) {
		double l = 40.0;
		Point3d[][] points = new Point3d[2][2];
		AxisAngle4d rot_axis = new AxisAngle4d(Math.cos(theta + Math.PI / 2), Math.sin(theta + Math.PI / 2), 0.0,
				slope);
		Matrix3d rot_mat = new Matrix3d();
		rot_mat.set(rot_axis);
		Vector3d p1 = new Vector3d(-l / scale / 2, l * scale / 2, 0.0);
		p1 = Vector3d.__mul(rot_mat, p1);
		points[0][0] = new Point3d(p1.x, p1.y, p1.z);
		Vector3d p2 = new Vector3d(-l / scale / 2, -l * scale / 2, 0.0);
		p2 = Vector3d.__mul(rot_mat, p2);
		points[0][1] = new Point3d(p2.x, p2.y, p2.z);
		Vector3d p3 = new Vector3d(l / scale / 2, l * scale / 2, 0.0);
		p3 = Vector3d.__mul(rot_mat, p3);
		points[1][0] = new Point3d(p3.x, p3.y, p3.z);
		Vector3d p4 = new Vector3d(l / scale / 2, -l * scale / 2, 0.0);
		p4 = Vector3d.__mul(rot_mat, p4);
		points[1][1] = new Point3d(p4.x, p4.y, p4.z);

		return (new BezierSurface(points));
	}

	public static Surface getTriangle(Vector3d p1, Vector3d p2) {
		Point3d[][] points = new Point3d[2][2];
		points[0][0] = new Point3d(0.0, 0.0, 0.0);
		points[0][1] = new Point3d(0.0, 0.0, 0.0);
		points[1][0] = new Point3d(p1.x, p1.y, p1.z);
		points[1][1] = new Point3d(p2.x, p2.y, p2.z);
		return (new BezierSurface(points));
	}

	public static Surface getS01() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 38.1);
		points[0][1] = new Point3d(12.5, 25.4, 30.48);
		points[0][2] = new Point3d(-3.0, 50.8, 30.48);
		points[0][3] = new Point3d(2.0, 76.2, 38.1);
		points[1][0] = new Point3d(17.78, -3.0, 30.48);
		points[1][1] = new Point3d(17.78, 25.4, 22.86);
		points[1][2] = new Point3d(17.78, 50.8, 22.86);
		points[1][3] = new Point3d(17.78, 86.2, 30.48);
		points[2][0] = new Point3d(35.56, -10.0, 38.1);
		points[2][1] = new Point3d(35.56, 25.4, 45.48);
		points[2][2] = new Point3d(35.56, 50.8, 45.48);
		points[2][3] = new Point3d(35.56, 80.2, 38.1);
		points[3][0] = new Point3d(50.8, -5.0, 10.48);
		points[3][1] = new Point3d(55.8, 25.4, 22.86);
		points[3][2] = new Point3d(58.8, 50.8, 22.86);
		points[3][3] = new Point3d(52.8, 70.2, 10.48);

		return (new BezierSurface(points));
	}

	public static Surface getS02() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 38.1);
		points[0][1] = new Point3d(12.5, 25.4, 30.48);
		points[0][2] = new Point3d(-3.0, 50.8, 30.48);
		points[0][3] = new Point3d(2.0, 76.2, 38.1);
		points[1][0] = new Point3d(17.78, -3.0, -10.48);
		points[1][1] = new Point3d(17.78, 25.4, -22.86);
		points[1][2] = new Point3d(17.78, 50.8, -22.86);
		points[1][3] = new Point3d(17.78, 86.2, -10.48);
		points[2][0] = new Point3d(35.56, -10.0, 18.1);
		points[2][1] = new Point3d(35.0, 25.4, 25.48);
		points[2][2] = new Point3d(36.0, 50.8, 25.48);
		points[2][3] = new Point3d(36.56, 80.2, 18.1);
		points[3][0] = new Point3d(60.8, -5.0, 10.48);
		points[3][1] = new Point3d(53.8, 25.4, -2.86);
		points[3][2] = new Point3d(58.8, 50.8, -2.86);
		points[3][3] = new Point3d(62.8, 70.2, 10.48);

		return (new BezierSurface(points));
	}

	public static Surface getSdegen1() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(2.0, 40.8, 30.48);
		points[0][1] = new Point3d(2.0, 40.8, 30.48);
		points[0][2] = new Point3d(2.0, 40.8, 30.48);
		points[0][3] = new Point3d(2.0, 40.8, 30.48);
		points[1][0] = new Point3d(17.78, -3.0, -10.48);
		points[1][1] = new Point3d(17.78, 25.4, -22.86);
		points[1][2] = new Point3d(17.78, 50.8, -22.86);
		points[1][3] = new Point3d(17.78, 86.2, -10.48);
		points[2][0] = new Point3d(35.56, -10.0, 18.1);
		points[2][1] = new Point3d(35.0, 25.4, 25.48);
		points[2][2] = new Point3d(36.0, 50.8, 25.48);
		points[2][3] = new Point3d(36.56, 80.2, 18.1);
		points[3][0] = new Point3d(60.8, -5.0, 10.48);
		points[3][1] = new Point3d(53.8, 25.4, -2.86);
		points[3][2] = new Point3d(58.8, 50.8, -2.86);
		points[3][3] = new Point3d(62.8, 70.2, 10.48);

		return (new BezierSurface(points));
	}

	public static Surface getSdegen2() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(2.0, 40.8, 30.48);
		points[0][1] = new Point3d(2.0, 40.8, 30.48);
		points[0][2] = new Point3d(2.0, 40.8, 30.48);
		points[0][3] = new Point3d(2.0, 40.8, 30.48);
		points[1][0] = new Point3d(17.78, -3.0, -10.48);
		points[1][1] = new Point3d(17.78, 25.4, -22.86);
		points[1][2] = new Point3d(17.78, 50.8, 22.86);
		points[1][3] = new Point3d(17.78, 86.2, -10.48);
		points[2][0] = new Point3d(35.56, -10.0, 18.1);
		points[2][1] = new Point3d(35.0, 25.4, 25.48);
		points[2][2] = new Point3d(36.0, 50.8, 25.48);
		points[2][3] = new Point3d(36.56, 80.2, 18.1);
		points[3][0] = new Point3d(60.8, -5.0, 10.48);
		points[3][1] = new Point3d(53.8, 25.4, -2.86);
		points[3][2] = new Point3d(58.8, 50.8, -2.86);
		points[3][3] = new Point3d(62.8, 70.2, 10.48);

		return (new BezierSurface(points));
	}

	public static Surface getSdegen3() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 38.1);
		points[0][1] = new Point3d(12.5, 25.4, 30.48);
		points[0][2] = new Point3d(-3.0, 50.8, 30.48);
		points[0][3] = new Point3d(10.0, 76.2, 38.1);
		points[1][0] = new Point3d(17.78, -3.0, -10.48);
		points[1][1] = new Point3d(17.78, 25.4, -22.86);
		points[1][2] = new Point3d(17.78, 50.8, -22.86);
		points[1][3] = new Point3d(10.0, 76.2, 38.1);
		points[2][0] = new Point3d(35.56, -10.0, 18.1);
		points[2][1] = new Point3d(35.0, 25.4, 25.48);
		points[2][2] = new Point3d(36.0, 50.8, 25.48);
		points[2][3] = new Point3d(10.0, 76.2, 38.1);
		points[3][0] = new Point3d(60.8, -5.0, 10.48);
		points[3][1] = new Point3d(53.8, 25.4, -2.86);
		points[3][2] = new Point3d(58.8, 50.8, -2.86);
		points[3][3] = new Point3d(10.0, 76.2, 38.1);

		return (new BezierSurface(points));
	}

	public static Surface getDome(double scaleX, double scaleY, double height) {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(-10.0, -10.0, 0.0);
		points[0][1] = new Point3d(-15.0, -5.0, 0.0);
		points[0][2] = new Point3d(-15.0, 5.0, 0.0);
		points[0][3] = new Point3d(-10.0, 10.0, 0.0);
		points[1][0] = new Point3d(-5.0, -15.0, 0.0);
		points[1][1] = new Point3d(-5.0, -5.0, height);
		points[1][2] = new Point3d(-5.0, 5.0, height);
		points[1][3] = new Point3d(-5.0, 15.0, 0.0);
		points[2][0] = new Point3d(5.0, -15.0, 0.0);
		points[2][1] = new Point3d(5.0, -5.0, height);
		points[2][2] = new Point3d(5.0, 5.0, height);
		points[2][3] = new Point3d(5.0, 15.0, 0.0);
		points[3][0] = new Point3d(10.0, -10.0, 0.0);
		points[3][1] = new Point3d(15.0, -5.0, 0.0);
		points[3][2] = new Point3d(15.0, 5.0, 0.0);
		points[3][3] = new Point3d(10.0, 10.0, 0.0);

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++) {
				points[i][j].setX(points[i][j].x * scaleX);
				points[i][j].setY(points[i][j].y * scaleY);
			}

		return (new BezierSurface(points));
	}

	public static Surface getHalfSphere(final double radius) {
		BivarVec3Function v3f = new BivarVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return radius * cos(u) * cos(v);
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return radius * sin(u) * cos(v);
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return radius * sin(v);
			}
		});

		return (new AlgebraicSurface(v3f, 0.0, 2 * PI, 0.0, PI / 2.0));
	}

	public static Surface getTiltedTile(double alpha) {
		AxisAngle4d rot_axis = new AxisAngle4d(1.0, 0.0, 0.0, alpha);
		Matrix3d rot_mat = new Matrix3d();
		rot_mat.set(rot_axis);
//		Vector3d p1 = new Vector3d(0.0, 0.0, 0.0);
//		Vector3d p2 = new Vector3d(0.0, 20.0, 10.0);
//		Vector3d p3 = new Vector3d(0.0, 40.0, 0.0);
//		Vector3d p4 = new Vector3d(40.0, 0.0, 5.0);
//		Vector3d p5 = new Vector3d(40.0, 20.0, 15.0);
//		Vector3d p6 = new Vector3d(40.0, 40.0, 5.0);
//		Vector3d p7 = new Vector3d(80.0, 0.0, 20.0);
//		Vector3d p8 = new Vector3d(80.0, 20.0, 35.0);
//		Vector3d p9 = new Vector3d(80.0, 40.0, 20.0);
		Vector3d p1 = new Vector3d(0.0, -20.0, 0.0);
		Vector3d p2 = new Vector3d(0.0, 0.0, 10.0);
		Vector3d p3 = new Vector3d(0.0, 20.0, 0.0);
		Vector3d p4 = new Vector3d(40.0, -20.0, 5.0);
		Vector3d p5 = new Vector3d(40.0, 0.0, 15.0);
		Vector3d p6 = new Vector3d(40.0, 20.0, 5.0);
		Vector3d p7 = new Vector3d(80.0, -20.0, 20.0);
		Vector3d p8 = new Vector3d(80.0, 0.0, 35.0);
		Vector3d p9 = new Vector3d(80.0, 20.0, 20.0);
		p1 = Vector3d.__mul(rot_mat, p1);
		p2 = Vector3d.__mul(rot_mat, p2);
		p3 = Vector3d.__mul(rot_mat, p3);
		p4 = Vector3d.__mul(rot_mat, p4);
		p5 = Vector3d.__mul(rot_mat, p5);
		p6 = Vector3d.__mul(rot_mat, p6);
		p7 = Vector3d.__mul(rot_mat, p7);
		p8 = Vector3d.__mul(rot_mat, p8);
		p9 = Vector3d.__mul(rot_mat, p9);
		Point3d[][] points = new Point3d[3][3];
		points[0][0] = new Point3d((Tuple3d) p1);
		points[0][1] = new Point3d((Tuple3d) p2);
		points[0][2] = new Point3d((Tuple3d) p3);
		points[1][0] = new Point3d((Tuple3d) p4);
		points[1][1] = new Point3d((Tuple3d) p5);
		points[1][2] = new Point3d((Tuple3d) p6);
		points[2][0] = new Point3d((Tuple3d) p7);
		points[2][1] = new Point3d((Tuple3d) p8);
		points[2][2] = new Point3d((Tuple3d) p9);
		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println(p3.toString());
		System.out.println(p4.toString());
		System.out.println(p5.toString());
		System.out.println(p6.toString());
		System.out.println(p7.toString());
		System.out.println(p8.toString());
		System.out.println(p9.toString());

		return (new BezierSurface(points));
	}

	public static Surface getTiltedChoi(double alpha) {
		AxisAngle4d rot_axis = new AxisAngle4d(0.0, 1.0, 0.0, alpha);
		Matrix3d rot_mat = new Matrix3d();
		rot_mat.set(rot_axis);
		Vector3d p1 = new Vector3d(0.0, 0.0, 38.1);
		Vector3d p2 = new Vector3d(0.0, 20.4, 30.48);
		Vector3d p3 = new Vector3d(0.0, 40.8, 30.48);
		Vector3d p4 = new Vector3d(0.0, 61.2, 38.1);
		Vector3d p5 = new Vector3d(17.78, 0.0, 30.48);
		Vector3d p6 = new Vector3d(17.78, 20.4, 22.86);
		Vector3d p7 = new Vector3d(17.78, 40.8, 22.86);
		Vector3d p8 = new Vector3d(17.78, 61.2, 30.48);
		Vector3d p9 = new Vector3d(35.56, 0.0, 38.1);
		Vector3d p10 = new Vector3d(35.56, 20.4, 30.48);
		Vector3d p11 = new Vector3d(35.56, 40.8, 30.48);
		Vector3d p12 = new Vector3d(35.56, 61.2, 38.1);
		Vector3d p13 = new Vector3d(50.8, 0.0, 30.48);
		Vector3d p14 = new Vector3d(50.8, 20.4, 22.86);
		Vector3d p15 = new Vector3d(50.8, 40.8, 22.86);
		Vector3d p16 = new Vector3d(50.8, 61.2, 30.48);

		p1 = Vector3d.__mul(rot_mat, p1);
		p2 = Vector3d.__mul(rot_mat, p2);
		p3 = Vector3d.__mul(rot_mat, p3);
		p4 = Vector3d.__mul(rot_mat, p4);
		p5 = Vector3d.__mul(rot_mat, p5);
		p6 = Vector3d.__mul(rot_mat, p6);
		p7 = Vector3d.__mul(rot_mat, p7);
		p8 = Vector3d.__mul(rot_mat, p8);
		p9 = Vector3d.__mul(rot_mat, p9);
		p10 = Vector3d.__mul(rot_mat, p10);
		p11 = Vector3d.__mul(rot_mat, p11);
		p12 = Vector3d.__mul(rot_mat, p12);
		p13 = Vector3d.__mul(rot_mat, p13);
		p14 = Vector3d.__mul(rot_mat, p14);
		p15 = Vector3d.__mul(rot_mat, p15);
		p16 = Vector3d.__mul(rot_mat, p16);

		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d((Tuple3d) p1);
		points[0][1] = new Point3d((Tuple3d) p2);
		points[0][2] = new Point3d((Tuple3d) p3);
		points[0][3] = new Point3d((Tuple3d) p4);
		points[1][0] = new Point3d((Tuple3d) p5);
		points[1][1] = new Point3d((Tuple3d) p6);
		points[1][2] = new Point3d((Tuple3d) p7);
		points[1][3] = new Point3d((Tuple3d) p8);
		points[2][0] = new Point3d((Tuple3d) p9);
		points[2][1] = new Point3d((Tuple3d) p10);
		points[2][2] = new Point3d((Tuple3d) p11);
		points[2][3] = new Point3d((Tuple3d) p12);
		points[3][0] = new Point3d((Tuple3d) p13);
		points[3][1] = new Point3d((Tuple3d) p14);
		points[3][2] = new Point3d((Tuple3d) p15);
		points[3][3] = new Point3d((Tuple3d) p16);

		System.out.println(p1.toString());
		System.out.println(p2.toString());
		System.out.println(p3.toString());
		System.out.println(p4.toString());
		System.out.println(p5.toString());
		System.out.println(p6.toString());
		System.out.println(p7.toString());
		System.out.println(p8.toString());
		System.out.println(p9.toString());
		System.out.println(p10.toString());
		System.out.println(p11.toString());
		System.out.println(p12.toString());
		System.out.println(p13.toString());
		System.out.println(p14.toString());
		System.out.println(p15.toString());
		System.out.println(p16.toString());

		return (new BezierSurface(points));
	}

	public static Surface getBlade() {
		Point3d[][] points = new Point3d[8][8];
		points[0][0] = new Point3d(182.1751497, -73.75467352, 37.44793806);
		points[0][1] = new Point3d(169.887214, -84.43907871, 34.24192386);
		points[0][2] = new Point3d(150.3447418, -84.8655777, 32.90084076);
		points[0][3] = new Point3d(132.7953866, -80.45766808, 42.07759631);
		points[0][4] = new Point3d(111.4926223, -64.88657181, 50.13389036);
		points[0][5] = new Point3d(84.42778778, -64.51494598, 60.60494614);
		points[0][6] = new Point3d(64.91183353, -45.25469155, 65.16531374);
		points[0][7] = new Point3d(45.60230255, -37.55263901, 72.82514191);

		points[1][0] = new Point3d(189.0377187, -69.5582254, 38.85544705);
		points[1][1] = new Point3d(176.6755159, -73.43278847, 38.65780203);
		points[1][2] = new Point3d(160.9559836, -71.34431902, 43.53583623);
		points[1][3] = new Point3d(141.0357409, -65.21854425, 48.91852413);
		points[1][4] = new Point3d(117.8346567, -57.33498193, 57.75910998);
		points[1][5] = new Point3d(94.01124073, -49.98746308, 73.95115358);
		points[1][6] = new Point3d(73.77868652, -46.99302292, 88.91507721);
		points[1][7] = new Point3d(47.71957809, -34.78909365, 90.13243213);

		points[2][0] = new Point3d(193.8571838, -61.52907601, 40.66767732);
		points[2][1] = new Point3d(181.8377221, -62.04977131, 42.83591206);
		points[2][2] = new Point3d(167.2202668, -57.2378604, 48.90655264);
		points[2][3] = new Point3d(148.6238443, -49.67019911, 57.31682173);
		points[2][4] = new Point3d(124.4194024, -37.89267282, 66.1840063);
		points[2][5] = new Point3d(105.3708273, -37.13907386, 89.59768983);
		points[2][6] = new Point3d(81.81653816, -29.6920493, 105.8519888);
		points[2][7] = new Point3d(54.438009, -27.90876245, 105.5737478);

		points[3][0] = new Point3d(195.5058815, -53.25551081, 42.33643785);
		points[3][1] = new Point3d(185.6866741, -50.49976479, 46.39862845);
		points[3][2] = new Point3d(171.7912331, -42.36034435, 54.08036949);
		points[3][3] = new Point3d(153.014275, -31.35978351, 64.0640081);
		points[3][4] = new Point3d(128.1855289, -18.60508351, 75.45610405);
		points[3][5] = new Point3d(112.6558592, -16.05421774, 106.9246704);
		points[3][6] = new Point3d(90.73419251, -11.17706842, 123.1765737);
		points[3][7] = new Point3d(60.76818913, -17.96536251, 122.2933721);

		points[4][0] = new Point3d(195.1210572, -44.77786916, 44.63685256);
		points[4][1] = new Point3d(188.6246827, -39.41648483, 49.27357767);
		points[4][2] = new Point3d(175.1970096, -27.10548719, 58.88953007);
		points[4][3] = new Point3d(155.9392076, -12.68611929, 72.25164176);
		points[4][4] = new Point3d(129.9454982, 1.321115183, 85.25290066);
		points[4][5] = new Point3d(117.9587617, 2.066249144, 115.8809114);
		points[4][6] = new Point3d(93.88584006, 3.194979659, 131.107942);
		points[4][7] = new Point3d(66.05333056, -1.179031966, 134.2534031);

		points[5][0] = new Point3d(196.03867, -36.73843925, 46.74120826);
		points[5][1] = new Point3d(191.0266026, -28.77390424, 51.63208427);
		points[5][2] = new Point3d(177.998577, -11.69291273, 64.0731083);
		points[5][3] = new Point3d(156.817882, 7.802173033, 77.54500631);
		points[5][4] = new Point3d(130.2770925, 22.61923285, 94.519819);
		points[5][5] = new Point3d(116.6984618, 25.63176631, 119.2115005);
		points[5][6] = new Point3d(98.33232459, 23.75773607, 143.9515815);
		points[5][7] = new Point3d(62.80228549, 12.95741777, 143.4718316);

		points[6][0] = new Point3d(196.8761688, -28.15320878, 48.89479466);
		points[6][1] = new Point3d(193.4058873, -19.24923516, 53.28726389);
		points[6][2] = new Point3d(179.9697729, 5.836830653, 68.3460449);
		points[6][3] = new Point3d(159.4476892, 31.1504592, 85.93252114);
		points[6][4] = new Point3d(131.2324018, 48.8379558, 103.5423204);
		points[6][5] = new Point3d(110.4335664, 53.08034187, 119.4732446);
		points[6][6] = new Point3d(95.82902636, 48.50453687, 149.5788464);
		points[6][7] = new Point3d(56.89727922, 33.54856537, 155.2801448);

		points[7][0] = new Point3d(195.7247925, -21.12810135, 51.15703201);
		points[7][1] = new Point3d(195.8814545, -4.980244637, 56.35812378);
		points[7][2] = new Point3d(178.0072174, 28.40402412, 73.67666626);
		points[7][3] = new Point3d(154.7647836, 66.21030121, 87.91799302);
		points[7][4] = new Point3d(130.1123299, 87.40373388, 102.8013569);
		points[7][5] = new Point3d(87.91460553, 76.92580551, 110.3681244);
		points[7][6] = new Point3d(89.78755908, 78.27442784, 150.3596021);
		points[7][7] = new Point3d(32.59567261, 49.12865829, 158.3162842);

		return (new BezierSurface(points));
	}
	
	public static Surface getCroix(double w, double h, double p, double q) {
		BivarVec3Function v3f = new BivarVec3Function(
				new BivarEvaluable() {
					@Override
					public double eval(double u, double v) {
						if (v <= p/(2*p+h)) {
							return u*q - q/2.0;
						} else if (v > p/(2*p+h) & v <= (p+h)/(2*p+h)) {
							return u*w - w/2.0;
						} else {
						return u*q - q/2.0;
						}
					}
				},
				new BivarEvaluable() {
					@Override
					public double eval(double u, double v) {
						return (2*p+h)*v-(p+h/2.0);
					}
				},
				new BivarEvaluable() {
					@Override
					public double eval(double u, double v) {
						return 0.0;
//						return -5.0 + 10*v;
					}
				}
		);
		
		AlgebraicSurface croix = new AlgebraicSurface(v3f, 0.0, 1.0, 0.0, 1.0);
		return croix;
	}
	
	public static Surface getSurfParam4(double[] z) {
		assert (z.length == 16);
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, z[0]);
		points[0][1] = new Point3d(0.0, 25.4, z[1]);
		points[0][2] = new Point3d(0.0, 50.8, z[2]);
		points[0][3] = new Point3d(0.0, 76.2, z[3]);
		points[1][0] = new Point3d(17.78, 0.0, z[4]);
		points[1][1] = new Point3d(17.78, 25.4, z[5]);
		points[1][2] = new Point3d(17.78, 50.8, z[6]);
		points[1][3] = new Point3d(17.78, 76.2, z[7]);
		points[2][0] = new Point3d(35.56, 0.0, z[8]);
		points[2][1] = new Point3d(35.56, 25.4, z[9]);
		points[2][2] = new Point3d(35.56, 50.8, z[10]);
		points[2][3] = new Point3d(35.56, 76.2, z[11]);
		points[3][0] = new Point3d(50.8, 0.0, z[12]);
		points[3][1] = new Point3d(50.8, 25.4, z[13]);
		points[3][2] = new Point3d(50.8, 50.8, z[14]);
		points[3][3] = new Point3d(50.8, 76.2, z[15]);

		return (new BezierSurface(points));
	}
	
	public static Surface getAS1() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 0.1);
		points[0][1] = new Point3d(0.0, 25.4, 1.48);
		points[0][2] = new Point3d(0.0, 50.8, 1.48);
		points[0][3] = new Point3d(0.0, 76.2, 0.1);
		points[1][0] = new Point3d(17.78, 0.0, 5.48);
		points[1][1] = new Point3d(17.78, 25.4, 22.86);
		points[1][2] = new Point3d(17.78, 50.8, 22.86);
		points[1][3] = new Point3d(17.78, 76.2, 5.48);
		points[2][0] = new Point3d(35.56, 0.0, 8.1);
		points[2][1] = new Point3d(35.56, 25.4, 30.48);
		points[2][2] = new Point3d(35.56, 50.8, 30.48);
		points[2][3] = new Point3d(35.56, 76.2, 8.1);
		points[3][0] = new Point3d(50.8, 0.0, 5.48);
		points[3][1] = new Point3d(50.8, 25.4, 12.86);
		points[3][2] = new Point3d(50.8, 50.8, 12.86);
		points[3][3] = new Point3d(50.8, 76.2, 5.48);

		return (new BezierSurface(points));
	}
	
	public static Surface getAS2() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 0.1);
		points[0][1] = new Point3d(0.0, 25.4, 1.48);
		points[0][2] = new Point3d(0.0, 50.8, 1.48);
		points[0][3] = new Point3d(0.0, 76.2, 0.1);
		points[1][0] = new Point3d(17.78, 0.0, -5.48);
		points[1][1] = new Point3d(20.78, 25.4, -22.86);
		points[1][2] = new Point3d(20.78, 50.8, -22.86);
		points[1][3] = new Point3d(17.78, 76.2, -5.48);
		points[2][0] = new Point3d(35.56, 0.0, -8.1);
		points[2][1] = new Point3d(25.56, 25.4, -30.48);
		points[2][2] = new Point3d(25.56, 50.8, -30.48);
		points[2][3] = new Point3d(35.56, 76.2, -8.1);
		points[3][0] = new Point3d(50.8, 0.0, -5.48);
		points[3][1] = new Point3d(60.8, 25.4, -12.86);
		points[3][2] = new Point3d(60.8, 50.8, -12.86);
		points[3][3] = new Point3d(50.8, 76.2, -5.48);

		return (new BezierSurface(points));
	}
	
	public static Surface getAS3() {
		Point3d[][] points = new Point3d[6][4];
		points[0][0] = new Point3d(-20.0, 0.0, 25.0);
		points[0][1] = new Point3d(-20.0, 25.4, 22.48);
		points[0][2] = new Point3d(-20.0, 50.8, 22.48);
		points[0][3] = new Point3d(-20.0, 76.2, 25.0);
		points[1][0] = new Point3d(0.0, 0.0, 24.0);
		points[1][1] = new Point3d(0.0, 25.4, 21.48);
		points[1][2] = new Point3d(0.0, 50.8, 21.48);
		points[1][3] = new Point3d(0.0, 76.2, 24.0);
		points[2][0] = new Point3d(15.78, 0.0, 20.48);
		points[2][1] = new Point3d(16.78, 25.4, 20.86);
		points[2][2] = new Point3d(16.78, 50.8, 20.86);
		points[2][3] = new Point3d(15.78, 76.2, 20.48);
		points[3][0] = new Point3d(35.56, 0.0, -20.1);
		points[3][1] = new Point3d(32.56, 25.4, -20.48);
		points[3][2] = new Point3d(33.56, 50.8, -20.48);
		points[3][3] = new Point3d(35.56, 76.2, -20.1);
		points[4][0] = new Point3d(55.8, 0.0, -24.48);
		points[4][1] = new Point3d(55.8, 25.4, -21.86);
		points[4][2] = new Point3d(55.8, 50.8, -21.86);
		points[4][3] = new Point3d(55.8, 76.2, -24.48);
		points[5][0] = new Point3d(80.8, 0.0, -25.48);
		points[5][1] = new Point3d(81.8, 25.4, -22.86);
		points[5][2] = new Point3d(79.8, 50.8, -22.86);
		points[5][3] = new Point3d(80.8, 76.2, -25.48);

		return (new BezierSurface(points));
	}
	
	public static Surface getAS4() {
		Point3d[][] points = new Point3d[4][4];
		points[0][0] = new Point3d(0.0, 0.0, 0.1);
		points[0][1] = new Point3d(0.0, 25.4, 20.48);
		points[0][2] = new Point3d(0.0, 50.8, 20.48);
		points[0][3] = new Point3d(0.0, 76.2, 0.1);
		points[1][0] = new Point3d(20.78, 0.0, -5.48);
		points[1][1] = new Point3d(20.78, 25.4, 10.86);
		points[1][2] = new Point3d(20.78, 50.8, 10.86);
		points[1][3] = new Point3d(20.78, 76.2, -5.48);
		points[2][0] = new Point3d(60.56, 0.0, -5.1);
		points[2][1] = new Point3d(60.56, 25.4, 10.48);
		points[2][2] = new Point3d(60.56, 50.8, 10.48);
		points[2][3] = new Point3d(60.56, 76.2, -5.1);
		points[3][0] = new Point3d(80.8, 0.0, 0.48);
		points[3][1] = new Point3d(80.8, 25.4, 20.86);
		points[3][2] = new Point3d(80.8, 50.8, 20.86);
		points[3][3] = new Point3d(80.8, 76.2, 0.48);

		return (new BezierSurface(points));
	}


}
