package org.ica.cosmo.demos;

import java.util.ArrayList;

import org.ica.cosmo.clustering.ALGO_TYPE;
import org.ica.cosmo.clustering.Clustering;
import org.ica.cosmo.clustering.METRIC;
import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.vtkviewer.VtkViewer;
import org.ica.vtkviewer.model.VtkModel;
import org.ica.vtkviewer.model.VtkSurfaceMap;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.jcam.cutter.Cutter;
import org.lgmt.jcam.strategy.MillingContext;

import vtk.vtkNativeLibrary;

public class CLusteringDemo {
	static {
		if (!vtkNativeLibrary.LoadAllNativeLibraries()) {
			for (vtkNativeLibrary lib : vtkNativeLibrary.values()) {
				if (!lib.IsLoaded()) {
					System.out.println(lib.GetLibraryName() + " not loaded");
				}
			}
		}
		vtkNativeLibrary.DisableOutputWindow(null);
	}

	/** méthode de clustering à utiliser */
	private ALGO_TYPE algo_type = ALGO_TYPE.KMEANS;

	/** métrique */
	private METRIC metric = METRIC.SLOPE;

	/** surface */
	private Surface surface;

	/** tessellation */
	private int tessU = 200;
	private int tessV = 200;

	/** nombre de zones */
	private int k = 3;

	/** main program */
	public static void main(String[] args) {
		new CLusteringDemo();
	}

	public CLusteringDemo() {
		super();
		MillingContext.getInstance().setCutter(new Cutter(5.0, 2.0, 100.0));
		MillingContext.getInstance().setScallopHeight(0.01);
		this.surface = SurfaceBuilder.getChoi();

		// Choose the clustering tessellation
		// Clustering with a given tessellation
//		SurfaceMap mapK = new SurfaceMap(surface, tessU, tessV);
		// Clustering with a tessellation computed from an approximation level
		SurfaceMap mapK = new SurfaceMap(surface, APPROX_LEVEL.MEDIUM);

		mapK.initNZone(k); // map must be initialized before display

		VtkModel modelK = new VtkModel();
		modelK.add(new VtkSurfaceMap(mapK));
		String title = new String(algo_type.name() + " à partir du nombre de zones");
		VtkViewer viewerK = new VtkViewer(modelK, title);
		viewerK.run();

		Clustering c = new Clustering(k, mapK, algo_type, metric, true);
		c.run();
		try {
			mapK = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}

		k = c.getNbClusters();
		System.out.println("Clustering process results in " + k + " zones");

		// Clustering from a given set of centroids
		SurfaceMap mapC = new SurfaceMap(surface, tessU, tessV);
		mapC.initNZone(1); // map must be initialized before display

		VtkModel modelC = new VtkModel();
		modelC.add(new VtkSurfaceMap(mapC));
		title = new String(algo_type.name() + " à partir des centroides");
		VtkViewer viewerC = new VtkViewer(modelC, title);
		viewerC.run();

		ArrayList<Vector2d> centers = new ArrayList<Vector2d>();
		centers.add(new Vector2d(0.2, 0.2));
		centers.add(new Vector2d(0.8, 0.2));
		centers.add(new Vector2d(0.5, 0.8));
		c = new Clustering(centers, mapC, algo_type, metric, true);
		c.run();
		try {
			mapC = c.call();
		} catch (Exception e) {
			e.printStackTrace();
		}

		k = c.getNbClusters();
		System.out.println("Clustering process results in " + k + " zones");

	}
}
