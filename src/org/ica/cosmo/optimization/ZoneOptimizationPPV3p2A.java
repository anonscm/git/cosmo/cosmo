package org.ica.cosmo.optimization;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.ica.cosmo.machining.APPROX_LEVEL;
import org.ica.cosmo.machining.SodException;
import org.ica.cosmo.machining.ZoneMilling_PPV3A;
import org.ica.cosmo.util.SurfaceMapSignal;
import org.ica.cosmo.zoning.SurfaceMap;
import org.ica.cosmo.zoning.Zone;
import org.ica.cosmo.zoning.ZoneApproxPlane;
import org.ica.support.EventManager;
import org.ica.support.IListener;
import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.commons.FrameSet;
import org.lgmt.dgl.curves.SurfacePointsCurve;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.EulerAngles;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Vector3d;

import jNomad.BBOTVector;
import jNomad.Display;
import jNomad.Double;
import jNomad.Eval_Point;
import jNomad.Evaluator;
import jNomad.Mads;
import jNomad.Parameters;
import jNomad.Point;
import jNomad.Slave;
import jNomad.Stats;
import jNomad.bb_output_type;
import jNomad.direction_type;
import jNomad.eval_type;
import jNomad.jNomad;

public class ZoneOptimizationPPV3p2A {
	static {
		String nomadHome = System.getenv("NOMAD_HOME");
		if (nomadHome == null) {
			System.out.println("NOMAD_HOME environment variable not set...");
			System.out.println(" Using default location /opt/Apps/Nomad/");
			System.out.println(
					" if Nomad is installed elsewhere, " + "please set NOMAD_HOME environment variable accordingly");
			nomadHome = new String("/opt/Apps/Nomad"); // default location
		}
		try {
			String libpath = new String(nomadHome + "/examples/interfaces/jNomad/");
			String os = System.getProperty("os.name");
			if (os.startsWith("Mac"))
				System.load(libpath + "libjNomad.jnilib");
			else if (os.startsWith("Windows"))
				System.load(libpath + "libjNomad.dll");
			else if (os.startsWith("Linux"))
				System.load(libpath + "libjNomad.so");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load.\n" + e);
			System.exit(1);
		}
	}

	private static Logger logger = LogManager.getLogManager().getLogger("org.ica.cosmo");
	private final EventManager emitter = new EventManager(this);

	private SurfaceMap baseMap;
	private Zone zone;
	private String[] args;
	private int k;
	private EulerAngles angles;
	private SurfaceMap initMap;
	private Matrix4d initTransform;
	private double millingTime;
	private Mads mads;
	private Frame workingFrame; // repère de travail = repère du rectangle approximant
	private Matrix4d orientation; // orientation à partir de la position initial (gouge free) => optim
	private Matrix4d wfOrientation; // orientation dans le repère de travail

	private Matrix4d resultTransform; // transformation globale
	private SurfaceMap resultMap; // map finale

	public ZoneOptimizationPPV3p2A(SurfaceMap map, Zone zone, String[] args) {
		this.baseMap = map;
		this.zone = zone;
		this.args = args;
		this.k = zone.getId();
		this.angles = new EulerAngles(0.0, 0.0, 0.0);
	}

	public ZoneOptimizationPPV3p2A(SurfaceMap map, int k, String[] args) {
		this(map, map.getZone(k), args);
	}

	public ZoneOptimizationPPV3p2A(SurfaceMap map, int k) {
		this(map, map.getZone(k), new String[] {});
	}

	public ZoneOptimizationPPV3p2A(Zone zone) {
		this(zone.getSurfaceMap(), zone, new String[] {});
	}

	public void init() {
		Matrix4d m = zone.getApproxPlaneTransform(); // m : rep global -> rep plan
		this.workingFrame = new Frame(FrameSet.getInstance().getGlobalFrame(), m);
		Matrix4d m1 = new Matrix4d(m);
		m1.invert();
		SurfaceMap mapK = baseMap.transformClone(m1); // map dans le repère de travail
		// à partir de là, tout se fait dans le repère de travail
//		this.emitter.sendSignal(new SurfaceSignal("AddSurface", mapK.getSurface(), k));
		this.initTransform = this.getGougeFreeMatrix(mapK.getZone(k)); // calcule la rotation pour éviter de talonner
//		this.orientation = new Matrix4d(initTransform);
		this.initMap = mapK.transformClone(initTransform);
		this.emitter.sendSignal(new SurfaceMapSignal("AddSurfaceMap", initMap, k));
	}

	/*
	 * Optimisation
	 * 
	 * L'analyse PCA permet de définir la position initiale de l'optimisation.
	 * 
	 * On affine l'orientation de la zone en optimisant les variables qui seront
	 * programmées : psi, theta, phi. Ces variables sont interprétées comme des angles
	 * d'Euler :
	 * <!-- @formatter:off -->
	 * <ul>
	 * <li>psi : rotation p/ à Z qui est perpendiculaire au rectangle approximant la zone</li>
	 * <li>theta : rotation p/ au nouveau X qui vient de la plus grande direction du rectangle approximant la zone</li>
	 * <li>phi : rotation p/ au nouveau Z</li>
	 * </ul>
	 * <!-- @formatter:on -->
	 */
	public void run() {
		logger.info("Optimization in progress...");
		Display out = new Display();
		out.precision(jNomad.getDISPLAY_PRECISION_STD());
		int nVar = 3;

		try {
			jNomad.begin(args.length, args);

			/* Parameters */
			Parameters p = new Parameters(out);
			p.set_DIMENSION(nVar);
			BBOTVector bbot = new BBOTVector(4);
			bbot.set(0, bb_output_type.OBJ);
			bbot.set(1, bb_output_type.EB);
			bbot.set(2, bb_output_type.EB);
			bbot.set(3, bb_output_type.EB);
			p.set_BB_OUTPUT_TYPE(bbot);

			p.set_HAS_SGTE(true);
//			p.set_SGTE_COST(4000);

			/* Options d'affichage */
			p.set_DISPLAY_STATS("bbe : f( sol ) = obj");
			p.set_MAX_BB_EVAL(200);
			p.set_DISPLAY_DEGREE(2);
			p.set_DIRECTION_TYPE(direction_type.ORTHO_2N);
			p.set_NM_SEARCH(false);
			p.set_VNS_SEARCH(false);

			/* Variables : X(0) = psi, X(1) = theta, X(2) = phi */
			Point x0 = new Point(3);
			Point lowbound = new Point(3);
			Point upbound = new Point(3);

			/* Initialisations */
			x0.set_coord(0, new Double(0.0));
			lowbound.set_coord(0, new Double(Math.toRadians(-40.0)));
			upbound.set_coord(0, new Double(Math.toRadians(40.0)));

			x0.set_coord(1, new Double(0.0));
			lowbound.set_coord(1, new Double(Math.toRadians(-20.0)));
			upbound.set_coord(1, new Double(Math.toRadians(20.0)));

			x0.set_coord(2, new Double(0.0));
			lowbound.set_coord(2, new Double(Math.toRadians(-35.0)));
			upbound.set_coord(2, new Double(Math.toRadians(35.0)));

			p.set_X0(x0);
			p.set_LOWER_BOUND(lowbound);
			p.set_UPPER_BOUND(upbound);

			// parameters validation:
			p.check();

			// custom evaluator creation:
			My_Evaluator ev = new My_Evaluator(p);
			// algorithm creation and execution:
			mads = new Mads(p, ev);
			mads.run();
			Eval_Point sol = mads.get_best_feasible();
			this.angles = new EulerAngles(sol.value(0), sol.value(1), sol.value(2));
			orientation = angles.getRotationMatrixHomogen();
			wfOrientation = new Matrix4d(initTransform);
			wfOrientation.mul(orientation);
			resultTransform = new Matrix4d(workingFrame.getTransform());
			resultTransform.mul(wfOrientation);

			resultMap = initMap.transformClone(orientation);
			emitter.sendSignal(new SurfaceMapSignal("AddSurfaceMap", resultMap, k));

			this.millingTime = sol.get_f().value();
			System.out.println("Objectif : " + this.millingTime);
			Slave.stop_slaves(out);
			jNomad.end();
		} catch (RuntimeException e) {
			System.err.println("\nNOMAD has been interrupted (" + e.toString() + ")\n\n");
		}
	}

	public EulerAngles getAngles() {
		return angles;
	}

	public Matrix4d getInitTransform() {
		return initTransform;
	}

	public double getMillingTime() {
		return millingTime;
	}

	/**
	 * Retourne la matrice de rotation correspondant à l'angle d'inclinaison
	 * nécessaire pour éviter de talonner.
	 * 
	 * @param zone
	 * @return
	 */
	private Matrix4d getGougeFreeMatrix(Zone zone) {
		double slope = Math.toRadians(0.0);

		for (ArrayList<SurfacePointsCurve> listC : zone.getListIsoU().values())
			for (SurfacePointsCurve curve : listC) {
				for (SurfacePoint p : curve.getPoints()) {
					Vector3d n = p.getSurface().normal(p.u, p.v);
					n.y = 0.0;
					n.normalize();
					if (n.x > 0) {
						double s = n.angle(Vector3d.Z);
						if (s > slope)
							slope = s;
					}
				}
			}

		slope = slope + Math.toRadians(5.0); // petit coeff de sécurité car le test n'est pas fait "en tout point"

		AxisAngle4d aa = new AxisAngle4d(Vector3d.Y, -slope);
		Matrix4d m = new Matrix4d();
		m.setRotation(aa);

		return m;
	}

	public SurfaceMap getInitMap() {
		return initMap;
	}

	public Frame getWorkingFrame() {
		return workingFrame;
	}

	public Matrix4d getOrientation() {
		return wfOrientation;
	}

	public Matrix4d getResultTransform() {
		return resultTransform;
	}

	public SurfaceMap getResultMap() {
		return resultMap;
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addListener(IListener l) {
		emitter.addListener(l);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removeListener(IListener l) {
		emitter.removeListener(l);
	}

	public class My_Evaluator extends Evaluator {
		private int last_succes_index;
		private double best_feasible;

		public My_Evaluator(Parameters p) {
			super(p);
		}

		public boolean eval_x(Eval_Point x, Double h_max, boolean[] count_eval) {
			double zoneTime = 0.0;
			if (x.get_eval_type() == eval_type.SGTE) {
				zoneTime = sgteEval(x);
			} else {
				zoneTime = bbEval(x);
			}
			x.set_bb_output(0, new Double(zoneTime)); // objective value
			count_eval[0] = true;

			return true;
		}

		public double bbEval(Eval_Point x) {
			EulerAngles ea = new EulerAngles(x.value(0), x.value(1), x.value(2));
			Matrix4d mR = new Matrix4d(ea.getRotationMatrixHomogen());

			SurfaceMap map = initMap.transformClone(mR);

			ZoneMilling_PPV3A zMilling = new ZoneMilling_PPV3A(map.getZone(k), 0.0, APPROX_LEVEL.MEDIUM, false);
			try {
				zMilling.run();
			} catch (SodException e) {
			}

			double time = zMilling.getMillingTime();
			logger.fine("BB eval : " + time + " s");
			emitter.sendSignal(new SurfaceMapSignal("AddSurfaceMap", map, k));

			int index = mads.get_stats().get_bb_eval() + 1;
			if (index - last_succes_index > 40) {
				Logger.getLogger("cosmo").log(Level.INFO,
						"Too much BB eval without progress: exiting optimization... ");
				Mads.force_quit(0);
			}

			return time;
		}

		private double sgteEval(Eval_Point x) {
			EulerAngles ea = new EulerAngles(x.value(0), x.value(1), x.value(2));
			Matrix4d mR = new Matrix4d(ea.getRotationMatrixHomogen());

			SurfaceMap map = initMap.transformClone(mR);

			Zone z = map.getZone(k);
			ZoneApproxPlane zpa = new ZoneApproxPlane(z);
			zpa.calc(0.0);
			double time = zpa.getMillingTime();
			logger.finer("SGTE eval : " + time + " s");

			return time;
		}

		public void update_success(Stats stats, Eval_Point x) {
			EulerAngles ea = new EulerAngles(x.value(0), x.value(1), x.value(2));
			orientation = new Matrix4d(ea.getRotationMatrixHomogen());
			resultMap = initMap.transformClone(orientation);
			emitter.sendSignal(new SurfaceMapSignal("AddSurfaceMap", resultMap, k));

			int index = stats.get_bb_eval() + 1;
			double objective = x.get_f().value();

			logger.info(String.format("succes at index %d: f(%.3f, %.3f, %.3f) = %.3f\n", index, Math.toDegrees(ea.psi),
					Math.toDegrees(ea.theta), Math.toDegrees(ea.phi), objective));
			if (Math.abs(best_feasible - objective) < 0.1 && (index - last_succes_index) > 20) {
				logger.log(Level.INFO, "No more progress: exiting optimization... ");
				Mads.force_quit(0);
			}

			this.last_succes_index = index;
			this.best_feasible = objective;
		}

	}
}
